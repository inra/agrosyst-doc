#!/bin/bash

###
# Agrosyst
#
# Copyright (C) 2022 INRAE, CodeLutin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

source ../config_database_export.txt

function addHeader() {
  echo "addHeader()"
  echo "$jour"
  echo "#!/bin/bash" > executeExportToCsvHeader.sh
  echo "declare -a query_name" >> executeExportToCsvHeader.sh
  echo "declare -a query" >> executeExportToCsvHeader.sh
  echo "repertoire_export=\"$HOME/EXP_MASS_CSV/PERFORMANCE_MASS/\"" >> executeExportToCsvHeader.sh
  echo "jour=\"$jour\"" >> executeExportToCsvHeader.sh
  echo "prefixe=\"AGS_\"\$jour\"_exports_performance_\"" >> executeExportToCsvHeader.sh
  echo "suffixe=\".csv\"" >> executeExportToCsvHeader.sh
  echo "arch_sufix=\"archive\"" >> executeExportToCsvHeader.sh
  echo "" >> executeExportToCsvHeader.sh
  echo "pg_host=\"$pg_host\"" >> executeExportToCsvHeader.sh
  echo "pg_port=\"$pg_port\"" >> executeExportToCsvHeader.sh
  echo "pg_user=\"$pg_user\"" >> executeExportToCsvHeader.sh
  echo "pg_password=\"$pg_password\"" >> executeExportToCsvHeader.sh
  echo "pg_database=\"$pg_database\"" >> executeExportToCsvHeader.sh
  echo "" >> executeExportToCsvHeader.sh
  cp executeExportToCsvHeader.sh executeExportToCsv.sh
}

function addQueries() {
    find . -maxdepth 1 -type f -name '*.sql' -exec sh -c '
        i=1
        for pathname do
            fullPath=$pathname
            pathname=$( basename "$pathname" )
            printf "%s\n" "query_name[$i]=\"${pathname%.*}\"" >> executeExportToCsv.sh
            printf "%s\n" "query[$i]=\""  >> executeExportToCsv.sh
            cat $fullPath >> executeExportToCsv.sh
            printf "\"\n" >> executeExportToCsv.sh
            i=$(($i+1))
        done' sh {} +
}

function generateExecutable() {
    cat executeExportToCsvExecPart.sh >> executeExportToCsv.sh
    echo "ficher executeExportToCsv.sh généré"
}

function executeExport() {
    echo "exécution du script"
    /bin/bash executeExportToCsv.sh
}

function addAnonymizeHeader() {
  cp executeExportToCsvHeader.sh ANONYMIZED/executeExportToCsv.sh
  cp executeExportToCsvExecPart.sh ANONYMIZED/executeExportToCsvExecPart.sh
  anonymize="PERFORMANCE_MASS\/ANONYMIZED"
  sed -i "s/PERFORMANCE_MASS/${anonymize}/g" ANONYMIZED/executeExportToCsv.sh
  sed -i "s/archive/anonymize_archive/g" ANONYMIZED/executeExportToCsv.sh
}

function addAnonymizeQueries() {
    cd ANONYMIZED/ || exit
    addQueries
}

function cleanUp() {
    rm -f executeExportToCsv.sh
    rm -f executeExportToCsvExecPart.sh
    cd ..
    rm -f executeExportToCsv.sh
    rm -f executeExportToCsvHeader.sh
    rm $HOME/EXP_MASS_CSV/PERFORMANCE_MASS/*.csv
    rm $HOME/EXP_MASS_CSV/PERFORMANCE_MASS/ANONYMIZED/*.csv
}

addHeader
addQueries
generateExecutable
executeExport

# process anonymize
addAnonymizeHeader
addAnonymizeQueries
generateExecutable
executeExport
cleanUp

# remove variable config connexion
unset pg_host
unset pg_port
unset pg_user
unset pg_password
unset pg_database
unset jour
