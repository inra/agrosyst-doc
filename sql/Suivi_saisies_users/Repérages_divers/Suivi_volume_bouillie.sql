﻿------------------------------------------
-- Suivi des saisies de volume de bouillie
------------------------------------------

-- Nombre de saisi par valeur de volume de bouillie
SELECT boiledquantity, count(*) nb
FROM abstractaction 
GROUP BY boiledquantity
ORDER BY boiledquantity

-- Nombre d'intrants phyto
SELECT *
FROM abstractaction 
WHERE boiledquantity is not null