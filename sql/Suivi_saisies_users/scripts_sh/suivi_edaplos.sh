#!/bin/bash
#echo "test cron edaplos" >> /tmp/test\_vl.log
repertoire="/mnt/save/postgresql/147.100.179.208/12-ags_priv/AGS\_"
jour="$(date +%Y%m%d)"
nomfichiercsv="\_suivi\_imports-edaplos.csv"
REQUETE="COPY(select to\_char(importdate,'yyyy-mm-dd hh24:MI:ss') importdate, importfilename, issueridentification SIRET, issuername DOMAINE, softwarename,
au.firstname, au.lastname, au.email
from edaplosimport ei
LEFT JOIN agrosystuser au on ei.agrosystuser = au.topiaid order by importdate DESC
) TO '$repertoire$jour$nomfichiercsv' DELIMITER ';' CSV HEADER
"
psql -U postgres -h 147.100.179.208 -d agrosystprod -p 5439 -c "$REQUETE"

#echo "envoi du mail" >> /tmp/test\_vl.log
mail -s '\[AGS\] Fichier suivi eDaplos' -A $repertoire$jour$nomfichiercsv agrosyst-team@inrae.fr \<\<\< 'Fichier de suivi eDaplos'
#echo "fin test" >>/tmp/test\_vl.log