﻿ SELECT *
FROM refactadosagespc rads
JOIN modif_refactadosagespc mrads
-- WHERE rads.topiaid = mrads.topiaid
ON rads.id_produit = mrads.id_produit
AND	rads.id_culture = mrads.id_culture
AND	rads.id_traitement = mrads.id_traitement
AND	COALESCE(rads.dosage_spc_valeur::text,'') = COALESCE(mrads.dosage_spc_valeur::text,'')
AND	COALESCE(rads.dosage_spc_commentaire,'') = COALESCE(mrads.dosage_spc_commentaire, '')
AND	COALESCE(rads.commentaire_agrosyst,'') = COALESCE(mrads.commentaire_agrosyst,'')

WHERE (rads.id_produit, 
	rads.id_culture,
	rads.id_traitement ,
	COALESCE(rads.dosage_spc_valeur::text,''),
	COALESCE(rads.dosage_spc_commentaire, ''),
	COALESCE(rads.commentaire_agrosyst,'')) 
IN 
	(SELECT id_produit,
	 id_culture,
	 id_traitement,
	 COALESCE(dosage_spc_valeur::text,''),
	 COALESCE(dosage_spc_commentaire,''),
	  COALESCE(commentaire_agrosyst,'') FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('modifier dose unité')
)