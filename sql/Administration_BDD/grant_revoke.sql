﻿--Grant connect on Database agrosystprod to agsselect;
--REVOKE connect on Database agrosystprod FROM agsselect;
--Grant usage on schema public to estelleselect
--REVOKE usage ON schema public FROM estelleselect
--GRANT SELECT ON ALL TABLES IN SCHEMA public TO agsselect;
--REVOKE SELECT ON ALL TABLES IN SCHEMA public FROM estelleselect;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO agsselect;

GRANT UPDATE(nodu) ON refactatraitementsproduit TO maj_referentiel;

