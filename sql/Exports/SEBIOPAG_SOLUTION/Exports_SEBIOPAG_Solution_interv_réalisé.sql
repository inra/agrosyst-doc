﻿
-----------------------------------------
-- Interventions Réalisé
-----------------------------------------

WITH toutes_interventions AS ( 
	---------------------------
	-- Interventions Assolées
	---------------------------
	(SELECT 
	d.code domaine_code,
	d.topiaid domaine_id,
	d.name domaine_nom, -- à anonymiser
	d.campaign domaine_campagne,
	gs.code sdc_code, gs.topiaid sdc_id, gs.name sdc_nom,
	p.topiaid parcelle_id, p.name parcelle_nom,
	z.topiaid zone_id, z.name zone_nom,
	(-- Requête renvoyant l'id de la culture associé au noeud
	SELECT cpe.topiaid 
	FROM croppingplanentry cpe
	WHERE cpe.topiaid = eccn.croppingplanentry)	
	culture_id,
	(-- Requête renvoyant le nom de la culture associé au noeud
	SELECT cpe1.name 
	FROM croppingplanentry cpe1
	WHERE cpe1.topiaid = eccn.croppingplanentry)	
	culture_nom,
	(-- Requête renvoyant l'id de la culture intermédiaire 
	SELECT eccc.intermediatecroppingplanentry
	FROM effectivecropcycleconnection eccc
	WHERE eccc.target = eccn.topiaid
	) ci_id,
	(-- Requête renvoyant le nom de la culture intermédiaire 
	SELECT cpe.name
	FROM effectivecropcycleconnection eccc
	JOIN croppingplanentry cpe ON cpe.topiaid = eccc.intermediatecroppingplanentry
	WHERE eccc.target = eccn.topiaid
	) ci_nom, 
	
	CASE intermediatecrop 
		WHEN true then 'oui'
		WHEN false then 'non'
	END 
	concerne_la_ci,
	
	-- Requête renvoyant les noms EDI des espèces cochées de l'interventions
	(WITH esp_var AS
		(-- Requête filtrée pour espèces avec variétés GEVES
		SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvg2.denomination),' ; ') AS espvar
		FROM effectivespeciesstade ess2
		JOIN croppingplanspecies cps2 ON ess2.croppingplanspecies = cps2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		JOIN refvarietegeves rvg2 ON cps2.variety = rvg2.topiaid
		WHERE ess2.effectiveintervention = ei.topiaid
		
		UNION 
     
		-- Requête filtrée pour espèces avec variétés PLANTGRAPE
		SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvpg2.variete),' ; ') AS espvar
		FROM effectivespeciesstade ess2
		JOIN croppingplanspecies cps2 ON ess2.croppingplanspecies = cps2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		JOIN refvarieteplantgrape rvpg2 ON cps2.variety = rvpg2.topiaid
		WHERE ess2.effectiveintervention = ei.topiaid
		
		UNION
		
		-- Requête filtrée pour espèces sans variétés
		SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') AS espvar
		FROM effectivespeciesstade ess2
		JOIN croppingplanspecies cps2 ON ess2.croppingplanspecies = cps2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		WHERE ess2.effectiveintervention = ei.topiaid
		AND cps2.variety IS null
		)
	SELECT string_agg(espvar, ';')
	FROM esp_var
	) especes_de_l_intervention,

	(-- Requête renvoyant l'id de la culture précédente
	SELECT cpe.topiaid
	FROM effectivecropcycleconnection eccc
	JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
	JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid 
	WHERE eccc.target = eccn.topiaid
	) precedent_id,
	
	(-- Requête renvoyant le nom de la culture précédente
	SELECT cpe.name
	FROM effectivecropcycleconnection eccc
	JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
	JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid 
	WHERE eccc.target = eccn.topiaid
	) precedent_nom,

	(-- Requête renvoyant les noms EDI des espèces de la culture précédente
	SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), 
	NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') 
	FROM effectivecropcycleconnection eccc
	JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
	JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid 
	JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaid
	JOIN refespece re2 ON cps.species = re2.topiaid
	WHERE eccc.target = eccn.topiaid--eccn2.topiaid = ei.effectivecropcyclenode
	) precedent_especes_edi,

	(-- Requête renvoyant le rang de la culture
	SELECT rank + 1 
	FROM effectivecropcyclenode eccn2 
	WHERE eccn2.topiaid = ei.effectivecropcyclenode
	)culture_rang,

	ei.topiaid intervention_id,

	ei.type	intervention_type,

	(-- Requête renvoyant les actions de l'intervention
	SELECT string_agg(riate2.reference_label, ' ; ')
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE aa2.effectiveintervention = ei.topiaid --Pas besoin d'un "GROUP BY ei.topiaid" ?
	) interventions_actions, 

	ei.name intervention_nom,
		
	ei.comment intervention_comment,
		
	et.toolcouplings combinaison_outils_id,
		
	(-- Requête donnant le nom de la CO associée à l'intervention
	SELECT tc.toolscouplingname
	FROM toolscoupling tc
	WHERE et.toolcouplings = tc.topiaid
	) combinaison_outils_nom,

	(-- Requête donnant pour le topiaid de CO, le type matériel 1 du tracteur ou de l'automoteur concerné
	SELECT typemateriel1
	FROM toolscoupling tc
	JOIN equipment e2 ON tc.tractor = e2.topiaid
	JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
	WHERE et.toolcouplings = tc.topiaid
	) tracteur_ou_automoteur,

	(WITH materiel_noms AS 
		(-- Requête donnant pour un topiaid de CO la liste des outils concernés
		SELECT typemateriel1 type_materiel_1
		FROM toolscoupling tc
		JOIN equipments_toolscoupling eqt ON eqt.toolscoupling = tc.topiaid
		JOIN equipment e ON eqt.equipments = e.topiaid
		JOIN refmateriel rm ON e.refmateriel = rm.topiaid 
		WHERE et.toolcouplings = tc.topiaid)
	SELECT string_agg(type_materiel_1, ' ; ')
	FROM materiel_noms -- Pas besoin d'un "GROUP BY tc.topiaid" ?
	) outils,

		-- Autre manière ?
		--(-- Requête donnant pour un topiaid de CO la liste des outils concernés
			--SELECT string_agg (typemateriel1, ', ') type_materiel_1
			--FROM toolscoupling tc
			--JOIN equipments_toolscoupling eqt ON eqt.toolscoupling = tc.topiaid
			--JOIN equipment e ON eqt.equipments = e.topiaid
			--JOIN refmateriel rm ON e.refmateriel = rm.topiaid 
			--WHERE et.toolcouplings = tc.topiaid) 
			
	ei.startinterventiondate date_debut,
	ei.endinterventiondate date_fin,
	ei.spatialfrequency freq_spatiale,
	ei.transitcount nombre_de_passage,
	ei.spatialfrequency*ei.transitcount psci,

	(-- Requête renvoyant la proportion de surface traitée de l'action phyto
	SELECT proportionoftreatedsurface --||' %' -- c'est mieux sans le % ?
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
	AND aa2.effectiveintervention = ei.topiaid -- on peut utiliser le effectiveintervention du dessous ?
	) proportion_surface_traitee_phyto,

	(SELECT proportionoftreatedsurface * ei.spatialfrequency * ei.transitcount
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
	AND aa2.effectiveintervention = ei.topiaid
	) psci_phyto,	
	
	(-- Requête renvoyant la proportion de surface traitée de l'action lutte bio
	SELECT proportionoftreatedsurface -- ||' %'
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
	AND aa2.effectiveintervention = ei.topiaid
	) proportion_surface_traitee_lutte_bio,
		
	(SELECT proportionoftreatedsurface * ei.spatialfrequency * ei.transitcount
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
	AND aa2.effectiveintervention = ei.topiaid
	) psci_lutte_bio,
		
	ei.workrate debit_de_chantier,
	ei.workrateunit debit_de_chantier_unite,

	(-- Requête renvoyant la quantité d'eau de l'action irrigation
	SELECT waterquantityaverage 
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid -- Autre manière : WHERE topiaiddiscriminator = '... irigation'
	WHERE intervention_agrosyst = 'IRRIGATION'
	AND aa2.effectiveintervention = ei.topiaid
	) quantite_eau_mm,

	(WITH liste_especes_semees AS 
		(-- Requête renvoyant la liste des noms des espèces semées
		SELECT concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), 
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', sas2.treatment, sas2.biologicalseedinoculation) re2libelle
		FROM seedingactionspecies sas2
		JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
		JOIN croppingplanspecies cps2 ON sas2.speciescode = cps2.code
		JOIN croppingplanentry cpe2 ON eccn.croppingplanentry = cpe2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		WHERE cpe2.topiaid = eccn.croppingplanentry
		AND aa2.effectiveintervention = ei.topiaid)
	SELECT string_agg(les.re2libelle,' ; ') 
	FROM liste_especes_semees les)
	especes_semees,

	(SELECT string_agg(quantity :: text, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre) -- pas de WITH ?
		(SELECT quantity, aa2.practicedintervention
		FROM seedingactionspecies sas2
		JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
		WHERE aa2.effectiveintervention = ei.topiaid
		ORDER BY sas2.topiaid) AS foo
		GROUP BY effectiveintervention
	) densite_semis,
	
	(SELECT string_agg(seedplantunit, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre)
		(SELECT seedplantunit, aa2.practicedintervention
		FROM seedingactionspecies sas2
		JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
		WHERE aa2.effectiveintervention = ei.topiaid
		ORDER BY sas2.topiaid) AS foo
		GROUP BY effectiveintervention
	) unite_semis,

	(-- Requête renvoyant le type de semence 
	SELECT seedtype
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid -- Autre manière : WHERE topiaiddiscriminator = '... irigation'
	WHERE intervention_agrosyst = 'SEMIS'
	AND aa2.effectiveintervention = ei.topiaid
	) type_semence
		
 	FROM effectiveintervention ei
	JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
	JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
	JOIN zone z ON ecc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN growingsystem gs ON p.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	LEFT JOIN effectiveintervention_toolcouplings et ON ei.topiaid = et.effectiveintervention -- vérifier que dans le synthé c'est bien un left join ?

	WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f')
                   )
                )
            )

	
	)

	UNION

	------------------------------
	-- Interventions Pérennes
	------------------------------	

	SELECT 
	d.code domaine_code,
	d.topiaid domaine_id,
	d.name domaine_nom, -- à anonymiser
	d.campaign domaine_campagne,
	gs.code sdc_code,
	gs.topiaid sdc_id, gs.name sdc_nom,
	p.topiaid parcelle_id, p.name parcelle_nom,
	z.topiaid zone_id, z.name zone_nom,
		
	(-- Requête renvoyant l'id de la culture pérenne
	SELECT cpe.topiaid 
	FROM croppingplanentry cpe 
	WHERE cpe.topiaid = epcc.croppingplanentry)	
	culture_id,
		
	(-- Requête renvoyant le nom de la culture associé au noeud
	SELECT cpe.name 
	FROM croppingplanentry cpe
	WHERE cpe.topiaid = epcc.croppingplanentry)	
	culture_nom,

	'NA' ci_id,
	'NA' ci_nom, 
	'NA' concerne_la_ci,

	(WITH esp_var AS
		(-- Requête filtrée pour espèces avec variétés GEVES
		SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvg2.denomination),' ; ') AS espvar
		FROM effectivespeciesstade ess2
		JOIN croppingplanspecies cps2 ON ess2.croppingplanspecies = cps2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		JOIN refvarietegeves rvg2 ON cps2.variety = rvg2.topiaid
		WHERE ess2.effectiveintervention = ei.topiaid
		
		UNION 
     
		-- Requête filtrée pour espèces avec variétés PLANTGRAPE
		SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvpg2.variete),' ; ') AS espvar
		FROM effectivespeciesstade ess2
		JOIN croppingplanspecies cps2 ON ess2.croppingplanspecies = cps2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		JOIN refvarieteplantgrape rvpg2 ON cps2.variety = rvpg2.topiaid
		WHERE ess2.effectiveintervention = ei.topiaid
		
		UNION
		
		-- Requête filtrée pour espèces sans variétés
		SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') AS espvar
		FROM effectivespeciesstade ess2
		JOIN croppingplanspecies cps2 ON ess2.croppingplanspecies = cps2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		WHERE ess2.effectiveintervention = ei.topiaid
		AND cps2.variety IS null
		)
	SELECT string_agg(espvar, ';')
	FROM esp_var
	) especes_de_l_intervention,
			
	'NA' precedent_id,
	'NA' precedent_nom,
	'NA' precedent_especes_edi,
	'1' culture_rang,
	ei.topiaid intervention_id,

	ei.type	intervention_type,
		
	(-- Requête renvoyant les actions de l'intervention
	SELECT string_agg(riate2.reference_label, ' ; ')
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE aa2.effectiveintervention = ei.topiaid) interventions_actions, --Pas besoin d'un "GROUP BY ei.topiaid" ?
	
	ei.name intervention_nom,
	
	ei.comment intervention_comment,
		
	et.toolcouplings combinaison_outils_id,
		
	(-- Requête donnant le nom de la CO associée à l'intervention
	SELECT tc.toolscouplingname
	FROM toolscoupling tc
	WHERE et.toolcouplings = tc.topiaid) combinaison_outils_nom,

	(-- Requête donnant pour le topiaid de CO, le type matériel 1 du tracteur ou de l'automoteur concerné
	SELECT typemateriel1
	FROM toolscoupling tc
	JOIN equipment e2 ON tc.tractor = e2.topiaid
	JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
	WHERE et.toolcouplings = tc.topiaid
	) tracteur_ou_automoteur,

	(WITH materiel_noms AS 
		(-- Requête donnant pour un topiaid de CO la liste des outils concernés
		SELECT typemateriel1 type_materiel_1
		FROM toolscoupling tc
		JOIN equipments_toolscoupling eqt ON eqt.toolscoupling = tc.topiaid
		JOIN equipment e ON eqt.equipments = e.topiaid
		JOIN refmateriel rm ON e.refmateriel = rm.topiaid 
		WHERE et.toolcouplings = tc.topiaid)
	SELECT string_agg(type_materiel_1, ' ; ')
	FROM materiel_noms -- Pas besoin d'un "GROUP BY tc.topiaid" ?
	) outils,

		-- Autre manière ?
		--(-- Requête donnant pour un topiaid de CO la liste des outils concernés
			--SELECT string_agg (typemateriel1, ', ') type_materiel_1
			--FROM toolscoupling tc
			--JOIN equipments_toolscoupling eqt ON eqt.toolscoupling = tc.topiaid
			--JOIN equipment e ON eqt.equipments = e.topiaid
			--JOIN refmateriel rm ON e.refmateriel = rm.topiaid 
			--WHERE et.toolcouplings = tc.topiaid) 
			
	ei.startinterventiondate date_debut,
	ei.endinterventiondate date_fin,
	ei.spatialfrequency freq_spatiale,
	ei.transitcount nombre_de_passage,
	ei.spatialfrequency*ei.transitcount psci,

	(-- Requête renvoyant la proportion de surface traitée de l'action phyto
	SELECT proportionoftreatedsurface --||' %' -- c'est mieux sans le % ?
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
	AND aa2.effectiveintervention = ei.topiaid -- on peut utiliser le effectiveintervention du dessous ?
	) proportion_surface_traitee_phyto,

	(SELECT proportionoftreatedsurface * ei.spatialfrequency * ei.transitcount
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
	AND aa2.effectiveintervention = ei.topiaid
	) psci_phyto,	

		(-- Requête renvoyant la proportion de surface traitée de l'action lutte bio
	SELECT proportionoftreatedsurface -- ||' %'
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
	AND aa2.effectiveintervention = ei.topiaid
	) proportion_surface_traitee_lutte_bio,
		
	(SELECT proportionoftreatedsurface * ei.spatialfrequency * ei.transitcount
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
	WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
	AND aa2.effectiveintervention = ei.topiaid
	) psci_lutte_bio,
		
	ei.workrate debit_de_chantier,
	ei.workrateunit debit_de_chantier_unite,

	(-- Requête renvoyant la quantité d'eau de l'action irrigation
	SELECT waterquantityaverage 
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid -- Autre manière : WHERE topiaiddiscriminator = '... irigation'
	WHERE intervention_agrosyst = 'IRRIGATION'
	AND aa2.effectiveintervention = ei.topiaid
	) quantite_eau_mm,

	(WITH liste_especes_semees AS 
		(-- Requête renvoyant la liste des noms des espèces semées
		SELECT concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), 
		NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', sas2.treatment, sas2.biologicalseedinoculation) re2libelle
		FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
		JOIN croppingplanspecies cps2 ON sas2.speciescode = cps2.code
		JOIN croppingplanentry cpe2 ON epcc.croppingplanentry = cpe2.topiaid
		JOIN refespece re2 ON cps2.species = re2.topiaid
		WHERE cpe2.topiaid = epcc.croppingplanentry
		AND aa2.effectiveintervention = ei.topiaid)
	SELECT string_agg(les.re2libelle,' ; ') 
	FROM liste_especes_semees les)
	especes_semees,

	(SELECT string_agg(quantity :: text, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre) -- pas de WITH ?
		(SELECT quantity, aa2.practicedintervention
		FROM seedingactionspecies sas2
		JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
		WHERE aa2.effectiveintervention = ei.topiaid
		ORDER BY sas2.topiaid) AS foo
		GROUP BY effectiveintervention
	) densite_semis,
		
	(SELECT string_agg(seedplantunit, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre)
		(SELECT seedplantunit, aa2.practicedintervention
		FROM seedingactionspecies sas2
		JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
		WHERE aa2.effectiveintervention = ei.topiaid
		ORDER BY sas2.topiaid) AS foo
		GROUP BY effectiveintervention
	) unite_semis, 

	(-- Requête renvoyant le type de semence 
	SELECT seedtype
	FROM abstractaction aa2
	JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid -- Autre manière : WHERE topiaiddiscriminator = '... irigation'
	WHERE intervention_agrosyst = 'SEMIS'
	AND aa2.effectiveintervention = ei.topiaid
	) type_semence
		
 	FROM effectiveintervention ei
	JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
	JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
	JOIN zone z ON epcc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN growingsystem gs ON p.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	LEFT JOIN effectiveintervention_toolcouplings et ON ei.topiaid = et.effectiveintervention -- vérifier que dans le synthé c'est bien un left join ?
	
WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f')
                   )
                )
            )

	)


SELECT * FROM toutes_interventions
ORDER BY domaine_nom, domaine_campagne, sdc_nom, parcelle_nom, date_debut

	