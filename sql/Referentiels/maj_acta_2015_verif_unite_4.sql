﻿
-------------------------------------------
-- Détéction des incohérences unités phyto 
-------------------------------------------

-- Détéction des incohérences entre unités stockées dans abstractinput et unité fournie pour la dose de référence par la moulinette Agrosyst.
-- Ces incohérences peuvent venir d'actualisations du référentiel refactadosagespc ou de corrections suite à des demandes d'utilisateurs.
-- Elles peuvent aussi venir du fait que l'utilisateur n'a pas coché d'espèce dans son intervention :
-- modifier requête pour calquer complètement la fonctionnalité d'Agrosyst ?

-- Modifications v4 : 
-- Ajouts de commentaires
-- Ajouts du volume de bouillie
-- Ajout dans le filtre de la clasue "phytoproductunit is not null"

WITH eis AS (	
	WITH group_species_code AS (
		-- requête donnant un seul code de refespece par code d'espèce (utile pour le synthétisé)
		SELECT code, max(species) species
		FROM croppingplanspecies
		GROUP BY code
	), group_entry_code AS (
		SELECT code, max(topiaid) topiaid
		FROM croppingplanentry
		GROUP BY code
	)
(
-------------------
	-- pour le réalisé
-------------------	
	-- pour les cultures assolées principales
	SELECT p.name plot_synth_name, z.topiaid as z_id, null as synth_id, ei.topiaid as interv_id, ei.name AS interv_name, aa.topiaid action_id, ai.topiaid abstractinput_id, ratp.id_produit, ratp.nom_produit,
	ratp.id_traitement, ratp.nom_traitement, rlcea.id_culture, re.libelle_espece_botanique, ai.qtavg, ai.phytoproductunit, aa.boiledquantity vol_bouillie
	FROM abstractinput ai
	JOIN abstractaction aa ON COALESCE(ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction) = aa.topiaid
	JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
	JOIN effectivespeciesstade ess ON ei.topiaid = ess.effectiveintervention -- modifié
	JOIN croppingplanspecies cps ON ess.croppingplanspecies = cps.topiaid -- modifié
	JOIN refespece re ON cps.species = re.topiaid
	JOIN reflienculturesediacta rlcea ON (re.code_espece_botanique,re.code_qualifiant_aee,re.code_type_saisonnier_aee,re.code_destination_aee) = 
	(rlcea.code_espece_botanique,rlcea.code_qualifiant_aee,rlcea.code_type_saisonnier_aee,rlcea.code_destination_aee)
	JOIN refactatraitementsproduit ratp ON ai.phytoproduct = ratp.topiaid
	JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid -- décalée
	JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
	JOIN zone z ON escc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid	
	WHERE ei.intermediatecrop IS FALSE
	
	UNION
	-- pour les cultures assolées intermédiaires
	SELECT p.name plot_synth_name, z.topiaid as z_id, null as synth_id, ei.topiaid as interv_id, ei.name AS interv_name, aa.topiaid action_id, ai.topiaid abstractinput_id, ratp.id_produit, ratp.nom_produit,
	ratp.id_traitement, ratp.nom_traitement, rlcea.id_culture, re.libelle_espece_botanique, ai.qtavg, ai.phytoproductunit, aa.boiledquantity vol_bouillie
	FROM abstractinput ai
	JOIN abstractaction aa ON COALESCE(ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction) = aa.topiaid
	JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
	JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
	JOIN effectivecropcycleconnection eccc ON eccn.topiaid = eccc.target
	JOIN croppingplanentry cpe ON eccc.intermediatecroppingplanentry = cpe.topiaid
	JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
	JOIN refespece re ON cps.species = re.topiaid
	JOIN reflienculturesediacta rlcea ON (re.code_espece_botanique,re.code_qualifiant_aee,re.code_type_saisonnier_aee,re.code_destination_aee) = 
	(rlcea.code_espece_botanique,rlcea.code_qualifiant_aee,rlcea.code_type_saisonnier_aee,rlcea.code_destination_aee)
	JOIN refactatraitementsproduit ratp ON ai.phytoproduct = ratp.topiaid
	JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
	JOIN zone z ON escc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid	
	WHERE ei.intermediatecrop IS TRUE

	UNION 
	-- pour les cultures pérennes	
	-- 	sous-requête permettant de créer une table liant zone / intervention / action / produit / traitement / culture cochée dans l'intervention/ unité saisie_ OK 
	SELECT p.name plot_synth_name, z.topiaid as z_id, null as synth_id, ei.topiaid as interv_id, ei.name AS interv_name, aa.topiaid action_id, ai.topiaid abstractinput_id, ratp.id_produit, ratp.nom_produit,ratp.id_traitement, 
	ratp.nom_traitement, rlcea.id_culture, re.libelle_espece_botanique, ai.qtavg, ai.phytoproductunit, aa.boiledquantity vol_bouillie
	FROM abstractinput ai
	JOIN abstractaction aa ON COALESCE(ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction)=aa.topiaid
	JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
	JOIN effectivespeciesstade ess ON ei.topiaid = ess.effectiveintervention
	JOIN croppingplanspecies cps ON ess.croppingplanspecies = cps.topiaid
	JOIN refespece re ON cps.species = re.topiaid
	-- 	lien avec les espèces
	JOIN reflienculturesediacta rlcea ON (re.code_espece_botanique,re.code_qualifiant_aee,re.code_type_saisonnier_aee,re.code_destination_aee) = (rlcea.code_espece_botanique,rlcea.code_qualifiant_aee,rlcea.code_type_saisonnier_aee,rlcea.code_destination_aee)
	JOIN refactatraitementsproduit ratp ON ai.phytoproduct = ratp.topiaid
	JOIN effectiveperennialcropcycle epcc ON ei.effectivecropcyclephase = epcc.phase
	JOIN zone z ON epcc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid	
 
	UNION 
------------------
	-- pour le synthétisé
------------------
	-- pour les cultures pérennes
	-- remarque à garder en tête : (16:46:50) Estelle: attention car pour un code il peut y avoir plusieurs lignes de croppingplanspecies, 
	-- ça pourra créer des doublons (voir des triplets). 
	-- Si ça gène il faudra ne prendre qu'une ligne

	SELECT ps.name plot_synth_name, null as z_id, ps.topiaid AS synth_id, pi.topiaid AS inter_id, pi.name AS inter_name, aa.topiaid action_id, ai.topiaid abstractinput_id, ratp.id_produit, 
		ratp.nom_produit,ratp.id_traitement, ratp.nom_traitement, rlcea.id_culture, re.libelle_espece_botanique, 
		ai.qtavg, ai.phytoproductunit, aa.boiledquantity vol_bouillie
	FROM abstractinput ai
	JOIN abstractaction aa ON COALESCE(ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction) = aa.topiaid
	JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
	JOIN practicedspeciesstade pss ON pi.topiaid = pss.practicedintervention
	JOIN group_species_code gsc ON pss.speciescode = gsc.code
	JOIN refespece re ON gsc.species = re.topiaid
	JOIN reflienculturesediacta rlcea ON (re.code_espece_botanique,re.code_qualifiant_aee,re.code_type_saisonnier_aee,re.code_destination_aee) = (rlcea.code_espece_botanique,rlcea.code_qualifiant_aee,rlcea.code_type_saisonnier_aee,rlcea.code_destination_aee)
	JOIN refactatraitementsproduit ratp ON ai.phytoproduct = ratp.topiaid
	JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
	JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
	JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid

	UNION
	-- pour les cultures assolées	
	-- 	pour les cultures assolées principales cochées dans les interventions
	SELECT ps.name plot_synth_name, null as z_id, ps.topiaid AS synth_id, pi.topiaid AS inter_id, pi.name AS inter_name, aa.topiaid action_id, ai.topiaid abstractinput_id, ratp.id_produit, 
		ratp.nom_produit,ratp.id_traitement, ratp.nom_traitement, rlcea.id_culture, re.libelle_espece_botanique, ai.qtavg, ai.phytoproductunit, aa.boiledquantity vol_bouillie
	FROM abstractinput ai
	JOIN abstractaction aa ON COALESCE(ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction) = aa.topiaid
	JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
	JOIN practicedspeciesstade pss ON pi.topiaid = pss.practicedintervention
	JOIN group_species_code gsc ON pss.speciescode = gsc.code
	JOIN refespece re ON gsc.species = re.topiaid
	JOIN reflienculturesediacta rlcea ON (re.code_espece_botanique,re.code_qualifiant_aee,re.code_type_saisonnier_aee,re.code_destination_aee) = (rlcea.code_espece_botanique,rlcea.code_qualifiant_aee,rlcea.code_type_saisonnier_aee,rlcea.code_destination_aee)
	JOIN refactatraitementsproduit ratp ON ai.phytoproduct = ratp.topiaid
	JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
	JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
	JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
	JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	WHERE pi.intermediatecrop IS FALSE

	UNION
	-- 	pour les cultures intermédiaires (toutes sont nécessairement cochées dans l'intervention donc le chemin jusqu'aux espèces est différent)_OK 
	------- La méthode proposée ci-dessous prend en compte le code culture de la CI si une intervention est affectée à une CI. 
	--	Pour ce code culture, un seul topiaid de croppingplanentry est renvoyé (cf. "groupe_entry_code"). Ca n'est pas totalement rigoureux. 
	--	Dans l'idéal, il faudrait récupérer tous les topiaid et toutes les espèces composant la culture sur les années du synthétisé
	-------	Autre méthode possible : utiliser les ref espèces des espèces de la culture intermédiaire cochées dans l'intervention. 
	--	Or dans Agrosyst, un soucis avec les copier-coller fait que certaines interventions affectées à une CI n'ont pas d'espèces cochées (interventions déclarées avant mai 2015 potentiellement concernées).
	--	Cette 2ème méthode ne peut donc pas être utilisée tant que le soucis n'a pas été réglé. POur le moment, on lui préfère la première méthode.
	SELECT ps.name plot_synth_name, null as z_id, ps.topiaid AS synth_id, pi.topiaid AS inter_id, pi.name AS inter_name, aa.topiaid action_id, ai.topiaid abstractinput_id, ratp.id_produit, ratp.nom_produit,ratp.id_traitement, 
	ratp.nom_traitement, rlcea.id_culture, re.libelle_espece_botanique, ai.qtavg, ai.phytoproductunit, aa.boiledquantity vol_bouillie
	FROM abstractinput ai
	JOIN abstractaction aa ON COALESCE(ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction) = aa.topiaid
	JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
	JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
	JOIN group_entry_code gec ON pccc.intermediatecroppingplanentrycode = gec.code
	JOIN croppingplanspecies cps ON gec.topiaid = cps.croppingplanentry
	JOIN refespece re ON cps.species = re.topiaid
	JOIN reflienculturesediacta rlcea ON (re.code_espece_botanique,re.code_qualifiant_aee,re.code_type_saisonnier_aee,re.code_destination_aee) = (rlcea.code_espece_botanique,rlcea.code_qualifiant_aee,rlcea.code_type_saisonnier_aee,rlcea.code_destination_aee)
	JOIN refactatraitementsproduit ratp ON ai.phytoproduct = ratp.topiaid
	JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
	JOIN practicedseasonalcropcycle pscc ON pccn.practicedseasonalcropcycle = pscc.topiaid
	JOIN practicedcropcycle pcc ON pscc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	WHERE pi.intermediatecrop IS TRUE
	)
)

-- requête créant le tableau final avec zone, intervention, action, produit, traitement, culture,  dose appliquée, unité saisie et unité attendue

SELECT eis.plot_synth_name, eis.z_id, eis.synth_id, eis.interv_id, eis.interv_name, eis.action_id, eis.abstractinput_id, eis.id_produit, eis.nom_produit, eis.id_traitement, eis.nom_traitement,
eis.id_culture, eis.libelle_espece_botanique, eis.qtavg, eis.phytoproductunit, eis.vol_bouillie,
	-- sous-requête au niveau de la clause SELECT
	-- elle donne une unité unique pour la culture + les cultures de son groupe
	-- cette requête va être executée pour chaque ligne de eis
	-- les unités étant toutes les mêmes pour une culture et son groupe, il faut bien en choisir une, on prend la maximale, on pourrait aussi prendre la minimale
	-- le résultat est ajouté dans la colonne unite_base_phyto
	(SELECT max(dosage_spc_unite) 
	FROM refactadosagespc 
	WHERE refactadosagespc.active IS TRUE
	AND refactadosagespc.id_produit = eis.id_produit	
	AND refactadosagespc.id_traitement = eis.id_traitement
		AND refactadosagespc.id_culture
	IN (
		SELECT id_groupe_culture 
		FROM refactagroupecultures
		WHERE id_culture = eis.id_culture
		UNION
		SELECT eis.id_culture id_groupe_culture
	)) AS unite_base_phyto,
	(SELECT min(dosage_spc_valeur) 
	FROM refactadosagespc 
	WHERE refactadosagespc.active IS TRUE
	AND refactadosagespc.id_produit = eis.id_produit	
	AND refactadosagespc.id_traitement = eis.id_traitement
		AND refactadosagespc.id_culture
	IN (
		SELECT id_groupe_culture 
		FROM refactagroupecultures
		WHERE id_culture = eis.id_culture
		UNION
		SELECT eis.id_culture id_groupe_culture
	)) AS valeur_ref_base_phyto
FROM eis 

-- on rajoute un filtre pour ne garder que les interventions pour lesquelles il y a une différence entre unité de l'intervention et unité du référentiel
-- (on cherche l'unité max du référentiel comme au-dessus)
-- le COALESCE c'est pour avoir aussi les cas où l'unité n'est pas renseignée (null)
WHERE COALESCE(eis.phytoproductunit,'bla') <> COALESCE((select max(dosage_spc_unite) 
	FROM refactadosagespc 	
	WHERE refactadosagespc.active IS TRUE 
	AND refactadosagespc.id_produit = eis.id_produit
	AND refactadosagespc.id_traitement = eis.id_traitement
	AND refactadosagespc.id_culture
	IN (
		SELECT id_groupe_culture from refactagroupecultures
		WHERE id_culture = eis.id_culture
		UNION
		SELECT eis.id_culture id_groupe_culture
	)),'bla')
-- on ne veut pas les cas où il n'y a pas d'unité renvoyée pour un triple produit / traitement / culture 
-- car dans ce cas l'utilisateur choisi l'unité qu'il veut et c'est normal que ça soit différent. 
AND (select max(dosage_spc_unite) 
	from refactadosagespc
	WHERE refactadosagespc.active IS TRUE
	AND refactadosagespc.id_produit = eis.id_produit
	AND refactadosagespc.id_traitement = eis.id_traitement
	AND refactadosagespc.id_culture
	IN (
		SELECT id_groupe_culture from refactagroupecultures
		WHERE id_culture = eis.id_culture
		UNION
		SELECT eis.id_culture id_groupe_culture
	)) IS NOT NULL 

AND phytoproductunit IS NOT NULL

ORDER BY id_produit, id_culture ;

-- Grouper les espèces, grouper les cultures, enlever les lignes qui n'ont pas de dose ?
