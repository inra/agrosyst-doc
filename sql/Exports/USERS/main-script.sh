#!/bin/bash

echo `date +%T`

declare -a query_name
declare -a query
repertoire_export=$HOME"/EXP_MASS_CSV/USERS/"
source ../config_database_export.txt

prefixe="AGS_"$jour"_exports_agronomes_"
suffixe=".csv"

psql -U$pg_user -h $pg_host -p $pg_port -d $pg_database -f "users.sql"

query_name[1]="exports_users"
query[1]="
	SELECT * from export_users
"

query_name[2]="linked_network"
query[2]="
	SELECT * from linked_network
"


nb_queries=${#query[@]}
for ((i=1;i<=$nb_queries;i++)) do

        echo "i= "$i"/"$nb_queries
        echo "lancement requete ${query_name[$i]}"

        #echo "requete = "
        #echo ${query[$i]}
        nom_fic="AGS_"$jour"_"${query_name[$i]}$suffixe
        fichier_export=$repertoire_export$nom_fic

        echo "fichier_export="$fichier_export
        #liste des noms de fichiers pour la compression
        lst_noms_fic=$lst_noms_fic" "$nom_fic

        commande_export="COPY("${query[$i]}") TO STDOUT WITH (FORMAT csv, HEADER, DELIMITER '@', FORCE_QUOTE *, NULL '')"

        #echo $commande_export
        psql -U$pg_user -h $pg_host -p $pg_port -d $pg_database  -c "$commande_export" > $fichier_export

        echo "${query_name[$i]}" exporté
done


