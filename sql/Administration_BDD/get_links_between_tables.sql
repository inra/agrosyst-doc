﻿/* Requêtes pour avoir les tables liées entre elles
Il faut être utilisateur postgres pour avoir les infos. 
Ou alors donner les droits sur information_schema ?*/

-- On cherche toutes les tables référençant une certaine table
-- Le lien est fait sur la clef étrangère
SELECT tc.table_schema, tc.constraint_name, tc.table_name, kcu.column_name, ccu.table_name
AS foreign_table_name, ccu.column_name AS foreign_column_name
FROM information_schema.table_constraints tc
JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY'
AND ccu.table_name='abstractinput'