-------------------------------
-- modele decisionnel
-------------------------------
-- creation de table commentee en attendant finition et validation
-- create table exports_agronomes_modele_decisionnel as

SELECT 
--crit.domaine_id, 
--crit.dispositif_id, 
--gs.topiaid sdc_id, 
--rgs.topiaid bilan_de_campagne_sdc_id,

-- data management mode
mm.category,
mm.growingsystem,
mm.versionnumber,
mm.mainchanges,
mm.changereason,
mm.active,
mm.changereasonfromplanned,
mm.mainchangesfromplanned,
mm.historical

FROM managementmode mm
--JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
WHERE gs.active IS TRUE;