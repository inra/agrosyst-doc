﻿

-------------
-- PLAN
-------------

-- Domaines
-- Parcelles
-- SdC
-- Intrants_Réalisé
-- Récolte
-- Récolte 2
-- Matériels
-- Pérennes_Réalisé
-- A retenir

-- Note : Exports pour SEBIOPAG : requête rédigée à partir du fichier 'Exports agronomes v3'

-------------
-- Domaines
-------------

WITH domainresponsibles as (
	SELECT domaincode, string_agg(au.lastname||' '||au.firstname, ', ') AS responsibles FROM userrole ur
	JOIN agrosystuser au ON ur.agrosystuser = au.topiaid
	WHERE domaincode IS NOT NULL
	GROUP BY domaincode
)

SELECT 
d.code domaine_code,
d.topiaid domaine_id,
d.name domaine_name, -- à anonymiser
d.campaign domaine_campagne,
d.type domaine_type,
rl.departement département,
rl.codeinsee||' - '||rl.commune commune, d.siret,-- à anonymiser
d.zoning domaine_zonage,
d.description domain_description,
rls.libelle_insee statut_juridique_nom,
d.statuscomment statut_juridique_commentaire,
d.usedagriculturalarea SAU,
d.croppingplancomment cultures_commentaire,
d.otheractivitiescomment autres_activités_commentaire,
d.workforcecomment MO_commentaire,
d.partnersnumber nombre_associés,
d.otherworkforce MO_familiale_et_associés,	
d.permanentemployeesworkforce MO_permanente,
d.temporaryemployeesworkforce MO_temporaire,
d.familyworkforcewage MO_familiale_rémunération,
d.wagecosts charges_salariales,
d.cropsworkforce MO_conduite_cultures_dans_domaine_expé,
d. msafee cotisation_MSA,
d.averagetenantfarming fermage_moyen,
d.decoupledassistance aides_découplées,
ro1.libelle_otex_18_postes otex_18_nom,
ro2.libelle_otex_70_postes otex_70_nom,
d.orientation otex_commentaire,
dr.responsibles responsables_domaine
	-- d.active actif
FROM domain d 
JOIN domainresponsibles dr ON d.code = dr.domaincode
LEFT JOIN reflocation rl ON d.location = rl.topiaid
LEFT JOIN reflegalstatus rls ON d.legalstatus = rls.topiaid
LEFT JOIN refotex ro1 ON d.otex18 = ro1.topiaid
LEFT JOIN refotex ro2 ON d.otex70 = ro2.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE


---------------------
-- Parcelles
---------------------

WITH plot_zone AS (
	SELECT z.plot, count(*) nombre_de_zones
	FROM zone z
	GROUP BY z.plot )


SELECT ------ Informations générales
	d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne, 
	gs.topiaid sdc_id, 
	p.code parcelle_code, p.topiaid parcelle_id, p.name parcelle_nom, p.area parcelle_surface,
	rl.codepostal code_postal,
	rl.commune commune, 	-- à anonymiser
	p.comment parcelle_commentaire, 
	pz.nombre_de_zones, 
	------ Informations sur le zonage de la parcelle
	CASE p.outofzoning
		WHEN true then 'oui'
		WHEN false then 'non'
	END parcelle_hors_zonage,
		(WITH liste_zonages AS (					
		SELECT rpze.libelle_engagement_parcelle zonages
		FROM basicplot_plotzonings bpz
		JOIN refparcellezonageedi rpze ON rpze.topiaid = bpz.plotzonings
		WHERE bpz.basicplot = p.topiaid )
	SELECT string_agg (zonages, ' ; ')
	FROM liste_zonages)zonages_parcelle,
	------ Informations sur les équipements de la parcelle
	p.equipmentcomment equipement_commentaire, 
	CASE p.drainage
		WHEN true then 'oui'
		WHEN false then 'non'
	END drainage,
	p.drainageyear drainage_annee_realisation, 
	CASE p.frostprotection
		WHEN true then 'oui'
		WHEN false then 'non'
	END protection_anti_gel,
	p.frostprotectiontype protection_anti_gel_type, 
	CASE p.hailprotection
		WHEN true then 'oui'
		WHEN false then 'non'
	END protection_anti_grele,
	CASE p.rainproofprotection
		WHEN true then 'oui'
		WHEN false then 'non'
	END protection_anti_pluie,
	CASE p.pestprotection
		WHEN true then 'oui'
		WHEN false then 'non'
	END protection_anti_insectes,
	p.otherequipment autre_equipement, 
	------ Informations sur le sol de la parcelle
	p.solcomment sol_commentaire, 
	(-- Requête pour avoir le nom du sol
	SELECT rs.sol_nom
	FROM ground g
	JOIN refsolarvalis rs ON g.refsolarvalis = rs.topiaid
	WHERE p.ground = g.topiaid)
	sol_nom_ref, 
	(-- Requête pour avoir le nom de la texture de surface
	SELECT rstg.classes_texturales_gepaa
	FROM refsoltexturegeppa rstg
	WHERE rstg.topiaid = p.surfacetexture
	) texture_surface, 
	(-- Requête pour avoir le nom de la texture de surface
	SELECT rstg.classes_texturales_gepaa
	FROM refsoltexturegeppa rstg
	WHERE rstg.topiaid = p.subsoiltexture)
	texture_sous_sol, 
	p.solwaterph sol_ph, 
	p.solstoniness pierrosite_moyenne, 
	(-- Requête pour avoir la classe de profondeur du sol
	SELECT rspi.libelle_classe
	FROM refsolprofondeurindigo rspi
	WHERE rspi.topiaid = p.soldepth)
	sol_profondeur, 
	p.solmaxdepth sol_profondeur_max, 
	p.solorganicmaterialpercent teneur_MO,
	CASE p.solhydromorphisms
		WHEN true THEN 'oui'
		WHEN false THEN 'non'
	END hydromorphie, 
		CASE p.sollimestone
		WHEN true THEN 'oui'
		WHEN false THEN 'non'
	END calcaire,
	p.soltotallimestone proportion_calcaire_total, 
	p.solactivelimestone proportion_calclaire_actif,
		CASE p.solbattance
		WHEN true THEN 'oui'
		WHEN false THEN 'non'
	END battance,
	------ Informations sur le type d'irrigation de la parcelle
	CASE p.irrigationsystem
		WHEN true THEN 'oui'
		WHEN false THEN 'non'
	END irrigation,
	CASE p.fertigationsystem
		WHEN true THEN 'oui'
		WHEN false THEN 'non'
	END fertirrigation, 
	p. waterorigin origine_eau, p.irrigationsystemtype irrigation_type, p.pompenginetype irrigation_pompe, 
	p.hosespositionning irrigation_position_tuyaux,	
	------ Autres informations générales
	CASE p. maxslope 
		WHEN 'MORE_THAN_FIVE' THEN '> 5%'
		WHEN 'TWO_TO_FIVE' THEN '2 - 5%'
		WHEN 'ZERO' THEN '0%'
		WHEN 'ZERO_TO_TWO' THEN '0 - 2%'
	END pente, 
	CASE p.waterflowdistance 
		WHEN 'FIVE_TO_TEN' THEN '5 - 10 m'
		WHEN 'LESS_THAN_THREE' THEN '< 3 m'
		WHEN 'MORE_THAN_TEN' THEN '> 10 m'
		WHEN 'THREE_TO_FIVE' THEN '3 - 5 m'
	END distance_cours_eau, 
	CASE p.bufferstrip 
		WHEN 'BUFFER_STRIP_NEXT_TO_WATER_FLOW' THEN 'Bande tampon (5m) en bord de cours deau' -- On ne puet mettre ' dans le texte ?
		WHEN 'BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW' THEN 'Bande tampon pérenne enherbée dau moins 5m hors bord de cours deau'
		WHEN 'NONE' THEN 'Aucune'
	END bande_enherbee
	
FROM plot p
JOIN domain d ON p.domain = d.topiaid
JOIN growingsystem gs ON p.growingsystem = gs.topiaid
JOIN plot_zone pz ON pz.plot = p.topiaid
LEFT JOIN refsolarvalis rs ON p.ground = rs.topiaid
LEFT JOIN reflocation rl ON p.location = rl.topiaid
--LEFT JOIN basicplot_plotzonings bpz ON p.topiaid = bpz.basicplot
--LEFT JOIN refparcellezonageedi rpze ON bpz.plotzonings = rpze.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

-----------------
-- SdC
-----------------

-- Correction(s) restante(s) :  
	-- Il manque le réseau.

SELECT d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne, gp.code dispositif_code, gp.topiaid dispositif_id, gp.type dispositif_type,
gs.code sdc_code, gs.topiaid sdc_id, gs.name sdc_nom, gs.dephynumber sdc_code_dephy, 
NULL AS reseau, 
gs.description sdc_commentaire, 
CASE gs.validated
	WHEN true then 'oui'
	WHEN false then 'non'
END sdc_validé,
gs.sector sdc_filiere, rt.reference_label sdc_type_agriculture,
gs.categorystrategy sdc_stratégie_catégorie,
gs.affectedarearate sdc_part_SAU_domaine, 
gs.affectedworkforcerate sdc_part_MO_domaine, 
gs.domainstoolsusagerate sdc_part_matériel_domaine,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Couverts associés,  plantes de service'),
         'non') couverts_associes,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Cultures intermédiaires à effet allélopathique ou biocide'),
         'non') ci_effet_allelo_ou_biocide,
         
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Cultures intermédiaires attractives pour les auxiliaires'),
         'non') ci_attractives_pour_auxiliaires,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Cultures intermédiaires piège à nitrate'),
         'non') cipan,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Evitement par la date de semis - semis précoce'),
         'non') semis_precoce,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Evitement par la date de semis - semis tardif'),
         'non') semis_tardif,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Exportation des menu-pailles'),
         'non') exportation_menu_pailles,
         
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Destruction de la litière des feuilles'),
         'non') destrcution_litiere,     

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Efficience - Dose adaptée à la surface foliaire'),
         'non') dose_adaptee_surf_foliaire,     

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Filets anti-insectes'),
         'non') filet_anti_insectes,     

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Taille des organes contaminés'),
	'non') taille_organes_contamines,     

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Taille limitant les risques sanitaires'),
	'non') taille_limitant_risques_sanitaires,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Taille limitant les risques sanitaires'),
	'non') taille_limitant_risques_sanitaires,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Adaptation de la densité - faible densité'),
	'non') faible_densite,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Adaptation de la densité - forte densité'),
	'non') forte_densite,
	
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND id_trait = '34'), 
	'non') faible_ecartement,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND id_trait = '33'), 
	'non') fort_ecartement,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND id_trait = '36'), 
	'non') ajustement_fertilisation,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND id_trait = '35'), 
	'non') ajustement_irrigation,
	
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Autre forme de désherbage'), 
	'non') desherbage_autre_forme,  
	  
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Cultures pièges'), 
	'non') cultures_pieges,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Désherbage mécanique fréquent'),
	'non') desherbage_meca_frequent,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Désherbage mécanique occasionel'),
	'non') desherbage_meca_occasionnel,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Désherbage thermique'),
	'non') desherbage_thermique,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Adaptation de la lutte à la parcelle'),
	'non') adaptation_lutte_a_la_parcelle,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND id_trait = '20'),
	'non') optim_conditions_application,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose autres produits phytosanitaires'),
	'non') reduction_dose_autres_phyto,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose fongicides'),
	'non') reduction_dose_fongi,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose herbicides'),
	'non') reduction_dose_herbi,    

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose insecticides'),
	'non') reduction_dose_insec,
  
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Traitements localisés en intra-parcellaire'),
	'non') traitement_localise,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Utilisation d''adjuvants'),
	'non') utilisation_adjuvants,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Utilisation de seuils pour les décisions de traitement'),
	'non') utilisation_seuils,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Utilisation de stimulateur de défense'),
	'non') utilisation_stim_defense,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Efficience - Utilisation d''outil d''aide à la décision pour les traitements'),
	'non') utilisation_oad,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Levier génétique - Variétés compétitives sur adventices'),
	'non') var_competitives_adventice,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles à la verse'),
	'non') var_peu_sensibles_verse,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles aux maladies'),
	'non') var_peu_sensibles_maladies,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles aux ravageurs'),
	'non') var_peu_sensibles_ravageurs,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Lutte biologique par confusion sexuelle'),
	'non') lutte_bio_confu_sexuelle,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Lutte biologique autre'),
	'non') lutte_bio_autre,
	
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Toutes cultures - Mélange d''espèces ou de variétés'),
	'non') melange,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Gestion spécifique des résidus supports d''inoculum (broyage, enfouissement)'),
	'non') gestion_residus,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Monoculture ou rotation courte'),
	'non') monoculture_rotation_courte,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Rotation avec cultures rustiques ou étouffantes (adventices)'),
	'non') rotation_cultures_rustiques,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Rotation diversifiée équilibrée avec prairie temporaire'),
	'non') rotation_diversifiee_avec_pt,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Rotation diversifiée équilibrée avec prairie temporaire'),
	'non') rotation_diversifiee_sans_pt,
	
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Rotation diversifiée par l''introduction d''une culture dans une rotation courte'),
	'non') rotation_diversifiee_intro_une_culture,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Enherbement (espèces semées)'),
	'non') enherbement_seme,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures pérennes - Enherbement naturel maîtrisé'),
	'non') enheberment_naturel,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Agroforesterie'),
	'non') agroforesterie,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Arbres isolés ou alignements en bordure de parcelle'),
	'non') arbres_bordure_parcelle,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Broyage des bordures'),
	'non') broyage_bordure,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Gestion des bordures de bois pour favoriser la biodiversité'),
	'non') gestion_bordure_de_bois,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Présence de haies anciennes'),
	'non') haies_anciennes,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Présence de jeunes haies'),
	'non') haies_jeunes,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Proximité de bandes enherbées favorisant les auxiliaires'),
	'non') bandes_enherbees,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Proximité de bandes fleuries favorisant les auxiliaires'),
	'non') bandes_fleuries,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Proximité de bois et bosquets'),
	'non') bois_bosquet,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Décompactage  occasionnel'),
	'non') decompactage_occasionnel,
	
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Décompactage  profond fréquent (fréquence > 50%)'),
	'non') decompactage_frequent,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Faux semis ponctuels'),
	'non') faux_semis_ponctuels,
	
COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Faux semis intensifs (travaux superficiels répétés spécifiques)'),
	'non') faux_semis_intensifs,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Labour occasionnel'),
	'non') labour_occasionnel,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Labour fréquent (fréquence > 50%)'),
	'non') labour_frequent,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Labour systématique (fréquence > 80%)'),
	'non') labour_systematique,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Semis direct sans travail du sol occasionnel'),
	'non') semis_direct_occasionnel,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Semis direct sans travail du sol systématique'),
	'non') semis_direct_systematique,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Strip till occasionnel'),
	'non') strip_till_occasionnel,


COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Strip till fréquent ou systématique'),
	'non') strip_till_frequent,

COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
		FROM reftraitsdc rts2
		JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
		WHERE cgs2.growingsystem = gs.topiaid
		AND nom_trait = 'Cultures assolées - Techniques culturales superficielles uniquement'),
	'non') tcs
	
FROM growingsystem gs
JOIN growingplan gp ON gs.growingplan = gp.topiaid
JOIN domain d ON gp.domain = d.topiaid
LEFT JOIN reftypeagriculture rt ON gs.typeagriculture = rt.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE
	
GROUP BY d.topiaid, d.code, d.campaign, gp.topiaid, gp.code, gp.type,
gs.topiaid, gs.code, gs.name,  gs.dephynumber, gs.description, 
gs.validated, gs.sector, 
rt.reference_label,
gs.categorystrategy,
gs.affectedarearate
ORDER BY d.code, d.campaign, gs.dephynumber


----------------------------
-- Intrants_Réalisé
----------------------------

-- Correction(s) restante(s) :  
	-- Il manque les informations des cultures.
	-- Il manque les prix.

WITH ferti_min_orga AS (
	SELECT ai.topiaid intrant_id, NULL AS forme, ai.n n, ai.p2o5, ai.k2o, NULL AS bore, NULL AS calcium, 
	NULL AS fer, NULL AS manganese, NULL AS molybdene, NULL AS mgo, NULL AS oxyde_de_sodium, NULL AS so3, NULL AS cuivre, NULL AS zinc
	FROM abstractinput ai
	WHERE ai.inputtype = 'EPANDAGES_ORGANIQUES'
    
	UNION ALL

	SELECT ai.topiaid intrant_id, rfm.forme, rfm.n, rfm.p2o5, rfm.k2o, rfm.bore, rfm.calcium, 
	rfm.fer, rfm.manganese, rfm.molybdene, rfm.mgo, rfm.oxyde_de_sodium, rfm.so3, rfm.cuivre, rfm.zinc
	FROM abstractinput ai
	JOIN reffertiminunifa rfm ON rfm.topiaid = ai.mineralproduct
	WHERE ai.inputtype = 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
	)

SELECT 
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne, 
p.topiaid parcelle_id, p.name parcelle_nom, 
ei.topiaid intervention_id, NULL especes_de_l_intervention, ai.topiaid intrant_id, 

CASE ai.inputtype
	WHEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' THEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
	WHEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX' THEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
	WHEN 'EPANDAGES_ORGANIQUES' THEN 'EPANDAGES_ORGANIQUES'
	WHEN 'LUTTE_BIOLOGIQUE' THEN 'LUTTE_BIOLOGIQUE'
	WHEN 'SEMIS' THEN 'TRAITEMENT_SEMENCE' 
	WHEN 'AUTRE' THEN 'AUTRE' 
END intrant_type,

COALESCE (phytoproduct, organicproduct, mineralproduct) intrant_ref_id,

CASE ai.inputtype
	WHEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' THEN (SELECT nom_produit FROM refactatraitementsproduit ratp2 WHERE ratp2.topiaid = ai.phytoproduct)
	WHEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX' THEN (SELECT type_produit FROM reffertiminunifa rfm2 WHERE rfm2.topiaid = ai.mineralproduct)
	WHEN 'EPANDAGES_ORGANIQUES' THEN (SELECT libelle FROM reffertiorga rfo2 WHERE rfo2.topiaid = ai.organicproduct)
	WHEN 'LUTTE_BIOLOGIQUE' THEN (SELECT nom_produit FROM refactatraitementsproduit ratp3 WHERE ratp3.topiaid = ai.phytoproduct)
	WHEN 'SEMIS' THEN (SELECT nom_produit FROM refactatraitementsproduit ratp4 WHERE ratp4.topiaid = ai.phytoproduct)
END intrant_ref_nom,

ai.productname intrant_nom_utilisateur, 
ai.qtavg dose, 
COALESCE (phytoproductunit, otherproductqtunit,organicproductunit,mineralproductunit) unité, 
CASE 
	WHEN ai.inputtype IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES','LUTTE_BIOLOGIQUE','SEMIS') 
	THEN (SELECT CASE nodu WHEN true THEN 'oui' WHEN false THEN 'non' END nodu FROM refactatraitementsproduit ratp5 WHERE ratp5.topiaid = ai.phytoproduct)
END biocontrole,

ai.producttype intrant_phyto_type, fmo.forme forme_ferti_min, fmo.n n, fmo.p2o5, fmo.k2o, fmo.bore, fmo.calcium, fmo.fer, fmo.manganese, 
fmo.molybdene, fmo.mgo, fmo.oxyde_de_sodium, fmo.so3, fmo.cuivre, fmo.zinc, 

CASE 
	WHEN ai.inputtype IN ('EPANDAGES_ORGANIQUES')
	THEN (SELECT unite_teneur_ferti_orga FROM reffertiorga rfo2 WHERE rfo2.topiaid = ai.organicproduct) 
END unite_teneur_ferti_orga,

CASE  ai.phytoeffect 
	WHEN true THEN 'oui'
	WHEN false THEN 'non'
END ferti_effet_phyto_attendu,

NULL AS prix,
NULL AS prix_unite

FROM ferti_min_orga fmo
RIGHT JOIN abstractinput ai ON ai.topiaid = fmo.intrant_id
JOIN abstractaction aa ON COALESCE(organicfertilizersspreadingaction, mineralfertilizersspreadingaction, pesticidesspreadingaction, seedingaction, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, otheraction) = aa.topiaid

JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
JOIN zone z ON ecc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN domain d ON p.domain = d.topiaid 

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION                   )
                )
            ))

AND d.active IS TRUE

ORDER BY ei.topiaid

	
--------------------
-- Récolte
--------------------

-- Présentation requête : par rapport au document EXCEL, on a décidé :
	-- de ne pas mettre toutes les actions d'une intervention, uniquement l'identifiant de l'action de récolte
	-- de ne pas mettre les informations sur les prix 

-- Correction(s) restante(s) : 
	-- il y a apriori 211 actions de récoltes non reliées à une intervention (réalisé ou synthétisé). La dernière ligne de code est-elle à enlever ?


SELECT 
COALESCE (practicedintervention, effectiveintervention) intervention_id,
aa.topiaid action_id,
riate.reference_label intervention_action,
hy.yealdcategory rendement_type,	
hy.yealdaverage rendement_moyen,
hy.yealdmedian rendement_median,
hy.yealdmin rendement_min,
hy.yealdmax rendement_max,
hy.yealdunit unité
FROM harvestingyeald hy
JOIN abstractaction aa ON hy.harvestingaction = aa.topiaid
JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid

JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention  -- Pour SEBIOPAG (pas de synthé)
JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
JOIN zone z ON ecc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN domain d ON p.domain = d.topiaid 

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

-- WHERE COALESCE (practicedintervention, effectiveintervention) IS NOT NULL -- à enlever ?

--------------------
-- Récolte 2
--------------------

-- Nouvelle requête depuis les développements 2.3

SELECT 
COALESCE (practicedintervention, effectiveintervention) intervention_id,
aa.topiaid action_id,
riate.reference_label intervention_action,
hav.destination destination,	
hav.yealdaverage rendement_moyen,
hav.yealdmedian rendement_median,
hav.yealdmin rendement_min,
hav.yealdmax rendement_max,
hav.yealdunit unité
FROM harvestingactionvalorisation hav
JOIN abstractaction aa ON hav.harvestingaction = aa.topiaid
JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid

JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention  -- Pour SEBIOPAG (pas de synthé)
JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
JOIN zone z ON ecc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN domain d ON p.domain = d.topiaid 

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

--------------------
-- Matériels
--------------------

-- Présentation requête : 
	-- 2 sous requêtes (concernent les domaines actifs) 
		-- CAS 1 : on recherche tous les outils déclarés dans les combinaisons d'outils
		-- CAS 2 : on recherche tous les tracteurs/automoteurs déclarés dans les combinaisons d'outils
-- CAS 1
SELECT
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne,
tc.topiaid combinaison_outil_id, tc.code combinaison_outil_code, tc.toolscouplingname combinaison_outils_nom,
-- rm.topiaid materiel_ref_id,
e.topiaid materiel_id,
CASE e.materieleta 
WHEN true THEN 'oui'
WHEN false THEN 'non'
END materiel_ETA_CUMA,
e.name materiel_nom,
-- tc.tractor tracteur_id,
rm.typemateriel1 materiel_caractéristique1,
rm.typemateriel2 materiel_caractéristique2,
rm.typemateriel3 materiel_caractéristique3,
rm.typemateriel4 materiel_caractéristique4,
rm.uniteparan utilisation_annuelle,
rm.unite utilisation_annuelle_unité,
rm.chargesfixesparunitedevolumedetravailannuel coût_par_unité_travail_annuel
FROM equipment e
LEFT JOIN equipments_toolscoupling etc ON e.topiaid = etc.equipments
LEFT JOIN toolscoupling tc ON etc.toolscoupling = tc.topiaid -- c'est à cette étape que l'on associe à une CO un tracteur, dans la table toolscoupling ! 
JOIN domain d ON e.domain = d.topiaid
JOIN refmateriel rm ON e.refmateriel = rm.topiaid
WHERE rm.topiadiscriminator IN ('fr.inra.agrosyst.api.entities.referential.RefMaterielOutilImpl','fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationImpl')
AND d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

UNION 

-- CAS 2
SELECT
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne,
tc.topiaid combinaison_outil_id,
tc.code combinaison_outil_code,
tc.toolscouplingname combinaison_outils_nom,
-- rm.topiaid materiel_ref_id,
e.topiaid materiel_id,
CASE e.materieleta 
WHEN true THEN 'oui'
WHEN false THEN 'non'
END materiel_ETA_CUMA,
e.name materiel_nom,
-- tc.tractor tracteur_id,
rm.typemateriel1 materiel_caractéristique1,
rm.typemateriel2 materiel_caractéristique2,
rm.typemateriel3 materiel_caractéristique3,
rm.typemateriel4 materiel_caractéristique4,
rm.uniteparan utilisation_annuelle,
rm.unite utilisation_annuelle_unité,
rm.chargesfixesparunitedevolumedetravailannuel coût_par_unité_travail_annuel
FROM equipment e
LEFT JOIN toolscoupling tc ON e.topiaid = tc.tractor
JOIN domain d ON e.domain = d.topiaid
JOIN refmateriel rm ON e.refmateriel = rm.topiaid
WHERE rm.topiadiscriminator IN ('fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurImpl','fr.inra.agrosyst.api.entities.referential.RefMaterielTractionImpl')

AND d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

ORDER BY domaine_id,domaine_campagne,combinaison_outil_id, combinaison_outil_code



--------------------
-- Cultures
--------------------

-- Présentation requête : Liste des espèces et variétés de chaque culture
	-- 3 sous requêtes (concernent les domaines actifs) 
		-- CAS 1 : espèces (dont l'espèce EDI n'est pas la vigne) associées à une variété
		-- CAS 2 : espèces (dont espèce EDI = vigne) associés à un cépage
		-- CAS 3 : espèces non associées à une variété
		-- CAS 4 : cultures sans espèces EDI

-- CAS 1 : espèces (dont l'espèce EDI n'est pas la vigne) des cultures associées à une variété
SELECT 
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne,
cpe.code culture_code,
cpe.topiaid culture_id,
cpe.name culture_nom, 
CASE cpe.type
	WHEN 'MAIN' then 'PRINCIPALE'
	WHEN 'CATCH' then 'DEROBEE'
	WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
END culture_type,
cps.topiaid AS espèce_id,
cps.code AS espèce_code,
-- cps.species AS espèce_EDI_id,
re.code_espece_botanique AS code_EDI_espece_botanique,
re.code_qualifiant_aee AS code_EDI_qualifiant,
re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
re.code_destination_aee AS code_EDI_destination,
re.libelle_espece_botanique AS espèce_EDI_nom_botanique,
re.libelle_qualifiant_aee AS espèce_EDI_qualifiant,
re.libelle_type_saisonnier_aee AS espèce_EDI_type_saisonnier,
re.libelle_destination_aee AS espèce_EDI_destination, 
-- cps.variety AS variété_id,
rvg.denomination AS variété_nom 
FROM croppingplanentry cpe
JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry 
JOIN refespece re ON cps.species = re.topiaid
JOIN refvarietegeves rvg ON cps.variety = rvg.topiaid
JOIN domain d ON cpe.domain = d.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

UNION 

-- CAS 2 : espèces (dont espèce EDI = vigne) associés à un cépage
SELECT 
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne,
cpe.code culture_code,
cpe.topiaid culture_id,
cpe.name culture_nom, 
CASE cpe.type
	WHEN 'MAIN' then 'PRINCIPALE'
	WHEN 'CATCH' then 'DEROBEE'
	WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
END culture_type,
cps.topiaid AS espèce_id,
cps.code AS espèce_code,
-- cps.species AS espèce_EDI_id,
re.code_espece_botanique AS code_EDI_espece_botanique,
re.code_qualifiant_aee AS code_EDI_qualifiant,
re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
re.code_destination_aee AS code_EDI_destination,
re.libelle_espece_botanique AS espèce_EDI_nom_botanique,
re.libelle_qualifiant_aee AS espèce_EDI_qualifiant,
re.libelle_type_saisonnier_aee AS espèce_EDI_type_saisonnier,
re.libelle_destination_aee AS espèce_EDI_destination,
-- cps.variety AS variété_id,
rvpg.variete AS variété_nom 
FROM croppingplanentry cpe
JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
JOIN refespece re ON cps.species = re.topiaid
JOIN refvarieteplantgrape rvpg ON cps.variety = rvpg.topiaid
JOIN domain d ON cpe.domain = d.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

UNION 

-- CAS 3 : espèces non associées à une variété
SELECT
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne,
cpe.code culture_code,
cpe.topiaid culture_id,
cpe.name culture_nom, 
CASE cpe.type
	WHEN 'MAIN' then 'PRINCIPALE'
	WHEN 'CATCH' then 'DEROBEE'
	WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
END culture_type,
cps.topiaid AS espèce_id,
cps.code AS espèce_code,
-- cps.species AS espèce_EDI_id,
re.code_espece_botanique AS code_EDI_espece_botanique,
re.code_qualifiant_aee AS code_EDI_qualifiant,
re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
re.code_destination_aee AS code_EDI_destination,
re.libelle_espece_botanique AS espèce_EDI_nom_botanique,
re.libelle_qualifiant_aee AS espèce_EDI_qualifiant, 
re.libelle_type_saisonnier_aee AS espèce_EDI_type_saisonnier, 
re.libelle_destination_aee AS espèce_EDI_destination, 
-- null AS variété_id,
null AS variété_nom 
FROM croppingplanentry cpe
JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry -- on part sur un join plutot qu'un left join car on cherche à avoir uniquement les cultures qui ont des espèces
JOIN refespece re ON cps.species = re.topiaid
JOIN domain d ON cpe.domain = d.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE
	
	AND cps.variety IS NULL

UNION 

-- CAS 4 : cultures sans espèces EDI
SELECT
d.code domaine_code, d.topiaid domaine_id, d.name domaine_nom, d.campaign domaine_campagne,
cpe.code culture_code,
cpe.topiaid culture_id,
cpe.name culture_nom, 
CASE cpe.type
	WHEN 'MAIN' then 'PRINCIPALE'
	WHEN 'CATCH' then 'DEROBEE'
	WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
END culture_type,
cps.topiaid AS espèce_id,
cps.code AS espèce_code,
-- cps.species AS espèce_EDI_id,
re.code_espece_botanique AS code_EDI_espece_botanique, 
re.code_qualifiant_aee AS code_EDI_qualifiant, 
re.code_type_saisonnier_aee AS code_EDI_type_saisonnier, 
re.code_destination_aee AS code_EDI_destination, 
re.libelle_espece_botanique AS espèce_EDI_nom_botanique, 
re.libelle_qualifiant_aee AS espèce_EDI_qualifiant,
re.libelle_type_saisonnier_aee AS espèce_EDI_type_saisonnier,
re.libelle_destination_aee AS espèce_EDI_destination,
-- null AS variété_id,
null AS variété_nom
FROM croppingplanentry cpe
LEFT JOIN domain d ON cpe.domain = d.topiaid
LEFT JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
LEFT JOIN refespece re ON cps.species = re.topiaid

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

		AND cps.topiaid is null -- pour avoir les cultures qui n'ont pas d'espèces


-------------------------
-- Pérennes_Réalisé
-------------------------

SELECT 
d.code domaine_code,
d.topiaid domaine_id,
d.name domaine_nom, -- à anonymiser
d.campaign domaine_campagne,		
gs.code sdc_code,
gs.topiaid sdc_id,
gs.name sdc_nom,
p.topiaid parcelle_id,
p.name parcelle_nom,
z.topiaid zone_id,
z.name zone_nom,
epcc.croppingplanentry culture_id,
cpe.name culture_nom,

(WITH liste_especes AS 
	(SELECT concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')) re2libelle
	FROM croppingplanspecies cps
	JOIN refespece re2 ON cps.species = re2.topiaid
	WHERE cps.croppingplanentry = cpe.topiaid)
SELECT string_agg(le.re2libelle,' ; ') 
FROM liste_especes le)
culture_especes_EDI,

epcc.plantingyear annee_plantation,
epcc.plantinginterfurrow inter_rang,
epcc.plantingspacing espacement_sur_le_rang,
epcc.plantingdensity densite_plantation,
epcc.orchardfrutalform forme_fruitiere_verger,
epcc.foliageheight hauteur_frondaison,
epcc.foliagethickness epaisseur_frondaison,
epcc.vinefrutalform forme_fruitiere_vigne,
epcc.orientation orientation_rangs,
epcc.plantingdeathrate taux_mortalite_plantation,
epcc.plantingdeathratemeasureyear annee_mesure_taux_mortalite,
epcc.weedtype type_enherbement,
epcc.othercharacteristics couvert_vegetal_commentaire,
epcc.pollinator pollinisateurs,
epcc.pollinatorpercent pourcentage_de_pollinisateurs,
epcc.pollinatorspreadmode mode_repartition_pollinisateurs


FROM effectivecropcyclephase eccp 
JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
JOIN croppingplanentry cpe ON epcc.croppingplanentry = cpe.topiaid
JOIN zone z ON epcc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN growingsystem gs ON p.growingsystem = gs.topiaid
JOIN growingplan gp ON gs.growingplan = gp.topiaid
JOIN domain d ON gp.domain = d.topiaid 

WHERE d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (select growingplan FROM growingsystem WHERE topiaid IN
            (Select growingsystem FROM growingsystem_networks
                WHERE networks IN ('fr.inra.agrosyst.api.entities.Network_3b79561d-010f-40eb-bc4c-6b7cde21f973', 
                'fr.inra.agrosyst.api.entities.Network_d709e8d6-2cf8-4372-9338-bc194d79769f') -- Topiaid du réseau SEBIOPAG et SOLUTION
                   )
                )
            )

AND d.active IS TRUE

	
----------------
-- A retenir
----------------

-- ORDER BY : les noms des champs doivent être ceux qui ont servis à faire l'union, donc les champs renommés et non les noms des champs initiaux.

-- LEFT JOIN / RIGHT JOIN
	-- Quand on commence à faire un LEFT / RIGHT JOIN, on doit continuer à le faire pour toutes les tables qui suivent ce join spécial 
	-- et qui sont en lien avec cette table." 
	-- c'est vrai pour le LEFT JOIN, par pour le RIGHT JOIN. Voir requête sur les intrants.
