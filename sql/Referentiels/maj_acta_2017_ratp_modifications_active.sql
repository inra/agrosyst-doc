﻿-- Table: modif_ratp

-- DROP TABLE modif_ratp;

CREATE TABLE modif_ratp
(
  id_produit text NOT NULL,
  action text,
  CONSTRAINT modif_ratp_pkey PRIMARY KEY (id_produit)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE modif_abstractinput
  OWNER TO ojolys;
  
GRANT ALL ON TABLE modif_abstractinput TO ojolys;

GRANT ALL ON TABLE modif_abstractinput TO agrosystuser;


-- verification
SELECT * FROM refactatraitementsproduit ratp 
WHERE active = FALSE 
and id_produit IN (
SELECT id_produit FROM modif_ratp   WHERE TRIM(action) IN ('désactiver ligne') )

-- inactiver
UPDATE refactatraitementsproduit ratp
SET active = false
WHERE id_produit IN 
	(SELECT id_produit FROM modif_ratp
	WHERE TRIM(action) IN ('désactiver ligne')
	)

-- changer les id : à faire en dernier !!!
UPDATE refactatraitementsproduit ratp
SET id_produit = nouvel_id_produit
FROM modif_radpsc_id 
WHERE ratp.id_produit = modif_radpsc_id.id_produit 

-- supprimer ligne
Alter table refactatraitementsproduit drop constraint uk_9if0hax45cju32g8lnrqd07x1 ;

DELETE FROM refactatraitementsproduit ratp
WHERE id_produit IN
(SELECT id_produit FROM modif_ratp
	WHERE action IN ('désactiver ligne'))
AND source = 'ACTA 2017'

select * from abstractinput where phytoproduct = 'fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit_c940d947-cf0d-4813-a087-f604e52cc08f'
