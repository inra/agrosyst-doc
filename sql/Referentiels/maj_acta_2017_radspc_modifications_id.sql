﻿-- Table: modif_radpsc_id

-- DROP TABLE modif_radpsc_id ;

CREATE TABLE modif_radpsc_id
(
  id_produit text NOT NULL,
  nouvel_id_produit text NOT NULL,
  CONSTRAINT modif_radpsc_id_pkey PRIMARY KEY (id_produit)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE modif_abstractinput
  OWNER TO ojolys;
  
GRANT ALL ON TABLE modif_abstractinput TO ojolys;

----------
SELECT DISTINCT id_produit FROM modif_radpsc_id

----------

-- Modifier id_produit
Alter table refactadosagespc drop constraint uk_8hwli7k7d1m0rcmgb3t3tn3fu ;

UPDATE refactadosagespc radspc
SET id_produit = nouvel_id_produit
FROM modif_radpsc_id 
WHERE radspc.id_produit = modif_radpsc_id.id_produit

----------------

