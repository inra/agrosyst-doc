﻿------------------------------------
-- Modifications RefActaDosageSpc
------------------------------------

CREATE TABLE modif_refactadosagespc
(
  topiaid character varying(255) NOT NULL,
  id_produit text NOT NULL,
  id_traitement integer NOT NULL,
  id_culture integer NOT NULL,
  dosage_spc_valeur double precision,
  dosage_spc_commentaire text NOT NULL,
  nom_produit text,
  code_traitement text,
  nom_culture text,
  remarque_culture text,
  commentaire_agrosyst text,
  active boolean,
  dosage_spc_unite character varying(255),
  id_produit_count text,
  id_traitement_count integer,
  id_culture_count integer,
  action text,
  dosage_spc_2 double precision, -- dosage_spc_2
  dosage_spc_unite_2 character varying(255),
  commentaire_agrosyst_2 text,
  CONSTRAINT modif_refactadosagespc_pkey PRIMARY KEY (topiaid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE modif_refactadosagespc
OWNER TO agrosystuser;

GRANT ALL ON TABLE modif_refactadosagespc TO agrosystuser ;

SELECT DISTINCT action FROM modif_refactadosagespc 
ORDER BY action ;

"désactiver ligne"
"modifier com_agrosyst"
"modifier dosage_spc_com com_agrosyst "
"modifier dose unité"
"modifier unité"
"rien"
"supprimer ligne"

-- Activer ligne
UPDATE refactadosagespc rads
SET active = TRUE
-- SELECT * FROM refactadosagespc rads
WHERE topiaid IN 
	(SELECT topiaid FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('activer ligne')
	)
	
-- Désactiver ligne
UPDATE refactadosagespc rads
SET active = FALSE
-- SELECT * FROM refactadosagespc rads
WHERE topiaid IN 
	(SELECT topiaid FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('désactiver ligne', 'supprimer ligne', 'supprimer ligne / désactiver')
	)

-- Supprimer ligne
DELETE FROM refactadosagespc  
WHERE topiaid IN
(SELECT topiaid FROM modif_refactadosagespc 
	WHERE action IN ('supprimer ligne', 'supprimer ligne / désactiver')
)


-- Modifier dose unité
UPDATE refactadosagespc rads
SET dosage_spc_valeur = mrads.dosage_spc_2, dosage_spc_unite = mrads.dosage_spc_unite_2
FROM modif_refactadosagespc mrads
WHERE rads.topiaid = mrads.topiaid
AND rads.topiaid IN 
	(SELECT topiaid FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('modifier dose unité')
	)

-- Modifier unité
UPDATE refactadosagespc rads
SET dosage_spc_unite = mrads.dosage_spc_unite_2
FROM modif_refactadosagespc mrads
WHERE rads.topiaid = mrads.topiaid
AND rads.topiaid IN 
	(SELECT topiaid FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('modifier unité')
	)

-- Modifier modifier com_agrosyst -- Modifier dosage_spc_com com_agrosyst
UPDATE refactadosagespc rads
SET commentaire_agrosyst= mrads.commentaire_agrosyst_2
FROM modif_refactadosagespc mrads
WHERE rads.topiaid = mrads.topiaid
AND rads.topiaid IN 
	(SELECT topiaid FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('modifier com_agrosyst', 'modifier dosage_spc_com com_agrosyst')
	)

-- Export du référentiel actualisé
SELECT * FROM refactadosagespc

-- Export du référentiel ratp
SELECT * FROM refactatraitementsproduit

-- Export du référentiel 
SELECT * FROM refactatraitementsproduitscateg