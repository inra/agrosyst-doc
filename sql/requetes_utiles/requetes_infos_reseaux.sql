select count(*) from network where active is true;

select * from network where name ='IT_XXXXX';
 
select * from network_parents where parents in (select topiaid  from network where name ='IT_XXXXX');

select * from network where topiaid in (select network from network_parents where parents in (select topiaid  from network where name ='IT_XXXXX')) and active is true;
																							  
select * from growingsystem_networks where networks in 
(select topiaid from network where topiaid in (select network from network_parents where parents in (select topiaid  from network where name ='IT_XXXXX')) and active is true);
											   
select count(*) from growingsystem gs where not exists (select * from growingsystem_networks gsn where gsn.growingsystem = gs.topiaid)

											   
-- Réseaux ne commençant pas par IR_ et dont le réseau parent commence par IT_ :
SELECT string_agg(n2.name,' ; ') reseau_it, n.name reseau_ir
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	WHERE n2.name like 'IT%'
   and n.name not LIKE 'IR_%'
	and n.active is true
	GROUP BY n.name
   order by 1, 2
																							  
-- Réseaux commençant par IR_ et dont le réseau parent ne commence pas par IT_
 SELECT string_agg(n2.name,' ; ') reseau_it, n.name reseau_ir
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	WHERE n2.name not like 'IT%'
   and n.name  LIKE 'IR_%'
	and n.active is true
	GROUP BY n.name
   order by 1, 2
																							  
										   
-- Réseaux directement dépendants du réseau CAN
SELECT string_agg(n2.name,' ; ') reseau_it, n.name reseau_ir
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	WHERE n2.name ='CAN'
	and n.active is true
	GROUP BY n.name
   order by 1, 2
											   
-- Réseaux directement dépendants des réseaux EXPE et DEPHY_EXPE_II
SELECT string_agg(n2.name,' ; ') reseau_parent, n.name reseau
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	WHERE n2.name in ('EXPE', 'DEPHY_EXPE_II')
	and n.active is true
	GROUP BY n.name
   order by 1, 2	
											   
											   
-- réseau d'un ou pls IT
SELECT string_agg(n2.name,' ; ') reseau_it, n.name reseau_ir
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	WHERE n2.name in ('IT_XXXXX','IT_YYYYY')
	and n.active is true
	GROUP BY n.name
   order by 1, 2