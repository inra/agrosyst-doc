drop table trad_libelles_reportsdc;

create table trad_libelles_reportsdc
(
	nom_rubrique VARCHAR(500),
	nom_base VARCHAR(500),
	traduction_interface VARCHAR(500)
);

INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('echelle de pression maladie ravageur','NONE','Nulle (absence)'),
	 ('echelle de pression maladie ravageur','LOW','Faible (un peu mais pas d''impact)'),
	 ('echelle de pression maladie ravageur','MODERATE','Moyenne (impact sur le rendement possible)'),
	 ('echelle de pression maladie ravageur','HIGH','Forte (impact certain sur le rendement et la marge)'),
	 ('echelle de maitrise maladie ravageur viti','NONE','Aucun symptôme'),
	 ('echelle de maitrise maladie ravageur viti','LOW','Symptômes sans effet sur le rendement ou la qualité'),
	 ('echelle de maitrise maladie ravageur viti','MODERATE','Symptômes avec effet très limité sur le rendement et la qualité'),
	 ('echelle de maitrise maladie ravageur viti','HIGH','Pertes économiques dues à la maladie'),
	 ('echelle de maitrise maladie ravageur assolee','NONE','Aucun symptome ou presence'),
	 ('echelle de maitrise maladie ravageur assolee','LOW','Symptomes sans effet sur le rendement');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('echelle de maitrise maladie ravageur assolee','MODERATE','Rendement un peu affecté sans effet sur la marge de la culture'),
	 ('echelle de maitrise maladie ravageur assolee','HIGH','Pertes économiques sur la culture'),
	 ('echelle de maitrise maladie ravageur arbo','NONE','Aucun symptôme'),
	 ('echelle de maitrise maladie ravageur arbo','LOW','Symptômes sans effet sur le rendement ou la qualité'),
	 ('echelle de maitrise maladie ravageur arbo','MODERATE','Symptômes avec effet très limité sur le rendement et la qualité'),
	 ('echelle de maitrise maladie ravageur arbo','HIGH','Pertes économiques dues à des dégâts sur fruits, feuilles, ou arbres'),
	 ('Note globale attaque maladie feuille','ZERO','0% de ceps touchés, 0% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_0_TO_10_2_INTENSITY','0 à 10 % de ceps touchés, moins de 2% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_10_TO_35_2_INTENSITY','10 à 35 % de ceps touchés, moins de 2% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_35_TO_70_2_INTENSITY','35 à 70 % de ceps touchés, moins de 2% d''intensité d''attaque');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('Note globale attaque maladie feuille','FROM_70_TO_100_2_INTENSITY','70 à 100 % de ceps touchés, moins de 2% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_35_TO_70_2_TO_5_INTENSITY','35 à 70 % de ceps touchés, 2 à 5% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_70_TO_100_2_TO_5_INTENSITY','70 à 100 % de ceps touchés, 2 à 5% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_70_TO_100_5_TO_15_INTENSITY','70 à 100 % de ceps touchés, 5 à 15% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_70_TO_100_15_TO_25_INTENSITY','70 à 100 % de ceps touchés, 15 à 25% d''intensité d''attaque'),
	 ('Note globale attaque maladie feuille','FROM_70_TO_100_25_INTENSITY','70 à 100 % de ceps touchés, > 25% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','ZERO','0% de ceps touchés, 0% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_0_TO_10_2_INTENSITY','0 à 10 % de ceps touchés, moins de 2% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_10_TO_35_2_INTENSITY','10 à 35 % de ceps touchés, moins de 2% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_35_TO_70_2_INTENSITY','35 à 70 % de ceps touchés, moins de 2% d''intensité d''attaque');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('Note globale attaque maladie grappe','FROM_70_TO_100_2_INTENSITY','70 à 100 % de ceps touchés, moins de 2% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_35_TO_70_2_TO_5_INTENSITY','35 à 70 % de ceps touchés, 2 à 5% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_70_TO_100_2_TO_5_INTENSITY','70 à 100 % de ceps touchés, 2 à 5% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_70_TO_100_5_TO_15_INTENSITY','70 à 100 % de ceps touchés, 5 à 15% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_70_TO_100_15_TO_25_INTENSITY','70 à 100 % de ceps touchés, 15 à 25% d''intensité d''attaque'),
	 ('Note globale attaque maladie grappe','FROM_70_TO_100_25_INTENSITY','70 à 100 % de ceps touchés, > 25% d''intensité d''attaque'),
	 ('Note globale attaque ravageurs feuille','NONE','pas de dégâts'),
	 ('Note globale attaque ravageurs feuille','LOW','attaque faible'),
	 ('Note globale attaque ravageurs feuille','MODERATE','attaque moyenne'),
	 ('Note globale attaque ravageurs feuille','HIGH','attaque forte');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('Note globale attaque ravageurs grappe','NONE','pas de dégâts'),
	 ('Note globale attaque ravageurs grappe','LOW','attaque faible'),
	 ('Note globale attaque ravageurs grappe','MODERATE','attaque moyenne'),
	 ('Note globale attaque ravageurs grappe','HIGH','attaque forte'),
	 ('echelle pression adventice viti','NONE','sol nu'),
	 ('echelle pression adventice viti','LOW','sol globalement nu avec toutefois quelques taches, sans gravité ni concurrence hydrique'),
	 ('echelle pression adventice viti','MODERATE','enherbement sans concurrence hydrique'),
	 ('echelle pression adventice viti','HIGH','enherbement generalise (voulu ou accidentel) avec baisse de vigeur et rendement'),
	 ('irrigation','NON_IRRIGABLE','Non irrigable'),
	 ('irrigation','NOT_IRRIGATED_CROP','Irrigable, culture non irriguée');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('irrigation','IRRIGATED_CROP_LIMITED_QUANTITY','Culture irriguée, quantité limitée'),
	 ('irrigation','IRRIGATED_CROP','Culture irriguée'),
	 ('stress hydrique mineral temperature','NO_STRESS','Pas de stress'),
	 ('stress hydrique mineral temperature','STRESS_NO_IMPACT_YIELD','Stress limité sans impact sur le rendement'),
	 ('stress hydrique mineral temperature','STRESS_NO_IMPACT_PROFIT','Stress limité sans impact sur la marge'),
	 ('stress hydrique mineral temperature','STRESS_IMPACT_YIELD_PROFIT','Stress impactant le rendement et la marge'),
	 ('echelle pression adventice assolee','NONE','Nulle (absence)'),
	 ('echelle pression adventice assolee','LOW','Faible (presence sporadique)'),
	 ('echelle pression adventice assolee','MODERATE','Moyenne (premiers ronds)'),
	 ('echelle pression adventice assolee','HIGH','Forte (concurrence en voie de generalisation)');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('echelle de maitrise adventice assolee','NONE','Pas de presence'),
	 ('echelle de maitrise adventice assolee','LOW','Presence sans concurrence ni multiplication significative (sous couvert ou sur couvert)'),
	 ('echelle de maitrise adventice assolee','MODERATE','Presence de premiers ronds de concurrence directe sur ou sous couvert (avec risque de multiplication futur)'),
	 ('echelle de maitrise adventice assolee','HIGH','Concurrence en voie de generalisation (multication des zones ou "ronds") sous ou sur couvert cultive'),
	 ('echelle de risque verse','NONE','Pas de risque'),
	 ('echelle de risque verse','LOW','Risque faible (un peu mais pas d''impact)'),
	 ('echelle de risque verse','MODERATE','Risque moyenne (impact sur le rendement possible)'),
	 ('echelle de risque verse','HIGH','Risque fort (impact certain sur le rendement et la marge)'),
	 ('echelle de maitrise verse','NONE','Pas de verse'),
	 ('echelle de maitrise verse','LOW','Un peu de verse sans effet sur le rendement');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('echelle de maitrise verse','MODERATE','Verse sans effet sur la marge de la culture'),
	 ('echelle de maitrise verse','HIGH','Verse impactant la performance economique'),
	 ('echelle de maitrise visee adv arboexpe','NONE','Rang nu toute la saison'),
	 ('echelle de maitrise visee adv arboexpe','LOW','Rang nu à certaines periodes'),
	 ('echelle de maitrise visee adv arboexpe','HIGH','Enherbement permanent'),
	 ('echelle de maitrise arbo ferme','NONE','Sol propre toute la saison'),
	 ('echelle de maitrise arbo ferme','LOW','Sol propre à certaines periodes (recolte)'),
	 ('echelle de maitrise arbo ferme','MODERATE','Enherbement permanent sans concurrence hydrique'),
	 ('echelle de maitrise arbo ferme','HIGH','Enherbement permanent avec concurrence (eau,vigueur,coloration …)'),
	 ('objectif rendement echelle int','4','LESS_50');
INSERT INTO public.trad_libelles_reportsdc (nom_rubrique,nom_base,traduction_interface) VALUES
	 ('objectif rendement echelle int','3','FROM_50_TO_75'),
	 ('objectif rendement echelle int','2','FROM_75_TO_95'),
	 ('objectif rendement echelle int','1','MORE_95');
