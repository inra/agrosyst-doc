select * from performance order by updatedate desc;

select p.topiaid, p.name as nom_export, CASE WHEN p.practiced is TRUE THEN 'Synthétisé'
            WHEN p.practiced is FALSE THEN 'Réalisé' ELSE 'OTHER' END, p.topiacreatedate,  p.updatedate, au.firstname, au.lastname, p.computestatus
from performance p
LEFT JOIN agrosystuser au  ON p.author = au.topiaid
where p.updatedate > TIMESTAMP '2019-01-01 00:00:00'
--and firstname = 'Anne'
and p.computestatus != 'SUCCESS'
order by updatedate desc;

