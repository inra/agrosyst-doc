﻿-----------------------------------------------
-- Modifications du référentiel Abstract Input
-----------------------------------------------

-- Création de la table listant les modifications à faire dans abstractinput

CREATE TABLE modif_abstractinput
(
  abstractinput_id character varying(255) NOT NULL,
  nom_produit text,
  qtavg double precision, 
  action text,
  phytoproductunit_2 character varying(255),
  qtavg_2 double precision,
    CONSTRAINT modif_abstractinput_pkey PRIMARY KEY (abstractinput_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE modif_abstractinput
OWNER TO agrosystuser;

GRANT ALL ON TABLE modif_abstractinput TO agrosystuser ;

SELECT DISTINCT action FROM modif_abstractinput
ORDER BY action ;

"modifier unité ai"
"modifier unité ai + conversion base phyto"
"modifier dose unité ai"
"modifier unité base phyto + conversion ai"
"modifier unité, qtavg divisé par 100"
"modifier unité, qtavg divisé par 1000"
"modifier unité, qtavg multiplié par 10"
"modifier unité, qtavg multiplié par 10 000"

-- Modifier abstractinput

-- Modifier unité
UPDATE abstractinput ai
SET phytoproductunit = mai.phytoproductunit_2
FROM modif_abstractinput mai
WHERE ai.topiaid = mai.abstractinput_id 
AND ai.topiaid IN 
	(SELECT abstractinput_id FROM modif_abstractinput
	WHERE action IN ('modifier unité ai', 'modifier unité ai + conversion base phyto')
	)

-- Modifier dose unité
UPDATE abstractinput ai
SET qtavg = mai.qtavg_2, phytoproductunit = mai.phytoproductunit_2
FROM modif_abstractinput mai
WHERE ai.topiaid = mai.abstractinput_id
AND ai.topiaid IN 
	(SELECT abstractinput_id FROM modif_abstractinput
	WHERE action IN ('modifier dose unité ai', 'modifier dose unité ai (vol bouillie ok = 10)', 'modifier dose unité ai (vol bouillie mis à 10)',
	'modifier unité, qtavg divisé par 100', 'modifier unité, qtavg divisé par 1000', 'modifier unité, qtavg multiplié par 10',
	'modifier unité, qtavg multiplié par 10 000')
	)



