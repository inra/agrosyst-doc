-----------------------------------------------
-- Exports_Agronomes_modeles_decisionels NON ANONYME
-----------------------------------------------

-- Modifications
-- 12/04/2023 : creation du script pour l'export. La table finale a pour cle unique : Modele_decionel_id , type_rubrique, une strategie et une culture

/*
Creation d'une table temporaire pour supprimer les doublons de codegroupecible dans le referentiel des groupes cibles 
on ne selectione que ceux actifs
il reste le cas 38 : on selectionne 'Cicadelles, punaises et psylles' puisque l'autre nom utilisé est 'Cicadelles, cercopides et psylles' alors qu'il correspond au 37
le cas 82 est un doublon avec une orthographe differente 
*/

create temporary table refgrpcibleunique as 
select distinct
code_groupe_cible_maa ,groupe_cible_maa ,active 
from refciblesagrosystgroupesciblesmaa 
where active = true and code_groupe_cible_maa not in ('38','82')
union 
select code_groupe_cible_maa ,groupe_cible_maa ,active
from refciblesagrosystgroupesciblesmaa
where active = true and (groupe_cible_maa in ('Cicadelles, punaises et psylles','Maladies des tâches foliaires'));

-----------------------
-- MODELES DECISIONELS
-----------------------

-- CAS 1 : Quand le bioagresseur est renseigne (avec et sans le groupe cible)
create table exports_agronomes_modeles_decisionels as 
select 
mm.topiaid md_ID,
d.name domaine_nom,  --domaine
d.topiaid domaine_id,
d.code domaine_code,
d.campaign campagne,
gp.name dispositif_nom,
gp.code dispositif_code,
gp.topiaid dispositif_id,
gs.sector filliere,
gs.name sdc_nom,     -- sdc
gs.topiaid sdc_id,
gs.code sdc_code,
reftypa.reference_label agriculture_type,
gs.dephynumber sdc_code_dephy,
mm.category categorie_md,
mm.mainchanges principaux_changements,
mm.changereason raison_changement,
mm.mainchangesfromplanned principaux_changements_depuisprevu,
mm.changereasonfromplanned raison_changements_depuisprevu,
sec.sectiontype type_rubrique,  -- section = rubrique
sec.bioagressortype type_bioagresseur,
refcible.groupe_cible_maa grouble_cible,
case 
	when sec.bioagressor like '%RefNuisibleEDI%' then refnui.reference_label
	when sec.bioagressor like '%RefAdventice%' then refadv.adventice
end bioagresseur_considere , 
sec.categoryobjective categorie_objectif,
sec.agronomicobjective objectif_agronomique,
sec.expectedresult resultat_attendu,
sec.categorystrategy categorie_strategie, 
sec.damagetype type_dommage,
str.implementationtype type_strategie,
str.strategytype type_levier,
reflev.lever levier,
str.explanation strategie_explication,
cpe.name culture_nom,
cpe.code culture_code,
cpe.topiaid culture_id
FROM (select * from section sec where bioagressor is not null) sec
left JOIN strategy str ON sec.topiaid = str.section -- left join : une section (rubrique) n'a pas toujours de strategie
left JOIN refstrategylever reflev on str.refstrategylever = reflev.topiaid
left JOIN refadventice refadv on sec.bioagressor = refadv.topiaid 
left JOIN refnuisibleedi refnui on sec.bioagressor = refnui.topiaid
/*
Pour avoir les groupes cibles il faut lier le refgroupecible avec : 
- quand c'est une adventice l'identifiant de l'adventice et le code
- quand c'est un nuisible une double clé : codegroupecible entre section et refgroupecible + reference_code entre refnuisible et refgroupecible. Ne marche que si le sec.bioagresseur est renseigné (Cas 1)
*/
left join (select * from refciblesagrosystgroupesciblesmaa refcible where active = true) refcible on case 
	when sec.bioagressor like '%RefAdventice%' then (refadv.identifiant = refcible.cible_edi_ref_code) -- quand c'est une adventice, la colonne codegroupeciblemaa de section n'est pas remplie. on joint donc refgrpcible seulement avec l'ID d'adventice
	when sec.bioagressor like '%RefNuisibleEDI%' then (refnui.reference_code = refcible.cible_edi_ref_code and sec.codegroupeciblemaa = refcible.code_groupe_cible_maa)
end
left JOIN crops_strategy cs on str.topiaid = cs.strategy  -- la culture n'est pas forcement renseignee
left JOIN croppingplanentry cpe on cs.crops = cpe.topiaid 
JOIN managementmode mm on sec.managementmode = mm.topiaid
JOIN growingsystem gs on mm.growingsystem = gs.topiaid 
join reftypeagriculture reftypa on gs.typeagriculture = reftypa.topiaid 
JOIN growingplan gp on gs.growingplan = gp.topiaid 
JOIN domain d on gp.domain = d.topiaid 
UNION
-- CAS 2 : Quand le groupe cible seul est renseigné ou que ni le grp cible ni le bioagresseur est saisi 
select 
mm.topiaid md_ID,
d.name domaine_nom,
d.topiaid domaine_id,
d.code domaine_code,
d.campaign campagne,
gp.name dispositif_nom,
gp.code dispositif_code,
gp.topiaid dispositif_id,
gs.sector filliere,
gs.name sdc_nom, 
gs.topiaid sdc_id,
gs.code sdc_code,
reftypa.reference_label agriculture_type,
gs.dephynumber sdc_code_dephy,
mm.category categorie_md,
mm.mainchanges principaux_changements,
mm.changereason raison_changement,
mm.mainchangesfromplanned principaux_changements_depuisprevu,
mm.changereasonfromplanned raison_changements_depuisprevu,
sec.sectiontype type_rubrique,
sec.bioagressortype type_bioagresseur,
refcible2.groupe_cible_maa grouble_cible,
sec.bioagressor bioagresseur_considere, -- meme si on est dans le cas sans bioagresseur saisi, on ajoute cette colonne pour unir avec le cas 1
sec.categoryobjective categorie_objectif,
sec.agronomicobjective objectif_agronomique,
sec.expectedresult resultat_attendu,
sec.categorystrategy categorie_strategie, 
sec.damagetype type_dommage,
str.implementationtype type_strategie,
str.strategytype type_levier,
reflev.lever levier,
str.explanation strategie_explication,
cpe.name culture_nom,
cpe.code culture_code,
cpe.topiaid culture_id
FROM (select * from section sec where bioagressor is null) sec
left JOIN strategy str ON sec.topiaid = str.section
left JOIN refstrategylever reflev on str.refstrategylever = reflev.topiaid
/*
Pour avoir le groupe cible quand le bioagresseur n'est pas saisi il faut lier le code groupe cible de section avec la table temporaire cree refgrpcibleunique 
qui permet de supprimer les doublons de nom mal orthographie pour un meme code 
*/
left join refgrpcibleunique refcible2 on sec.codegroupeciblemaa = refcible2.code_groupe_cible_maa
left JOIN crops_strategy cs on str.topiaid = cs.strategy
left JOIN croppingplanentry cpe on cs.crops = cpe.topiaid 
JOIN managementmode mm on sec.managementmode = mm.topiaid
JOIN growingsystem gs on mm.growingsystem = gs.topiaid 
join reftypeagriculture reftypa on gs.typeagriculture = reftypa.topiaid 
JOIN growingplan gp on gs.growingplan = gp.topiaid 
JOIN domain d on gp.domain = d.topiaid;
