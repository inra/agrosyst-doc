#!/bin/bash

# initialisation des couleurs
bold='\033[1;34m'
NC='\033[0m' # No Color
erreur=`echo '\033[0;31mERREUR :' $NC`
ok=`echo '\033[0;32mOK' $NC`
warning=`echo '\033[0;33mATTENTION :' $NC`

# REPERTOIRES donnees
old_export="./data/old/"
new_export="./data/new/"

mode=("anonyme" "non_anonyme")
type_fichier=("_brute.zip" "_perf.zip")

# Check if path data exist
path_test1=$new_export${mode[0]}"/"${mode[0]}${type_fichier[0]}
path_test2=$new_export${mode[0]}"/"${mode[0]}${type_fichier[1]}
path_test3=$new_export${mode[1]}"/"${mode[1]}${type_fichier[0]}
path_test4=$new_export${mode[1]}"/"${mode[1]}${type_fichier[1]}

path_test5=$old_export${mode[0]}"/"${mode[0]}${type_fichier[0]}
path_test6=$old_export${mode[0]}"/"${mode[0]}${type_fichier[1]}
path_test7=$old_export${mode[1]}"/"${mode[1]}${type_fichier[0]}
path_test8=$old_export${mode[1]}"/"${mode[1]}${type_fichier[1]}


if ! [ -f $path_test1 ]; then echo "L'archive de data n'existe pas" $path_test1; fi
if ! [ -f $path_test2 ]; then echo "L'archive de data n'existe pas" $path_test2; fi
if ! [ -f $path_test3 ]; then echo "L'archive de data n'existe pas" $path_test3; fi
if ! [ -f $path_test4 ]; then echo "L'archive de data n'existe pas" $path_test4; fi

if ! [ -f $path_test5 ]; then echo "L'archive de data n'existe pas" $path_test5; fi
if ! [ -f $path_test6 ]; then echo "L'archive de data n'existe pas" $path_test6; fi
if ! [ -f $path_test7 ]; then echo "L'archive de data n'existe pas" $path_test7; fi
if ! [ -f $path_test8 ]; then echo "L'archive de data n'existe pas" $path_test8; fi

## Fonctions
# message_erreur : affiche le message d'erreur
# en arg $1 : le test concerné
# $2 : le message que on veut afficher
# $3 : si on doit completer le message
function message_erreur(){
    test=$1
    messag1=$2
    messag2=$3
    echo ""
    echo -ne "${erreur} pour le test" $test $messag1 $messag2 ${NC}
    echo ""
}

# message_ok : affiche le message s'il y a pas eu d'erreur
function message_ok(){
    echo -e "............. ${ok}"
}


## separateur_colonne : vérifie l'homogénéité des fichiers : que toutes les lignes aient le même nombre de champs  
# $1 : path file
# $2 : file
function separateur_colonne(){
    path_file=$1
    f=$2
    testeur_sep_colonne=0

    head_nb_lines=`awk 'BEGIN{FS="@"} NR==1{print NF}' $path_file$f` # avoir le nb de champs de la 1ere ligne pour le comparer aux suivantes
    if [ `awk -v a=$head_nb_lines 'BEGIN{FS="@"} NF<a{print}' $path_file$f | wc -l` != 0 ]
    then
        testeur_sep_colonne=1
        nb_lines=`awk -v a=$head_nb_lines 'BEGIN{FS="@"} NF!=a{print}' $path_file$f | wc -l`
        message_erreur "Probleme d'homogénéité de separateurs de colonnes"
    fi
}

# FICHIERS VIDES
# $1 : path file
# $2 : file
function empty_file(){
    path_file=$1
    f=$2
    testeur_empty=0

    if [ `wc -l < $path_file$f` == 1 ]
    then
        testeur_empty=1
        message_erreur "fichier vide"  
    fi
}

## SEPARATEURS DE FIN DE LIGNES. 
# $1 : path file
# $2 : file
function end_lines(){
    # Si on fait file "nomfi", affiche with CRLF line terminators, mais quand c'est juste LF, ça n'affiche rien
    # va donc comparer avec en regardant la 1ere ligne : ^M$ => CRLF et LF => $
    path_file=$1
    f=$2
    testeur_end_lines=0

    if [ `head -n1 $path_file$f | cat -e | grep -c '\^M'` == 1 ]
    then 
        testeur_end_lines=1
        message_erreur "probleme de separateur : " `file $path_file$f`
    fi
}

## Encodage
# $1 : path file
# $2 : file
function encodage(){
    path_file=$1
    f=$2
    testeur_encodage=0

    if [ `file -b $path_file$f | grep -c 'UTF-8'` != 1 ]
        then
            testeur_encodage=1
            message_erreur "Encodage différent de UTF-8 : " `file -b $path_file$f`
    fi
}

## apply_test_in_zipfile
# $1 : quel repertoire de data : new
# $2 : anonyme ou non
# $3 : donnees brutes ou de perf
function apply_test_in_zipfile(){
    path_zip=$1$2"/"$2$3
    path_extract=$1$2"/extract_tmp/"
    declare -a liste_file_in_zip
    
    liste_file_in_zip=`unzip -l $path_zip | awk '{print $4}'` 
    
    rm -r $path_extract
    mkdir $path_extract
    
    echo ""
    echo -e ${bold}$path_zip${NC}
    for f in ${liste_file_in_zip[*]}
    do
        if [[ "$f" == *.csv ]]
        then
            unzip -qj $path_zip $f -d $path_extract

            echo -n $f

            ## apply tests
            separateur_colonne $path_extract $f
            empty_file $path_extract $f
            end_lines $path_extract $f
            encodage $path_extract $f

            if [ $testeur_sep_colonne == 0 ] && [ $testeur_empty == 0 ] && [ $testeur_end_lines == 0 ] && [ $testeur_encodage == 0 ]
            then
                message_ok
            fi

            rm $path_extract$f
        fi
    done
}

### Apply all test on anonyme and non_anonyme + perf and brute
for m in ${mode[*]}
do 
    for type in ${type_fichier[*]}
    do
        apply_test_in_zipfile $new_export $m $type
    done
done

# SCRIPT PYTHON : necessite d'ouvrir les tables

python ./main.py
