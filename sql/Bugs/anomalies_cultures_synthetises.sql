-- remonte les cultures intermdiaires qui n'existent plus ou qui existent mais sur une campagne diffrente de celle du synthtis :
SELECT ps.topiaid, pccc.intermediatecroppingplanentrycode
FROM practicedcropcycleconnection pccc
INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan INNER JOIN domain d0 ON gp.domain = d0.topiaid WHERE pccc.intermediatecroppingplanentrycode IS NOT NULL AND NOT EXISTS (
   SELECT 1 FROM croppingplanentry cpe0, domain d1 WHERE cpe0.domain = d1.topiaid
   AND pccc.intermediatecroppingplanentrycode = cpe0.code
   AND d0.code = d1.code
   AND ps.campaigns LIKE CONCAT('%' ,d1.campaign, '%') );
  
 
-- remonte les cultures intermdiaires qui n'existent effectivement plus :
SELECT ps.topiaid, pccc.intermediatecroppingplanentrycode
FROM practicedcropcycleconnection pccc
INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan INNER JOIN domain d0 ON gp.domain = d0.topiaid WHERE pccc.intermediatecroppingplanentrycode IS NOT NULL AND NOT EXISTS (
   SELECT 1 FROM croppingplanentry cpe0, domain d1 WHERE cpe0.domain = d1.topiaid
   AND pccc.intermediatecroppingplanentrycode = cpe0.code
   AND d0.code = d1.code
);
   
 

-- remonte les cultures principales qui n'existent plus ou qui existent mais sur une campagne diffrente de celle du synthtis :
SELECT distinct ps.topiaid, pccn.croppingplanentrycode
FROM practicedcropcyclenode pccn INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan INNER JOIN domain d0 ON gp.domain = d0.topiaid WHERE  NOT EXISTS (
   SELECT 1 FROM croppingplanentry cpe0, domain d1 WHERE cpe0.domain = d1.topiaid
   AND pccn.croppingplanentrycode = cpe0.code
   AND d0.code = d1.code
   AND ps.campaigns LIKE CONCAT('%' ,d1.campaign, '%') );
 

 
-- remonte les cultures principales qui n'existent effectivement plus :
SELECT ps.topiaid, ps.name, ps.campaigns, pccn.croppingplanentrycode FROM practicedcropcyclenode pccn INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan INNER JOIN domain d0 ON gp.domain = d0.topiaid WHERE  NOT EXISTS (
   SELECT 1 FROM croppingplanentry cpe0, domain d1 WHERE cpe0.domain = d1.topiaid
   AND pccn.croppingplanentrycode = cpe0.code
   AND d0.code = d1.code
);


-- identifier les interventions à supprimer/migrer ?

-- recherche des systèmes synthétisés, stades de culture, intervention liés à une connexion dont l'espèce Agrosyst n'existe plus en base de données
SELECT DISTINCT ps.topiaid topiaid_synthetise, pss.topiaid topiaid_stade, pi.topiaid topiaid_intervention
FROM practicedspeciesstade pss
INNER JOIN practicedintervention pi ON pss.practicedintervention = pi.topiaid
INNER JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
INNER JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccn.practicedseasonalcropcycle
INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
INNER JOIN domain d0 ON gp.domain = d0.topiaid
WHERE NOT EXISTS (
    SELECT 1 FROM croppingplanspecies cps
    INNER JOIN croppingplanentry cpe ON cps.croppingplanentry = cpe.topiaid
    INNER JOIN domain d1 ON d1.code = d0.code
    WHERE cpe.domain = d1.topiaid
    AND pss.speciescode = cps.code
);

-- recherche des systèmes synthétisés, stades de culture, interventions liés à une phase dont l'espèce Agrosyst n'existe plus en base de données 
SELECT DISTINCT ps.topiaid topiaid_synthetise, pss.topiaid topiaid_stade, pi.topiaid topiaid_intervention
FROM practicedspeciesstade pss
INNER JOIN practicedintervention pi ON pss.practicedintervention = pi.topiaid
INNER JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
INNER JOIN practicedcropcycle pcc ON pcc.topiaid = pccp.practicedperennialcropcycle
INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
INNER JOIN domain d0 ON gp.domain = d0.topiaid
WHERE NOT EXISTS (
    SELECT 1 FROM croppingplanspecies cps
    INNER JOIN croppingplanentry cpe ON cps.croppingplanentry = cpe.topiaid
    INNER JOIN domain d1 ON d1.code = d0.code
    WHERE cpe.domain = d1.topiaid
    AND pss.speciescode = cps.code
);
