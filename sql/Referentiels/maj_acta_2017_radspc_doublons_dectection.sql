﻿
-------------------------------------------------------------------------------
-- Requête comptant le nombre de doublons id_produit*id_traitement*id_culture
-------------------------------------------------------------------------------

WITH countref AS (

SELECT id_produit, id_traitement, id_culture, count (*)
FROM refactadosagespc
GROUP BY id_produit, id_traitement, id_culture
HAVING count(*) > 1 
)

SELECT *
FROM countref cr
JOIN refactadosagespc rads 
ON rads.id_produit = cr.id_produit 
AND rads.id_traitement = cr.id_traitement
AND rads.id_culture = cr.id_culture

ORDER BY rads.id_produit, rads.id_traitement, rads.id_culture