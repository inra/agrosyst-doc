-----------------------------------------------
-- Exports_Agronomes_contexte_NON ANONYME
-----------------------------------------------

-- Modifications 1-2 :
-- Ajout du volume de bouillie dans les intrants
-- rectification du nom de la colonne des unites d'intrants type autres. Car il semblerait que les ameliorations d'ergo autour de ce type aient engendrees des modifs du nom
-- Intrants_Realise : correction => ajout du filtre oublie pour ne ramener que les intrants des interventions sur les cultures assolees des parcelles affectees a un sdc
-- 1.3 : ajout de la cible des produits phyto (phytoproductinput_targets)
-- 1.4 : suppression des caracteres accentues au niveau des en-tetes des colonnes
-- 1.5 : ajout d'un filtre sur le champ ACTIVE (is true) sur tous les elements qui possedent ce champ en plus du domaine : sdc, dispositif, synthetise, parcelle, zone
-- 1.6 : stockage des resultats des requetes dans des tables temporaires pour faciliter le traitement des donnees a extraire
-- 2019-12-04 : ajout du stockage des resultats des requetes dans des tables temporaires
--              suppression d'une occurrence de la colonne resultat taille_limitant_risques_sanitaires qui etait en doublon
-- 2019-12-05 : ajout du type de production au niveau des sdc
-- 2019-12-12 : sdc : ajout et regroupement des reseaux "IR" dans une seule cellule dans le cas où il y a plusieurs réseaux IR pour un meme sdc (séparateur "|") (avant 1 ligne par couple sdc/reseau IR)
-- 	            sdc : ajout des reseaux parents "IT" associes avec le meme principe (regroupement + separateur "|")
-- 2019-12-13 : correction du nom d'une colonne : 	enheberment_naturel => enherbement_naturel
--              ajout des modes de commercialisation (sdc) au format json
-- 2020-01-07 : ajout de 2 infos au niveau de la requete Rotation_assolees_Synthetise pour chacune des cultures composant la rotation :
--				Fin de la rotation (OUI/NON)
--				Même campagne agricole que la culture précédente (OUI/NON)
-- 2020-05    : ajout pourcentage sole sdc de la culture dans la requete perennes_synthetise
--              ajout de la phase + durée de la phase dans la requete perennes_synthetise
--              ajout du code AMM dans intrants_synthetise et intrants_realise
--              ajout de la modalité de suivi dans DEPHY dans la requete sdc
--              ajout de la petite région agricole dans la requete domaines
--              ajout des variables relatives aux enjeux environnementaux dans la requete domaines
--              ajout de la phase + durée de la phase dans la requete perennes_realise
--              ajout de la surface de la zone dans les requetes Recolte_realise, Caracteristiques_Perennes_Realise et Succession_assolees_realise
--              ajout du numéro de branche de la culture dans la rotation (=ordonnée d'une culture) dans la requete Rotation_assolees_Synthetise
--              ajout de la requete Coordonnees_gps_domaines
--              ajout de la requete Ateliers_elevage
--              ajout d'une requete specifique aux zones, suppression de la surface de la zone dans les requetes Recolte_realise, Caracteristiques_Perennes_Realise et Succession_assolees_realise
--              ajout des infos de culture absente, des id des cultures dans la rotation, ainsi que du rang et de l'indicateur de branche du precedent dans la requete Rotation_assolees_Synthetise
-- 2020-10   :  ajout du champ phase_id de la table practicedcropcyclephase au niveau de la requête Caracteristiques_Perennes_Synthetise
-- 2020-11    : ajout des champs sdc_id, sdc_code et sdc_nom dans la requete Intrants_Realise
-- 2020-12    : ajout phase_id dans la requete perennes_realise
--            : ajout de la requete dispositifs
--            : requetes Recolte_realise et Recolte_synthetise : le union ramenait les lignes distinctement (contrairement au union ALL) et donc une seule ligne si plusieurs rendements
--              identiques pour un meme couple intervention/destination
--              => ajout de la somme sur les rendements pour tenir compte de ces cas
-- 2021-04    : ajout de critères d'extraction pour n'extraire que certaines données selon des critères définis (dispositifs DEPHY par exemple)
--              avec pour les domaines et les données directement associées présnce d'au moins 1 dispositif actif dans le domaine (domaine, coordonnees GPS domaine, ateliers d'elevage, parc materiel, cultures) 
-- 				et pour les autres données liées à un dispositif, le rattachement à  un dispositif actif (parcelles, zones, dipositifs, sdc, intrants, récoltes, pérennes, rotation, ITK)
--				=> les données non liées à un dispositif ne seront pas exportées alors qu'auparavant les parcelles et zones non rattachées à un dispositif étaient qd meme exportées
--              suppression du champ especes_de_l_intervention au niveau des tables intrants car ce champ n'était pas alimenté par les requetes et qu'il est présent dans les tables interventions
--				renommage de tous les champs avec le suffixe _name => suffixe _nom
-- 2021-05    : prise en compte du changement de table des cibles des traitements phyto : phytoproductinput_targets => phytoproducttarget
-- 2021-08    : ajout des champs mixspecies et mixvariety dans la requete cultures
-- 2021-10    : remplacement de la somme des rendements sur une intervention par la somme des moyennes des rendements pondérés par la surface utilisée parl'espèce pou les cultures non déclarée comme mix espèces
-- 2021-12    : mise à jour des requêtes d'export des intrants
-- 2022-08    : ajout des codes_convention_dephy des réseaux dans l'export sdc
-- 2023       : multiples mise à jour suite à local à intrant (exemple : ajout de l'export du local à intrant)
-- 2023-08    : ajout des informations sur les intrants de semences


-- mémo
-- copy (SELECT * FROM exports_agronomes_context_recolte_synthetise) TO '/tmp/exports_agronomes_context_recolte_synthetise.csv' WITH DELIMITER AS ';' CSV HEADER;

-- Domaines
-- Coordonnees_gps_domaines
-- Ateliers_elevage
-- Parcelles
-- Zones
-- Materiels
-- Dispositifs
-- SdC
-- Intrants_Realise
-- Intrants_Synthetise
-- Parcelle_Type
-- Recolte_realise  >>> requete a verifier
-- Recolte_synthetise  >>> requete a verifier
-- Cultures
-- Caracteristiques_Perennes_Realise
-- Caracteristiques_Perennes_Synthetise
-- Rotation_assolees_Synthetise
-- Succession_assolees_realise
-- A retenir
--itk non rattaché à un itk en realisé non exploitées

-- suppression des tables temporaires de stockage des resultats des requetes
DROP TABLE IF EXISTS exp_effective_hav; -- utilisé pour le calcul de la moyenne pondérée d'un rendement en rapport avec la surface cultivée d'une espèce/variété
DROP TABLE IF EXISTS exports_agronomes_context_ateliers_elevage;-- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_coordonnees_gps_domaines;-- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_cultures; -- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_dispositifs; -- dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_domaines; -- au moins 1 dispositif DEPHY actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_interventions_realise;
DROP TABLE IF EXISTS exports_agronomes_context_interventions_synthetise;
DROP TABLE IF EXISTS exports_agronomes_context_materiels; -- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_parcelles; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_parcelle_type; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_perennes_realise;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_perennes_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_recolte_realise;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_recolte_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_rotation_assolees_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_sdc; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_succession_assolees_realise;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_zones;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_intrant; 
DROP TABLE IF EXISTS exports_agronomes_context_local_intrant;
DROP TABLE IF EXISTS exports_agronomes_criteres;
DROP TABLE IF EXISTS exports_agronomes_parcelles_non_rattachees;
DROP TABLE IF EXISTS exp_practiced_hav; -- utilisé pour le calcul de la moyenne pondérée d'un rendement en rapport avec la surface cultivée d'une espèce/variété
DROP TABLE IF EXISTS perf_harvestingaction; -- utiliser pour les récolte pour avoir la caractéristique mix espece
DROP TABLE IF EXISTS perf_reportregional;

------------------------
-- criteres extraction
------------------------
-- domaines actifs avec au moins un dispositif DEPHY actif (et dates de campagnes cohérentes)
CREATE TEMPORARY TABLE exports_agronomes_criteres as
  SELECT
    d.topiaid domaine_id,
    d.code domaine_code,
    d.name domaine_nom,
    d.campaign domaine_campagne,
    gp.topiaid dispositif_id,
    gp.code dispositif_code,
    gp.name dispositif_nom,
    gp.type dispositif_type
  FROM growingplan gp
  JOIN domain d ON gp.domain = d.topiaid
  WHERE d.active IS TRUE
  AND gp.active IS TRUE
  AND gp.type !='NOT_DEPHY'
  AND campaign>1999
  AND campaign<2026;

CREATE INDEX exports_agronomes_criteres_idx1 on exports_agronomes_criteres(domaine_id, dispositif_id);
CREATE INDEX exports_agronomes_criteres_idx2 on exports_agronomes_criteres(dispositif_id);


-------------------------------------------
-- Recolte >>> tables utilitaire pour le calcul moyenne pondérée des rendement
-- on ajoute la colonne mixspecies pour remonter cette information provenant de la culture sur l'action
-------------------------------------------

CREATE TEMPORARY TABLE perf_harvestingaction (
  effectiveintervention character varying(255),
  practicedintervention character varying(255),
  topiaid character varying(255) NOT NULL,
  mainaction text,
  mixspecies boolean default false
);

INSERT INTO perf_harvestingaction
  SELECT aa.effectiveintervention, aa.practicedintervention, aa.topiaid, aa.mainaction
  FROM abstractaction aa
  WHERE topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl';

CREATE INDEX perf_harvestingaction_idx on perf_harvestingaction(topiaid);
CREATE INDEX perf_harvestingaction_practicedintervention_idx on perf_harvestingaction(practicedintervention);
CREATE INDEX perf_harvestingaction_effectiveintervention_idx on perf_harvestingaction(effectiveintervention);

UPDATE perf_harvestingaction aa SET mixspecies = TRUE
    WHERE aa.practicedintervention IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM practicedintervention pi
      JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
      JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
      JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
      JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
      JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
      JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      JOIN domain d ON crit.domaine_id = d.topiaid AND ps.campaigns LIKE CONCAT('%' ,d.campaign, '%')
      JOIN practicedspeciesstade pss ON pss.practicedintervention = pi.topiaid
      JOIN croppingplanspecies cps ON cps.code = pss.speciescode
      JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry AND (cpe.mixspecies IS TRUE OR cpe.mixvariety IS TRUE)
      WHERE aa.practicedintervention = pi.topiaid
      AND gs.active IS TRUE
      AND ps.active IS TRUE
    );

UPDATE perf_harvestingaction aa SET mixspecies = TRUE
    WHERE aa.practicedintervention IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM practicedintervention pi
      JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
      JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
      JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
      JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
      JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      JOIN domain d ON crit.domaine_id = d.topiaid AND ps.campaigns LIKE CONCAT('%' ,d.campaign, '%')
      JOIN practicedspeciesstade pss ON pss.practicedintervention = pi.topiaid
      JOIN croppingplanspecies cps ON cps.code = pss.speciescode
      JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry AND (cpe.mixspecies IS TRUE OR cpe.mixvariety IS TRUE)
      WHERE aa.practicedintervention = pi.topiaid
      AND gs.active IS TRUE
      AND ps.active IS TRUE
    );

UPDATE perf_harvestingaction aa SET mixspecies = TRUE
    WHERE aa.effectiveintervention IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM effectiveintervention ei
      INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
      INNER JOIN croppingplanspecies cps ON cps.topiaid = ess.croppingplanspecies
      JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry AND (cpe.mixspecies IS TRUE OR cpe.mixvariety IS TRUE)
      WHERE aa.effectiveintervention = ei.topiaid
    ) ;

-------------------------------------------
-- Recolte >>> tables utilitaire pour le calcul moyenne pondérée des rendement
-- à la surface des espèces sélectionnées sur une intervention
-------------------------------------------

CREATE TEMPORARY TABLE exp_effective_hav AS
  SELECT
  hav.topiaid,
  hav.harvestingaction,
  hav.destination,
  hav.yealdaverage,
  hav.yealdmedian,
  hav.yealdmin,
  hav.yealdmax,
  hav.yealdunit,
  hav.salespercent,
  hav.selfconsumedpersent,
  hav.novalorisationpercent,
  (SELECT count(ess.croppingplanspecies) FROM effectivespeciesstade ess WHERE ess.effectiveintervention = ei.topiaid ) AS nb_species,
  (SELECT SUM(cps.speciesarea::NUMERIC) FROM effectivespeciesstade ess INNER JOIN croppingplanspecies cps ON cps.topiaid = ess.croppingplanspecies WHERE ess.effectiveintervention = ei.topiaid ) AS crop_area,
  (SELECT cps.speciesarea FROM effectivespeciesstade ess INNER JOIN croppingplanspecies cps ON cps.topiaid = ess.croppingplanspecies AND cps.code = hav.speciescode WHERE ess.effectiveintervention = ei.topiaid ) AS species_area
  FROM harvestingactionvalorisation hav
  INNER JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention;

CREATE INDEX perf_exp_effective_hav0 ON exp_effective_hav(harvestingaction);
DELETE FROM exp_effective_hav WHERE nb_species IS NULL OR nb_species = 0;

-- la répartition des espèce n'a pas été déclarée, on applique une répartition par défaut
UPDATE exp_effective_hav SET species_area = 100.0/nb_species WHERE species_area IS NULL AND nb_species > 0;
UPDATE exp_effective_hav eeh SET crop_area =
  (SELECT SUM(eeh1.species_area)
   FROM exp_effective_hav eeh1
   WHERE eeh1.harvestingaction = eeh.harvestingaction
   AND eeh1.destination = eeh.destination)
   WHERE nb_species > 0
   AND crop_area IS NULL;

CREATE TEMPORARY TABLE exp_practiced_hav AS
  SELECT
    hav.topiaid,
    hav.harvestingaction,
    hav.destination,
    hav.yealdaverage,
    hav.yealdmedian,
    hav.yealdmin,
    hav.yealdmax,
    hav.yealdunit,
    hav.salespercent,
    hav.selfconsumedpersent,
    hav.novalorisationpercent,
    (SELECT count(pss.topiaid) FROM practicedspeciesstade pss WHERE pss.practicedintervention = pi.topiaid ) AS nb_species,
    (SELECT SUM(cps.speciesarea::NUMERIC) FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid ) AS crop_area,
	(
	    	case 
	    		when (SELECT COUNT(cps.speciesarea) FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.code = hav.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid) = 1 then (SELECT cps.speciesarea FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.code = hav.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid)   
	    		else null
	    	end 
	)as species_area
    FROM harvestingactionvalorisation hav
    INNER JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
    INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
    INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
    INNER JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
    INNER JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
    INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
    INNER JOIN croppingplanentry cpe ON crit.domaine_id = cpe.domain AND ((cpe.code = pccnt.croppingplanentrycode AND pi.intermediatecrop IS FALSE) OR (cpe.code = pccc.intermediatecroppingplanentrycode AND pi.intermediatecrop IS TRUE))
  UNION ALL
    SELECT
    hav.topiaid,
    hav.harvestingaction,
    hav.destination,
    hav.yealdaverage,
    hav.yealdmedian,
    hav.yealdmin,
    hav.yealdmax,
    hav.yealdunit,
    hav.salespercent,
    hav.selfconsumedpersent,
    hav.novalorisationpercent,
    (SELECT count(pss.topiaid) FROM practicedspeciesstade pss WHERE pss.practicedintervention = pi.topiaid ) AS nb_species,
    (SELECT SUM(cps.speciesarea::NUMERIC) FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid ) AS crop_area,
    (SELECT cps.speciesarea FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.code = hav.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid) AS species_area
    FROM harvestingactionvalorisation hav
    INNER JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
    INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
    INNER JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
    INNER JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
    INNER JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
    INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
    INNER JOIN croppingplanentry cpe ON crit.domaine_id = cpe.domain AND ppcc.croppingplanentrycode = cpe.code;

CREATE INDEX perf_exp_practiced_hav0 ON exp_practiced_hav(harvestingaction);
-- la répartition des espèce n'a pas été déclarée, on applique une répartition par défaut
UPDATE exp_practiced_hav SET species_area = 100.0/nb_species WHERE species_area IS NULL AND nb_species > 0;
UPDATE exp_practiced_hav eph SET crop_area = (SELECT SUM(eph1.species_area) FROM exp_practiced_hav eph1 WHERE eph1.harvestingaction = eph.harvestingaction AND eph.destination = eph1.destination) WHERE nb_species > 0 AND crop_area IS NULL;

DELETE FROM exp_practiced_hav WHERE nb_species IS NULL OR nb_species = 0;
-------------
-- Domaines
-------------
CREATE TABLE exports_agronomes_context_domaines AS
  WITH domainresponsibles as (
    SELECT domaincode, string_agg(au.lastname||' '||au.firstname, ', ') AS responsibles
    FROM userrole ur
    JOIN agrosystuser au ON ur.agrosystuser = au.topiaid
    WHERE domaincode IS NOT NULL
    GROUP BY domaincode
  )
  SELECT
    d.code domaine_code,
    d.topiaid domaine_id,
    d.name domaine_nom,
    d.siret,
    d.campaign domaine_campagne,
    d.type domaine_type,
    rl.departement departement,
    rl.codeinsee||' - '||rl.commune commune,
    rl.petiteRegionAgricoleCode|| ' - '||rl.petiteRegionAgricoleNom petite_region_agricole,
    d.zoning domaine_zonage,
    d.uaavulnarablepart pct_SAU_zone_vulnerable,
    d.uaastructuralsurplusareapart pct_SAU_zone_excedent_structurel,
    d.uaaactionpart pct_SAU_zone_actions_complementaires,
    d.uaanatura2000part pct_SAU_zone_natura_2000,
    d.uaaerosionregionpart pct_SAU_zone_erosion,
    d.uaawaterresourceprotectionpart pct_SAU_perimetre_protection_captage,
    d.description domain_description,
    rls.libelle_insee statut_juridique_nom,
    d.statuscomment statut_juridique_commentaire,
    d.usedagriculturalarea SAU,
    d.croppingplancomment cultures_commentaire,
    d.otheractivitiescomment autres_activites_commentaire,
    d.workforcecomment MO_commentaire,
    d.partnersnumber nombre_associes,
    d.otherworkforce MO_familiale_et_associes,
    d.permanentemployeesworkforce MO_permanente,
    d.temporaryemployeesworkforce MO_temporaire,
    d.familyworkforcewage MO_familiale_remuneration,
    d.wagecosts charges_salariales,
    d.cropsworkforce MO_conduite_cultures_dans_domaine_expe,
    d. msafee cotisation_MSA,
    d.averagetenantfarming fermage_moyen,
    d.decoupledassistance aides_decouplees,
    ro1.libelle_otex_18_postes otex_18_nom,
    ro2.libelle_otex_70_postes otex_70_nom,
    d.orientation otex_commentaire,
    dr.responsibles responsables_domaine

  FROM domain d
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON d.topiaid = crit.domaine_id
  LEFT JOIN domainresponsibles dr ON d.code = dr.domaincode
  LEFT JOIN reflocation rl ON d.location = rl.topiaid
  LEFT JOIN reflegalstatus rls ON d.legalstatus = rls.topiaid
  LEFT JOIN refotex ro1 ON d.otex18 = ro1.topiaid
  LEFT JOIN refotex ro2 ON d.otex70 = ro2.topiaid;

-----------------------------
-- Coordonnees_gps_domaines
-----------------------------
CREATE TABLE exports_agronomes_context_coordonnees_gps_domaines AS
  SELECT
    crit.domaine_id,
    g.name AS coordonnees_nom_centre_operationnel,
    g.description AS coordonnees_description_centre_operationnel,
    g.longitude,
    g.latitude
  FROM geopoint g
  --JOIN domain d ON g.domain = d.topiaid
  --where d.active is true
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON g.domain = crit.domaine_id
  AND g.longitude!=0
  AND g.latitude !=0
  AND g.validated is true;

---------------------
-- Ateliers_elevage
---------------------
CREATE TABLE exports_agronomes_context_ateliers_elevage AS
  SELECT
  crit.domaine_id,
  l.topiaid AS atelier_elevage_id,
  r.animaltype AS atelier_elevage_type_animaux,
  l.livestockunitsize AS atelier_elevage_taille,
  r.animalpopulationunits AS atelier_elevage_unite
  FROM livestockunit l
  --JOIN domain d ON l.domain = d.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON l.domain = crit.domaine_id
  JOIN refanimaltype r ON l.refanimaltype = r.topiaid;
--where d.active is true;

---------------------
-- Parcelles
---------------------
CREATE TABLE exports_agronomes_context_parcelles AS
  WITH plot_zone AS (
    SELECT z.plot, count(*) nombre_de_zones
    FROM zone z
    where z.active IS TRUE
    GROUP BY z.plot )

  SELECT
    ------ Informations generales
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    p.code parcelle_code, p.topiaid parcelle_id,
    p.name parcelle_nom, -- a anonymiser
    p.area parcelle_surface,
    rl.codepostal code_postal,
    rl.commune commune, 	-- a anonymiser
    p.comment parcelle_commentaire,
    pz.nombre_de_zones,
    ------ Informations sur le zonage de la parcelle
    CASE p.outofzoning
      WHEN true then 'oui'
      WHEN false then 'non'
      END parcelle_hors_zonage,

    (WITH liste_zonages AS (
      SELECT rpze.libelle_engagement_parcelle zonages
      FROM basicplot_plotzonings bpz
      JOIN refparcellezonageedi rpze ON rpze.topiaid = bpz.plotzonings
      WHERE bpz.basicplot = p.topiaid
      )
      SELECT string_agg (zonages, ' ; ') FROM liste_zonages
    ) AS zonages_parcelle,

    ------ Informations sur les equipements de la parcelle
    p.equipmentcomment AS equipement_commentaire,

    CASE p.drainage
      WHEN true then 'oui'
      WHEN false then 'non'
    END drainage,

    p.drainageyear AS drainage_annee_realisation,

    CASE p.frostprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_gel,

    p.frostprotectiontype AS protection_anti_gel_type,

    CASE p.hailprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_grele,

    CASE p.rainproofprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_pluie,

    CASE p.pestprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_insectes,

    p.otherequipment autre_equipement,

    ------ Informations sur le sol de la parcelle
    p.solcomment sol_commentaire,

    (-- Requete pour avoir le nom du sol
      SELECT rs.sol_nom
      FROM ground g
      JOIN refsolarvalis rs ON g.refsolarvalis = rs.topiaid
      WHERE p.ground = g.topiaid
    ) AS sol_nom_ref,

    (-- Requete pour avoir le nom de la texture de surface
      SELECT rstg.classes_texturales_gepaa
      FROM refsoltexturegeppa rstg
      WHERE rstg.topiaid = p.surfacetexture
    ) AS texture_surface,

    (-- Requete pour avoir le nom de la texture de surface
      SELECT rstg.classes_texturales_gepaa
      FROM refsoltexturegeppa rstg
      WHERE rstg.topiaid = p.subsoiltexture
    ) AS texture_sous_sol,

    p.solwaterph sol_ph,

    p.solstoniness pierrosite_moyenne,

    (-- Requete pour avoir la classe de profondeur du sol
      SELECT rspi.libelle_classe
      FROM refsolprofondeurindigo rspi
      WHERE rspi.topiaid = p.soldepth
    ) AS sol_profondeur,

    p.solmaxdepth sol_profondeur_max,

    p.solorganicmaterialpercent teneur_MO,

    CASE p.solhydromorphisms
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END hydromorphie,

    CASE p.sollimestone
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END calcaire,

    p.soltotallimestone AS proportion_calcaire_total,

    p.solactivelimestone AS proportion_calclaire_actif,

    CASE p.solbattance
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END battance,

    ------ Informations sur le type d'irrigation de la parcelle
    CASE p.irrigationsystem
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END irrigation,

    CASE p.fertigationsystem
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END fertirrigation,

    p. waterorigin AS origine_eau,
    p.irrigationsystemtype AS irrigation_type,
    p.pompenginetype AS irrigation_pompe,
    p.hosespositionning AS irrigation_position_tuyaux,
    ------ Autres informations generales
    CASE p. maxslope
      WHEN 'MORE_THAN_FIVE' THEN '> 5%'
      WHEN 'TWO_TO_FIVE' THEN '2 - 5%'
      WHEN 'ZERO' THEN '0%'
      WHEN 'ZERO_TO_TWO' THEN '0 - 2%'
    END pente,

    CASE p.waterflowdistance
      WHEN 'FIVE_TO_TEN' THEN '5 - 10 m'
      WHEN 'LESS_THAN_THREE' THEN '< 3 m'
      WHEN 'MORE_THAN_TEN' THEN '> 10 m'
      WHEN 'THREE_TO_FIVE' THEN '3 - 5 m'
    END distance_cours_eau,

    CASE p.bufferstrip
      WHEN 'BUFFER_STRIP_NEXT_TO_WATER_FLOW' THEN 'Bande tampon (5m) en bord de cours deau' -- On ne peut mettre ' dans le texte ?
      WHEN 'BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW' THEN 'Bande tampon pérenne enherbée dau moins 5m hors bord de cours deau'
      WHEN 'NONE' THEN 'Aucune'
    END bande_enherbee

  FROM plot p
  --JOIN domain d ON p.domain = d.topiaid
  --LEFT JOIN growingsystem gs ON p.growingsystem = gs.topiaid -- Lister aussi les parcelles qui ne sont pas reliees a un SdC ?
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  JOIN plot_zone pz ON pz.plot = p.topiaid
  LEFT JOIN refsolarvalis rs ON p.ground = rs.topiaid
  LEFT JOIN reflocation rl ON p.location = rl.topiaid
  WHERE gs.active IS TRUE
  AND p.active IS TRUE;

---------------------
-- Zones
---------------------
CREATE TABLE exports_agronomes_context_zones AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    z.plot parcelle_id,
    z.topiaid zone_id,
    z.name zone_nom,
    z.area zone_surface,
    z.latitude,
    z.longitude,
    z.type zone_type
  FROM zone z
  JOIN plot p ON z.plot = p.topiaid
  --JOIN domain d ON p.domain = d.topiaid
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active is TRUE;

--------------------
-- Materiels
--------------------
-- Presentation requete : 
	-- 2 sous requetes (concernent les domaines actifs) 
		-- CAS 1 : on recherche tous les outils declares dans les combinaisons d'outils
		-- CAS 2 : on recherche tous les tracteurs/automoteurs declares dans les combinaisons d'outils

CREATE TABLE exports_agronomes_context_materiels as
-- CAS 1
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    tc.topiaid combinaison_outil_id,
    tc.code combinaison_outil_code,
    tc.toolscouplingname combinaison_outils_nom,
    rm.topiaid materiel_ref_id,
    e.topiaid materiel_id,

    CASE e.materieleta
     WHEN true THEN 'oui'
     WHEN false THEN 'non'
    END materiel_ETA_CUMA,

    e.name materiel_nom,
    rm.typemateriel1 AS materiel_caracteristique1,
    rm.typemateriel2 AS materiel_caracteristique2,
    rm.typemateriel3 AS materiel_caracteristique3,
    rm.typemateriel4 AS materiel_caracteristique4,
    rm.uniteparan AS utilisation_annuelle,
    rm.unite AS utilisation_annuelle_unite,
    rm.chargesfixesparunitedevolumedetravailannuel coût_par_unite_travail_annuel
  FROM equipment e
  LEFT JOIN equipments_toolscoupling etc ON e.topiaid = etc.equipments
  LEFT JOIN toolscoupling tc ON etc.toolscoupling = tc.topiaid -- c'est a cette etape que l'on associe a une CO un tracteur, dans la table toolscoupling !
  --JOIN domain d ON e.domain = d.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON e.domain = crit.domaine_id
  JOIN refmateriel rm ON e.refmateriel = rm.topiaid
  WHERE rm.topiadiscriminator IN ('fr.inra.agrosyst.api.entities.referential.RefMaterielOutilImpl','fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationImpl')
  --AND d.active IS TRUE
  UNION
  -- CAS 2
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    tc.topiaid AS combinaison_outil_id,
    tc.code AS combinaison_outil_code,
    tc.toolscouplingname AS combinaison_outils_nom,
    rm.topiaid AS materiel_ref_id,
    e.topiaid AS materiel_id,
    CASE e.materieleta
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END materiel_ETA_CUMA,
    e.name AS materiel_nom,
    rm.typemateriel1 AS materiel_caracteristique1,
    rm.typemateriel2 AS materiel_caracteristique2,
    rm.typemateriel3 AS materiel_caracteristique3,
    rm.typemateriel4 AS materiel_caracteristique4,
    rm.uniteparan AS utilisation_annuelle,
    rm.unite AS utilisation_annuelle_unite,
    rm.chargesfixesparunitedevolumedetravailannuel AS coût_par_unite_travail_annuel
  FROM equipment e
  LEFT JOIN toolscoupling tc ON e.topiaid = tc.tractor
  --JOIN domain d ON e.domain = d.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON e.domain = crit.domaine_id
  JOIN refmateriel rm ON e.refmateriel = rm.topiaid
  WHERE rm.topiadiscriminator IN (
    'fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurImpl',
    'fr.inra.agrosyst.api.entities.referential.RefMaterielTractionImpl');

--AND d.active IS TRUE;


-----------------
-- Dispositifs
-----------------
CREATE TABLE exports_agronomes_context_dispositifs AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    crit.dispositif_id,
    crit.dispositif_code,
    crit.dispositif_nom,
    crit.dispositif_type
  FROM exports_agronomes_criteres crit;

-----------------
-- SdC
-----------------

CREATE TABLE exports_agronomes_context_sdc AS

  WITH sdc AS (
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    crit.dispositif_code,
    crit.dispositif_id,
    crit.dispositif_type,
    gs.code AS sdc_code,
    gs.topiaid AS sdc_id,
    gs.name AS sdc_nom,
    gs.modality AS sdc_modalite_suivi_dephy,
    gs.dephynumber AS sdc_code_dephy,
    gs.description AS sdc_commentaire,
    CASE gs.validated
      WHEN true then 'oui'
      WHEN false then 'non'
    END sdc_valide,
    gs.sector AS sdc_filiere,
    gs.production AS sdc_type_production,
    rt.reference_label AS sdc_type_agriculture,
    gs.categorystrategy AS sdc_strategie_categorie,
    gs.affectedarearate AS sdc_part_SAU_domaine,
    gs.affectedworkforcerate AS sdc_part_MO_domaine,
    gs.domainstoolsusagerate AS sdc_part_materiel_domaine,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Couverts associés,  plantes de service'),
             'non') couverts_associes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Cultures intermédiaires a effet allélopathique ou biocide'),
             'non') ci_effet_allelo_ou_biocide,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Cultures intermédiaires attractives pour les auxiliaires'),
             'non') ci_attractives_pour_auxiliaires,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Cultures intermédiaires piège a nitrate'),
             'non') cipan,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Evitement par la date de semis - semis précoce'),
             'non') semis_precoce,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Evitement par la date de semis - semis tardif'),
             'non') semis_tardif,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Exportation des menu-pailles'),
             'non') exportation_menu_pailles,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Destruction de la litière des feuilles'),
             'non') destruction_litiere,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Efficience - Dose adaptée à la surface foliaire'),
             'non') dose_adaptee_surf_foliaire,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Filets anti-insectes'),
             'non') filet_anti_insectes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Taille des organes contaminés'),
      'non') taille_organes_contamines,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Taille limitant les risques sanitaires'),
      'non') taille_limitant_risques_sanitaires,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Adaptation de la densité - faible densité'),
      'non') faible_densite,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Adaptation de la densité - forte densité'),
      'non') forte_densite,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '34'),
      'non') faible_ecartement,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '33'),
      'non') fort_ecartement,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '36'),
      'non') ajustement_fertilisation,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '35'),
      'non') ajustement_irrigation,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Autre forme de désherbage'),
      'non') desherbage_autre_forme,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Cultures pièges'),
      'non') cultures_pieges,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Désherbage mécanique fréquent'),
      'non') desherbage_meca_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Désherbage mécanique occasionel'),
      'non') desherbage_meca_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Désherbage thermique'),
      'non') desherbage_thermique,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Adaptation de la lutte à la parcelle'),
      'non') adaptation_lutte_a_la_parcelle,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '20'),
      'non') optim_conditions_application,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose autres produits phytosanitaires'),
      'non') reduction_dose_autres_phyto,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose fongicides'),
      'non') reduction_dose_fongi,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose herbicides'),
      'non') reduction_dose_herbi,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose insecticides'),
      'non') reduction_dose_insec,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Traitements localisés en intra-parcellaire'),
      'non') traitement_localise,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation d''adjuvants'),
      'non') utilisation_adjuvants,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation de seuils pour les décisions de traitement'),
      'non') utilisation_seuils,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation de stimulateur de défense'),
      'non') utilisation_stim_defense,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation d''outil d''aide à la décision pour les traitements'),
      'non') utilisation_oad,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés compétitives sur adventices'),
      'non') var_competitives_adventice,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles à la verse'),
      'non') var_peu_sensibles_verse,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles aux maladies'),
      'non') var_peu_sensibles_maladies,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles aux ravageurs'),
      'non') var_peu_sensibles_ravageurs,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Lutte biologique par confusion sexuelle'),
      'non') lutte_bio_confu_sexuelle,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Lutte biologique autre'),
      'non') lutte_bio_autre,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Mélange d''espèces ou de variétés'),
      'non') melange,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Gestion spécifique des résidus supports d''inoculum (broyage, enfouissement)'),
      'non') gestion_residus,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Monoculture ou rotation courte'),
      'non') monoculture_rotation_courte,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation avec cultures rustiques ou étouffantes (adventices)'),
      'non') rotation_cultures_rustiques,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation diversifiée équilibrée avec prairie temporaire'),
      'non') rotation_diversifiee_avec_pt,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation diversifiée équilibrée avec prairie temporaire'),
      'non') rotation_diversifiee_sans_pt,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation diversifiée par l''introduction d''une culture dans une rotation courte'),
      'non') rotation_diversifiee_intro_une_culture,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Enherbement (espèces semées)'),
      'non') enherbement_seme,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Enherbement naturel maîtrisé'),
      'non') enherbement_naturel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Agroforesterie'),
      'non') agroforesterie,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Arbres isolés ou alignements en bordure de parcelle'),
      'non') arbres_bordure_parcelle,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Broyage des bordures'),
      'non') broyage_bordure,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Gestion des bordures de bois pour favoriser la biodiversité'),
      'non') gestion_bordure_de_bois,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Présence de haies anciennes'),
      'non') haies_anciennes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Présence de jeunes haies'),
      'non') haies_jeunes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Proximité de bandes enherbées favorisant les auxiliaires'),
      'non') bandes_enherbees,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Proximité de bandes fleuries favorisant les auxiliaires'),
      'non') bandes_fleuries,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Proximité de bois et bosquets'),
      'non') bois_bosquet,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Décompactage  occasionnel'),
      'non') decompactage_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Décompactage  profond fréquent (fréquence > 50%)'),
      'non') decompactage_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Faux semis ponctuels'),
      'non') faux_semis_ponctuels,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Faux semis intensifs (travaux superficiels répétés spécifiques)'),
      'non') faux_semis_intensifs,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Labour occasionnel'),
      'non') labour_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Labour fréquent (fréquence > 50%)'),
      'non') labour_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Labour systématique (fréquence > 80%)'),
      'non') labour_systematique,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Semis direct sans travail du sol occasionnel'),
      'non') semis_direct_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Semis direct sans travail du sol systématique'),
      'non') semis_direct_systematique,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Strip till occasionnel'),
      'non') strip_till_occasionnel,


    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Strip till fréquent ou systématique'),
      'non') strip_till_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Techniques culturales superficielles uniquement'),
      'non') tcs
	
  FROM growingsystem gs
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  LEFT JOIN reftypeagriculture rt ON gs.typeagriculture = rt.topiaid

  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  --and gs.active IS TRUE
  WHERE gs.active IS TRUE

  GROUP BY crit.domaine_id, crit.domaine_code, crit.domaine_nom, crit.domaine_campagne,
  crit.dispositif_id, crit.dispositif_code, crit.dispositif_type,
  gs.topiaid, gs.code, gs.name,  gs.dephynumber, gs.description,
  gs.validated, gs.sector,
  rt.reference_label,
  gs.categorystrategy,
  gs.affectedarearate

  ),
  reseaux_parents AS
    (SELECT n.topiaid, n.name reseau_ir, n.codeconventiondephy, string_agg(distinct n2.name,'|') reseau_it
      FROM network_parents np
      JOIN network n ON n.topiaid = np.network
      JOIN network n2 ON np.parents = n2.topiaid
      GROUP BY n.topiaid, n.name
    ),
  modes_commercialisation AS
    (SELECT dest.growingsystem,
      json_object_agg(
        dest.marketingdestination,
        CASE WHEN (dest.selectedforgs IS TRUE OR (dest.part IS NOT NULL AND dest.part>0)) THEN coalesce(dest.part::varchar(5),'OUI') ELSE 'NON' END
        ORDER BY dest.marketingdestination) modes_comm
    from (
      select mdo.growingsystem,  rfd.sector, rfd.marketingdestination, mdo.selectedforgs,	mdo.part
      from MarketingDestinationObjective mdo
      JOIN RefMarketingDestination rfd ON mdo.refmarketingdestination = rfd.topiaid
      ) dest
     GROUP BY dest.growingsystem)

    SELECT
      sdc.*,
      (select string_agg(distinct rp.reseau_ir, '|')
        FROM reseaux_parents rp
        JOIN growingsystem_networks gn ON rp.topiaid = gn.networks
        where gn.growingsystem = sdc.sdc_id
      ) reseaux_IR,

      (select string_agg(distinct rp.reseau_it, '|')
        FROM reseaux_parents rp
        JOIN growingsystem_networks gn ON rp.topiaid = gn.networks
        where gn.growingsystem = sdc.sdc_id
      ) reseaux_IT,

      (select string_agg(distinct rp.codeconventiondephy, '|')
        FROM reseaux_parents rp
        JOIN growingsystem_networks gn ON rp.topiaid = gn.networks
        where gn.growingsystem = sdc.sdc_id
      ) codes_convention_dephy,

      (select modes_comm
        FROM modes_commercialisation mc
        where mc.growingsystem = sdc.sdc_id
      ) modes_commercialisation
    FROM sdc;

--------------------------
-- Parcelle_Type
--------------------------
CREATE TABLE exports_agronomes_context_parcelle_type AS
  SELECT
    crit.domaine_id, crit.domaine_code, crit.domaine_nom, crit.domaine_campagne,
    gs.topiaid sdc_id,
    ps.topiaid systeme_synthetise_id, ps.name systeme_synthetise_nom, ps.campaigns systeme_synthetise_campagnes,
    pp.topiaid parcelle_type_id, pp.name parcelle_type_nom,
    pp.area parcelle_type_surface, pp.comment parcelle_type_commentaire,
    pp.outofzoning parcelle_type_hors_zonage, pp.equipmentcomment equipement_commentaire, pp.drainage drainage,
    pp.drainageyear drainage_annee_realisation,
    pp.frostprotection protection_anti_gel, pp.frostprotectiontype protection_anti_gel_type, pp.hailprotection protection_anti_grele,
    pp.rainproofprotection protection_anti_pluie, pp.pestprotection protection_anti_insectes,
    pp.otherequipment autre_equipement, pp.solcomment sol_commentaire,
    pp.surfacetexture texture_surface,
    pp.subsoiltexture texture_sous_sol, pp.solwaterph sol_ph,
    pp.solstoniness pierrosite_moyenne, pp.soldepth sol_profondeur,
    pp.solmaxdepth sol_profondeur_max,
    pp.solorganicmaterialpercent teneur_MO,
    pp.solhydromorphisms hydromorphie,
    pp.sollimestone calcaire,
    pp.soltotallimestone proportion_calcaire_total,
    pp.solactivelimestone proportion_calclaire_actif,
    pp. solbattance battance,
    pp.irrigationsystem irrigation, pp.fertigationsystem fertirrigation, pp. waterorigin origine_eau,
    pp.irrigationsystemtype irrigation_type,
    pp.pompenginetype irrigation_pompe,
    pp.hosespositionning irrigation_position_tuyaux,
    pp. maxslope pente, pp.waterflowdistance distance_cours_eau,
    pp.bufferstrip bande_enherbee
  FROM practicedplot pp
  JOIN practicedsystem ps ON ps.practicedplot = pp.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  AND pp.active IS TRUE

  ORDER BY crit.domaine_id, gs.topiaid;

-------------------------------------------
-- Recolte_realise  >>> requete a verifier
-------------------------------------------
-- Requete "Recoltes" divisee en deux (realise d'une part, synthetise d'autre part), pour pouvoir y appliquer un filtre "reseau"

CREATE TABLE exports_agronomes_context_recolte_realise AS

  WITH recolte AS (

    SELECT
      crit.domaine_code,
      crit.domaine_id,
      crit.domaine_nom,
      crit.domaine_campagne,
      gs.topiaid sdc_id,
      gs.name sdc_nom,
      p.topiaid parcelle_id,
      p.name parcelle_nom,
      z.topiaid zone_id,
      z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_moyen,
      SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_median,
      SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_min,
      SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_max,
      hav.yealdunit unite,
      SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
      SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
      SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
      FROM exp_effective_hav hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
      JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
      JOIN zone z ON ecc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE
      AND hav.crop_area > 0

      GROUP BY
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

      UNION ALL

      SELECT
      crit.domaine_code,
      crit.domaine_id,
      crit.domaine_nom,
      crit.domaine_campagne,
      gs.topiaid sdc_id,
      gs.name sdc_nom,
      p.topiaid parcelle_id,
      p.name parcelle_nom,
      z.topiaid zone_id,
      z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_moyen,
      SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_median,
      SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_min,
      SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_max,
      hav.yealdunit unite,
      SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
      SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
      SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
      FROM exp_effective_hav hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
      JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
      JOIN zone z ON epcc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE
      AND hav.crop_area > 0

      GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

      UNION ALL

      SELECT
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid sdc_id, gs.name sdc_nom,
      p.topiaid parcelle_id, p.name parcelle_nom, z.topiaid zone_id, z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM(hav.yealdaverage::NUMERIC) rendement_moyen,
      SUM(hav.yealdmedian::NUMERIC) rendement_median,
      SUM(hav.yealdmin::NUMERIC) rendement_min,
      SUM(hav.yealdmax::NUMERIC) rendement_max,
      hav.yealdunit unite,
      SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
      SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
      SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
      FROM harvestingactionvalorisation hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
      JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
      JOIN zone z ON ecc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE

      GROUP BY
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

      UNION ALL

      SELECT
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid sdc_id, gs.name sdc_nom,
      p.topiaid parcelle_id, p.name parcelle_nom, z.topiaid zone_id, z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM(hav.yealdaverage::NUMERIC) rendement_moyen,
      SUM(hav.yealdmedian::NUMERIC) rendement_median,
      SUM(hav.yealdmin::NUMERIC) rendement_min,
      SUM(hav.yealdmax::NUMERIC) rendement_max,
      hav.yealdunit unite,
      SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
      SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
      SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
      FROM harvestingactionvalorisation hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
      JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
      JOIN zone z ON epcc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE

      GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit
  )
SELECT * FROM recolte;

----------------------------------------------
-- Recolte_synthetise >>> requete a verifier
----------------------------------------------
CREATE TABLE exports_agronomes_context_recolte_synthetise AS

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    gs.name sdc_nom,
    ps.topiaid systeme_synthetise_id,
    ps.name systeme_synthetise_nom,
    ps.campaigns systeme_synthetise_campagnes,
    aa.practicedintervention intervention_id,
    aa.topiaid action_id,
    riate.reference_label type_action,
    hav.destination destination_id,
    rd.destination destination_nom,
    SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_moyen,
    SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_median,
    SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_min,
    SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_max,
    hav.yealdunit unite,
    SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
    SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
    SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
  FROM exp_practiced_hav hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  AND hav.crop_area > 0

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

  UNION ALL

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid AS sdc_id,
    gs.name AS sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    aa.practicedintervention AS intervention_id,
    aa.topiaid AS action_id,
    riate.reference_label AS type_action,
    hav.destination AS destination_id,
    rd.destination AS destination_nom,
    SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_moyen,
    SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_median,
    SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_min,
    SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_max,
    hav.yealdunit AS unite,
    SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
    SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
    SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
  FROM exp_practiced_hav AS hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  AND hav.crop_area > 0

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

  UNION ALL

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    gs.name AS sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    aa.practicedintervention AS intervention_id,
    aa.topiaid AS action_id,
    riate.reference_label AS type_action,
    hav.destination AS destination_id,
    rd.destination AS destination_nom,
    SUM(hav.yealdaverage::NUMERIC) AS rendement_moyen,
    SUM(hav.yealdmedian::NUMERIC) AS rendement_median,
    SUM(hav.yealdmin::NUMERIC) AS rendement_min,
    SUM(hav.yealdmax::NUMERIC) AS rendement_max,
    hav.yealdunit AS unite,
    SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
    SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
    SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
  FROM harvestingactionvalorisation hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

  UNION ALL

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid AS sdc_id,
    gs.name AS sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    aa.practicedintervention AS intervention_id,
    aa.topiaid AS action_id,
    riate.reference_label AS type_action,
    hav.destination AS destination_id,
    rd.destination AS destination_nom,
    SUM(hav.yealdaverage::NUMERIC) AS rendement_moyen,
    SUM(hav.yealdmedian::NUMERIC) AS rendement_median,
    SUM(hav.yealdmin::NUMERIC) AS rendement_min,
    SUM(hav.yealdmax::NUMERIC) AS rendement_max,
    hav.yealdunit AS unite,
    SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
    SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
    SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
  FROM harvestingactionvalorisation hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit;

------------
-- Cultures
------------
-- Presentation requete : Liste des especes et varietes de chaque culture
	-- 3 sous requetes (concernent les domaines actifs) 
		-- CAS 1 : especes (dont l'espece EDI n'est pas la vigne) associees a une variete
		-- CAS 2 : especes (dont espece EDI = vigne) associes a un cepage
		-- CAS 3 : especes non associees a une variete
		-- CAS 4 : cultures sans especes EDI


CREATE TABLE exports_agronomes_context_cultures AS

  -- CAS 1 : especes (dont l'espece EDI n'est pas la vigne) des cultures associees a une variete
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    cps.speciesarea as surface_relative,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- cps.variety AS variete_id,
    rvg.denomination AS variete_nom
  FROM croppingplanentry cpe
  JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
  JOIN refespece re ON cps.species = re.topiaid
  JOIN refvarietegeves rvg ON cps.variety = rvg.topiaid
  --JOIN domain d ON cpe.domain = d.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE

  UNION

  -- CAS 2 : especes (dont espece EDI = vigne) associes a un cepage
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    cps.speciesarea as surface_relative,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- cps.variety AS variete_id,
    rvpg.variete AS variete_nom
  FROM croppingplanentry cpe
  JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
  JOIN refespece re ON cps.species = re.topiaid
  JOIN refvarieteplantgrape rvpg ON cps.variety = rvpg.topiaid
  --JOIN domain d ON cpe.domain = d.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE

  UNION

  -- CAS 3 : especes non associees a une variete
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    cps.speciesarea as surface_relative,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- null AS variete_id,
    null AS variete_nom
  FROM croppingplanentry cpe
  JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry -- on part sur un join plutot qu'un left join car on cherche a avoir uniquement les cultures qui ont des especes
  JOIN refespece re ON cps.species = re.topiaid
  --JOIN domain d ON cpe.domain = d.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE
  WHERE cps.variety IS NULL

  UNION

  -- CAS 4 : cultures sans especes EDI
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    cps.speciesarea as surface_relative,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- null AS variete_id,
    null AS variete_nom
  FROM croppingplanentry cpe
  --LEFT JOIN domain d ON cpe.domain = d.topiaid
  LEFT JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
  LEFT JOIN refespece re ON cps.species = re.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE
  WHERE cps.topiaid is null; -- pour avoir les cultures qui n'ont pas d'especes

--------------------------------------
-- Caracteristiques_Perennes_Realise
--------------------------------------
CREATE TABLE exports_agronomes_context_perennes_realise AS

SELECT 
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.code sdc_code,
  gs.topiaid sdc_id,
  gs.name sdc_nom,
  p.topiaid parcelle_id,
  p.name parcelle_nom,
  z.topiaid zone_id,
  z.name zone_nom,
  epcc.croppingplanentry culture_id,
  cpe.name culture_nom,

  (WITH liste_especes AS
    (SELECT concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')) re2libelle
    FROM croppingplanspecies cps
    JOIN refespece re2 ON cps.species = re2.topiaid
    WHERE cps.croppingplanentry = cpe.topiaid)
  SELECT string_agg(le.re2libelle,' ; ')
  FROM liste_especes le) culture_especes_EDI,
  eccp.topiaid phase_id,
  eccp.type phase,
  eccp.duration duree_phase_ans,
  epcc.plantingyear annee_plantation,
  epcc.plantinginterfurrow inter_rang,
  epcc.plantingspacing espacement_sur_le_rang,
  epcc.plantingdensity densite_plantation,
  epcc.orchardfrutalform forme_fruitiere_verger,
  epcc.foliageheight hauteur_frondaison,
  epcc.foliagethickness epaisseur_frondaison,
  epcc.vinefrutalform forme_fruitiere_vigne,
  epcc.orientation orientation_rangs,
  epcc.plantingdeathrate taux_mortalite_plantation,
  epcc.plantingdeathratemeasureyear annee_mesure_taux_mortalite,
  epcc.weedtype type_enherbement,
  epcc.othercharacteristics couvert_vegetal_commentaire,
  epcc.pollinator pollinisateurs,
  epcc.pollinatorpercent pourcentage_de_pollinisateurs,
  epcc.pollinatorspreadmode mode_repartition_pollinisateurs

FROM effectivecropcyclephase eccp 
JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
JOIN croppingplanentry cpe ON epcc.croppingplanentry = cpe.topiaid
JOIN zone z ON epcc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN growingsystem gs ON p.growingsystem = gs.topiaid
--JOIN growingplan gp ON gs.growingplan = gp.topiaid
--JOIN domain d ON gp.domain = d.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
--WHERE d.active IS TRUE
--and gp.active IS TRUE
WHERE gs.active IS TRUE
AND p.active IS TRUE
AND z.active IS TRUE;
	
-----------------------------------------
-- Caracteristiques_Perennes_Synthetise
-----------------------------------------
CREATE TABLE exports_agronomes_context_perennes_synthetise AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.code sdc_code,
    gs.topiaid sdc_id,
    gs.name sdc_nom,
    ps.topiaid systeme_synthetise_id,
    ps.name systeme_synthetise_nom,
    ps.campaigns systeme_synthetise_campagnes,
    CASE ps.validated
      WHEN true then 'oui'
      WHEN false then 'non'
    END systeme_synthetise_validation,
    ppcc.croppingplanentrycode culture_code,
    pccp.topiaid phase_id,
    pccp.type phase,
    pccp.duration duree_phase_ans,
    ppcc.soloccupationpercent pourcentage_sole_sdc,
    ppcc.plantingyear annee_plantation,
    ppcc.plantinginterfurrow inter_rang,
    ppcc.plantingspacing espacement_sur_le_rang,
    ppcc.plantingdensity densite_plantation,
    ppcc.orchardfrutalform forme_fruitiere_verger,
    ppcc.foliageheight hauteur_frondaison,
    ppcc.foliagethickness epaisseur_frondaison,
    ppcc.vinefrutalform forme_fruitiere_vigne,
    ppcc.orientation orientation_rangs,
    ppcc.plantingdeathrate taux_mortalite_plantation,
    ppcc.plantingdeathratemeasureyear annee_mesure_taux_mortalite,
    ppcc.weedtype type_enherbement,
    ppcc.othercharacteristics couvert_vegetal_commentaire,
    ppcc.pollinator pollinisateurs,
    ppcc.pollinatorpercent pourcentage_de_pollinisateurs,
    ppcc.pollinatorspreadmode mode_repartition_pollinisateurs
  FROM practicedcropcyclephase pccp
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE;

---------------------------------
-- Rotation_assolees_synthetise
---------------------------------
drop table if exists node_target_synthe;
CREATE TEMPORARY TABLE node_target_synthe as (
with culture_principale as (
  select 
  pccnt.topiaid node_target_id,
  max(cpe.name) culture_nom,
  max(cpe.code) culture_code,
  max((pccnt.rank +1)::text) culture_rang,
  max((pccnt.y+1)::text) AS culture_indicateur_branche
  FROM practicedcropcyclenode pccnt 
  join croppingplanentry cpe on pccnt.croppingplanentrycode = cpe.code 
  -- filtration selon les campagnes
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
  where crit.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
  group by pccnt.topiaid
), especes as (
  select node_target_id, string_agg(nom_sp,' ; ') culture_especes_EDI 
  from (
    SELECT 
    pccnt.topiaid node_target_id,
    max(
      concat_ws(' ', NULLIF(re.libelle_espece_botanique,''), NULLIF(re.libelle_qualifiant_aee,''),
                      NULLIF(re.libelle_type_saisonnier_aee,''), NULLIF(re.libelle_destination_aee,''))
        ) nom_sp
    FROM practicedcropcyclenode pccnt
    JOIN croppingplanentry cpe on cpe.code = pccnt.croppingplanentrycode
    JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
    JOIN refespece re ON cps.species = re.topiaid
    -- filtration selon les campagnes
    JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
    JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
    where crit.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
    GROUP BY node_target_id, species) i
 GROUP BY node_target_id
  )
select 
cp.node_target_id,
cp.culture_nom,
cp.culture_code, 
cp.culture_rang,
cp.culture_indicateur_branche,
e.culture_especes_EDI
from culture_principale cp 
left join especes e on e.node_target_id = cp.node_target_id);

-- noeud precedent et culture intermediaire (puisque indique dans la connexion)
drop table if exists node_precedent;
CREATE TEMPORARY TABLE node_precedent as
with culture_precedent as (
select 
  pccns.topiaid node_source_id,
  max(cpe.name) precedent_nom
FROM practicedcropcycleconnection pccc
JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid -- noeud source 
JOIN croppingplanentry cpe on pccns.croppingplanentrycode = cpe.code 
-- filtration selon les campagnes
JOIN practicedcropcycle pcc ON pccns.practicedseasonalcropcycle = pcc.topiaid
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
where crit.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
group by pccns.topiaid
), culture_intermediaire as (
  select 
  pccc.topiaid node_connection,
  max(cpe.name) ci_nom,
  max(cpe.code) ci_code
  FROM practicedcropcycleconnection pccc
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid  
  JOIN croppingplanentry cpe on pccc.intermediatecroppingplanentrycode = cpe.code 
  -- filtration selon les campagnes
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
  where crit.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
  group by pccc.topiaid
), especes as (
  select node_source_id, string_agg(nom_sp,', ') precedent_especes_EDI 
  from (
    SELECT 
    pccns.topiaid node_source_id,
    max(
      concat_ws(' ', NULLIF(re.libelle_espece_botanique,''), NULLIF(re.libelle_qualifiant_aee,''),
                      NULLIF(re.libelle_type_saisonnier_aee,''), NULLIF(re.libelle_destination_aee,''))
        ) nom_sp
    FROM practicedcropcyclenode pccns
    JOIN croppingplanentry cpe on cpe.code = pccns.croppingplanentrycode
    JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
    JOIN refespece re ON cps.species = re.topiaid
    -- filtration selon les campagnes
    JOIN practicedcropcycle pcc ON pccns.practicedseasonalcropcycle = pcc.topiaid
    JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
    where crit.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
    GROUP BY pccns.topiaid, species) i
 GROUP BY node_source_id
  )
select 
  pccns.topiaid AS precedent_rotation_id,
  (pccns.rank+1)::text AS precedent_rang,
  (pccns.y+1)::text AS precedent_indicateur_branche,
  pccns.croppingplanentrycode AS precedent_code,
  pccc.topiaid culture_precedent_rang_id, -- le nom de la colonne ne correspond pas vraiment, ce serait plutot connection_id
  cp.precedent_nom,
  ci.ci_nom,
  ci.ci_code,
  e.precedent_especes_EDI 
FROM practicedcropcycleconnection pccc
JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid 
join culture_precedent cp on cp.node_source_id = pccns.topiaid
left join culture_intermediaire ci on ci.node_connection = pccc.topiaid
left join especes e on e.node_source_id = pccns.topiaid;


CREATE TABLE exports_agronomes_context_rotation_assolees_synthetise AS
SELECT
crit.domaine_code,
crit.domaine_id,
crit.domaine_nom,
crit.domaine_campagne,
gs.code sdc_code,
gs.topiaid sdc_id,
gs.name sdc_nom,
ps.topiaid AS systeme_synthetise_id,
ps.name AS systeme_synthetise_nom,
ps.campaigns AS systeme_synthetise_campagnes,
ps.validated AS systeme_synthetise_validation,
pccnt.topiaid AS culture_rotation_id,
CASE pccnt.endcycle	WHEN true then 'OUI' ELSE 'NON' END fin_rotation,
CASE pccnt.samecampaignaspreviousnode WHEN true then 'OUI' ELSE 'NON' END meme_campagne_culture_precedente,
CASE pccc.notusedforthiscampaign WHEN true then 'OUI' ELSE 'NON' END culture_absente,
nt.culture_rang,
nt.culture_indicateur_branche,
nt.culture_code,
nt.culture_nom,
nt.culture_especes_EDI,
np.ci_nom,
np.ci_code,
np.culture_precedent_rang_id, 
np.precedent_nom,
np.precedent_code,
np.precedent_especes_EDI,
np.precedent_rotation_id,
np.precedent_rang,
np.precedent_indicateur_branche,
pccc.croppingplanentryfrequency AS frequence_connexion
FROM practicedcropcycleconnection pccc 
JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid -- noeud cible : lie a l intervention 
left JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid -- noeud source
JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
-- jointures des tables cree au prealable 
JOIN node_target_synthe nt on nt.node_target_id = pccnt.topiaid
left JOIN node_precedent np on np.precedent_rotation_id = pccns.topiaid and np.culture_precedent_rang_id = pccc.topiaid -- double cle , noeud source + connexion
WHERE gs.active IS TRUE
AND ps.active IS TRUE
ORDER BY systeme_synthetise_id, culture_rang;

-------------------------------
-- Succession_assolees_realise
-------------------------------
CREATE TABLE exports_agronomes_context_succession_assolees_realise AS
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.code sdc_code,
  gs.topiaid AS sdc_id,
  gs.name AS sdc_nom,
  p.topiaid AS parcelle_id,
  z.topiaid AS zone_id,
  z.name AS zone_nom,
  (eccn.rank+1)::text AS culture_rang,
  (-- Requete renvoyant l'id de la culture associe au noeud
    SELECT cpe.topiaid
    FROM croppingplanentry cpe
    WHERE cpe.topiaid = eccn.croppingplanentry)	AS culture_id,
  (-- Requete renvoyant le nom de la culture associe au noeud
    SELECT cpe1.name
    FROM croppingplanentry cpe1
    WHERE cpe1.topiaid = eccn.croppingplanentry) AS culture_nom,
  (-- Requete renvoyant les noms EDI des especes de la culture
    SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
    NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ')

    FROM croppingplanentry cpe1
    JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe1.topiaid
    JOIN refespece re2 ON cps.species = re2.topiaid
    WHERE cpe1.topiaid = eccn.croppingplanentry
    ) AS culture_especes_edi,
  (-- Requete renvoyant l'id de la culture intermediaire
    SELECT eccc.intermediatecroppingplanentry
    FROM effectivecropcycleconnection eccc
    WHERE eccc.target = eccn.topiaid
    ) AS ci_id,
  (-- Requete renvoyant le nom de la culture intermediaire
    SELECT cpe.name
    FROM effectivecropcycleconnection eccc
    JOIN croppingplanentry cpe ON cpe.topiaid = eccc.intermediatecroppingplanentry
    WHERE eccc.target = eccn.topiaid
    ) AS ci_nom,
  (-- Requete renvoyant l'id de la culture precedente
    SELECT cpe.topiaid
    FROM effectivecropcycleconnection eccc
    JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
    JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid
    WHERE eccc.target = eccn.topiaid
  ) AS precedent_id,
  (-- Requete renvoyant le nom de la culture precedente
    SELECT cpe.name
    FROM effectivecropcycleconnection eccc
    JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
    JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid
    WHERE eccc.target = eccn.topiaid
  ) AS precedent_nom,
  (-- Requete renvoyant les noms EDI des especes de la culture precedente
    SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
    NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ')
    FROM effectivecropcycleconnection eccc
    JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
    JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid
    JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaid
    JOIN refespece re2 ON cps.species = re2.topiaid
    WHERE eccc.target = eccn.topiaid--eccn2.topiaid = ei.effectivecropcyclenode
  ) AS precedent_especes_edi

  FROM effectivecropcyclenode eccn
  LEFT JOIN effectivecropcycleconnection eccc ON eccc.target = eccn.topiaid -- il faut garder les noeuds qui n'ont pas de connexion, d'où le LEFT JOIN
  JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
  JOIN zone z ON ecc.zone = z.topiaid
  JOIN plot p ON z.plot = p.topiaid
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active IS TRUE
  ORDER BY domaine_code, domaine_campagne, sdc_nom, zone_id, culture_rang;


--------------------
-- Export local à intrant
--------------------

DROP TABLE IF EXISTS exports_agronomes_context_local_intrant;
create table exports_agronomes_context_local_intrant(
	intrant_local_id character varying(255), 
	intrant_type character varying(255),
	intrant_type_produit character varying(255),
	domaine_id character varying(255),
	domaine_code character varying(255),
	domaine_nom text,
	campagne integer, 
	intrant_ref_id character varying(255),				-- si disponible l'identifiant de l'intrant de référence
	intrant_ref_nom text,				-- si disponible le nom de l'intrant de référence
	intrant_nom_utilisateur text,
	semence_id character varying (255), 				-- si il s'agit d'un traitement phyto concernant une semence --> identifiant de la semence que ce produit phyto traite. 
	lot_semence_bio boolean default false,				-- si le lot de semences est en bio
	lot_semence_culture_id character varying(255),		-- culture concernée par le traiteme
	lot_semence_type character varying(255), 			-- type de semences
	intrant_code_amm character varying(500),
	biocontrole boolean default false, 
	n float8, 
	p2o5 float8, 
	k2o float8, 
	bore float8, 
	calcium float8, 
	fer float8, 
	manganese float8, 
	molybdene float8, 
	mgo float8, 
	oxyde_de_sodium float8,
	so3 float8, 
	cuivre float8, 
	zinc float8, 
	cao float8, 
	s float8,
	--unite_teneur_fert_orga character varying(255),
	unite_teneur_fert character varying(500),
	unite_application character varying(500),
	effet_sdn boolean, --stimulateur des défenses naturelles
	forme_fert_min character varying(255),
	-- substance_active character varying(255),
	caracteristique_1 character varying(500),
	caracteristique_2 character varying(500),
	caracteristique_3 character varying(500),
	type_intrant_autre character varying(255),
	duree_de_vie character varying(255),
	type_produit_sans_amm character varying(255),
	prix_saisi float8, 
	prix_saisi_unite character varying(500), 
	prix_ref float8,
	prix_ref_unite character varying(255),
	prix_ref_bio float8,
	prix_ref_bio_unite character varying(255)
);

-- AJOUT DES SUBSTRATS : 
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_ref_id, intrant_nom_utilisateur, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, caracteristique_1, caracteristique_2, unite_application)
select 
	distinct on (dsi.topiaid) dsi.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domaine_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	dsi.refinput as intrant_ref_id,
	adisu.inputname as intrant_nom_utilisateur,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rps.price as prix_ref,
	rps.unit as prix_ref_unite,
	rs.caracteristic1 as caracteristique_1, 
	rs.caracteristic2 as caracteristique_2,
	dsi.usageunit as unite_application
	--riupuc.substrateinputunit
	from domainsubstrateinput dsi
	left join abstractdomaininputstockunit adisu on dsi.topiaid = adisu.topiaid
	left join domain d on adisu.domain = d.topiaid
	left join inputprice ip on adisu.inputprice = ip.topiaid
	left join refsubstrate rs on rs.topiaid = dsi.refinput
	left join refprixsubstrate rps on rps.caracteristic1 = rs.caracteristic1 and rps.caracteristic2 = rs.caracteristic2 and d.campaign = rps.campaign and rps.price != 0;
--

-- AJOUT DES CARBURANTS : 
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_nom_utilisateur, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite)
select 
	dfi.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domaine_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	adisu.inputname as intrant_nom_utilisateur,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rpc.price as prix_ref,
	rpc.unit as prix_ref_unite
from domainfuelinput dfi
left join abstractdomaininputstockunit adisu on dfi.topiaid = adisu.topiaid
left join domain d on adisu.domain = d.topiaid
left join inputprice ip on adisu.inputprice = ip.topiaid
left join refprixcarbu rpc on rpc.campaign = d.campaign;
--

-- AJOUT DES IRRIGATIONS :
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_nom_utilisateur, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite)
select 
	dii.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domain_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	adisu.inputname as intrant_nom_utilisateur,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rpi.price as prix_ref,
	rpi.unit as prix_ref_unite
from domainirrigationinput dii
left join abstractdomaininputstockunit adisu on dii.topiaid = adisu.topiaid
left join domain d on adisu.domain = d.topiaid
left join inputprice ip on adisu.inputprice = ip.topiaid
left join refprixirrig rpi on rpi.campaign = d.campaign;
--


-- AJOUT DES INTRANTS MINERAUX : 
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_ref_id,intrant_nom_utilisateur,
	n, p2o5, k2o, bore, calcium, fer, manganese, molybdene, mgo, oxyde_de_sodium, so3, cuivre, zinc, 
	prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, effet_sdn, unite_teneur_fert, unite_application)
select 	
	dmpi.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domaine_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	dmpi.refinput as intrant_ref_id,
	adisu.inputname as intrant_nom_utilisateur,
	rfmu.n n,
	rfmu.p2o5 p2o5,
	rfmu.k2o k2o,
	rfmu.bore bore, 
	rfmu.calcium calcium,
	rfmu.fer fer,
	rfmu.manganese manganese,
	rfmu.molybdene molybdene,
	rfmu.mgo mgo,
	rfmu.oxyde_de_sodium oxyde_de_sodium,
	rfmu.so3 so3,
	rfmu.cuivre cuivre,
	rfmu.zinc zinc,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	(rpfm_n.price*rfmu.n + rpfm_p2o5.price*rfmu.p2o5 + rpfm_k2o.price*rfmu.k2o + rpfm_bore.price*rfmu.bore + rpfm_calcium.price*rfmu.calcium + rpfm_fer.price*rfmu.fer + rpfm_manganese.price*rfmu.manganese + rpfm_molybdene.price*rfmu.molybdene + rpfm_mgo.price*rfmu.mgo + rpfm_oxyde_de_sodium.price*rfmu.oxyde_de_sodium + rpfm_so3.price*rfmu.so3 + rpfm_cuivre.price*rfmu.cuivre + rpfm_zinc.price*rfmu.zinc)/100 as prix_ref,
	rpfm_n.unit as prix_ref_unite,
	dmpi.phytoeffect as effet_sdn,
	'Unités fertilisantes/100kg' unite_teneur_fert,
	dmpi.usageunit as unite_application
from domainmineralproductinput dmpi
left join reffertiminunifa rfmu on dmpi.refinput = rfmu.topiaid
left join abstractdomaininputstockunit adisu on adisu.topiaid = dmpi.topiaid
left join inputprice ip on adisu.inputprice = ip.topiaid
left join domain d on adisu.domain = d.topiaid
left join refprixfertimin rpfm_bore on rpfm_bore."element" = 'BORE' and rpfm_bore.categ = rfmu.categ and rpfm_bore.forme = rfmu.forme and rpfm_bore.campaign = d.campaign
left join refprixfertimin rpfm_calcium on rpfm_calcium."element" = 'CALCIUM' and rpfm_calcium.categ = rfmu.categ and rpfm_calcium.forme = rfmu.forme and rpfm_calcium.campaign = d.campaign
left join refprixfertimin rpfm_cuivre on rpfm_cuivre."element" = 'CUIVRE' and rpfm_cuivre.categ = rfmu.categ and rpfm_cuivre.forme = rfmu.forme and rpfm_cuivre.campaign = d.campaign
left join refprixfertimin rpfm_fer on rpfm_fer."element" = 'FER' and rpfm_fer.categ = rfmu.categ and rpfm_fer.forme = rfmu.forme and rpfm_fer.campaign = d.campaign
left join refprixfertimin rpfm_k2o on rpfm_k2o."element" = 'K2_O' and rpfm_k2o.categ = rfmu.categ and rpfm_k2o.forme = rfmu.forme and rpfm_k2o.campaign = d.campaign
left join refprixfertimin rpfm_manganese on rpfm_manganese."element" = 'MANGANESE' and rpfm_manganese.categ = rfmu.categ and rpfm_manganese.forme = rfmu.forme and rpfm_manganese.campaign = d.campaign
left join refprixfertimin rpfm_mgo on rpfm_mgo."element" = 'MG_O' and rpfm_mgo.categ = rfmu.categ and rpfm_mgo.forme = rfmu.forme and rpfm_mgo.campaign = d.campaign
left join refprixfertimin rpfm_molybdene on rpfm_molybdene."element" = 'MOLYBDENE' and rpfm_molybdene.categ = rfmu.categ and rpfm_molybdene.forme = rfmu.forme and rpfm_molybdene.campaign = d.campaign
left join refprixfertimin rpfm_n on rpfm_n."element" = 'N' and rpfm_n.categ = rfmu.categ and rpfm_n.forme = rfmu.forme and rpfm_n.campaign = d.campaign
left join refprixfertimin rpfm_oxyde_de_sodium on rpfm_oxyde_de_sodium."element" = 'OXYDE_DE_SODIUM' and rpfm_oxyde_de_sodium.categ = rfmu.categ and rpfm_oxyde_de_sodium.forme = rfmu.forme and rpfm_oxyde_de_sodium.campaign = d.campaign
left join refprixfertimin rpfm_p2o5 on rpfm_p2o5."element" = 'P2_O5' and rpfm_p2o5.categ = rfmu.categ and rpfm_p2o5.forme = rfmu.forme and rpfm_p2o5.campaign = d.campaign
left join refprixfertimin rpfm_so3 on rpfm_so3."element" = 'S_O3' and rpfm_so3.categ = rfmu.categ and rpfm_so3.forme = rfmu.forme and rpfm_so3.campaign = d.campaign
left join refprixfertimin rpfm_zinc on rpfm_zinc."element" = 'ZINC' and rpfm_zinc.categ = rfmu.categ and rpfm_zinc.forme = rfmu.forme and rpfm_zinc.campaign = d.campaign;


-- AJOUT DES INTRANTS ORGANICS
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_ref_id, intrant_ref_nom, intrant_nom_utilisateur,
biocontrole, n, p2o5, k2o, cao, s, unite_teneur_fert, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, prix_ref_bio, prix_ref_bio_unite, unite_application)
select 
	dopi.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domain_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	dopi.refinput as intrant_ref_id,
	rpfo.nom as intrant_ref_nom,
	adisu.inputname as intrant_nom_utilisateur,
	dopi.organic as biocontrole,
	dopi.n as n,
	dopi.p2o5 as p2o5,
	dopi.k2o as k2o,
	dopi.cao as cao,
	dopi.s as s,
	dopi.usageunit unite_teneur_fert,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rpfo.price as prix_ref,
	rpfo.unit as prix_ref_unite,
	rpfo_bio.price as prix_ref_bio,
	rpfo_bio.unit as prix_ref_bio_unite,
	dopi.usageunit as unite_application
from domainorganicproductinput dopi
left join abstractdomaininputstockunit adisu on dopi.topiaid = adisu.topiaid
left join domain d on adisu.domain = d.topiaid
left join inputprice ip on adisu.inputprice = ip.topiaid
left join reffertiorga rfo on dopi.refinput = rfo.topiaid
left join refprixfertiorga rpfo on rpfo.campaign = d.campaign and rpfo.idtypeeffluent = rfo.idtypeeffluent and rpfo.organic = false 
left join refprixfertiorga rpfo_bio on rpfo.campaign = d.campaign and rpfo.idtypeeffluent = rfo.idtypeeffluent and rpfo.organic = true;
--


-- AUJOUT DES INTRANTS AUTRES
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_ref_id, intrant_nom_utilisateur, 
prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, type_intrant_autre, duree_de_vie, caracteristique_1, caracteristique_2, caracteristique_3, unite_application)
select 
	doi.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domain_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	doi.refinput as intrant_ref_id,
	adisu.inputname as intrant_nom_utilisateur,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rpa.price as prix_ref,
	rpa.unit as prix_ref_unite,
	roi.inputtype_c0  as type_intrant_autre,
	roi.lifetime as duree_de_vie,
	roi.caracteristic1 as caracteristique_1, 
	roi.caracteristic2 as caracteristique_2,
	roi.caracteristic2 as caracteristique_3,
	doi.usageunit as unite_application
from domainotherinput doi
left join abstractdomaininputstockunit adisu on doi.topiaid = adisu.topiaid
left join domain d on adisu.domain = d.topiaid
left join inputprice ip on adisu.inputprice = ip.topiaid
left join refotherinput roi on roi.topiaid = doi.topiaid
left join refprixautre rpa on rpa.campaign = d.campaign and roi.caracteristic1 = rpa.caracteristic1 and roi.caracteristic2 = rpa.caracteristic2 and roi.caracteristic3 = rpa.caracteristic3;
-- 

-- AJOUT DES PRODUITS PHYTOS (INTRANT TYPE = APPLICATION DE PRODUITS PHYTOSANITAIRES / TRAITEMENTS DE SEMENCE / LUTTE BIOLOGIQUE)
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, intrant_type_produit, domaine_id, domaine_code, domaine_nom, campagne, intrant_ref_id, intrant_nom_utilisateur, 
intrant_code_amm, biocontrole, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, unite_application, semence_id)
select 
		dppi.topiaid as intrant_local_id,
		adisu.inputtype as intrant_type,
		dppi.producttype as intrant_type_produit,
		adisu.domain as domain_id,
		d.code as domaine_code,
		d.name as domaine_nom,
		d.campaign as campagne,
		dppi.refinput as intrant_ref_id,
		adisu.inputname as intrant_nom_utilisateur,
		ratp.code_amm as intrant_code_amm,
		ratp.nodu biocontrole,
		ip.price as prix_saisi,
		ip.priceunit as prix_saisi_unite,
		rpp.price as prix_ref,
		rpp.unit as prix_ref_unite,
		dppi.usageunit as unite_application,
		dppi.domainseedspeciesinput as semence_id -- si le produit phyto est déclaré sur un traitement de semence, identifiant de la semence sur lequel il est déclaré.
	from domainphytoproductinput  dppi
	left join abstractdomaininputstockunit adisu on dppi.topiaid = adisu.topiaid
	left join domain d on adisu.domain = d.topiaid
	left join inputprice ip on adisu.inputprice = ip.topiaid
	left join refactatraitementsproduit ratp on ratp.topiaid = dppi.refinput
	left join ( -- on ne selectionne que les premières unités rencontrées
		select distinct on (rpp.campaign, rpp.id_produit, rpp.id_traitement) 
		rpp.campaign, rpp.price, rpp.unit, rpp.id_produit, rpp.id_traitement 
		from refprixphyto rpp
	) rpp on (rpp.campaign = d.campaign and rpp.id_produit = ratp.id_produit  and rpp.id_traitement = ratp.id_traitement);


-- AJOUT DES POTS
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, intrant_ref_id, intrant_nom_utilisateur, 
 prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, unite_application)
select 
	dpi.topiaid as intrant_local_id,
	adisu.inputtype as intrant_type,
	adisu.domain as domain_id,
	d.code as domaine_code,
	d.name as domaine_nom,
	d.campaign as campagne,
	dpi.refinput as intrant_ref_id,
	adisu.inputname as intrant_nom_utilisateur,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rpp.price as prix_ref,
	rpp.unit as prix_ref_unite,
	dpi.usageunit as unite_application
from domainpotinput dpi
left join abstractdomaininputstockunit adisu on dpi.topiaid = adisu.topiaid
left join domain d on adisu.domain = d.topiaid
left join inputprice ip on adisu.inputprice = ip.topiaid
left join refpot rp on rp.topiaid = dpi.refinput
left join refprixpot rpp on rpp.campaign = d.campaign and rpp.caracteristic1 = rp.caracteristic1;
-- 



-- AJOUT DES LOTS DE SEMENCES --> INTRANT TYPE = SEMIS
insert into exports_agronomes_context_local_intrant (intrant_local_id, intrant_type, domaine_id, domaine_code, domaine_nom, campagne, lot_semence_bio, lot_semence_culture_id, 
intrant_nom_utilisateur, lot_semence_type, prix_saisi, prix_saisi_unite, unite_application)
select 
		dsli.topiaid as intrant_local_id,
		adisu.inputtype as intrant_type,
		adisu.domain as domaine_id,
		d.code as domaine_code,
		d.name as domaine_nom,
		d.campaign as campagne,
		dsli.organic as lot_semence_bio,
		dsli.cropseed as lot_semence_culture_id, 
		adisu.inputname as intrant_nom_utilisateur,
		ip.seedtype as lot_semence_type,
		ip.price as prix_saisi,
		ip.priceunit as prix_saisi_unite,
		--dsli. as prix_ref -- pas possible d'obtenir le prix de référence du lot de semence car on a besoin de la proportion des espèces qui le composent, information uniquement disponible à l'intervention. 
		dsli.usageunit as unite_application
	from domainseedlotinput dsli
	left join abstractdomaininputstockunit adisu on dsli.topiaid = adisu.topiaid
	left join domain d on adisu.domain = d.topiaid
	left join inputprice ip on dsli.seedprice = ip.topiaid;
	--left join refactatraitementsproduit ratp on ratp.topiaid = dsli.;





--------------------
-- DES SEMENCES
--------------------

-- Cette table pointe vers la table local à intrant par l'intermédiaire de lot_semence_id 
-- Elle permet d'avoir l'ensemble des informations sur les données "lots de semences" 
-- Chaque ligne représente les informations pour la semence particulière d'une espèce 
-- C'est à ce niveau là qu'on peut calculer des prix
drop table if exists exports_agronomes_context_semence;
create table exports_agronomes_context_semence(
	semence_id character varying(255), 
	inoculation_biologique boolean default false,
	traitement_chimique boolean default false,
	bio boolean default false, 
	traitement_semence_id character varying(255), 
	espece_id character varying(255), 
	type_semence character varying(255),
	prix_saisi float8,
	prix_saisi_unite character varying(255),
	prix_ref float8,
	prix_ref_unite character varying(255),
	lot_semence_id character varying(255) -- lot de semence auquel appartient cette semence particulière
);

-- on insère d'abord sans les prix
insert into exports_agronomes_context_semence (semence_id, inoculation_biologique, traitement_chimique, bio, espece_id, type_semence, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, lot_semence_id)
	select
		dssi.topiaid as semence_id,
		dssi.biologicalseedinoculation  as inoculation_biologique,
		dssi.chemicaltreatment  as traitement_chimique,
		dssi.organic  as bio,
		dssi.speciesseed  as espece_id,
		dssi.seedtype as type_semence,
		ip.price as prix_saisi,
		ip.priceunit as prix_saisi_unite,
		null as prix_ref,
		null as prix_ref_unite,
		dssi.domainseedlotinput as lot_semence_id
		--dsli.usageunit as unite_application
	from domainseedspeciesinput dssi
	left join abstractdomaininputstockunit adisu on dssi.topiaid = adisu.topiaid
	left join domain d on adisu.domain = d.topiaid
	left join croppingplanspecies cps on cps.topiaid = dssi.speciesseed
	left join refespece re on re.topiaid = cps.species
	left join domainseedlotinput dsli on dsli.topiaid = dssi.domainseedlotinput 
	left join inputprice ip on dssi.seedprice = ip.topiaid;

-- on créé une table temporaire qui contient tous les doublons en fonction des différentes unités. 
drop table if exists semence_all;
create temporary table semence_all as 
select
	dssi.topiaid as semence_id,
	dssi.biologicalseedinoculation  as inoculation_biologique,
	dssi.chemicaltreatment  as traitement_chimique,
	dssi.organic  as bio,
	dssi.speciesseed  as espece_id,
	dssi.seedtype as type_semence,
	ip.price as prix_saisi,
	ip.priceunit as prix_saisi_unite,
	rpe.price  as prix_ref,
	rpe.unit as prix_ref_unite,
	dssi.domainseedlotinput as lot_semence_id
	--dsli.usageunit as unite_application
from domainseedspeciesinput dssi
left join abstractdomaininputstockunit adisu on dssi.topiaid = adisu.topiaid
left join domain d on adisu.domain = d.topiaid
left join croppingplanspecies cps on cps.topiaid = dssi.speciesseed
left join refespece re on re.topiaid = cps.species
left join domainseedlotinput dsli on dsli.topiaid = dssi.domainseedlotinput 
left join inputprice ip on dssi.seedprice = ip.topiaid
left join refinputunitpriceunitconverter riupuc on riupuc.seedplantunit = dsli.usageunit 
left join refprixespece rpe on (rpe.campaign = d.campaign and
	re.code_espece_botanique = rpe.code_espece_botanique and 
	re.code_qualifiant_aee = rpe.code_qualifiant_aee and 
	riupuc.priceunit = rpe.unit and 
	rpe.active = true and 
	rpe.seedtype = dssi.seedtype and
	rpe.treatment = dssi.chemicaltreatment and 
	rpe.organic = dssi.organic);
	

-- on met à jours les lignes qui ne posent pas de problème (prix de référence bien trouvé, et on en garde qu'un par id.)
update exports_agronomes_context_semence eacs
set prix_ref = sc.prix_ref,
	prix_ref_unite = sc.prix_ref_unite
from 
	(select 
		distinct on (semence_id) semence_id, inoculation_biologique, traitement_chimique, bio, espece_id, type_semence, prix_saisi, prix_saisi_unite, prix_ref, prix_ref_unite, lot_semence_id from semence_all
		where prix_ref is not null
	) sc
where eacs.semence_id = sc.semence_id;
		


----------------
-- A retenir
----------------

-- ORDER BY : les noms des champs doivent etre ceux qui ont servis a faire l'union, donc les champs renommes et non les noms des champs initiaux.

-- LEFT JOIN / RIGHT JOIN
	-- Quand on commence a faire un LEFT / RIGHT JOIN, on doit continuer a le faire pour toutes les tables qui suivent ce join special 
	-- et qui sont en lien avec cette table." 
	-- c'est vrai pour le LEFT JOIN, par pour le RIGHT JOIN. Voir requete sur les intrants.

	
-- quand on a besoin de regrouper des lignes, tout ce qui est dans la clause SELECT doit se retrouver dans la clause GROUP BY (sauf les aggregations type count, avg, string_agg etc.)
-- Par contre s'il n'y a pas besoin d'aggregation, il peut tres bien y avoir des champs dans SELECT 
-- sans faire intervenir la clause GROUP BY

-- Pas besoin de grouper dans cette requete quand toutes les tables utilisees ont une relation 1-1 
-- Ex. : une parcelle est liee a un seul domaine, a un seul sdc, a une seule plot_zone, a un seul refsolarvalis

-- Filtre : 
-- WHERE d.topiaid IN
--     (SELECT domain from growingplan Where topiaid IN
--         (select growingplan FROM growingsystem WHERE topiaid IN
--            (Select growingsystem FROM growingsystem_networks
--                 WHERE networks IN (
--                     WITH RECURSIVE reseaux AS (
--                         SELECT topiaid as network from network n WHERE n.name = 'CAN'
--                         UNION
--                         SELECT np.network from reseaux r, network_parents np WHERE r.network = np.parents
--                     )
--                     SELECT * FROM reseaux r
--                 )
--             )
--         )
--     )

