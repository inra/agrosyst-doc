#!/bin/bash

###
# Agrosyst
#
# Copyright (C) 2022 INRAE, CodeLutin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

# script permettant de générer un seul fichier de script SQL à partir des différents fichiers dont le nom commence par 0
# les fichiers sont ajoutés dans l'ordre de la numérotation.

echo "#!/bin/bash" > generate-all-in-one.sh
find -type f -name '0*.sql' | sort -n | awk -F '/' '{print "cat "$0" >> all-in-one.sql"}' >> generate-all-in-one.sh
/bin/bash generate-all-in-one.sh
#sed 's/\xe2\x80\x8b//g' all-in-one.sql
/bin/bash executeExportToCsv.sh
/bin/bash ../ANONYMIZED/executeExportToCsv.sh
rm generate-all-in-one.sh
rm all-in-one.sql
