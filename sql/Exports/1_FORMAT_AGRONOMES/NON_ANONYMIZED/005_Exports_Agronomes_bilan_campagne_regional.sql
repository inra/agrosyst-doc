-------------------------------
-- bilan de campagne regional
-------------------------------

-- Modifications :
-- 12/04/2023 : reprise du code du bilan de campagne regional : insertions de colonnes en plus et division en 2 cas en fonction de la saisie ou non du groupe cible
-- la table finale a pour cle unique : BCR_id , type_pression , un grpcible ou un ravageur defini

/*
Creation d'une table temporaire pour supprimer les doublons de codegroupecible dans le referentiel des groupes cibles 
on ne selectione que ceux actifs
il reste le cas 38 : on selectionne 'Cicadelles, punaises et psylles' puisque l'autre nom utilisé est 'Cicadelles, cercopides et psylles' alors qu'il correspond au 37
le cas 82 est un doublon avec une orthographe differente 
*/
create temporary table refgrpcibleunique as 
select distinct
code_groupe_cible_maa ,groupe_cible_maa ,active 
from refciblesagrosystgroupesciblesmaa 
where active = true and code_groupe_cible_maa not in ('38','82')
union 
select code_groupe_cible_maa ,groupe_cible_maa ,active
from refciblesagrosystgroupesciblesmaa
where active = true and (groupe_cible_maa in ('Cicadelles, punaises et psylles','Maladies des tâches foliaires'));


-----------------------------
-- Bilan campagne regionaux 
-----------------------------

create table exports_agronomes_bilan_campagne_regional as 
-- CAS 1 : quand le bioagresseur est renseigne (avec et sans le groupe cible)
select 
rr.name BCR_nom,
rr.topiaid BCR_ID,
rr.code BCR_code,
rr.author auteur, 
nr.reseau reseau,
rrs.sectors secteurs,
rrss.sectorspecies arbo_esp,
rr.campaign campagne,
rr.highlights faits_marquants,
rr.rainfallmarchjune pluvio_marsjuin,
rr.rainydaysmarchjune jourspluie_marsjuin,
rr.rainfalljulyoctober pluvio_juiloct,
rr.primarycontaminations INOKI_nb_contaminationprimaire,
rr.numberofdayswithprimarycontaminations INOKI_nbjours_contamitationprimaire,
rr.secondarycontaminations RIMpro_nb_contaminationsecondaire,
rr.rimsum RIMpro_sommeRIM,
case -- on renseigne soit des ravageurs soit des maladies. Mais dans la table pp, cela est rempli dans deux colonnes differentes. Je regroupe en 1 colonne avec deux modalites 
	when pp.diseasepressurereportregional like '%ReportRegional%' then 'maladies'
	when pp.pestpressurereportregional like '%ReportRegional%' then 'ravageurs'
end type_pression,
refcible.groupe_cible_maa groupe_cible,
refnui.reference_label nom_maladie_ravageur,
pp.crops culture_concernee,
pp.pressurescale pression_annee,
pp.pressureevolution pression_evolution,
pp.comment commentaires
from (select * from pestpressure where topiaid in (select pestpressure from agressors_pestpressure)) pp
left join reportregional rr on (rr.topiaid = pp.diseasepressurereportregional or rr.topiaid = pp.pestpressurereportregional) 
left join agressors_pestpressure ap on pp.topiaid = ap.pestpressure
left JOIN refnuisibleedi refnui on ap.agressors = refnui.topiaid 
-- Pour les grp cibles, meme principe que les modeles decisionels : quand l'agresseur est speciefie on l'utilise comme double cle pour avoir le grp cible
left join refciblesagrosystgroupesciblesmaa refcible on (refnui.reference_code = refcible.cible_edi_ref_code and pp.codegroupeciblemaa = refcible.code_groupe_cible_maa)
join (select nrg.reportregional, string_agg(distinct n.name,'|') reseau from networks_reportregional nrg -- Quand il y a plusieurs IR pour un meme id de rr, on agrege les noms d'IR sur la meme ligne  
	join network n on nrg.networks = n.topiaid group by nrg.reportregional) nr on nr.reportregional = rr.topiaid
left join reportregional_sectors rrs on rr.topiaid = rrs.owner
left join reportregional_sectorspecies rrss on rr.topiaid = rrss.owner
UNION
-- CAS 2 : Quand le groupe cible seul est renseigné ou que ni le grp cible ni le bioagresseur est saisi 
select 
rr.name BCR_nom,
rr.topiaid BCR_ID,
rr.code BCR_code,
rr.author auteur, 
nr.reseau reseau,
rrs.sectors secteurs,
rrss.sectorspecies arbo_esp,
rr.campaign campagne,
rr.highlights faits_marquants,
rr.rainfallmarchjune pluvio_marsjuin,
rr.rainydaysmarchjune jourspluie_marsjuin,
rr.rainfalljulyoctober pluvio_juiloct,
rr.primarycontaminations INOKI_nb_contaminationprimaire,
rr.numberofdayswithprimarycontaminations INOKI_nbjours_contamitationprimaire,
rr.secondarycontaminations RIMpro_nb_contaminationsecondaire,
rr.rimsum RIMpro_sommeRIM,
case 
	when pp.diseasepressurereportregional like '%ReportRegional%' then 'maladies'
	when pp.pestpressurereportregional like '%ReportRegional%' then 'ravageurs'
end type_pression,
refcible2.groupe_cible_maa groupe_cible,
refnui.reference_label nom_maladie_ravageur,
pp.crops culture_concernee,
pp.pressurescale pression_annee,
pp.pressureevolution pression_evolution,
pp.comment commentaires
from (select * from pestpressure where topiaid not in (select pestpressure from agressors_pestpressure)) pp
left join reportregional rr on (rr.topiaid = pp.diseasepressurereportregional or rr.topiaid = pp.pestpressurereportregional) 
left join agressors_pestpressure ap on pp.topiaid = ap.pestpressure
left JOIN refnuisibleedi refnui on ap.agressors = refnui.topiaid 
-- Pour les grp cibles, meme principe que les modeles decisionels : quand l'agresseur n'est pas speciefie utilise la table temporaire cree au debut pour n'avoir qu'un seul grpcible par code groupe cible
left join refgrpcibleunique refcible2 on (pp.codegroupeciblemaa = refcible2.code_groupe_cible_maa)
join (select nrg.reportregional, string_agg(distinct n.name,'|') reseau from networks_reportregional nrg 
	join network n on nrg.networks = n.topiaid group by nrg.reportregional) nr on nr.reportregional = rr.topiaid 
left join reportregional_sectors rrs on rr.topiaid = rrs.owner
left join reportregional_sectorspecies rrss on rr.topiaid = rrss.owner;
