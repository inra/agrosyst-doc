------------------------------------------------
-- Interventions Synthetise non anonymes
------------------------------------------------
-- 1.2 : ajout d'un filtre sur le champ ACTIVE (is true) sur tous les elements qui possedent ce champ en plus du domaine : sdc, dispositif, synthetise
-- 2019-12-04 : ajout du stockage des resultats dans une table temporaire
-- 2019-17-12 : ajout de 2 colonnes pour indiquer la presence de traitement chimique ou inoculation biologique des semis
-- 2020-10    : ajout de l'id de la phase pour les cultures perennes
-- 2020-12    : ajout de la phase pour les cultures perennes
-- 2021-04	  : ajout prise en compte de la table de criteres
--				tri par domain_code/campagne plutot que domaine_id/campagne
-- suppression de la table temporaire de stockage des resultats de la requete
DROP TABLE IF EXISTS exports_agronomes_context_interventions_synthetise;

CREATE TABLE exports_agronomes_context_interventions_synthetise AS

  WITH toutes_interventions AS (

  ---------------------------
  -- Interventions Assolees
  ---------------------------

    (SELECT
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.code sdc_code, gs.topiaid sdc_id, gs.name sdc_nom,
      ps.topiaid systeme_synthetise_id, ps.name systeme_synthetise_nom, ps.campaigns systeme_synthetise_campagnes,
      CASE ps.validated
        WHEN true then 'oui'
        WHEN false then 'non'
      END systeme_synthetise_validation,
      'NA' phase_id, 'NA' phase,
      pccc.topiaid culture_precedent_rang_id, -- topiaid de la connexion
      pccnt.croppingplanentrycode culture_code,
      (-- Requete donnant pour un code culture et les campagnes du synthetise, le nom d'une culture
        SELECT max(cpe2.name) as name
        FROM croppingplanentry cpe2
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE cpe2.code = pccnt.croppingplanentrycode
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer) -- campagnes du synthetise
        --AND d2.active IS TRUE
        GROUP BY cpe2.code
      ) culture_nom,
      pccc.intermediatecroppingplanentrycode ci_code,
      (-- Meme chose avec la culture intermediaire
        SELECT max(cpe2.name) as name
        FROM croppingplanentry cpe2
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE cpe2.code = pccc.intermediatecroppingplanentrycode
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE
        GROUP BY cpe2.code
      ) ci_nom,
      CASE intermediatecrop
        WHEN true then 'oui'
        WHEN false then 'non'
      END concerne_la_ci,

      (WITH esp_var AS
        (-- Requete filtree pour especes avec varietes GEVES
        SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
        NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvg2.denomination),' ; ') AS espvar
        FROM practicedspeciesstade pss2
        JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
        JOIN refespece re2 ON cps2.species = re2.topiaid
        JOIN refvarietegeves rvg2 ON cps2.variety = rvg2.topiaid
        JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE pss2.practicedintervention = pi.topiaid
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE

        UNION

        -- Requete filtree pour especes avec varietes PLANTGRAPE
        SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
        NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-',rvpg2.variete),' ; ') AS espvar
        FROM practicedspeciesstade pss2
        JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
        JOIN refespece re2 ON cps2.species = re2.topiaid
        JOIN refvarieteplantgrape rvpg2 ON cps2.variety = rvpg2.topiaid
        JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE pss2.practicedintervention = pi.topiaid
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE

        UNION

        -- Requete filtree pour especes sans varietes
        SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
        NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') AS espvar
        FROM practicedspeciesstade pss2
        JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
        JOIN refespece re2 ON cps2.species = re2.topiaid
        JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE pss2.practicedintervention = pi.topiaid
        AND cps2.variety IS null
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE

        )

        SELECT string_agg(espvar, ';')
        FROM esp_var
        ) especes_de_l_intervention,

      pccns.croppingplanentrycode precedent_code,
      (
        -- Requete donnant pour un code culture et les campagnes du synthetise, le nom d'une culture
        SELECT max(cpe2.name) as name
        FROM croppingplanentry cpe2
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE cpe2.code = pccns.croppingplanentrycode
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE
        GROUP BY cpe2.code
      ) precedent_nom,
      (	WITH source_species AS (
          -- Requete donnant pour un code de culture et les campagnes du synthetise, la liste des especes concernees, sans doublons
          SELECT max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
            NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
          FROM croppingplanentry cpe2
          JOIN croppingplanspecies cps2 ON cpe2.topiaid = cps2.croppingplanentry
          JOIN refespece re2 ON cps2.species = re2.topiaid
          JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
          WHERE cpe2.code = pccns.croppingplanentrycode
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE
          GROUP BY species)
        SELECT string_agg(nom_sp,' ; ')
        FROM source_species
      ) precedent_especes_EDI,
      (pccnt.rank+1)::text culture_rang,
      pi.topiaid intervention_id,
      pi.type	intervention_type,
      (	SELECT string_agg(riate2.reference_label, ' ; ')
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE aa2.practicedintervention = pi.topiaid
      ) interventions_actions,
      pi.name intervention_nom,
      pi.comment intervention_comment,
      pit.toolscouplingcodes combinaison_outils_code,
      (	SELECT max(t2.toolscouplingname)
        FROM toolscoupling t2
        WHERE t2.code = pit.toolscouplingcodes
      ) combinaison_outils_nom,
      ( 	-- Requete donnant pour un code de CO et les campagnes du synthetise, le tracteur ou l'automoteur concerne, sans doublons
        SELECT typemateriel1
        FROM toolscoupling t2
        JOIN equipment e2 ON t2.tractor = e2.topiaid
        JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
        JOIN exports_agronomes_criteres crit2 ON t2.domain = crit2.domaine_id
        WHERE t2.code = pit.toolscouplingcodes
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE
        LIMIT 1
      ) tracteur_ou_automoteur,
      (	WITH materiel_noms AS (
          -- Requete donnant pour un code de CO et les campagnes du synthetise, la liste des outils concernes, sans doublons
          SELECT max(typemateriel1) as type_materiel_1
          FROM toolscoupling t2
          JOIN equipments_toolscoupling et2 ON t2.topiaid = et2.toolscoupling
          JOIN equipment e2 ON et2.equipments = e2.topiaid
          JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
          JOIN exports_agronomes_criteres crit2 ON t2.domain = crit2.domaine_id
          WHERE t2.code = pit.toolscouplingcodes
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE
          GROUP BY e2.refmateriel
        )
        SELECT string_agg(type_materiel_1, ' ; ')
        FROM materiel_noms
      ) outils,
      pi.startingperioddate date_debut,
      pi.endingperioddate date_fin,
      pi.spatialfrequency freq_spatiale,
      pi.temporalfrequency freq_temporelle,
      pi.spatialfrequency*pi.temporalfrequency psci,
      (	SELECT proportionoftreatedsurface--||' %'
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
        AND aa2.practicedintervention = pi.topiaid
      ) proportion_surface_traitee_phyto,
      (	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
        AND aa2.practicedintervention = pi.topiaid
      ) psci_phyto,
      (	SELECT proportionoftreatedsurface--||' %'
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
        AND aa2.practicedintervention = pi.topiaid
      ) proportion_surface_traitee_lutte_bio,
      (	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
        AND aa2.practicedintervention = pi.topiaid
      ) psci_lutte_bio,
      pi.workrate debit_de_chantier,
      pi.workrateunit debit_de_chantier_unite,
      (	SELECT waterquantityaverage
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'IRRIGATION'
        AND aa2.practicedintervention = pi.topiaid
      ) quantite_eau_mm,
      (	WITH species_code_and_name as (
          -- Requete donnant les noms des sp semees pour une action de semis et les campagnes du synthetise, sans doublons
          SELECT max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
                  NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          JOIN croppingplanspecies cps2 ON sas2.speciescode = cps2.code
          JOIN refespece re2 ON cps2.species = re2.topiaid
          JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
          JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
          WHERE aa2.practicedintervention = pi.topiaid
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE
          GROUP BY sas2.speciescode, sas2.topiaid
          ORDER BY sas2.topiaid
        )
        SELECT string_agg(scan2.nom_sp, ' ; ')
        FROM species_code_and_name scan2
      ) especes_semees,
      (	SELECT string_agg(quantity :: text, ', ') FROM -- sous requete pour avoir les especes, la quantite et les unites dans le meme ordre)
          (SELECT quantity, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) densite_semis,
      (	SELECT string_agg(seedplantunit, ', ') FROM -- sous requete pour avoir les especes, la quantite et les unites dans le meme ordre)
          (SELECT seedplantunit, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) unite_semis,
      (SELECT string_agg(CASE treatment WHEN TRUE THEN 'oui' ELSE 'non' END, ', ') FROM -- sous requete pour avoir les especes, la quantite, les unites et la presence de traitements dans le meme ordre)
        (SELECT treatment, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) traitement_chimique_semis,
      (SELECT string_agg(CASE biologicalseedinoculation WHEN TRUE THEN 'oui' else 'non' END, ', ') FROM -- sous requete pour avoir les especes, la quantite, les unites et la presence de traitements dans le meme ordre)
        (SELECT biologicalseedinoculation, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) inoculation_biologique_semis,
      pi.rank rang_intervention

    FROM practicedintervention pi
    JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
    JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
    JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
    JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
    JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    --JOIN growingplan gp ON gs.growingplan = gp.topiaid
    --JOIN domain d ON gp.domain = d.topiaid
    JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
    LEFT JOIN practicedintervention_toolscouplingcodes pit ON pi.topiaid = pit.owner

    WHERE
      -- d.topiaid IN
      -- (SELECT domain from growingplan Where topiaid IN
          -- (select growingplan FROM growingsystem WHERE topiaid IN
              -- (Select growingsystem FROM growingsystem_networks
                  -- WHERE networks IN (
                      -- WITH RECURSIVE reseaux AS (
                          -- SELECT topiaid as network from network n WHERE n.name = 'CAN'
                          -- UNION
                          -- SELECT np.network from reseaux r, network_parents np WHERE r.network = np.parents
                      -- )
                      -- SELECT * FROM reseaux r
                  -- )
              -- )
          -- )
      -- )
      -- AND
      --d.active IS true
      --and gp.active IS TRUE
      gs.active IS TRUE
      and ps.active IS TRUE

    )

    UNION

  -----------------------------
  -- Interventions Perennes
  -----------------------------

    (SELECT
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.code sdc_code,
      gs.topiaid sdc_id,
      gs.name sdc_nom,
      ps.topiaid systeme_synthetise_id,
      ps.name systeme_synthetise_nom,
      ps.campaigns systeme_synthetise_campagnes,
      CASE ps.validated
        WHEN true then 'oui'
        WHEN false then 'non'
      END systeme_synthetise_validation,
      pccp.topiaid phase_id,
      pccp.type phase,
      'NA' culture_precedent_rang_id, -- topiaid de la connexion
      ppcc.croppingplanentrycode culture_code,
      (	-- Requete donnant pour un code culture et les campagnes du synthetise, le nom d'une culture
        SELECT max(cpe2.name) as name
        FROM croppingplanentry cpe2
        JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
        WHERE cpe2.code = ppcc.croppingplanentrycode
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE
        GROUP BY cpe2.code
      ) culture_nom,
      'NA' ci_code,
      'NA' ci_nom,
      'NA' concerne_la_ci,
      (WITH esp_var AS
        (-- Requete filtree pour especes avec varietes GEVES
          SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
          NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvg2.denomination),' ; ') AS espvar
          FROM practicedspeciesstade pss2
          JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
          JOIN refespece re2 ON cps2.species = re2.topiaid
          JOIN refvarietegeves rvg2 ON cps2.variety = rvg2.topiaid
          JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
          JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
          WHERE pss2.practicedintervention = pi.topiaid
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE

          UNION

          -- Requete filtree pour especes avec varietes PLANTGRAPE
          SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
          NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvpg2.variete),' ; ') AS espvar
          FROM practicedspeciesstade pss2
          JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
          JOIN refespece re2 ON cps2.species = re2.topiaid
          JOIN refvarieteplantgrape rvpg2 ON cps2.variety = rvpg2.topiaid
          JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
          JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
          WHERE pss2.practicedintervention = pi.topiaid
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE

          UNION

          -- Requete filtree pour especes sans varietes
          SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
          NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') AS espvar
          FROM practicedspeciesstade pss2
          JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
          JOIN refespece re2 ON cps2.species = re2.topiaid
          JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
          JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
          WHERE pss2.practicedintervention = pi.topiaid
          AND cps2.variety IS null
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE

        )

        SELECT string_agg(espvar, ';')
        FROM esp_var
      ) especes_de_l_intervention,

      'NA' precedent_code,
      'NA' precedent_nom,
      'NA' precedent_especes_EDI,
      'NA' culture_rang,
      pi.topiaid intervention_id,
      pi.type	intervention_type,
      (	SELECT string_agg(riate2.reference_label, ' ; ')
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE aa2.practicedintervention = pi.topiaid
      ) interventions_actions,
      pi.name intervention_nom,
      pi.comment intervention_comment,
      pit.toolscouplingcodes combinaison_outils_code,
      (	SELECT max(t2.toolscouplingname)
        FROM toolscoupling t2
        WHERE t2.code = pit.toolscouplingcodes
      ) combinaison_outils_nom,
      ( 	-- Requete donnant pour un code de CO et les campagnes du synthetise, le tracteur ou l'automoteur concerne, sans doublons
        SELECT typemateriel1
        FROM toolscoupling t2
        JOIN equipment e2 ON t2.tractor = e2.topiaid
        JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
        JOIN exports_agronomes_criteres crit2 ON t2.domain = crit2.domaine_id
        WHERE t2.code = pit.toolscouplingcodes
        AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        -- AND d2.active IS TRUE
        LIMIT 1
      ) tracteur_ou_automoteur,
      (	WITH materiel_noms AS (
          -- Requete donnant pour un code de CO et les campagnes du synthetise, la liste des outils concernes, sans doublons
          SELECT max(typemateriel1) as type_materiel_1
          FROM toolscoupling t2
          JOIN equipments_toolscoupling et2 ON t2.topiaid = et2.toolscoupling
          JOIN equipment e2 ON et2.equipments = e2.topiaid
          JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
          JOIN exports_agronomes_criteres crit2 ON t2.domain = crit2.domaine_id
          WHERE t2.code = pit.toolscouplingcodes
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE
          GROUP BY e2.refmateriel
        )
        SELECT string_agg(type_materiel_1, ' ; ')
        FROM materiel_noms
      ) outils,
      pi.startingperioddate date_debut,
      pi.endingperioddate date_fin,
      pi.spatialfrequency freq_spatiale,
      pi.temporalfrequency freq_temporelle,
      pi.spatialfrequency*pi.temporalfrequency psci,
      (	SELECT proportionoftreatedsurface--||' %'
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
        AND aa2.practicedintervention = pi.topiaid
      ) proportion_surface_traitee_phyto,
      (	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
        AND aa2.practicedintervention = pi.topiaid
      ) psci_phyto,
      (	SELECT proportionoftreatedsurface--||' %'
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
        AND aa2.practicedintervention = pi.topiaid
      ) proportion_surface_traitee_lutte_bio,
      (	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
        AND aa2.practicedintervention = pi.topiaid
      ) psci_lutte_bio,
      pi.workrate debit_de_chantier,
      pi.workrateunit debit_de_chantier_unite,
      (	SELECT waterquantityaverage
        FROM abstractaction aa2
        JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
        WHERE intervention_agrosyst = 'IRRIGATION'
        AND aa2.practicedintervention = pi.topiaid
      ) quantite_eau_mm,
      (	WITH species_code_and_name as (
          -- Requete donnant les noms des sp semees pour une action de semis et les campagnes du synthetise, sans doublons
          Select max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
                  NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          JOIN croppingplanspecies cps2 ON sas2.speciescode = cps2.code
          JOIN refespece re2 ON cps2.species = re2.topiaid
          JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
          JOIN exports_agronomes_criteres crit2 ON cpe2.domain = crit2.domaine_id
          WHERE aa2.practicedintervention = pi.topiaid
          AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
          -- AND d2.active IS TRUE
          GROUP BY sas2.speciescode, sas2.topiaid
          ORDER BY sas2.topiaid
        )
        SELECT string_agg(scan2.nom_sp, ' ; ')
        FROM species_code_and_name scan2
      ) especes_semees,
      (	SELECT string_agg(quantity :: text, ', ') FROM -- sous requete pour avoir les especes, la quantite et les unites dans le meme ordre)
          (SELECT quantity, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) densite_semis,
      (	SELECT string_agg(seedplantunit, ', ') FROM -- sous requete pour avoir les especes, la quantite et les unites dans le meme ordre)
          (SELECT seedplantunit, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) unite_semis,
      (SELECT string_agg(CASE treatment WHEN TRUE THEN 'oui' ELSE 'non' END, ', ') FROM -- sous requete pour avoir les especes, la quantite, les unites et la presence de traitements dans le meme ordre)
        (SELECT treatment, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) traitement_chimique_semis,
      (SELECT string_agg(CASE biologicalseedinoculation WHEN TRUE THEN 'oui' else 'non' END, ', ') FROM -- sous requete pour avoir les especes, la quantite, les unites et la presence de traitements dans le meme ordre)
        (SELECT biologicalseedinoculation, aa2.practicedintervention
          FROM seedingactionspecies sas2
          JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
          WHERE aa2.practicedintervention = pi.topiaid
          ORDER BY sas2.topiaid) AS foo
        GROUP BY practicedintervention
      ) AS inoculation_biologique_semis,
      pi.rank AS rang_intervention

    FROM practicedintervention pi
    JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
    JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
    JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
    JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    --JOIN growingplan gp ON gs.growingplan = gp.topiaid
    --JOIN domain d ON gp.domain = d.topiaid
    JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
    LEFT JOIN practicedintervention_toolscouplingcodes pit ON pi.topiaid = pit.owner
    WHERE
      -- d.topiaid IN
        -- (SELECT domain FROM growingplan WHERE topiaid IN
            -- (SELECT growingplan FROM growingsystem WHERE topiaid IN
                -- (SELECT growingsystem FROM growingsystem_networks
                    -- WHERE networks IN (
                        -- WITH RECURSIVE reseaux AS (
                            -- SELECT topiaid AS network FROM network n WHERE n.name = 'CAN'
                            -- UNION
                            -- SELECT np.network from reseaux r, network_parents np WHERE r.network = np.parents
                        -- )
                        -- SELECT * FROM reseaux r
                    -- )
                -- )
            -- )
        -- )
        -- AND
      --d.active IS true
      --and gp.active IS TRUE
      gs.active IS TRUE
      and ps.active IS TRUE
    )
  )
  SELECT * FROM toutes_interventions
  ORDER BY domaine_code, domaine_campagne, sdc_nom, systeme_synthetise_nom, rang_intervention;

