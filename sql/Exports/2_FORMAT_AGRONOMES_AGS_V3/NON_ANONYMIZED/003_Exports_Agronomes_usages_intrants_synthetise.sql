DROP TABLE IF EXISTS exports_agronomes_usages_intrants_synthetise; 

-------------------------------
-- Intrants_Synthetise
-------------------------------
CREATE TABLE exports_agronomes_usages_intrants_synthetise (
  domaine_code text,
  domaine_id character varying(255),
  domaine_nom text,
  domaine_campagne integer,
  sdc_id character varying(255),
  sdc_code text,
  sdc_nom text,
  systeme_synthetise_id character varying(255),
  intervention_id character varying(255),
  usage_id character varying(255),
  intrant_id character varying(255),
  intrant_type text,
  intrant_ref_id character varying(255),
  intrant_ref_nom text,
  intrant_nom_utilisateur text,
  dose double precision,
  volume_bouillie_hl double precision,
  unite text,
  profondeur_semis_cm  double precision,
  biocontrole text,
  intrant_phyto_type text,
  intrant_phyto_cible_nom text,
  forme_ferti_min text,
  n double precision,
  p2o5 double precision,
  k2o double precision,
  bore double precision,
  calcium double precision,
  fer double precision,
  manganese double precision,
  molybdene double precision,
  mgo double precision,
  oxyde_de_sodium double precision,
  so3 double precision,
  cuivre double precision,
  zinc double precision,
  cao double precision,
  s double precision,
  unite_teneur_ferti_orga character varying(255),
  ferti_effet_phyto_attendu text,
  prix double precision,
  prix_unite text
);


-- Cultures assolees
INSERT INTO exports_agronomes_usages_intrants_synthetise (
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.topiaid sdc_id,
  gs.code sdc_code,
  gs.name sdc_nom,
  ps.topiaid systeme_synthetise_id,
  pi.topiaid intervention_id,
  cu.usage_id usage_id,
  null intrant_id,
  null intrant_type,
  null intrant_ref_id,
  NULL intrant_ref_nom,
  null intrant_nom_utilisateur,
  null dose,
  aa.boiledquantity volume_bouillie_hl,
  null unite,
  null profondeur_semis_cm,
  null biocontrole,
  null intrant_phyto_type,
  null intrant_phyto_cible_nom,
  null n,
  null p2o5,
  null k2o,
  null bore,
  null calcium,
  null fer,
  null manganese,
  null molybdene,
  null mgo,
  null oxyde_de_sodium,
  null so3,
  null cuivre,
  null zinc,
  null cao,
  null s,
  null unite_teneur_ferti_orga,
  null ferti_effet_phyto_attendu,
  NULL prix,
  NULL prix_unite
  FROM abstractaction aa
  JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  join exports_agronomes_context_input_usages cu on aa.topiaid = cu.action_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
);


-- Cultures perennes
INSERT INTO exports_agronomes_usages_intrants_synthetise (
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.topiaid sdc_id,
  gs.code sdc_code,
  gs.name sdc_nom,
  ps.topiaid system_synthetise_id,	
  pi.topiaid intervention_id,
  cu.usage_id usage_id,
  null intrant_id,
  null intrant_type,
  null intrant_ref_id,
  NULL intrant_ref_nom,
  null intrant_nom_utilisateur,
  null dose,
  aa.boiledquantity volume_bouillie_hl,
  null unite,
  null profondeur_semis_cm,
  null biocontrole,
  null intrant_phyto_type,
  null intrant_phyto_cible_nom,
  null n,
  null p2o5,
  null k2o,
  null bore,
  null calcium,
  null fer,
  null manganese,
  null molybdene,
  null mgo,
  null oxyde_de_sodium,
  null so3,
  null cuivre,
  null zinc,
  null cao,
  null s,
  null unite_teneur_ferti_orga,
  null ferti_effet_phyto_attendu,
  NULL prix,
  NULL prix_unite
  FROM abstractaction aa
  JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  join exports_agronomes_context_input_usages cu on aa.topiaid = cu.action_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
);



CREATE INDEX exports_agronomes_usages_intrants_synthetise_inputid_idx ON exports_agronomes_usages_intrants_synthetise (usage_id);



-- FERTILISATION MINÉRALE
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname  ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
n = r.n,
p2o5 = r.p2o5,
k2o = r.k2o,
bore = r.bore,
calcium  = r.calcium ,
fer = r.fer ,
manganese = r.manganese ,
molybdene = r.molybdene ,
mgo = r.mgo ,
oxyde_de_sodium = r.oxyde_de_sodium ,
so3 = r.so3 ,
cuivre = r.cuivre ,
zinc = r.zinc ,
intrant_ref_nom = r.type_produit ,
forme_ferti_min = r.forme,
ferti_effet_phyto_attendu =  CASE  d.phytoeffect
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
  end
from mineralproductinputusage m 
join abstractinputusage u on m.topiaid = u.topiaid 
join domainmineralproductinput d on m.domainmineralproductinput = d.topiaid 
left join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
left join reffertiminunifa r on d.refinput = r.topiaid 
left join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;


-- GÉNÉRALE, contient les traitements de semences
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
intrant_ref_nom = r.nom_produit, --
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname  ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
intrant_phyto_type = d.producttype 
from abstractphytoproductinputusage p 
join abstractinputusage u on p.topiaid = u.topiaid 
join domainphytoproductinput d on p.domainphytoproductinput = d.topiaid 
left join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
left join inputprice i on su.inputprice = i.topiaid 
left join refactatraitementsproduit r on d.refinput = r.topiaid 
where u.topiaid = e.usage_id;


-- IRRIGATION
update exports_agronomes_usages_intrants_synthetise e set
unite = d.usageunit,
intrant_nom_utilisateur = a2.inputname,
intrant_id = d.topiaid,
intrant_type = u.inputtype,
dose = u.qtavg
from irrigationinputusage i 
join abstractinputusage u on i.topiaid = u.topiaid 
join domainirrigationinput d on i.domainirrigationinput = d.topiaid 
join abstractdomaininputstockunit a2 on a2.topiaid = d.topiaid
join abstractaction a on a.irrigationinputusage = i.topiaid 
where u.topiaid = e.usage_id;


-- ESPECES DE SEMENCES.
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = dl.topiaid,
unite = dl.usageunit,
intrant_nom_utilisateur = a2.inputname,
intrant_ref_id = dssi.topiaid,
intrant_ref_nom = resp.libelle_espece_botanique,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
profondeur_semis_cm = s.deepness
from seedspeciesinputusage s 
join abstractinputusage u on s.topiaid = u.topiaid 
join domainseedspeciesinput dssi on s.domainseedspeciesinput = dssi.topiaid
join domainseedlotinput dl on dl.topiaid = dssi.domainseedlotinput  
join croppingplanspecies cps on cps.topiaid = dssi.speciesseed 
join refespece resp on resp.topiaid = cps.species 
join abstractdomaininputstockunit a2 on a2.topiaid = dl.topiaid
left join inputprice i on i.topiaid = dssi.seedprice 
where u.topiaid = e.usage_id;

-- Lots de semence, uniquement lorsque au sein de ce lot il n'y a aucune especes déclarées (aucunes esp dans la culture)
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = dl.topiaid,
unite = dl.usageunit,
intrant_nom_utilisateur = a2.inputname ,
intrant_type = u.inputtype
from seedlotinputusage slu 
join abstractinputusage u on slu.topiaid = u.topiaid 
join domainseedlotinput dl on dl.topiaid = slu.domainseedlotinput 
join abstractdomaininputstockunit a2 on a2.topiaid = dl.topiaid
left join domainseedspeciesinput dssi on dssi.domainseedlotinput = dl.topiaid 
where dssi.topiaid is null and u.topiaid = e.usage_id;


-- FERTILISATION ORGANIQUE
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
n = d.n,
p2o5 = d.p2o5,
k2o = d.k2o,
mgo = d.mgo,
cao = d.cao,
s = d.s,
intrant_ref_nom = r.libelle ,
unite_teneur_ferti_orga = r.unite_teneur_ferti_orga
from organicproductinputusage o 
join abstractinputusage u on o.topiaid = u.topiaid 
join domainorganicproductinput d on o.domainorganicproductinput = d.topiaid 
join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
left join inputprice i on su.inputprice = i.topiaid 
left join reffertiorga r on d.refinput = r.topiaid 
where u.topiaid = e.usage_id;

-- INTRANTS AUTRES
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg
from otherproductinputusage o 
join abstractinputusage u on o.topiaid = u.topiaid 
join domainotherinput d on o.domainotherinput  = d.topiaid 
left join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
left join inputprice i on su.inputprice = i.topiaid
where u.topiaid = e.usage_id;


-- POTS 
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg
from potinputusage p
join abstractinputusage u on p.topiaid = u.topiaid 
join domainpotinput d on p.domainpotinput  = d.topiaid 
left join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
left join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;


-- SUBSTRATS
update exports_agronomes_usages_intrants_synthetise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg
from substrateinputusage s
join abstractinputusage u on s.topiaid = u.topiaid 
join domainsubstrateinput d on s.domainsubstrateinput  = d.topiaid 
left join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
left join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;



 -- 20 secondes : Récupération et aggrégations des espèces cibles, mis à jour le 11/10/2023
UPDATE exports_agronomes_usages_intrants_synthetise eacir SET intrant_phyto_cible_nom = (
    SELECT string_agg(
      (SELECT rnedi.reference_label FROM RefNuisibleEDI rnedi WHERE rnedi.topiaid = ppt.target AND ppt.abstractphytoproductinputusage = eacir.usage_id  UNION
       SELECT ra.adventice FROM RefAdventice ra WHERE ra.topiaid = ppt.target AND ppt.abstractphytoproductinputusage = eacir.usage_id ),'|')
       FROM phytoproducttarget ppt
       WHERE ppt.abstractphytoproductinputusage = eacir.usage_id
	   AND ppt0.topiaid = ppt.topiaid
  )
  FROM phytoproducttarget ppt0
  WHERE ppt0.abstractphytoproductinputusage = eacir.usage_id
  AND eacir.intrant_type IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'TRAITEMENT_SEMENCE');

