COPY(select to_char(importdate,'yyyy-mm-dd hh24:MI:ss') importdate, importfilename, issueridentification SIRET, issuername DOMAINE, softwarename,
au.firstname, au.lastname, au.email
from edaplosimport ei
LEFT JOIN agrosystuser au on ei.agrosystuser = au.topiaid order by importdate DESC
) TO STDOUT WITH (FORMAT csv, HEADER, DELIMITER ';', FORCE_QUOTE *, NULL '') \g /home/vlanglois/sql/suivi_imports_edaplos/results/AGS_2020XXXX_suivi_imports-edaplos.csv
