-- destinations déclarées par espèces. Nombre d'occurences pour la filière GCPE (afin de prioriser les prix à renseigner)
CREATE TEMPORARY TABLE prix_ref_conv_brut AS (
  select
  COALESCE(rhp.code_espece_botanique,'') AS code_espece_botanique,
  COALESCE(rhp.code_qualifiant_aee,'') AS code_qualifiant_aee,
  COALESCE(rhp.destination,'') AS destination,
  COALESCE(rhp.priceunit,'') AS priceunit,
  (CASE WHEN rhp.campaign='2003' THEN rhp.price END) "a2003",
  (CASE WHEN rhp.campaign='2004' THEN rhp.price END) "a2004",
  (CASE WHEN rhp.campaign='2005' THEN rhp.price END) "a2005",
  (CASE WHEN rhp.campaign='2006' THEN rhp.price END) "a2006",
  (CASE WHEN rhp.campaign='2007' THEN rhp.price END) "a2007",
  (CASE WHEN rhp.campaign='2008' THEN rhp.price END) "a2008",
  (CASE WHEN rhp.campaign='2009' THEN rhp.price END) "a2009",
  (CASE WHEN rhp.campaign='2010' THEN rhp.price END) "a2010",
  (CASE WHEN rhp.campaign='2011' THEN rhp.price END) "a2011",
  (CASE WHEN rhp.campaign='2012' THEN rhp.price END) "a2012",
  (CASE WHEN rhp.campaign='2013' THEN rhp.price END) "a2013",
  (CASE WHEN rhp.campaign='2014' THEN rhp.price END) "a2014",
  (CASE WHEN rhp.campaign='2015' THEN rhp.price END) "a2015",
  (CASE WHEN rhp.campaign='2016' THEN rhp.price END) "a2016",
  (CASE WHEN rhp.campaign='2017' THEN rhp.price END) "a2017",
  (CASE WHEN rhp.campaign='2018' THEN rhp.price END) "a2018",
  (CASE WHEN rhp.campaign='2019' THEN rhp.price END) "a2019",
  (CASE WHEN rhp.campaign='2020' THEN rhp.price END) "a2020",
  (CASE WHEN rhp.campaign='2021' THEN rhp.price END) "a2021"
  from refharvestingprice rhp
  WHERE rhp.organic IS FALSE -- prix conventionnel pour la première table
  AND rhp.code_espece_botanique <> '' -- on évite comme ça toutes les lignes avec 'Toutes' renseigné dans le champ 'Espèce'
  ORDER BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_sce_conv_brut AS (
  select
  COALESCE(rhp.code_espece_botanique,'') AS code_espece_botanique,
  COALESCE(rhp.code_qualifiant_aee,'') AS code_qualifiant_aee,
  COALESCE(rhp.destination,'') AS destination,
  COALESCE(rhp.priceunit,'') AS priceunit,
  (CASE WHEN rhp.scenario LIKE '%2003%' THEN rhp.price END) "sc2003",
  (CASE WHEN rhp.scenario LIKE '%2004%' THEN rhp.price END) "sc2004",
  (CASE WHEN rhp.scenario LIKE '%2005%' THEN rhp.price END) "sc2005",
  (CASE WHEN rhp.scenario LIKE '%2006%' THEN rhp.price END) "sc2006",
  (CASE WHEN rhp.scenario LIKE '%2007%' THEN rhp.price END) "sc2007",
  (CASE WHEN rhp.scenario LIKE '%2008%' THEN rhp.price END) "sc2008",
  (CASE WHEN rhp.scenario LIKE '%2009%' THEN rhp.price END) "sc2009",
  (CASE WHEN rhp.scenario LIKE '%2010%' THEN rhp.price END) "sc2010",
  (CASE WHEN rhp.scenario LIKE '%2011%' THEN rhp.price END) "sc2011",
  (CASE WHEN rhp.scenario LIKE '%2012%' THEN rhp.price END) "sc2012",
  (CASE WHEN rhp.scenario LIKE '%2013%' THEN rhp.price END) "sc2013",
  (CASE WHEN rhp.scenario LIKE '%2014%' THEN rhp.price END) "sc2014",
  (CASE WHEN rhp.scenario LIKE '%2015%' THEN rhp.price END) "sc2015",
  (CASE WHEN rhp.scenario LIKE '%2016%' THEN rhp.price END) "sc2016",
  (CASE WHEN rhp.scenario LIKE '%2017%' THEN rhp.price END) "sc2017",
  (CASE WHEN rhp.scenario LIKE '%2018%' THEN rhp.price END) "sc2018",
  (CASE WHEN rhp.scenario LIKE '%2019%' THEN rhp.price END) "sc2019",
  (CASE WHEN rhp.scenario LIKE '%2020%' THEN rhp.price END) "sc2020",
  (CASE WHEN rhp.scenario LIKE '%2021%' THEN rhp.price END) "sc2021"
  from refharvestingprice rhp
  WHERE rhp.organic IS FALSE -- prix conventionnel pour la première table
  AND rhp.code_espece_botanique <> '' -- on évite comme ça toutes les lignes avec 'Toutes' renseigné dans le champ 'Espèce'
  ORDER BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_ref_conv_trie AS (
	select
	COALESCE(prcb.code_espece_botanique,'') AS code_espece_botanique,
	COALESCE(prcb.code_qualifiant_aee,'') AS code_qualifiant_aee,
	COALESCE(prcb.destination,'') AS destination,
	COALESCE(prcb.priceunit,'') AS priceunit,
	avg(prcb.a2003) conv_2003,
	avg(prcb.a2004) conv_2004,
	avg(prcb.a2005) conv_2005,
	avg(prcb.a2006) conv_2006,
	avg(prcb.a2007) conv_2007,
	avg(prcb.a2008) conv_2008,
	avg(prcb.a2009) conv_2009,
	avg(prcb.a2010) conv_2010,
	avg(prcb.a2011) conv_2011,
	avg(prcb.a2012) conv_2012,
	avg(prcb.a2013) conv_2013,
	avg(prcb.a2014) conv_2014,
	avg(prcb.a2015) conv_2015,
	avg(prcb.a2016) conv_2016,
	avg(prcb.a2017) conv_2017,
	avg(prcb.a2018) conv_2018,
	avg(prcb.a2019) conv_2019,
	avg(prcb.a2020) conv_2020,
	avg(prcb.a2021) conv_2021
	from prix_ref_conv_brut prcb
	GROUP BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_sce_conv_trie AS (
	select
	COALESCE(pscb.code_espece_botanique,'') AS code_espece_botanique,
	COALESCE(pscb.code_qualifiant_aee,'') AS code_qualifiant_aee,
	COALESCE(pscb.destination,'') AS destination,
	COALESCE(pscb.priceunit,'') AS priceunit,
	avg(pscb.sc2003) sce_conv_2003,
	avg(pscb.sc2004) sce_conv_2004,
	avg(pscb.sc2005) sce_conv_2005,
	avg(pscb.sc2006) sce_conv_2006,
	avg(pscb.sc2007) sce_conv_2007,
	avg(pscb.sc2008) sce_conv_2008,
	avg(pscb.sc2009) sce_conv_2009,
	avg(pscb.sc2010) sce_conv_2010,
	avg(pscb.sc2011) sce_conv_2011,
	avg(pscb.sc2012) sce_conv_2012,
	avg(pscb.sc2013) sce_conv_2013,
	avg(pscb.sc2014) sce_conv_2014,
	avg(pscb.sc2015) sce_conv_2015,
	avg(pscb.sc2016) sce_conv_2016,
	avg(pscb.sc2017) sce_conv_2017,
	avg(pscb.sc2018) sce_conv_2018,
	avg(pscb.sc2019) sce_conv_2019,
	avg(pscb.sc2020) sce_conv_2020,
	avg(pscb.sc2021) sce_conv_2021
	from prix_sce_conv_brut pscb
	GROUP BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_ref_AB_brut AS (
  select
  COALESCE(rhp.code_espece_botanique,'') AS code_espece_botanique,
  COALESCE(rhp.code_qualifiant_aee,'') AS code_qualifiant_aee,
  COALESCE(rhp.destination,'') AS destination,
  COALESCE(rhp.priceunit,'') AS priceunit,
  (CASE WHEN rhp.campaign='2003' THEN rhp.price END) "a2003",
  (CASE WHEN rhp.campaign='2004' THEN rhp.price END) "a2004",
  (CASE WHEN rhp.campaign='2005' THEN rhp.price END) "a2005",
  (CASE WHEN rhp.campaign='2006' THEN rhp.price END) "a2006",
  (CASE WHEN rhp.campaign='2007' THEN rhp.price END) "a2007",
  (CASE WHEN rhp.campaign='2008' THEN rhp.price END) "a2008",
  (CASE WHEN rhp.campaign='2009' THEN rhp.price END) "a2009",
  (CASE WHEN rhp.campaign='2010' THEN rhp.price END) "a2010",
  (CASE WHEN rhp.campaign='2011' THEN rhp.price END) "a2011",
  (CASE WHEN rhp.campaign='2012' THEN rhp.price END) "a2012",
  (CASE WHEN rhp.campaign='2013' THEN rhp.price END) "a2013",
  (CASE WHEN rhp.campaign='2014' THEN rhp.price END) "a2014",
  (CASE WHEN rhp.campaign='2015' THEN rhp.price END) "a2015",
  (CASE WHEN rhp.campaign='2016' THEN rhp.price END) "a2016",
  (CASE WHEN rhp.campaign='2017' THEN rhp.price END) "a2017",
  (CASE WHEN rhp.campaign='2018' THEN rhp.price END) "a2018",
  (CASE WHEN rhp.campaign='2019' THEN rhp.price END) "a2019",
  (CASE WHEN rhp.campaign='2020' THEN rhp.price END) "a2020",
  (CASE WHEN rhp.campaign='2021' THEN rhp.price END) "a2021"
  from refharvestingprice rhp
  WHERE rhp.organic IS TRUE -- prix en agriculture biologique
  AND rhp.code_espece_botanique <> '' -- on évite comme ça toutes les lignes avec 'Toutes' renseigné dans le champ 'Espèce'
  ORDER BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_sce_AB_brut AS (
  select
  COALESCE(rhp.code_espece_botanique,'') AS code_espece_botanique,
  COALESCE(rhp.code_qualifiant_aee,'') AS code_qualifiant_aee,
  COALESCE(rhp.destination,'') AS destination,
  COALESCE(rhp.priceunit,'') AS priceunit,
  (CASE WHEN rhp.scenario LIKE '%2003%' THEN rhp.price END) "sc2003",
  (CASE WHEN rhp.scenario LIKE '%2004%' THEN rhp.price END) "sc2004",
  (CASE WHEN rhp.scenario LIKE '%2005%' THEN rhp.price END) "sc2005",
  (CASE WHEN rhp.scenario LIKE '%2006%' THEN rhp.price END) "sc2006",
  (CASE WHEN rhp.scenario LIKE '%2007%' THEN rhp.price END) "sc2007",
  (CASE WHEN rhp.scenario LIKE '%2008%' THEN rhp.price END) "sc2008",
  (CASE WHEN rhp.scenario LIKE '%2009%' THEN rhp.price END) "sc2009",
  (CASE WHEN rhp.scenario LIKE '%2010%' THEN rhp.price END) "sc2010",
  (CASE WHEN rhp.scenario LIKE '%2011%' THEN rhp.price END) "sc2011",
  (CASE WHEN rhp.scenario LIKE '%2012%' THEN rhp.price END) "sc2012",
  (CASE WHEN rhp.scenario LIKE '%2013%' THEN rhp.price END) "sc2013",
  (CASE WHEN rhp.scenario LIKE '%2014%' THEN rhp.price END) "sc2014",
  (CASE WHEN rhp.scenario LIKE '%2015%' THEN rhp.price END) "sc2015",
  (CASE WHEN rhp.scenario LIKE '%2016%' THEN rhp.price END) "sc2016",
  (CASE WHEN rhp.scenario LIKE '%2017%' THEN rhp.price END) "sc2017",
  (CASE WHEN rhp.scenario LIKE '%2018%' THEN rhp.price END) "sc2018",
  (CASE WHEN rhp.scenario LIKE '%2019%' THEN rhp.price END) "sc2019",
  (CASE WHEN rhp.scenario LIKE '%2020%' THEN rhp.price END) "sc2020",
  (CASE WHEN rhp.scenario LIKE '%2021%' THEN rhp.price END) "sc2021"
  from refharvestingprice rhp
  WHERE rhp.organic IS TRUE -- prix conventionnel pour la première table
  AND rhp.code_espece_botanique <> '' -- on évite comme ça toutes les lignes avec 'Toutes' renseigné dans le champ 'Espèce'
  ORDER BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_ref_AB_trie AS (
	select
	COALESCE(code_espece_botanique,'') AS code_espece_botanique,
	COALESCE(code_qualifiant_aee,'') AS code_qualifiant_aee,
	COALESCE(destination,'') AS destination,
	COALESCE(priceunit,'') AS priceunit,
	avg(prab.a2003) bio_2003,
	avg(prab.a2004) bio_2004,
	avg(prab.a2005) bio_2005,
	avg(prab.a2006) bio_2006,
	avg(prab.a2007) bio_2007,
	avg(prab.a2008) bio_2008,
	avg(prab.a2009) bio_2009,
	avg(prab.a2010) bio_2010,
	avg(prab.a2011) bio_2011,
	avg(prab.a2012) bio_2012,
	avg(prab.a2013) bio_2013,
	avg(prab.a2014) bio_2014,
	avg(prab.a2015) bio_2015,
	avg(prab.a2016) bio_2016,
	avg(prab.a2017) bio_2017,
	avg(prab.a2018) bio_2018,
	avg(prab.a2019) bio_2019,
	avg(prab.a2020) bio_2020,
	avg(prab.a2021) bio_2021
	from prix_ref_AB_brut prab
	GROUP BY 1, 2, 3, 4
);

CREATE TEMPORARY TABLE prix_sce_AB_trie AS (
	select
	COALESCE(psab.code_espece_botanique,'') AS code_espece_botanique,
	COALESCE(psab.code_qualifiant_aee,'') AS code_qualifiant_aee,
	COALESCE(psab.destination,'') AS destination,
	COALESCE(psab.priceunit,'') AS priceunit,
	avg(psab.sc2003) sce_bio_2003,
	avg(psab.sc2004) sce_bio_2004,
	avg(psab.sc2005) sce_bio_2005,
	avg(psab.sc2006) sce_bio_2006,
	avg(psab.sc2007) sce_bio_2007,
	avg(psab.sc2008) sce_bio_2008,
	avg(psab.sc2009) sce_bio_2009,
	avg(psab.sc2010) sce_bio_2010,
	avg(psab.sc2011) sce_bio_2011,
	avg(psab.sc2012) sce_bio_2012,
	avg(psab.sc2013) sce_bio_2013,
	avg(psab.sc2014) sce_bio_2014,
	avg(psab.sc2015) sce_bio_2015,
	avg(psab.sc2016) sce_bio_2016,
	avg(psab.sc2017) sce_bio_2017,
	avg(psab.sc2018) sce_bio_2018,
	avg(psab.sc2019) sce_bio_2019,
	avg(psab.sc2020) sce_bio_2020,
	avg(psab.sc2021) sce_bio_2021
	from prix_sce_AB_brut psab
	GROUP BY 1, 2, 3, 4
);

select
COALESCE(re.libelle_espece_botanique,'') AS espece,
COALESCE(re.libelle_qualifiant_aee,'') AS qualifiant,
COALESCE(rd.destination,'') AS destination,
count(distinct hav.topiaid) AS nb_saisies,
count(distinct hav.topiaid) FILTER (WHERE hav.isorganiccrop IS TRUE) nb_saisies_AB,
count(distinct hav.topiaid) FILTER (WHERE p.price IS NOT NULL AND hav.isorganiccrop IS FALSE) nb_saisies_avec_prix_conventionnel,
count(distinct hav.topiaid) FILTER (WHERE p.price IS NOT NULL AND hav.isorganiccrop IS TRUE) nb_saisies_avec_prix_AB,
prct.*,
prat.*,
pscb.*,
psab.*
from harvestingactionvalorisation hav
JOIN price p ON p.harvestingactionvalorisation = hav.topiaid
JOIN refdestination rd ON rd.topiaid = hav.destination
JOIN croppingplanspecies cps ON cps.code = hav.speciescode
JOIN refespece re ON cps.species = re.topiaid
LEFT JOIN prix_ref_conv_trie prct ON concat_ws(COALESCE(re.code_espece_botanique,''),COALESCE(re.code_qualifiant_aee,''),COALESCE(rd.destination,'')) = concat_ws(COALESCE(prct.code_espece_botanique,''),COALESCE(prct.code_qualifiant_aee,''),COALESCE(prct.destination,''))
LEFT JOIN prix_ref_AB_trie prat ON concat_ws(COALESCE(re.code_espece_botanique,''),COALESCE(re.code_qualifiant_aee,''),COALESCE(rd.destination,'')) = concat_ws(COALESCE(prat.code_espece_botanique,''),COALESCE(prat.code_qualifiant_aee,''),COALESCE(prat.destination,''))
LEFT JOIN prix_sce_conv_trie pscb ON concat_ws(COALESCE(re.code_espece_botanique,''),COALESCE(re.code_qualifiant_aee,''),COALESCE(rd.destination,'')) = concat_ws(COALESCE(pscb.code_espece_botanique,''),COALESCE(pscb.code_qualifiant_aee,''),COALESCE(pscb.destination,''))
LEFT JOIN prix_sce_AB_trie psab ON concat_ws(COALESCE(re.code_espece_botanique,''),COALESCE(re.code_qualifiant_aee,''),COALESCE(rd.destination,'')) = concat_ws(COALESCE(psab.code_espece_botanique,''),COALESCE(psab.code_qualifiant_aee,''),COALESCE(psab.destination,''))
WHERE rd.sector ='GRANDES_CULTURES' OR rd.espece = 'Pomme de terre' --pas de polyculture élevage dans ce référentiel
GROUP BY
1,
2,
3,
prct.code_espece_botanique,
prct.code_qualifiant_aee,
prct.destination,
prct.priceunit,
prct.conv_2003,
prct.conv_2004,
prct.conv_2005,
prct.conv_2006,
prct.conv_2007,
prct.conv_2008,
prct.conv_2009,
prct.conv_2010,
prct.conv_2011,
prct.conv_2012,
prct.conv_2013,
prct.conv_2014,
prct.conv_2015,
prct.conv_2016,
prct.conv_2017,
prct.conv_2018,
prct.conv_2019,
prct.conv_2020,
prct.conv_2021,
prat.code_espece_botanique,
prat.code_qualifiant_aee,
prat.destination,
prat.priceunit,
prat.bio_2003,
prat.bio_2004,
prat.bio_2005,
prat.bio_2006,
prat.bio_2007,
prat.bio_2008,
prat.bio_2009,
prat.bio_2010,
prat.bio_2011,
prat.bio_2012,
prat.bio_2013,
prat.bio_2014,
prat.bio_2015,
prat.bio_2016,
prat.bio_2017,
prat.bio_2018,
prat.bio_2019,
prat.bio_2020,
prat.bio_2021,
pscb.code_espece_botanique,
pscb.code_qualifiant_aee,
pscb.destination,
pscb.priceunit,
pscb.sce_conv_2003,
pscb.sce_conv_2004,
pscb.sce_conv_2005,
pscb.sce_conv_2006,
pscb.sce_conv_2007,
pscb.sce_conv_2008,
pscb.sce_conv_2009,
pscb.sce_conv_2010,
pscb.sce_conv_2011,
pscb.sce_conv_2012,
pscb.sce_conv_2013,
pscb.sce_conv_2014,
pscb.sce_conv_2015,
pscb.sce_conv_2016,
pscb.sce_conv_2017,
pscb.sce_conv_2018,
pscb.sce_conv_2019,
pscb.sce_conv_2020,
pscb.sce_conv_2021,
psab.code_espece_botanique,
psab.code_qualifiant_aee,
psab.destination,
psab.priceunit,
psab.sce_bio_2003,
psab.sce_bio_2004,
psab.sce_bio_2005,
psab.sce_bio_2006,
psab.sce_bio_2007,
psab.sce_bio_2008,
psab.sce_bio_2009,
psab.sce_bio_2010,
psab.sce_bio_2011,
psab.sce_bio_2012,
psab.sce_bio_2013,
psab.sce_bio_2014,
psab.sce_bio_2015,
psab.sce_bio_2016,
psab.sce_bio_2017,
psab.sce_bio_2018,
psab.sce_bio_2019,
psab.sce_bio_2020,
psab.sce_bio_2021
ORDER BY 4 DESC;


