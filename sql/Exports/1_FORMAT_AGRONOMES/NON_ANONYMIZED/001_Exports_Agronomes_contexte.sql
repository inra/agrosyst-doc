-----------------------------------------------
-- Exports_Agronomes_contexte_NON ANONYME
-----------------------------------------------

-- Modifications 1-2 :
-- Ajout du volume de bouillie dans les intrants
-- rectification du nom de la colonne des unites d'intrants type autres. Car il semblerait que les ameliorations d'ergo autour de ce type aient engendrees des modifs du nom
-- Intrants_Realise : correction => ajout du filtre oublie pour ne ramener que les intrants des interventions sur les cultures assolees des parcelles affectees a un sdc
-- 1.3 : ajout de la cible des produits phyto (phytoproductinput_targets)
-- 1.4 : suppression des caracteres accentues au niveau des en-tetes des colonnes
-- 1.5 : ajout d'un filtre sur le champ ACTIVE (is true) sur tous les elements qui possedent ce champ en plus du domaine : sdc, dispositif, synthetise, parcelle, zone
-- 1.6 : stockage des resultats des requetes dans des tables temporaires pour faciliter le traitement des donnees a extraire
-- 2019-12-04 : ajout du stockage des resultats des requetes dans des tables temporaires
--              suppression d'une occurrence de la colonne resultat taille_limitant_risques_sanitaires qui etait en doublon
-- 2019-12-05 : ajout du type de production au niveau des sdc
-- 2019-12-12 : sdc : ajout et regroupement des reseaux "IR" dans une seule cellule dans le cas où il y a plusieurs réseaux IR pour un meme sdc (séparateur "|") (avant 1 ligne par couple sdc/reseau IR)
-- 	            sdc : ajout des reseaux parents "IT" associes avec le meme principe (regroupement + separateur "|")
-- 2019-12-13 : correction du nom d'une colonne : 	enheberment_naturel => enherbement_naturel
--              ajout des modes de commercialisation (sdc) au format json
-- 2020-01-07 : ajout de 2 infos au niveau de la requete Rotation_assolees_Synthetise pour chacune des cultures composant la rotation :
--				Fin de la rotation (OUI/NON)
--				Même campagne agricole que la culture précédente (OUI/NON)
-- 2020-05    : ajout pourcentage sole sdc de la culture dans la requete perennes_synthetise
--              ajout de la phase + durée de la phase dans la requete perennes_synthetise
--              ajout du code AMM dans intrants_synthetise et intrants_realise
--              ajout de la modalité de suivi dans DEPHY dans la requete sdc
--              ajout de la petite région agricole dans la requete domaines
--              ajout des variables relatives aux enjeux environnementaux dans la requete domaines
--              ajout de la phase + durée de la phase dans la requete perennes_realise
--              ajout de la surface de la zone dans les requetes Recolte_realise, Caracteristiques_Perennes_Realise et Succession_assolees_realise
--              ajout du numéro de branche de la culture dans la rotation (=ordonnée d'une culture) dans la requete Rotation_assolees_Synthetise
--              ajout de la requete Coordonnees_gps_domaines
--              ajout de la requete Ateliers_elevage
--              ajout d'une requete specifique aux zones, suppression de la surface de la zone dans les requetes Recolte_realise, Caracteristiques_Perennes_Realise et Succession_assolees_realise
--              ajout des infos de culture absente, des id des cultures dans la rotation, ainsi que du rang et de l'indicateur de branche du precedent dans la requete Rotation_assolees_Synthetise
-- 2020-10   :  ajout du champ phase_id de la table practicedcropcyclephase au niveau de la requête Caracteristiques_Perennes_Synthetise
-- 2020-11    : ajout des champs sdc_id, sdc_code et sdc_nom dans la requete Intrants_Realise
-- 2020-12    : ajout phase_id dans la requete perennes_realise
--            : ajout de la requete dispositifs
--            : requetes Recolte_realise et Recolte_synthetise : le union ramenait les lignes distinctement (contrairement au union ALL) et donc une seule ligne si plusieurs rendements
--              identiques pour un meme couple intervention/destination
--              => ajout de la somme sur les rendements pour tenir compte de ces cas
-- 2021-04    : ajout de critères d'extraction pour n'extraire que certaines données selon des critères définis (dispositifs DEPHY par exemple)
--              avec pour les domaines et les données directement associées présnce d'au moins 1 dispositif actif dans le domaine (domaine, coordonnees GPS domaine, ateliers d'elevage, parc materiel, cultures) 
-- 				et pour les autres données liées à un dispositif, le rattachement à  un dispositif actif (parcelles, zones, dipositifs, sdc, intrants, récoltes, pérennes, rotation, ITK)
--				=> les données non liées à un dispositif ne seront pas exportées alors qu'auparavant les parcelles et zones non rattachées à un dispositif étaient qd meme exportées
--              suppression du champ especes_de_l_intervention au niveau des tables intrants car ce champ n'était pas alimenté par les requetes et qu'il est présent dans les tables interventions
--				renommage de tous les champs avec le suffixe _name => suffixe _nom
-- 2021-05    : prise en compte du changement de table des cibles des traitements phyto : phytoproductinput_targets => phytoproducttarget
-- 2021-08    : ajout des champs mixspecies et mixvariety dans la requete cultures
-- 2021-10    : remplacement de la somme des rendements sur une intervention par la somme des moyennes des rendements pondérés par la surface utilisée parl'espèce pou les cultures non déclarée comme mix espèces
-- 2021-12    : mise à jour des requêtes d'export des intrants
-- 2022-08    : ajout des codes_convention_dephy des réseaux dans l'export sdc

-- mémo
-- copy (SELECT * FROM exports_agronomes_context_recolte_synthetise) TO '/tmp/exports_agronomes_context_recolte_synthetise.csv' WITH DELIMITER AS ';' CSV HEADER;

-- Domaines
-- Coordonnees_gps_domaines
-- Ateliers_elevage
-- Parcelles
-- Zones
-- Materiels
-- Dispositifs
-- SdC
-- Intrants_Realise
-- Intrants_Synthetise
-- Parcelle_Type
-- Recolte_realise  >>> requete a verifier
-- Recolte_synthetise  >>> requete a verifier
-- Cultures
-- Caracteristiques_Perennes_Realise
-- Caracteristiques_Perennes_Synthetise
-- Rotation_assolees_Synthetise
-- Succession_assolees_realise
-- A retenir
--itk non rattaché à un itk en realisé non exploitées

-- suppression des tables temporaires de stockage des resultats des requetes
DROP TABLE IF EXISTS exp_effective_hav; -- utilisé pour le calcul de la moyenne pondérée d'un rendement en rapport avec la surface cultivée d'une espèce/variété
DROP TABLE IF EXISTS exports_agronomes_context_ateliers_elevage;-- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_coordonnees_gps_domaines;-- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_cultures; -- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_dispositifs; -- dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_domaines; -- au moins 1 dispositif DEPHY actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_interventions_realise;
DROP TABLE IF EXISTS exports_agronomes_context_interventions_synthetise;
DROP TABLE IF EXISTS exports_agronomes_context_intrants_realise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_intrants_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_materiels; -- au moins 1 dispositif DEPHY  actif dans le domaine
DROP TABLE IF EXISTS exports_agronomes_context_parcelles; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_parcelle_type; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_perennes_realise;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_perennes_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_recolte_realise;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_recolte_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_rotation_assolees_synthetise; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_sdc; -- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_succession_assolees_realise;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_context_zones;-- rattachement dispositif DEPHY actif
DROP TABLE IF EXISTS exports_agronomes_criteres;
DROP TABLE IF EXISTS exports_agronomes_parcelles_non_rattachees;
DROP TABLE IF EXISTS exp_practiced_hav; -- utilisé pour le calcul de la moyenne pondérée d'un rendement en rapport avec la surface cultivée d'une espèce/variété
DROP TABLE IF EXISTS ferti_min_orga;
DROP TABLE IF EXISTS perf_harvestingaction; -- utiliser pour les récolte pour avoir la caractéristique mix espece
DROP TABLE IF EXISTS perf_reportregional;

------------------------
-- criteres extraction
------------------------
-- domaines actifs avec au moins un dispositif DEPHY actif (et dates de campagnes cohérentes)
CREATE TEMPORARY TABLE exports_agronomes_criteres as
  SELECT
    d.topiaid domaine_id,
    d.code domaine_code,
    d.name domaine_nom,
    d.campaign domaine_campagne,
    gp.topiaid dispositif_id,
    gp.code dispositif_code,
    gp.name dispositif_nom,
    gp.type dispositif_type
  FROM growingplan gp
  JOIN domain d ON gp.domain = d.topiaid
  WHERE d.active IS TRUE
  AND gp.active IS TRUE
  AND gp.type !='NOT_DEPHY'
  AND campaign>1999
  AND campaign<2026;

CREATE INDEX exports_agronomes_criteres_idx1 on exports_agronomes_criteres(domaine_id, dispositif_id);
CREATE INDEX exports_agronomes_criteres_idx2 on exports_agronomes_criteres(dispositif_id);

-------------------------------------------
-- Recolte >>> tables utilitaire pour le calcul moyenne pondérée des rendement
-- on ajoute la colonne mixspecies pour remonter cette information provenant de la culture sur l'action
-------------------------------------------

CREATE TEMPORARY TABLE perf_harvestingaction (
  effectiveintervention character varying(255),
  practicedintervention character varying(255),
  topiaid character varying(255) NOT NULL,
  mainaction text,
  mixspecies boolean default false
);

INSERT INTO perf_harvestingaction
  SELECT aa.effectiveintervention, aa.practicedintervention, aa.topiaid, aa.mainaction
  FROM abstractaction aa
  WHERE topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl';

CREATE INDEX perf_harvestingaction_idx on perf_harvestingaction(topiaid);
CREATE INDEX perf_harvestingaction_practicedintervention_idx on perf_harvestingaction(practicedintervention);
CREATE INDEX perf_harvestingaction_effectiveintervention_idx on perf_harvestingaction(effectiveintervention);

UPDATE perf_harvestingaction aa SET mixspecies = TRUE
    WHERE aa.practicedintervention IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM practicedintervention pi
      JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
      JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
      JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
      JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
      JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
      JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      JOIN domain d ON crit.domaine_id = d.topiaid AND ps.campaigns LIKE CONCAT('%' ,d.campaign, '%')
      JOIN practicedspeciesstade pss ON pss.practicedintervention = pi.topiaid
      JOIN croppingplanspecies cps ON cps.code = pss.speciescode
      JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry AND (cpe.mixspecies IS TRUE OR cpe.mixvariety IS TRUE)
      WHERE aa.practicedintervention = pi.topiaid
      AND gs.active IS TRUE
      AND ps.active IS TRUE
    );

UPDATE perf_harvestingaction aa SET mixspecies = TRUE
    WHERE aa.practicedintervention IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM practicedintervention pi
      JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
      JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
      JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
      JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
      JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      JOIN domain d ON crit.domaine_id = d.topiaid AND ps.campaigns LIKE CONCAT('%' ,d.campaign, '%')
      JOIN practicedspeciesstade pss ON pss.practicedintervention = pi.topiaid
      JOIN croppingplanspecies cps ON cps.code = pss.speciescode
      JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry AND (cpe.mixspecies IS TRUE OR cpe.mixvariety IS TRUE)
      WHERE aa.practicedintervention = pi.topiaid
      AND gs.active IS TRUE
      AND ps.active IS TRUE
    );

UPDATE perf_harvestingaction aa SET mixspecies = TRUE
    WHERE aa.effectiveintervention IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM effectiveintervention ei
      INNER JOIN effectivespeciesstade ess ON ess.effectiveintervention = ei.topiaid
      INNER JOIN croppingplanspecies cps ON cps.topiaid = ess.croppingplanspecies
      JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry AND (cpe.mixspecies IS TRUE OR cpe.mixvariety IS TRUE)
      WHERE aa.effectiveintervention = ei.topiaid
    ) ;

-------------------------------------------
-- Recolte >>> tables utilitaire pour le calcul moyenne pondérée des rendement
-- à la surface des espèces sélectionnées sur une intervention
-------------------------------------------

CREATE TEMPORARY TABLE exp_effective_hav AS
  SELECT
  hav.topiaid,
  hav.harvestingaction,
  hav.destination,
  hav.yealdaverage,
  hav.yealdmedian,
  hav.yealdmin,
  hav.yealdmax,
  hav.yealdunit,
  hav.salespercent,
  hav.selfconsumedpersent,
  hav.novalorisationpercent,
  (SELECT count(ess.croppingplanspecies) FROM effectivespeciesstade ess WHERE ess.effectiveintervention = ei.topiaid ) AS nb_species,
  (SELECT SUM(cps.speciesarea::NUMERIC) FROM effectivespeciesstade ess INNER JOIN croppingplanspecies cps ON cps.topiaid = ess.croppingplanspecies WHERE ess.effectiveintervention = ei.topiaid ) AS crop_area,
  (SELECT cps.speciesarea FROM effectivespeciesstade ess INNER JOIN croppingplanspecies cps ON cps.topiaid = ess.croppingplanspecies AND cps.code = hav.speciescode WHERE ess.effectiveintervention = ei.topiaid ) AS species_area
  FROM harvestingactionvalorisation hav
  INNER JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention;

CREATE INDEX perf_exp_effective_hav0 ON exp_effective_hav(harvestingaction);
DELETE FROM exp_effective_hav WHERE nb_species IS NULL OR nb_species = 0;

-- la répartition des espèce n'a pas été déclarée, on applique une répartition par défaut
UPDATE exp_effective_hav SET species_area = 100.0/nb_species WHERE species_area IS NULL AND nb_species > 0;
UPDATE exp_effective_hav eeh SET crop_area =
  (SELECT SUM(eeh1.species_area)
   FROM exp_effective_hav eeh1
   WHERE eeh1.harvestingaction = eeh.harvestingaction
   AND eeh1.destination = eeh.destination)
   WHERE nb_species > 0
   AND crop_area IS NULL;

CREATE TEMPORARY TABLE exp_practiced_hav AS
  SELECT
    hav.topiaid,
    hav.harvestingaction,
    hav.destination,
    hav.yealdaverage,
    hav.yealdmedian,
    hav.yealdmin,
    hav.yealdmax,
    hav.yealdunit,
    hav.salespercent,
    hav.selfconsumedpersent,
    hav.novalorisationpercent,
    (SELECT count(pss.topiaid) FROM practicedspeciesstade pss WHERE pss.practicedintervention = pi.topiaid ) AS nb_species,
    (SELECT SUM(cps.speciesarea::NUMERIC) FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid ) AS crop_area,
    (SELECT cps.speciesarea FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.code = hav.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid) AS species_area
    FROM harvestingactionvalorisation hav
    INNER JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
    INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
    INNER JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
    INNER JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
    INNER JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
    INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
    INNER JOIN croppingplanentry cpe ON crit.domaine_id = cpe.domain AND ((cpe.code = pccnt.croppingplanentrycode AND pi.intermediatecrop IS FALSE) OR (cpe.code = pccc.intermediatecroppingplanentrycode AND pi.intermediatecrop IS TRUE))
  UNION ALL
    SELECT
    hav.topiaid,
    hav.harvestingaction,
    hav.destination,
    hav.yealdaverage,
    hav.yealdmedian,
    hav.yealdmin,
    hav.yealdmax,
    hav.yealdunit,
    hav.salespercent,
    hav.selfconsumedpersent,
    hav.novalorisationpercent,
    (SELECT count(pss.topiaid) FROM practicedspeciesstade pss WHERE pss.practicedintervention = pi.topiaid ) AS nb_species,
    (SELECT SUM(cps.speciesarea::NUMERIC) FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid ) AS crop_area,
    (SELECT cps.speciesarea FROM practicedspeciesstade pss INNER JOIN croppingplanspecies cps ON cps.code = pss.speciescode AND cps.code = hav.speciescode AND cps.croppingplanentry = cpe.topiaid WHERE pss.practicedintervention = pi.topiaid) AS species_area
    FROM harvestingactionvalorisation hav
    INNER JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
    INNER JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
    INNER JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
    INNER JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
    INNER JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
    INNER JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
    INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
    INNER JOIN croppingplanentry cpe ON crit.domaine_id = cpe.domain AND ppcc.croppingplanentrycode = cpe.code;

CREATE INDEX perf_exp_practiced_hav0 ON exp_practiced_hav(harvestingaction);
-- la répartition des espèce n'a pas été déclarée, on applique une répartition par défaut
UPDATE exp_practiced_hav SET species_area = 100.0/nb_species WHERE species_area IS NULL AND nb_species > 0;
UPDATE exp_practiced_hav eph SET crop_area = (SELECT SUM(eph1.species_area) FROM exp_practiced_hav eph1 WHERE eph1.harvestingaction = eph.harvestingaction AND eph.destination = eph1.destination) WHERE nb_species > 0 AND crop_area IS NULL;

DELETE FROM exp_practiced_hav WHERE nb_species IS NULL OR nb_species = 0;
-------------
-- Domaines
-------------
CREATE TABLE exports_agronomes_context_domaines AS
  WITH domainresponsibles as (
    SELECT domaincode, string_agg(au.lastname||' '||au.firstname, ', ') AS responsibles
    FROM userrole ur
    JOIN agrosystuser au ON ur.agrosystuser = au.topiaid
    WHERE domaincode IS NOT NULL
    GROUP BY domaincode
  )
  SELECT
    d.code domaine_code,
    d.topiaid domaine_id,
    d.name domaine_nom,
    d.siret,
    d.campaign domaine_campagne,
    d.type domaine_type,
    rl.departement departement,
    rl.codeinsee||' - '||rl.commune commune,
    rl.petiteRegionAgricoleCode|| ' - '||rl.petiteRegionAgricoleNom petite_region_agricole,
    d.zoning domaine_zonage,
    d.uaavulnarablepart pct_SAU_zone_vulnerable,
    d.uaastructuralsurplusareapart pct_SAU_zone_excedent_structurel,
    d.uaaactionpart pct_SAU_zone_actions_complementaires,
    d.uaanatura2000part pct_SAU_zone_natura_2000,
    d.uaaerosionregionpart pct_SAU_zone_erosion,
    d.uaawaterresourceprotectionpart pct_SAU_perimetre_protection_captage,
    d.description domain_description,
    rls.libelle_insee statut_juridique_nom,
    d.statuscomment statut_juridique_commentaire,
    d.usedagriculturalarea SAU,
    d.croppingplancomment cultures_commentaire,
    d.otheractivitiescomment autres_activites_commentaire,
    d.workforcecomment MO_commentaire,
    d.partnersnumber nombre_associes,
    d.otherworkforce MO_familiale_et_associes,
    d.permanentemployeesworkforce MO_permanente,
    d.temporaryemployeesworkforce MO_temporaire,
    d.familyworkforcewage MO_familiale_remuneration,
    d.wagecosts charges_salariales,
    d.cropsworkforce MO_conduite_cultures_dans_domaine_expe,
    d. msafee cotisation_MSA,
    d.averagetenantfarming fermage_moyen,
    d.decoupledassistance aides_decouplees,
    ro1.libelle_otex_18_postes otex_18_nom,
    ro2.libelle_otex_70_postes otex_70_nom,
    d.orientation otex_commentaire,
    dr.responsibles responsables_domaine

  FROM domain d
  JOIN exports_agronomes_criteres crit ON d.topiaid = crit.domaine_id
  LEFT JOIN domainresponsibles dr ON d.code = dr.domaincode
  LEFT JOIN reflocation rl ON d.location = rl.topiaid
  LEFT JOIN reflegalstatus rls ON d.legalstatus = rls.topiaid
  LEFT JOIN refotex ro1 ON d.otex18 = ro1.topiaid
  LEFT JOIN refotex ro2 ON d.otex70 = ro2.topiaid;

-----------------------------
-- Coordonnees_gps_domaines
-----------------------------
CREATE TABLE exports_agronomes_context_coordonnees_gps_domaines AS
  SELECT
    crit.domaine_id,
    g.name AS coordonnees_nom_centre_operationnel,
    g.description AS coordonnees_description_centre_operationnel,
    g.longitude,
    g.latitude
  FROM geopoint g
  --JOIN domain d ON g.domain = d.topiaid
  --where d.active is true
  JOIN exports_agronomes_criteres crit ON g.domain = crit.domaine_id
  AND g.longitude!=0
  AND g.latitude !=0
  AND g.validated is true;

---------------------
-- Ateliers_elevage
---------------------
CREATE TABLE exports_agronomes_context_ateliers_elevage AS
  SELECT
  crit.domaine_id,
  l.topiaid AS atelier_elevage_id,
  r.animaltype AS atelier_elevage_type_animaux,
  l.livestockunitsize AS atelier_elevage_taille,
  r.animalpopulationunits AS atelier_elevage_unite
  FROM livestockunit l
  --JOIN domain d ON l.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON l.domain = crit.domaine_id
  JOIN refanimaltype r ON l.refanimaltype = r.topiaid;
--where d.active is true;

---------------------
-- Parcelles
---------------------
CREATE TABLE exports_agronomes_context_parcelles AS
  WITH plot_zone AS (
    SELECT z.plot, count(*) nombre_de_zones
    FROM zone z
    where z.active IS TRUE
    GROUP BY z.plot )

  SELECT
    ------ Informations generales
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    p.code parcelle_code, p.topiaid parcelle_id,
    p.name parcelle_nom, -- a anonymiser
    p.area parcelle_surface,
    rl.codepostal code_postal,
    rl.commune commune, 	-- a anonymiser
    p.comment parcelle_commentaire,
    pz.nombre_de_zones,
    ------ Informations sur le zonage de la parcelle
    CASE p.outofzoning
      WHEN true then 'oui'
      WHEN false then 'non'
      END parcelle_hors_zonage,

    (WITH liste_zonages AS (
      SELECT rpze.libelle_engagement_parcelle zonages
      FROM basicplot_plotzonings bpz
      JOIN refparcellezonageedi rpze ON rpze.topiaid = bpz.plotzonings
      WHERE bpz.basicplot = p.topiaid
      )
      SELECT string_agg (zonages, ' ; ') FROM liste_zonages
    ) AS zonages_parcelle,

    ------ Informations sur les equipements de la parcelle
    p.equipmentcomment AS equipement_commentaire,

    CASE p.drainage
      WHEN true then 'oui'
      WHEN false then 'non'
    END drainage,

    p.drainageyear AS drainage_annee_realisation,

    CASE p.frostprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_gel,

    p.frostprotectiontype AS protection_anti_gel_type,

    CASE p.hailprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_grele,

    CASE p.rainproofprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_pluie,

    CASE p.pestprotection
      WHEN true then 'oui'
      WHEN false then 'non'
    END protection_anti_insectes,

    p.otherequipment autre_equipement,

    ------ Informations sur le sol de la parcelle
    p.solcomment sol_commentaire,

    (-- Requete pour avoir le nom du sol
      SELECT rs.sol_nom
      FROM ground g
      JOIN refsolarvalis rs ON g.refsolarvalis = rs.topiaid
      WHERE p.ground = g.topiaid
    ) AS sol_nom_ref,

    (-- Requete pour avoir le nom de la texture de surface
      SELECT rstg.classes_texturales_gepaa
      FROM refsoltexturegeppa rstg
      WHERE rstg.topiaid = p.surfacetexture
    ) AS texture_surface,

    (-- Requete pour avoir le nom de la texture de surface
      SELECT rstg.classes_texturales_gepaa
      FROM refsoltexturegeppa rstg
      WHERE rstg.topiaid = p.subsoiltexture
    ) AS texture_sous_sol,

    p.solwaterph sol_ph,

    p.solstoniness pierrosite_moyenne,

    (-- Requete pour avoir la classe de profondeur du sol
      SELECT rspi.libelle_classe
      FROM refsolprofondeurindigo rspi
      WHERE rspi.topiaid = p.soldepth
    ) AS sol_profondeur,

    p.solmaxdepth sol_profondeur_max,

    p.solorganicmaterialpercent teneur_MO,

    CASE p.solhydromorphisms
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END hydromorphie,

    CASE p.sollimestone
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END calcaire,

    p.soltotallimestone AS proportion_calcaire_total,

    p.solactivelimestone AS proportion_calclaire_actif,

    CASE p.solbattance
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END battance,

    ------ Informations sur le type d'irrigation de la parcelle
    CASE p.irrigationsystem
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END irrigation,

    CASE p.fertigationsystem
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END fertirrigation,

    p. waterorigin AS origine_eau,
    p.irrigationsystemtype AS irrigation_type,
    p.pompenginetype AS irrigation_pompe,
    p.hosespositionning AS irrigation_position_tuyaux,
    ------ Autres informations generales
    CASE p. maxslope
      WHEN 'MORE_THAN_FIVE' THEN '> 5%'
      WHEN 'TWO_TO_FIVE' THEN '2 - 5%'
      WHEN 'ZERO' THEN '0%'
      WHEN 'ZERO_TO_TWO' THEN '0 - 2%'
    END pente,

    CASE p.waterflowdistance
      WHEN 'FIVE_TO_TEN' THEN '5 - 10 m'
      WHEN 'LESS_THAN_THREE' THEN '< 3 m'
      WHEN 'MORE_THAN_TEN' THEN '> 10 m'
      WHEN 'THREE_TO_FIVE' THEN '3 - 5 m'
    END distance_cours_eau,

    CASE p.bufferstrip
      WHEN 'BUFFER_STRIP_NEXT_TO_WATER_FLOW' THEN 'Bande tampon (5m) en bord de cours deau' -- On ne peut mettre ' dans le texte ?
      WHEN 'BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW' THEN 'Bande tampon pérenne enherbée dau moins 5m hors bord de cours deau'
      WHEN 'NONE' THEN 'Aucune'
    END bande_enherbee

  FROM plot p
  --JOIN domain d ON p.domain = d.topiaid
  --LEFT JOIN growingsystem gs ON p.growingsystem = gs.topiaid -- Lister aussi les parcelles qui ne sont pas reliees a un SdC ?
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  JOIN plot_zone pz ON pz.plot = p.topiaid
  LEFT JOIN refsolarvalis rs ON p.ground = rs.topiaid
  LEFT JOIN reflocation rl ON p.location = rl.topiaid
  WHERE gs.active IS TRUE
  AND p.active IS TRUE;

---------------------
-- Zones
---------------------
CREATE TABLE exports_agronomes_context_zones AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    z.plot parcelle_id,
    z.topiaid zone_id,
    z.name zone_nom,
    z.area zone_surface,
    z.latitude,
    z.longitude,
    z.type zone_type
  FROM zone z
  JOIN plot p ON z.plot = p.topiaid
  --JOIN domain d ON p.domain = d.topiaid
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active is TRUE;

--------------------
-- Materiels
--------------------
-- Presentation requete : 
	-- 2 sous requetes (concernent les domaines actifs) 
		-- CAS 1 : on recherche tous les outils declares dans les combinaisons d'outils
		-- CAS 2 : on recherche tous les tracteurs/automoteurs declares dans les combinaisons d'outils

CREATE TABLE exports_agronomes_context_materiels as
-- CAS 1
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    tc.topiaid combinaison_outil_id,
    tc.code combinaison_outil_code,
    tc.toolscouplingname combinaison_outils_nom,
    rm.topiaid materiel_ref_id,
    e.topiaid materiel_id,

    CASE e.materieleta
     WHEN true THEN 'oui'
     WHEN false THEN 'non'
    END materiel_ETA_CUMA,

    e.name materiel_nom,
    rm.typemateriel1 AS materiel_caracteristique1,
    rm.typemateriel2 AS materiel_caracteristique2,
    rm.typemateriel3 AS materiel_caracteristique3,
    rm.typemateriel4 AS materiel_caracteristique4,
    rm.uniteparan AS utilisation_annuelle,
    rm.unite AS utilisation_annuelle_unite,
    rm.chargesfixesparunitedevolumedetravailannuel coût_par_unite_travail_annuel
  FROM equipment e
  LEFT JOIN equipments_toolscoupling etc ON e.topiaid = etc.equipments
  LEFT JOIN toolscoupling tc ON etc.toolscoupling = tc.topiaid -- c'est a cette etape que l'on associe a une CO un tracteur, dans la table toolscoupling !
  --JOIN domain d ON e.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON e.domain = crit.domaine_id
  JOIN refmateriel rm ON e.refmateriel = rm.topiaid
  WHERE rm.topiadiscriminator IN ('fr.inra.agrosyst.api.entities.referential.RefMaterielOutilImpl','fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationImpl')
  --AND d.active IS TRUE
  UNION
  -- CAS 2
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    tc.topiaid AS combinaison_outil_id,
    tc.code AS combinaison_outil_code,
    tc.toolscouplingname AS combinaison_outils_nom,
    rm.topiaid AS materiel_ref_id,
    e.topiaid AS materiel_id,
    CASE e.materieleta
      WHEN true THEN 'oui'
      WHEN false THEN 'non'
    END materiel_ETA_CUMA,
    e.name AS materiel_nom,
    rm.typemateriel1 AS materiel_caracteristique1,
    rm.typemateriel2 AS materiel_caracteristique2,
    rm.typemateriel3 AS materiel_caracteristique3,
    rm.typemateriel4 AS materiel_caracteristique4,
    rm.uniteparan AS utilisation_annuelle,
    rm.unite AS utilisation_annuelle_unite,
    rm.chargesfixesparunitedevolumedetravailannuel AS coût_par_unite_travail_annuel
  FROM equipment e
  LEFT JOIN toolscoupling tc ON e.topiaid = tc.tractor
  --JOIN domain d ON e.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON e.domain = crit.domaine_id
  JOIN refmateriel rm ON e.refmateriel = rm.topiaid
  WHERE rm.topiadiscriminator IN (
    'fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurImpl',
    'fr.inra.agrosyst.api.entities.referential.RefMaterielTractionImpl');

--AND d.active IS TRUE;


-----------------
-- Dispositifs
-----------------
CREATE TABLE exports_agronomes_context_dispositifs AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    crit.dispositif_id,
    crit.dispositif_code,
    crit.dispositif_nom,
    crit.dispositif_type
  FROM exports_agronomes_criteres crit;

-----------------
-- SdC
-----------------

CREATE TABLE exports_agronomes_context_sdc AS

  WITH sdc AS (
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    crit.dispositif_code,
    crit.dispositif_id,
    crit.dispositif_type,
    gs.code AS sdc_code,
    gs.topiaid AS sdc_id,
    gs.name AS sdc_nom,
    gs.modality AS sdc_modalite_suivi_dephy,
    gs.dephynumber AS sdc_code_dephy,
    gs.description AS sdc_commentaire,
    CASE gs.validated
      WHEN true then 'oui'
      WHEN false then 'non'
    END sdc_valide,
    gs.sector AS sdc_filiere,
    gs.production AS sdc_type_production,
    rt.reference_label AS sdc_type_agriculture,
    gs.categorystrategy AS sdc_strategie_categorie,
    gs.affectedarearate AS sdc_part_SAU_domaine,
    gs.affectedworkforcerate AS sdc_part_MO_domaine,
    gs.domainstoolsusagerate AS sdc_part_materiel_domaine,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Couverts associés,  plantes de service'),
             'non') couverts_associes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Cultures intermédiaires a effet allélopathique ou biocide'),
             'non') ci_effet_allelo_ou_biocide,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Cultures intermédiaires attractives pour les auxiliaires'),
             'non') ci_attractives_pour_auxiliaires,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Cultures intermédiaires piège a nitrate'),
             'non') cipan,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Evitement par la date de semis - semis précoce'),
             'non') semis_precoce,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Evitement par la date de semis - semis tardif'),
             'non') semis_tardif,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Exportation des menu-pailles'),
             'non') exportation_menu_pailles,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Destruction de la litière des feuilles'),
             'non') destruction_litiere,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Efficience - Dose adaptée à la surface foliaire'),
             'non') dose_adaptee_surf_foliaire,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Filets anti-insectes'),
             'non') filet_anti_insectes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Taille des organes contaminés'),
      'non') taille_organes_contamines,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Taille limitant les risques sanitaires'),
      'non') taille_limitant_risques_sanitaires,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Adaptation de la densité - faible densité'),
      'non') faible_densite,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Adaptation de la densité - forte densité'),
      'non') forte_densite,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '34'),
      'non') faible_ecartement,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '33'),
      'non') fort_ecartement,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '36'),
      'non') ajustement_fertilisation,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '35'),
      'non') ajustement_irrigation,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Autre forme de désherbage'),
      'non') desherbage_autre_forme,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Cultures pièges'),
      'non') cultures_pieges,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Désherbage mécanique fréquent'),
      'non') desherbage_meca_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Désherbage mécanique occasionel'),
      'non') desherbage_meca_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Désherbage thermique'),
      'non') desherbage_thermique,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Adaptation de la lutte à la parcelle'),
      'non') adaptation_lutte_a_la_parcelle,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND id_trait = '20'),
      'non') optim_conditions_application,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose autres produits phytosanitaires'),
      'non') reduction_dose_autres_phyto,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose fongicides'),
      'non') reduction_dose_fongi,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose herbicides'),
      'non') reduction_dose_herbi,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Réduction de dose insecticides'),
      'non') reduction_dose_insec,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Traitements localisés en intra-parcellaire'),
      'non') traitement_localise,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation d''adjuvants'),
      'non') utilisation_adjuvants,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation de seuils pour les décisions de traitement'),
      'non') utilisation_seuils,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation de stimulateur de défense'),
      'non') utilisation_stim_defense,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Efficience - Utilisation d''outil d''aide à la décision pour les traitements'),
      'non') utilisation_oad,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés compétitives sur adventices'),
      'non') var_competitives_adventice,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles à la verse'),
      'non') var_peu_sensibles_verse,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles aux maladies'),
      'non') var_peu_sensibles_maladies,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Levier génétique - Variétés peu sensibles aux ravageurs'),
      'non') var_peu_sensibles_ravageurs,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Lutte biologique par confusion sexuelle'),
      'non') lutte_bio_confu_sexuelle,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Lutte biologique autre'),
      'non') lutte_bio_autre,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Toutes cultures - Mélange d''espèces ou de variétés'),
      'non') melange,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Gestion spécifique des résidus supports d''inoculum (broyage, enfouissement)'),
      'non') gestion_residus,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Monoculture ou rotation courte'),
      'non') monoculture_rotation_courte,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation avec cultures rustiques ou étouffantes (adventices)'),
      'non') rotation_cultures_rustiques,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation diversifiée équilibrée avec prairie temporaire'),
      'non') rotation_diversifiee_avec_pt,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation diversifiée équilibrée avec prairie temporaire'),
      'non') rotation_diversifiee_sans_pt,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Rotation diversifiée par l''introduction d''une culture dans une rotation courte'),
      'non') rotation_diversifiee_intro_une_culture,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Enherbement (espèces semées)'),
      'non') enherbement_seme,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures pérennes - Enherbement naturel maîtrisé'),
      'non') enherbement_naturel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Agroforesterie'),
      'non') agroforesterie,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Arbres isolés ou alignements en bordure de parcelle'),
      'non') arbres_bordure_parcelle,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Broyage des bordures'),
      'non') broyage_bordure,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Gestion des bordures de bois pour favoriser la biodiversité'),
      'non') gestion_bordure_de_bois,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Présence de haies anciennes'),
      'non') haies_anciennes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Présence de jeunes haies'),
      'non') haies_jeunes,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Proximité de bandes enherbées favorisant les auxiliaires'),
      'non') bandes_enherbees,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Proximité de bandes fleuries favorisant les auxiliaires'),
      'non') bandes_fleuries,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Proximité de bois et bosquets'),
      'non') bois_bosquet,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Décompactage  occasionnel'),
      'non') decompactage_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Décompactage  profond fréquent (fréquence > 50%)'),
      'non') decompactage_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Faux semis ponctuels'),
      'non') faux_semis_ponctuels,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Faux semis intensifs (travaux superficiels répétés spécifiques)'),
      'non') faux_semis_intensifs,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Labour occasionnel'),
      'non') labour_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Labour fréquent (fréquence > 50%)'),
      'non') labour_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Labour systématique (fréquence > 80%)'),
      'non') labour_systematique,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Semis direct sans travail du sol occasionnel'),
      'non') semis_direct_occasionnel,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Semis direct sans travail du sol systématique'),
      'non') semis_direct_systematique,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Strip till occasionnel'),
      'non') strip_till_occasionnel,


    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Strip till fréquent ou systématique'),
      'non') strip_till_frequent,

    COALESCE ((SELECT CASE WHEN nom_trait IS NOT NULL THEN 'oui' END
        FROM reftraitsdc rts2
        JOIN characteristics_growingsystem cgs2 ON rts2.topiaid = cgs2.characteristics
        WHERE cgs2.growingsystem = gs.topiaid
        AND nom_trait = 'Cultures assolées - Techniques culturales superficielles uniquement'),
      'non') tcs
	
  FROM growingsystem gs
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  LEFT JOIN reftypeagriculture rt ON gs.typeagriculture = rt.topiaid

  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  --and gs.active IS TRUE
  WHERE gs.active IS TRUE

  GROUP BY crit.domaine_id, crit.domaine_code, crit.domaine_nom, crit.domaine_campagne,
  crit.dispositif_id, crit.dispositif_code, crit.dispositif_type,
  gs.topiaid, gs.code, gs.name,  gs.dephynumber, gs.description,
  gs.validated, gs.sector,
  rt.reference_label,
  gs.categorystrategy,
  gs.affectedarearate

  ),
  reseaux_parents AS
    (SELECT n.topiaid, n.name reseau_ir, n.codeconventiondephy, string_agg(distinct n2.name,'|') reseau_it
      FROM network_parents np
      JOIN network n ON n.topiaid = np.network
      JOIN network n2 ON np.parents = n2.topiaid
      GROUP BY n.topiaid, n.name
    ),
  modes_commercialisation AS
    (SELECT dest.growingsystem,
      json_object_agg(
        dest.marketingdestination,
        CASE WHEN (dest.selectedforgs IS TRUE OR (dest.part IS NOT NULL AND dest.part>0)) THEN coalesce(dest.part::varchar(5),'OUI') ELSE 'NON' END
        ORDER BY dest.marketingdestination) modes_comm
    from (
      select mdo.growingsystem,  rfd.sector, rfd.marketingdestination, mdo.selectedforgs,	mdo.part
      from MarketingDestinationObjective mdo
      JOIN RefMarketingDestination rfd ON mdo.refmarketingdestination = rfd.topiaid
      ) dest
     GROUP BY dest.growingsystem)

    SELECT
      sdc.*,
      (select string_agg(distinct rp.reseau_ir, '|')
        FROM reseaux_parents rp
        JOIN growingsystem_networks gn ON rp.topiaid = gn.networks
        where gn.growingsystem = sdc.sdc_id
      ) reseaux_IR,

      (select string_agg(distinct rp.reseau_it, '|')
        FROM reseaux_parents rp
        JOIN growingsystem_networks gn ON rp.topiaid = gn.networks
        where gn.growingsystem = sdc.sdc_id
      ) reseaux_IT,

      (select string_agg(distinct rp.codeconventiondephy, '|')
        FROM reseaux_parents rp
        JOIN growingsystem_networks gn ON rp.topiaid = gn.networks
        where gn.growingsystem = sdc.sdc_id
      ) codes_convention_dephy,

      (select modes_comm
        FROM modes_commercialisation mc
        where mc.growingsystem = sdc.sdc_id
      ) modes_commercialisation
    FROM sdc;

----------------------------
-- Intrants_Realise
----------------------------
-- Requete a verifier
-- Modifiee le 10/07/2017 pour ajouter les perennes.
-- Modifiée le 02/12/2021 en multi requêtes
-- Correction(s) restante(s) :  
	-- Il manque les informations des cultures.
	-- Il manque les prix.

CREATE TEMPORARY TABLE ferti_min_orga AS (
    SELECT ai.topiaid intrant_id,
      NULL AS forme,
      ai.n n,
      ai.p2o5,
      ai.k2o,
      NULL AS bore,
      NULL AS calcium,
      NULL AS fer,
      NULL AS manganese,
      NULL AS molybdene,
      NULL AS mgo,
      NULL AS oxyde_de_sodium,
      NULL AS so3,
      NULL AS cuivre,
      NULL AS zinc
    FROM abstractinput ai
    WHERE ai.inputtype = 'EPANDAGES_ORGANIQUES'

    UNION ALL

    SELECT
      ai.topiaid intrant_id,
      rfm.forme,
      rfm.n,
      rfm.p2o5,
      rfm.k2o,
      rfm.bore,
      rfm.calcium,
      rfm.fer,
      rfm.manganese,
      rfm.molybdene,
      rfm.mgo,
      rfm.oxyde_de_sodium,
      rfm.so3,
      rfm.cuivre,
      rfm.zinc
    FROM abstractinput ai
    JOIN reffertiminunifa rfm ON rfm.topiaid = ai.mineralproduct
    WHERE ai.inputtype = 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
);

CREATE INDEX ferti_min_orga_intrant_id_idx on ferti_min_orga(intrant_id);

CREATE TABLE exports_agronomes_context_intrants_realise (
  domaine_code text,
  domaine_id character varying(255),
  domaine_nom text,
  domaine_campagne integer,
  sdc_id character varying(255),
  sdc_code text,
  sdc_nom text,
  parcelle_id character varying(255),
  parcelle_nom text,
  intervention_id character varying(255),
  intrant_id character varying(255),
  intrant_type text,
  intrant_ref_id character varying(255),
  intrant_ref_nom text,
  intrant_nom_utilisateur text,
  intrant_code_AMM text,
  dose double precision,
  volume_bouillie_hl double precision,
  unite text,
  biocontrole text,
  intrant_phyto_type text,
  intrant_phyto_cible_nom text,
  forme_ferti_min text,
  n double precision,
  p2o5 double precision,
  k2o double precision,
  bore double precision,
  calcium double precision,
  fer double precision,
  manganese double precision,
  molybdene double precision,
  mgo double precision,
  oxyde_de_sodium double precision,
  so3 double precision,
  cuivre double precision,
  zinc double precision,
  unite_teneur_ferti_orga character varying(255),
  ferti_effet_phyto_attendu text,
  prix double precision,
  prix_unite text
);

INSERT INTO exports_agronomes_context_intrants_realise (
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.topiaid sdc_id,
  gs.code sdc_code,
  gs.name sdc_nom,
  p.topiaid parcelle_id,
  p.name parcelle_nom,
  ei.topiaid intervention_id,
  ai.topiaid intrant_id,

  CASE ai.inputtype
    WHEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' THEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
    WHEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX' THEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
    WHEN 'EPANDAGES_ORGANIQUES' THEN 'EPANDAGES_ORGANIQUES'
    WHEN 'LUTTE_BIOLOGIQUE' THEN 'LUTTE_BIOLOGIQUE'
    WHEN 'SEMIS' THEN 'TRAITEMENT_SEMENCE'
    WHEN 'AUTRE' THEN 'AUTRE'
  END intrant_type,

  COALESCE (ai.phytoproduct, ai.organicproduct, ai.mineralproduct) intrant_ref_id,

  NULL AS intrant_ref_nom,

  ai.productname AS intrant_nom_utilisateur,

  null AS intrant_code_AMM,

  ai.qtavg AS dose,
  aa.boiledquantity AS volume_bouillie_hl,

  COALESCE (ai.phytoproductunit, ai.otherproductinputunit,ai.organicproductunit,ai.mineralproductunit) AS unite,

  null AS biocontrole,

  ai.producttype AS intrant_phyto_type,

  null AS intrant_phyto_cible_nom,

  null AS forme_ferti_min,
  null AS n,
  null AS p2o5,
  null AS k2o,
  null AS bore,
  null AS calcium,
  null AS fer,
  null AS manganese,
  null AS molybdene,
  null AS mgo,
  null AS oxyde_de_sodium,
  null AS so3,
  null AS cuivre,
  null AS zinc,
  null AS unite_teneur_ferti_orga,

  CASE  ai.phytoeffect
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
  END AS ferti_effet_phyto_attendu,

  NULL AS prix,
  NULL AS prix_unite

  FROM abstractinput ai
  INNER JOIN abstractaction aa ON COALESCE(organicfertilizersspreadingaction, mineralfertilizersspreadingaction, pesticidesspreadingaction, seedingaction, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, otheraction) = aa.topiaid
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
  INNER JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
  INNER JOIN zone z ON ecc.zone = z.topiaid
  INNER JOIN plot p ON z.plot = p.topiaid
  INNER JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active IS TRUE
);

INSERT INTO exports_agronomes_context_intrants_realise (
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    gs.code sdc_code,
    gs.name sdc_nom,
    p.topiaid parcelle_id,
    p.name parcelle_nom,
    ei.topiaid intervention_id, ai.topiaid intrant_id,

  CASE ai.inputtype
    WHEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' THEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
    WHEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX' THEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
    WHEN 'EPANDAGES_ORGANIQUES' THEN 'EPANDAGES_ORGANIQUES'
    WHEN 'LUTTE_BIOLOGIQUE' THEN 'LUTTE_BIOLOGIQUE'
    WHEN 'SEMIS' THEN 'TRAITEMENT_SEMENCE'
    WHEN 'AUTRE' THEN 'AUTRE'
  END intrant_type,

  COALESCE (ai.phytoproduct, ai.organicproduct, ai.mineralproduct) intrant_ref_id,

  NULL AS intrant_ref_nom,

  ai.productname AS intrant_nom_utilisateur,

  null AS intrant_code_AMM,

  ai.qtavg AS dose,
  aa.boiledquantity AS volume_bouillie_hl,

  COALESCE (ai.phytoproductunit, ai.otherproductinputunit,ai.organicproductunit,ai.mineralproductunit) AS unite,

  null AS biocontrole,

  ai.producttype AS intrant_phyto_type,

  null AS intrant_phyto_cible_nom,

  null AS forme_ferti_min, null  AS n, null AS p2o5, null AS k2o, null AS bore, null AS calcium, null AS fer, null AS manganese,
  null AS molybdene, null AS mgo, null AS oxyde_de_sodium, null AS so3, null AS cuivre, null AS zinc,

  null AS unite_teneur_ferti_orga,

  CASE  ai.phytoeffect
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
  END AS ferti_effet_phyto_attendu,

  NULL AS prix,
  NULL AS prix_unite

  FROM abstractinput ai
  INNER JOIN abstractaction aa ON COALESCE(organicfertilizersspreadingaction, mineralfertilizersspreadingaction, pesticidesspreadingaction, seedingaction, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, otheraction) = aa.topiaid

  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
  JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
  JOIN zone z ON epcc.zone = z.topiaid
  JOIN plot p ON z.plot = p.topiaid
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active IS TRUE
);

CREATE INDEX exports_agronomes_intrants_realise_inputid_idx ON exports_agronomes_context_intrants_realise (intrant_id);

-- intrant_ref_nom ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'SEMIS')
-- intrant_code_AMM
-- biocontrole
UPDATE exports_agronomes_context_intrants_realise ai SET
  intrant_ref_nom = ratp.nom_produit,
  intrant_code_AMM = ratp.code_AMM,
  biocontrole = (SELECT CASE ratp.nodu WHEN true THEN 'oui' WHEN false THEN 'non' END)
FROM refactatraitementsproduit ratp
WHERE ratp.topiaid = ai.intrant_ref_id
AND ai.intrant_type IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'SEMIS');

-- intrant_phyto_cible_nom
UPDATE exports_agronomes_context_intrants_realise ai SET intrant_phyto_cible_nom = (
    SELECT string_agg(
      (SELECT rnedi.reference_label FROM RefNuisibleEDI rnedi WHERE rnedi.topiaid = ppt.target AND ppt.phytoproductinput = ai.intrant_id  UNION
       SELECT ra.adventice FROM RefAdventice ra WHERE ra.topiaid = ppt.target AND ppt.phytoproductinput = ai.intrant_id ),'|')
       FROM phytoproducttarget ppt
       WHERE ppt.phytoproductinput = ai.intrant_id
	   AND ppt0.topiaid = ppt.topiaid
  )
  FROM phytoproducttarget ppt0
  WHERE ppt0.phytoproductinput = ai.intrant_id
  AND ai.intrant_type IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'SEMIS');

-- intrant_ref_nom (APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
UPDATE exports_agronomes_context_intrants_realise ai SET intrant_ref_nom = rfmu.type_produit
  FROM reffertiminunifa rfmu
  WHERE ai.intrant_type = 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
  AND rfmu.topiaid = ai.intrant_ref_id;

--unite_teneur_ferti_orga
--intrant_ref_nom (EPANDAGES_ORGANIQUES)
UPDATE exports_agronomes_context_intrants_realise ai SET
  unite_teneur_ferti_orga = rfo.unite_teneur_ferti_orga,
  intrant_ref_nom = rfo.libelle
  FROM reffertiorga rfo WHERE ai.intrant_type = 'EPANDAGES_ORGANIQUES'
  AND rfo.topiaid = ai.intrant_ref_id;

UPDATE exports_agronomes_context_intrants_realise ai SET
  forme_ferti_min = fmo.forme,
  n = fmo.n,
  p2o5 = fmo.p2o5,
  k2o = fmo.k2o,
  bore = fmo.bore,
  calcium = fmo.calcium,
  fer = fmo.fer,
  manganese = fmo.manganese,
  molybdene = fmo.molybdene,
  mgo = fmo.mgo,
  oxyde_de_sodium = fmo.oxyde_de_sodium,
  so3 = fmo.so3,
  cuivre = fmo.cuivre,
  zinc = fmo.zinc
  FROM ferti_min_orga fmo
  WHERE fmo.intrant_id = ai.intrant_id
  AND ai.intrant_type IN ('EPANDAGES_ORGANIQUES', 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX');


-------------------------------
-- Intrants_Synthetise
-------------------------------
-- Requete a verifier
-- Modifiee le 10/07/2017 pour ajouter les perennes et le filtre sur les reseaux.
-- Correction(s) restante(s) :  
	-- Il manque les informations des cultures.
	-- Il manque les prix.

CREATE TABLE exports_agronomes_context_intrants_synthetise (
  domaine_code text,
  domaine_id character varying(255),
  domaine_nom text,
  domaine_campagne integer,
  sdc_id character varying(255),
  sdc_code text,
  sdc_nom text,
  systeme_synthetise_id character varying(255),
  intervention_id character varying(255),
  intrant_id character varying(255),
  intrant_type text,
  intrant_ref_id character varying(255),
  intrant_ref_nom text,
  intrant_nom_utilisateur text,
  intrant_code_AMM text,
  dose double precision,
  volume_bouillie_hl double precision,
  unite text,
  biocontrole text,
  intrant_phyto_type text,
  intrant_phyto_cible_nom text,
  forme_ferti_min text,
  n double precision,
  p2o5 double precision,
  k2o double precision,
  bore double precision,
  calcium double precision,
  fer double precision,
  manganese double precision,
  molybdene double precision,
  mgo double precision,
  oxyde_de_sodium double precision,
  so3 double precision,
  cuivre double precision,
  zinc double precision,
  unite_teneur_ferti_orga character varying(255),
  ferti_effet_phyto_attendu text,
  prix double precision,
  prix_unite text
);

INSERT INTO exports_agronomes_context_intrants_synthetise (
  SELECT
  crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid sdc_id, gs.code sdc_code, gs.name sdc_nom,
  ps.topiaid systeme_synthetise_id,
  pi.topiaid intervention_id,
  ai.topiaid intrant_id,

  CASE ai.inputtype
    WHEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' THEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
    WHEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX' THEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
    WHEN 'EPANDAGES_ORGANIQUES' THEN 'EPANDAGES_ORGANIQUES'
    WHEN 'LUTTE_BIOLOGIQUE' THEN 'LUTTE_BIOLOGIQUE'
    WHEN 'SEMIS' THEN 'TRAITEMENT_SEMENCE'
    WHEN 'AUTRE' THEN 'AUTRE'
  END intrant_type,

  COALESCE (ai.phytoproduct, ai.organicproduct, ai.mineralproduct) intrant_ref_id,

  NULL AS intrant_ref_nom,

  ai.productname AS intrant_nom_utilisateur,

  null AS intrant_code_AMM,

  ai.qtavg AS dose,
  aa.boiledquantity AS volume_bouillie_hl,

  COALESCE (ai.phytoproductunit, ai.otherproductinputunit,ai.organicproductunit,ai.mineralproductunit) AS unite,

  null AS biocontrole,

  ai.producttype AS intrant_phyto_type,

  null AS intrant_phyto_cible_nom,

  null AS forme_ferti_min, null AS n, null AS p2o5, null AS k2o, null AS bore, null AS calcium, null AS fer, null AS manganese,
  null AS molybdene, null AS mgo, null AS oxyde_de_sodium, null AS so3, null AS cuivre, null AS zinc,

  null AS unite_teneur_ferti_orga,

  CASE  ai.phytoeffect
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
  END AS ferti_effet_phyto_attendu,

  NULL AS prix,
  NULL AS prix_unite

  FROM ferti_min_orga fmo
  RIGHT JOIN abstractinput ai ON ai.topiaid = fmo.intrant_id
  JOIN abstractaction aa ON COALESCE(organicfertilizersspreadingaction, mineralfertilizersspreadingaction, pesticidesspreadingaction, seedingaction, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, otheraction) = aa.topiaid
  JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
);

INSERT INTO exports_agronomes_context_intrants_synthetise (
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.topiaid sdc_id,
  gs.code AS sdc_code,
  gs.name AS sdc_nom,
  ps.topiaid AS systeme_synthetise_id,
  pi.topiaid AS intervention_id,
  ai.topiaid AS intrant_id,

  CASE ai.inputtype
    WHEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' THEN 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
    WHEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX' THEN 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
    WHEN 'EPANDAGES_ORGANIQUES' THEN 'EPANDAGES_ORGANIQUES'
    WHEN 'LUTTE_BIOLOGIQUE' THEN 'LUTTE_BIOLOGIQUE'
    WHEN 'SEMIS' THEN 'TRAITEMENT_SEMENCE'
    WHEN 'AUTRE' THEN 'AUTRE'
  END intrant_type,

  COALESCE (ai.phytoproduct, ai.organicproduct, ai.mineralproduct) AS intrant_ref_id,

  NULL AS intrant_ref_nom,

  ai.productname AS intrant_nom_utilisateur,

  null AS intrant_code_AMM,

  ai.qtavg AS dose,
  aa.boiledquantity AS volume_bouillie_hl,

  COALESCE (ai.phytoproductunit, ai.otherproductinputunit,ai.organicproductunit,ai.mineralproductunit) AS unite,

  null AS biocontrole,

  ai.producttype AS intrant_phyto_type,

  null AS intrant_phyto_cible_nom,

  null AS forme_ferti_min, null AS n, null AS p2o5, null AS k2o, null AS bore, null AS calcium, null AS fer, null AS manganese,
  null AS molybdene, null AS mgo, null AS oxyde_de_sodium, null AS so3, null AS cuivre, null AS zinc,

  null AS unite_teneur_ferti_orga,

  CASE  ai.phytoeffect
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
  END AS ferti_effet_phyto_attendu,

  NULL AS prix,
  NULL AS prix_unite

  FROM ferti_min_orga fmo
  RIGHT JOIN abstractinput ai ON ai.topiaid = fmo.intrant_id
  JOIN abstractaction aa ON COALESCE(organicfertilizersspreadingaction, mineralfertilizersspreadingaction, pesticidesspreadingaction, seedingaction, biologicalcontrolaction, harvestingaction, irrigationaction, maintenancepruningvinesaction, otheraction) = aa.topiaid
  JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
);

CREATE INDEX exports_agronomes_context_intrants_synthetise_inputid_idx ON exports_agronomes_context_intrants_synthetise (intrant_id);

-- intrant_ref_nom ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'SEMIS')
-- intrant_code_AMM
-- biocontrole
UPDATE exports_agronomes_context_intrants_synthetise ai SET
    intrant_ref_nom = ratp.nom_produit,
    intrant_code_AMM = ratp.code_AMM,
    biocontrole = (SELECT CASE ratp.nodu WHEN true THEN 'oui' WHEN false THEN 'non' END)
  FROM refactatraitementsproduit ratp
  WHERE ratp.topiaid = ai.intrant_ref_id
  AND ai.intrant_type IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'SEMIS');

-- intrant_phyto_cible_nom
UPDATE exports_agronomes_context_intrants_synthetise ai SET intrant_phyto_cible_nom = (
    SELECT string_agg(
      (SELECT rnedi.reference_label FROM RefNuisibleEDI rnedi WHERE rnedi.topiaid = ppt.target AND ppt.phytoproductinput = ai.intrant_id  UNION
       SELECT ra.adventice FROM RefAdventice ra WHERE ra.topiaid = ppt.target AND ppt.phytoproductinput = ai.intrant_id ),'|')
       FROM phytoproducttarget ppt
       WHERE ppt.phytoproductinput = ai.intrant_id
	   AND ppt0.topiaid = ppt.topiaid
  )
  FROM phytoproducttarget ppt0
  WHERE ppt0.phytoproductinput = ai.intrant_id
  AND ai.intrant_type IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'SEMIS');

-- intrant_ref_nom (APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
UPDATE exports_agronomes_context_intrants_synthetise ai SET intrant_ref_nom = rfmu.type_produit
  FROM reffertiminunifa rfmu
  WHERE ai.intrant_type = 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'
  AND rfmu.topiaid = ai.intrant_ref_id;

--unite_teneur_ferti_orga
--intrant_ref_nom (EPANDAGES_ORGANIQUES)
UPDATE exports_agronomes_context_intrants_synthetise ai SET unite_teneur_ferti_orga = rfo.unite_teneur_ferti_orga,
  intrant_ref_nom = rfo.libelle
  FROM reffertiorga rfo WHERE ai.intrant_type = 'EPANDAGES_ORGANIQUES'
  AND rfo.topiaid = ai.intrant_ref_id;

UPDATE exports_agronomes_context_intrants_synthetise ai SET
  forme_ferti_min = fmo.forme,
  n = fmo.n,
  p2o5 = fmo.p2o5,
  k2o = fmo.k2o,
  bore = fmo.bore,
  calcium = fmo.calcium,
  fer = fmo.fer,
  manganese = fmo.manganese,
  molybdene = fmo.molybdene,
  mgo = fmo.mgo,
  oxyde_de_sodium = fmo.oxyde_de_sodium,
  so3 = fmo.so3,
  cuivre = fmo.cuivre,
  zinc = fmo.zinc
  FROM ferti_min_orga fmo
  WHERE fmo.intrant_id = ai.intrant_id
  AND ai.intrant_type IN ('EPANDAGES_ORGANIQUES', 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX');
DROP TABLE IF EXISTS ferti_min_orga;

--------------------------
-- Parcelle_Type
--------------------------
CREATE TABLE exports_agronomes_context_parcelle_type AS
  SELECT
    crit.domaine_id, crit.domaine_code, crit.domaine_nom, crit.domaine_campagne,
    gs.topiaid sdc_id,
    ps.topiaid systeme_synthetise_id, ps.name systeme_synthetise_nom, ps.campaigns systeme_synthetise_campagnes,
    pp.topiaid parcelle_type_id, pp.name parcelle_type_nom,
    pp.area parcelle_type_surface, pp.comment parcelle_type_commentaire,
    pp.outofzoning parcelle_type_hors_zonage, pp.equipmentcomment equipement_commentaire, pp.drainage drainage,
    pp.drainageyear drainage_annee_realisation,
    pp.frostprotection protection_anti_gel, pp.frostprotectiontype protection_anti_gel_type, pp.hailprotection protection_anti_grele,
    pp.rainproofprotection protection_anti_pluie, pp.pestprotection protection_anti_insectes,
    pp.otherequipment autre_equipement, pp.solcomment sol_commentaire,
    pp.surfacetexture texture_surface,
    pp.subsoiltexture texture_sous_sol, pp.solwaterph sol_ph,
    pp.solstoniness pierrosite_moyenne, pp.soldepth sol_profondeur,
    pp.solmaxdepth sol_profondeur_max,
    pp.solorganicmaterialpercent teneur_MO,
    pp.solhydromorphisms hydromorphie,
    pp.sollimestone calcaire,
    pp.soltotallimestone proportion_calcaire_total,
    pp.solactivelimestone proportion_calclaire_actif,
    pp. solbattance battance,
    pp.irrigationsystem irrigation, pp.fertigationsystem fertirrigation, pp. waterorigin origine_eau,
    pp.irrigationsystemtype irrigation_type,
    pp.pompenginetype irrigation_pompe,
    pp.hosespositionning irrigation_position_tuyaux,
    pp. maxslope pente, pp.waterflowdistance distance_cours_eau,
    pp.bufferstrip bande_enherbee
  FROM practicedplot pp
  JOIN practicedsystem ps ON ps.topiaid = pp.practicedsystem
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  AND pp.active IS TRUE

  ORDER BY crit.domaine_id, gs.topiaid;

-------------------------------------------
-- Recolte_realise  >>> requete a verifier
-------------------------------------------
-- Requete "Recoltes" divisee en deux (realise d'une part, synthetise d'autre part), pour pouvoir y appliquer un filtre "reseau"

CREATE TABLE exports_agronomes_context_recolte_realise AS

  WITH recolte AS (

    SELECT
      crit.domaine_code,
      crit.domaine_id,
      crit.domaine_nom,
      crit.domaine_campagne,
      gs.topiaid sdc_id,
      gs.name sdc_nom,
      p.topiaid parcelle_id,
      p.name parcelle_nom,
      z.topiaid zone_id,
      z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_moyen,
      SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_median,
      SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_min,
      SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_max,
      hav.yealdunit unite,
      SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
      SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
      SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
      FROM exp_effective_hav hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
      JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
      JOIN zone z ON ecc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE
      AND hav.crop_area > 0

      GROUP BY
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

      UNION ALL

      SELECT
      crit.domaine_code,
      crit.domaine_id,
      crit.domaine_nom,
      crit.domaine_campagne,
      gs.topiaid sdc_id,
      gs.name sdc_nom,
      p.topiaid parcelle_id,
      p.name parcelle_nom,
      z.topiaid zone_id,
      z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_moyen,
      SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_median,
      SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_min,
      SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_max,
      hav.yealdunit unite,
      SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
      SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
      SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
      FROM exp_effective_hav hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
      JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
      JOIN zone z ON epcc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE
      AND hav.crop_area > 0

      GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

      UNION ALL

      SELECT
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid sdc_id, gs.name sdc_nom,
      p.topiaid parcelle_id, p.name parcelle_nom, z.topiaid zone_id, z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM(hav.yealdaverage::NUMERIC) rendement_moyen,
      SUM(hav.yealdmedian::NUMERIC) rendement_median,
      SUM(hav.yealdmin::NUMERIC) rendement_min,
      SUM(hav.yealdmax::NUMERIC) rendement_max,
      hav.yealdunit unite,
      SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
      SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
      SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
      FROM harvestingactionvalorisation hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
      JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
      JOIN zone z ON ecc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE

      GROUP BY
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

      UNION ALL

      SELECT
      crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid sdc_id, gs.name sdc_nom,
      p.topiaid parcelle_id, p.name parcelle_nom, z.topiaid zone_id, z.name zone_nom,
      aa.effectiveintervention intervention_id,
      aa.topiaid action_id,
      riate.reference_label type_action,
      hav.destination destination_id,
      rd.destination destination_nom,
      SUM(hav.yealdaverage::NUMERIC) rendement_moyen,
      SUM(hav.yealdmedian::NUMERIC) rendement_median,
      SUM(hav.yealdmin::NUMERIC) rendement_min,
      SUM(hav.yealdmax::NUMERIC) rendement_max,
      hav.yealdunit unite,
      SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
      SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
      SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
      FROM harvestingactionvalorisation hav
      JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
      JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
      JOIN refdestination rd ON rd.topiaid = hav.destination
      JOIN effectiveintervention ei ON aa.effectiveintervention = ei.topiaid
      JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
      JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
      JOIN zone z ON epcc.zone = z.topiaid
      JOIN plot p ON z.plot = p.topiaid
      JOIN growingsystem gs ON p.growingsystem = gs.topiaid
      JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
      WHERE gs.active IS TRUE
      AND p.active IS TRUE
      AND z.active IS TRUE

      GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
      gs.topiaid, gs.name,
      p.topiaid, p.name, z.topiaid, z.name, aa.effectiveintervention, aa.topiaid,
      riate.reference_label, hav.destination,	rd.destination, hav.yealdunit
  )
SELECT * FROM recolte;

----------------------------------------------
-- Recolte_synthetise >>> requete a verifier
----------------------------------------------
CREATE TABLE exports_agronomes_context_recolte_synthetise AS

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    gs.name sdc_nom,
    ps.topiaid systeme_synthetise_id,
    ps.name systeme_synthetise_nom,
    ps.campaigns systeme_synthetise_campagnes,
    aa.practicedintervention intervention_id,
    aa.topiaid action_id,
    riate.reference_label type_action,
    hav.destination destination_id,
    rd.destination destination_nom,
    SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_moyen,
    SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_median,
    SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_min,
    SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) rendement_max,
    hav.yealdunit unite,
    SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
    SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
    SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
  FROM exp_practiced_hav hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  AND hav.crop_area > 0

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

  UNION ALL

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid AS sdc_id,
    gs.name AS sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    aa.practicedintervention AS intervention_id,
    aa.topiaid AS action_id,
    riate.reference_label AS type_action,
    hav.destination AS destination_id,
    rd.destination AS destination_nom,
    SUM((hav.yealdaverage::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_moyen,
    SUM((hav.yealdmedian::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_median,
    SUM((hav.yealdmin::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_min,
    SUM((hav.yealdmax::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS rendement_max,
    hav.yealdunit AS unite,
    SUM((hav.salespercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_commercialise,
    SUM((hav.selfConsumedPersent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_autoconsomme,
    SUM((hav.noValorisationPercent::NUMERIC * hav.species_area::NUMERIC * (100.0/hav.crop_area::NUMERIC))/100.0) AS perc_non_valorise
  FROM exp_practiced_hav AS hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS FALSE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  AND hav.crop_area > 0

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

  UNION ALL

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    gs.name AS sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    aa.practicedintervention AS intervention_id,
    aa.topiaid AS action_id,
    riate.reference_label AS type_action,
    hav.destination AS destination_id,
    rd.destination AS destination_nom,
    SUM(hav.yealdaverage::NUMERIC) AS rendement_moyen,
    SUM(hav.yealdmedian::NUMERIC) AS rendement_median,
    SUM(hav.yealdmin::NUMERIC) AS rendement_min,
    SUM(hav.yealdmax::NUMERIC) AS rendement_max,
    hav.yealdunit AS unite,
    SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
    SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
    SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
  FROM harvestingactionvalorisation hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit

  UNION ALL

  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid AS sdc_id,
    gs.name AS sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    aa.practicedintervention AS intervention_id,
    aa.topiaid AS action_id,
    riate.reference_label AS type_action,
    hav.destination AS destination_id,
    rd.destination AS destination_nom,
    SUM(hav.yealdaverage::NUMERIC) AS rendement_moyen,
    SUM(hav.yealdmedian::NUMERIC) AS rendement_median,
    SUM(hav.yealdmin::NUMERIC) AS rendement_min,
    SUM(hav.yealdmax::NUMERIC) AS rendement_max,
    hav.yealdunit AS unite,
    SUM(hav.salespercent::NUMERIC) AS perc_commercialise,
    SUM(hav.selfConsumedPersent::NUMERIC) AS perc_autoconsomme,
    SUM(hav.noValorisationPercent::NUMERIC) AS perc_non_valorise
  FROM harvestingactionvalorisation hav
  JOIN perf_harvestingaction aa ON hav.harvestingaction = aa.topiaid AND aa.mixspecies IS TRUE
  JOIN refinterventionagrosysttravailedi riate ON aa.mainaction = riate.topiaid
  JOIN refdestination rd ON rd.topiaid = hav.destination
  JOIN practicedintervention pi ON aa.practicedintervention = pi.topiaid
  JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE

  GROUP BY crit.domaine_code, crit.domaine_id, crit.domaine_nom, crit.domaine_campagne,
  gs.topiaid, gs.name,
  ps.topiaid, ps.name, ps.campaigns, aa.practicedintervention, aa.topiaid,
  riate.reference_label, hav.destination,	rd.destination, hav.yealdunit;

------------
-- Cultures
------------
-- Presentation requete : Liste des especes et varietes de chaque culture
	-- 3 sous requetes (concernent les domaines actifs) 
		-- CAS 1 : especes (dont l'espece EDI n'est pas la vigne) associees a une variete
		-- CAS 2 : especes (dont espece EDI = vigne) associes a un cepage
		-- CAS 3 : especes non associees a une variete
		-- CAS 4 : cultures sans especes EDI

CREATE TABLE exports_agronomes_context_cultures AS

  -- CAS 1 : especes (dont l'espece EDI n'est pas la vigne) des cultures associees a une variete
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- cps.variety AS variete_id,
    rvg.denomination AS variete_nom
  FROM croppingplanentry cpe
  JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
  JOIN refespece re ON cps.species = re.topiaid
  JOIN refvarietegeves rvg ON cps.variety = rvg.topiaid
  --JOIN domain d ON cpe.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE

  UNION

  -- CAS 2 : especes (dont espece EDI = vigne) associes a un cepage
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- cps.variety AS variete_id,
    rvpg.variete AS variete_nom
  FROM croppingplanentry cpe
  JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
  JOIN refespece re ON cps.species = re.topiaid
  JOIN refvarieteplantgrape rvpg ON cps.variety = rvpg.topiaid
  --JOIN domain d ON cpe.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE

  UNION

  -- CAS 3 : especes non associees a une variete
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- null AS variete_id,
    null AS variete_nom
  FROM croppingplanentry cpe
  JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry -- on part sur un join plutot qu'un left join car on cherche a avoir uniquement les cultures qui ont des especes
  JOIN refespece re ON cps.species = re.topiaid
  --JOIN domain d ON cpe.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE
  WHERE cps.variety IS NULL

  UNION

  -- CAS 4 : cultures sans especes EDI
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    cpe.code culture_code,
    cpe.topiaid culture_id,
    cpe.name culture_nom,
    CASE cpe.type
      WHEN 'MAIN' then 'PRINCIPALE'
      WHEN 'CATCH' then 'DEROBEE'
      WHEN 'INTERMEDIATE' then 'INTERMEDIAIRE'
    END culture_type,
    CASE cpe.mixspecies
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_especes,
    CASE cpe.mixvariety
      WHEN true then 'oui'
      WHEN false then 'non'
    END culture_melange_varietes,
    cps.topiaid AS espece_id,
    cps.code AS espece_code,
    -- cps.species AS espece_EDI_id,
    re.code_espece_botanique AS code_EDI_espece_botanique,
    re.code_qualifiant_aee AS code_EDI_qualifiant,
    re.code_type_saisonnier_aee AS code_EDI_type_saisonnier,
    re.code_destination_aee AS code_EDI_destination,
    re.libelle_espece_botanique AS espece_EDI_nom_botanique,
    re.libelle_qualifiant_aee AS espece_EDI_qualifiant,
    re.libelle_type_saisonnier_aee AS espece_EDI_type_saisonnier,
    re.libelle_destination_aee AS espece_EDI_destination,
    -- null AS variete_id,
    null AS variete_nom
  FROM croppingplanentry cpe
  --LEFT JOIN domain d ON cpe.domain = d.topiaid
  LEFT JOIN croppingplanspecies cps ON cpe.topiaid = cps.croppingplanentry
  LEFT JOIN refespece re ON cps.species = re.topiaid
  JOIN exports_agronomes_criteres crit ON cpe.domain = crit.domaine_id
  --WHERE d.active IS TRUE
  WHERE cps.topiaid is null; -- pour avoir les cultures qui n'ont pas d'especes


--------------------------------------
-- Caracteristiques_Perennes_Realise
--------------------------------------
CREATE TABLE exports_agronomes_context_perennes_realise AS

SELECT 
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.code sdc_code,
  gs.topiaid sdc_id,
  gs.name sdc_nom,
  p.topiaid parcelle_id,
  p.name parcelle_nom,
  z.topiaid zone_id,
  z.name zone_nom,
  epcc.croppingplanentry culture_id,
  cpe.name culture_nom,

  (WITH liste_especes AS
    (SELECT concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')) re2libelle
    FROM croppingplanspecies cps
    JOIN refespece re2 ON cps.species = re2.topiaid
    WHERE cps.croppingplanentry = cpe.topiaid)
  SELECT string_agg(le.re2libelle,' ; ')
  FROM liste_especes le) culture_especes_EDI,
  eccp.topiaid phase_id,
  eccp.type phase,
  eccp.duration duree_phase_ans,
  epcc.plantingyear annee_plantation,
  epcc.plantinginterfurrow inter_rang,
  epcc.plantingspacing espacement_sur_le_rang,
  epcc.plantingdensity densite_plantation,
  epcc.orchardfrutalform forme_fruitiere_verger,
  epcc.foliageheight hauteur_frondaison,
  epcc.foliagethickness epaisseur_frondaison,
  epcc.vinefrutalform forme_fruitiere_vigne,
  epcc.orientation orientation_rangs,
  epcc.plantingdeathrate taux_mortalite_plantation,
  epcc.plantingdeathratemeasureyear annee_mesure_taux_mortalite,
  epcc.weedtype type_enherbement,
  epcc.othercharacteristics couvert_vegetal_commentaire,
  epcc.pollinator pollinisateurs,
  epcc.pollinatorpercent pourcentage_de_pollinisateurs,
  epcc.pollinatorspreadmode mode_repartition_pollinisateurs

FROM effectivecropcyclephase eccp 
JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
JOIN croppingplanentry cpe ON epcc.croppingplanentry = cpe.topiaid
JOIN zone z ON epcc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN growingsystem gs ON p.growingsystem = gs.topiaid
--JOIN growingplan gp ON gs.growingplan = gp.topiaid
--JOIN domain d ON gp.domain = d.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
--WHERE d.active IS TRUE
--and gp.active IS TRUE
WHERE gs.active IS TRUE
AND p.active IS TRUE
AND z.active IS TRUE;
	
-----------------------------------------
-- Caracteristiques_Perennes_Synthetise
-----------------------------------------
CREATE TABLE exports_agronomes_context_perennes_synthetise AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.code sdc_code,
    gs.topiaid sdc_id,
    gs.name sdc_nom,
    ps.topiaid systeme_synthetise_id,
    ps.name systeme_synthetise_nom,
    ps.campaigns systeme_synthetise_campagnes,
    CASE ps.validated
      WHEN true then 'oui'
      WHEN false then 'non'
    END systeme_synthetise_validation,
    ppcc.croppingplanentrycode culture_code,
    pccp.topiaid phase_id,
    pccp.type phase,
    pccp.duration duree_phase_ans,
    ppcc.soloccupationpercent pourcentage_sole_sdc,
    ppcc.plantingyear annee_plantation,
    ppcc.plantinginterfurrow inter_rang,
    ppcc.plantingspacing espacement_sur_le_rang,
    ppcc.plantingdensity densite_plantation,
    ppcc.orchardfrutalform forme_fruitiere_verger,
    ppcc.foliageheight hauteur_frondaison,
    ppcc.foliagethickness epaisseur_frondaison,
    ppcc.vinefrutalform forme_fruitiere_vigne,
    ppcc.orientation orientation_rangs,
    ppcc.plantingdeathrate taux_mortalite_plantation,
    ppcc.plantingdeathratemeasureyear annee_mesure_taux_mortalite,
    ppcc.weedtype type_enherbement,
    ppcc.othercharacteristics couvert_vegetal_commentaire,
    ppcc.pollinator pollinisateurs,
    ppcc.pollinatorpercent pourcentage_de_pollinisateurs,
    ppcc.pollinatorspreadmode mode_repartition_pollinisateurs
  FROM practicedcropcyclephase pccp
  JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
  JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE;

---------------------------------
-- Rotation_assolees_synthetise
---------------------------------
CREATE TABLE exports_agronomes_context_rotation_assolees_synthetise AS
  SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.code sdc_code,
    gs.topiaid sdc_id,
    gs.name sdc_nom,
    ps.topiaid AS systeme_synthetise_id,
    ps.name AS systeme_synthetise_nom,
    ps.campaigns AS systeme_synthetise_campagnes,
    ps.validated AS systeme_synthetise_validation,
    pccc.topiaid AS culture_precedent_rang_id, -- topiaid de la connexion
    pccnt.topiaid AS culture_rotation_id,
    (pccnt.rank+1)::text AS culture_rang,
    (pccnt.y+1)::text AS culture_indicateur_branche,
    pccnt.croppingplanentrycode AS culture_code,
    (-- Requete donnant pour un code culture et les campagnes du synthetise, le nom d'une culture
      SELECT max(cpe2.name) AS name
      FROM croppingplanentry cpe2
      JOIN exports_agronomes_criteres crit1 ON cpe2.domain = crit1.domaine_id
      WHERE cpe2.code = pccnt.croppingplanentrycode
      AND crit1.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
      --AND d2.active IS TRUE
      GROUP BY cpe2.code
    ) AS culture_nom,
    CASE pccnt.endcycle	WHEN true then 'OUI' ELSE 'NON' END fin_rotation,
    CASE pccnt.samecampaignaspreviousnode WHEN true then 'OUI' ELSE 'NON' END meme_campagne_culture_precedente,
    CASE pccc.notusedforthiscampaign WHEN true then 'OUI' ELSE 'NON' END culture_absente,
    (WITH source_species1 AS (
      -- Requete donnant pour un code de culture et les campagnes du synthetise, la liste des especes concernees, sans doublons
      SELECT max(concat_ws(' ', NULLIF(re1.libelle_espece_botanique,''), NULLIF(re1.libelle_qualifiant_aee,''),
      NULLIF(re1.libelle_type_saisonnier_aee,''), NULLIF(re1.libelle_destination_aee,''))) nom_sp
      FROM croppingplanentry cpe1
      JOIN croppingplanspecies cps1 ON cpe1.topiaid = cps1.croppingplanentry
      JOIN refespece re1 ON cps1.species = re1.topiaid
      JOIN exports_agronomes_criteres crit2 ON cpe1.domain = crit2.domaine_id
      WHERE cpe1.code = pccnt.croppingplanentrycode
      AND crit2.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
      --AND d1.active IS TRUE
      GROUP BY species)
      SELECT string_agg(nom_sp,' ; ')
      FROM source_species1
    ) AS culture_especes_EDI,
    pccc.intermediatecroppingplanentrycode AS ci_code,
    (	-- Meme chose avec la culture intermediaire
        SELECT max(cpe2.name) as name
        FROM croppingplanentry cpe2
        JOIN exports_agronomes_criteres crit3 ON cpe2.domain = crit3.domaine_id
        WHERE cpe2.code = pccc.intermediatecroppingplanentrycode
        AND crit3.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
        --AND d2.active IS TRUE
        GROUP BY cpe2.code
    ) AS ci_nom,
    pccns.topiaid AS precedent_rotation_id,
    (pccns.rank+1)::text AS precedent_rang,
    (pccns.y+1)::text AS precedent_indicateur_branche,
    pccns.croppingplanentrycode AS precedent_code,
    (	-- Requete donnant pour un code culture et les campagnes du synthetise, le nom d'une culture
      SELECT max(cpe2.name) as name
      FROM croppingplanentry cpe2
      JOIN exports_agronomes_criteres crit4 ON cpe2.domain = crit4.domaine_id
      WHERE cpe2.code = pccns.croppingplanentrycode
      AND crit4.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
      --AND d2.active IS TRUE
      GROUP BY cpe2.code
    ) AS precedent_nom,
    (	WITH source_species AS (
      -- Requete donnant pour un code de culture et les campagnes du synthetise, la liste des especes concernees, sans doublons
      SELECT max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
        NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
      FROM croppingplanentry cpe2
      JOIN croppingplanspecies cps2 ON cpe2.topiaid = cps2.croppingplanentry
      JOIN refespece re2 ON cps2.species = re2.topiaid
      JOIN exports_agronomes_criteres crit5 ON cpe2.domain = crit5.domaine_id
      WHERE cpe2.code = pccns.croppingplanentrycode
      AND crit5.domaine_campagne IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
      --AND d2.active IS TRUE
      GROUP BY species)
    SELECT string_agg(nom_sp,', ')
    FROM source_species
    ) AS precedent_especes_EDI,
    pccc.croppingplanentryfrequency AS frequence_connexion
  FROM practicedcropcycleconnection pccc
  JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
  JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
  JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
  JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
  JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  WHERE gs.active IS TRUE
  AND ps.active IS TRUE
  ORDER BY systeme_synthetise_id, culture_rang;

-------------------------------
-- Succession_assolees_realise
-------------------------------
CREATE TABLE exports_agronomes_context_succession_assolees_realise AS
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.code sdc_code,
  gs.topiaid AS sdc_id,
  gs.name AS sdc_nom,
  p.topiaid AS parcelle_id,
  z.topiaid AS zone_id,
  z.name AS zone_nom,
  (eccn.rank+1)::text AS culture_rang,
  (-- Requete renvoyant l'id de la culture associe au noeud
    SELECT cpe.topiaid
    FROM croppingplanentry cpe
    WHERE cpe.topiaid = eccn.croppingplanentry)	AS culture_id,
  (-- Requete renvoyant le nom de la culture associe au noeud
    SELECT cpe1.name
    FROM croppingplanentry cpe1
    WHERE cpe1.topiaid = eccn.croppingplanentry) AS culture_nom,
  (-- Requete renvoyant les noms EDI des especes de la culture
    SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
    NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ')

    FROM croppingplanentry cpe1
    JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe1.topiaid
    JOIN refespece re2 ON cps.species = re2.topiaid
    WHERE cpe1.topiaid = eccn.croppingplanentry
    ) AS culture_especes_edi,
  (-- Requete renvoyant l'id de la culture intermediaire
    SELECT eccc.intermediatecroppingplanentry
    FROM effectivecropcycleconnection eccc
    WHERE eccc.target = eccn.topiaid
    ) AS ci_id,
  (-- Requete renvoyant le nom de la culture intermediaire
    SELECT cpe.name
    FROM effectivecropcycleconnection eccc
    JOIN croppingplanentry cpe ON cpe.topiaid = eccc.intermediatecroppingplanentry
    WHERE eccc.target = eccn.topiaid
    ) AS ci_nom,
  (-- Requete renvoyant l'id de la culture precedente
    SELECT cpe.topiaid
    FROM effectivecropcycleconnection eccc
    JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
    JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid
    WHERE eccc.target = eccn.topiaid
  ) AS precedent_id,
  (-- Requete renvoyant le nom de la culture precedente
    SELECT cpe.name
    FROM effectivecropcycleconnection eccc
    JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
    JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid
    WHERE eccc.target = eccn.topiaid
  ) AS precedent_nom,
  (-- Requete renvoyant les noms EDI des especes de la culture precedente
    SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
    NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ')
    FROM effectivecropcycleconnection eccc
    JOIN effectivecropcyclenode eccn2 ON eccc.source = eccn2.topiaid
    JOIN croppingplanentry cpe ON eccn2.croppingplanentry = cpe.topiaid
    JOIN croppingplanspecies cps ON cps.croppingplanentry = cpe.topiaid
    JOIN refespece re2 ON cps.species = re2.topiaid
    WHERE eccc.target = eccn.topiaid--eccn2.topiaid = ei.effectivecropcyclenode
  ) AS precedent_especes_edi

  FROM effectivecropcyclenode eccn
  LEFT JOIN effectivecropcycleconnection eccc ON eccc.target = eccn.topiaid -- il faut garder les noeuds qui n'ont pas de connexion, d'où le LEFT JOIN
  JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
  JOIN zone z ON ecc.zone = z.topiaid
  JOIN plot p ON z.plot = p.topiaid
  JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  --JOIN growingplan gp ON gs.growingplan = gp.topiaid
  --JOIN domain d ON gp.domain = d.topiaid
  JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  --WHERE d.active IS TRUE
  --and gp.active IS TRUE
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active IS TRUE
  ORDER BY domaine_code, domaine_campagne, sdc_nom, zone_id, culture_rang;


----------------
-- A retenir
----------------

-- ORDER BY : les noms des champs doivent etre ceux qui ont servis a faire l'union, donc les champs renommes et non les noms des champs initiaux.

-- LEFT JOIN / RIGHT JOIN
	-- Quand on commence a faire un LEFT / RIGHT JOIN, on doit continuer a le faire pour toutes les tables qui suivent ce join special 
	-- et qui sont en lien avec cette table." 
	-- c'est vrai pour le LEFT JOIN, par pour le RIGHT JOIN. Voir requete sur les intrants.

	
-- quand on a besoin de regrouper des lignes, tout ce qui est dans la clause SELECT doit se retrouver dans la clause GROUP BY (sauf les aggregations type count, avg, string_agg etc.)
-- Par contre s'il n'y a pas besoin d'aggregation, il peut tres bien y avoir des champs dans SELECT 
-- sans faire intervenir la clause GROUP BY

-- Pas besoin de grouper dans cette requete quand toutes les tables utilisees ont une relation 1-1 
-- Ex. : une parcelle est liee a un seul domaine, a un seul sdc, a une seule plot_zone, a un seul refsolarvalis

-- Filtre : 
-- WHERE d.topiaid IN
--     (SELECT domain from growingplan Where topiaid IN
--         (select growingplan FROM growingsystem WHERE topiaid IN
--            (Select growingsystem FROM growingsystem_networks
--                 WHERE networks IN (
--                     WITH RECURSIVE reseaux AS (
--                         SELECT topiaid as network from network n WHERE n.name = 'CAN'
--                         UNION
--                         SELECT np.network from reseaux r, network_parents np WHERE r.network = np.parents
--                     )
--                     SELECT * FROM reseaux r
--                 )
--             )
--         )
--     )
