﻿---------------------------------
-- Suivi des saisies des IR v1.4
---------------------------------

-- Suivi des validations des SdC
-- Suivi des interventions
-- 		2019-03-07 : suppression id modele deci dans le suivi des interventions
--      2019-03-08 : ajout d'un indicateur de rotations incompletes (synthetises)
--      2019-04-09 : suppression de la jointure sur la table managementmode qui n'a plus lieu d'exister et qui ramenait des doublons
-- Suivi des modèles décisionnels
--      2019-02-26 : ajout de stats sur les champs saisis

-- 2019-04-18 : changement du filtre sur les réseaux en filtre sur les dispositifs de type DEPHY_FERME
--              recuperation des sdc non rattaches a un reseau
-- 2019-05-02  : ajout de la surface de la parcelle pour les realises
--				 Gestion de la recuperation des reseaux d'IT dans le cas ou il y a plusieurs reseaux d'IR pour une meme ligne
                 



----------------------------------
-- Suivi des validations des SdC
----------------------------------
-- Requête : Nombre de SdC actifs par IR, par campagne, par état de validation de sdc, par modalité de suivi

WITH reseaux_parents AS 
	(SELECT n.topiaid, n.name reseau_ir, string_agg(n2.name,';') reseau_it
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	GROUP BY n.topiaid, n.name)

SELECT COALESCE(rp.reseau_it, 'donnée non renseignée') réseau_IT,
	COALESCE(n.name, 'donnée non renseignée') réseau_IR,
	d.campaign domaine_campagne,
	CASE gs.validated
	    WHEN true THEN 'SdC actifs validés'
	    WHEN false THEN 'SdC actifs non validés'
	END sdc_actifs_statut,
	gs.modality sdc_modalité_de_suivi,
	count(*) nombre_sdc,
	string_agg(CASE gs.dephynumber WHEN '' THEN 'donnée non renseignée' ELSE gs.dephynumber END, ',') numéros_DEPHY_des_SDC_concernés
FROM growingsystem gs
LEFT JOIN growingsystem_networks gsn ON gs.topiaid = gsn.growingsystem
LEFT JOIN network n ON gsn.networks = n.topiaid
LEFT JOIN reseaux_parents rp ON rp.reseau_ir = n.name 
JOIN growingplan gp ON gs.growingplan = gp.topiaid
JOIN domain d ON gp.domain = d.topiaid
WHERE gs.active IS TRUE
and gp.type = 'DEPHY_FERME'
GROUP BY rp.reseau_it, n.name, d.campaign, gs.validated, gs.modality
ORDER BY n.name, d.campaign;

----------------------------
-- Suivi des interventions
----------------------------
-- Requête : Nombre d'interventions par parcelle ou système synthétisé + indic rotation synthetise incomplete
-- 

WITH synthetise_realise AS (

-- Réalisé

(WITH plot_inter as (
    SELECT p.topiaid, count(*) nb_interventions
    FROM effectiveintervention ei
    JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
    JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
    JOIN zone z ON escc.zone = z.topiaid
    JOIN plot p ON z.plot = p.topiaid
    GROUP BY p.topiaid

    UNION

    SELECT p.topiaid, count(*) nb_interventions
    FROM effectiveintervention ei
    JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
    JOIN effectiveperennialcropcycle epcc ON eccp.topiaid = epcc.phase
    JOIN zone z ON epcc.zone = z.topiaid
    JOIN plot p ON z.plot = p.topiaid
    GROUP BY p.topiaid
)

SELECT 'réalisé' AS synthétisé_réalisé, string_agg (distinct n.name, ';') réseau_IR, d.campaign domaine_campagne, d.name domaine_nom,
gs.name sdc_nom, gs.dephynumber sdc_num_dephy, gs.active sdc_actif, gs.validated sdc_validation, 
p.topiaid synthe_id_ou_parcelle_id, 
p.name synthétisé_nom_ou_parcelle_nom,
p.area realise_parcelle_surface_ha,
NULL AS synthétisé_série_de_campagnes, NULL AS synthétisé_validation,
COALESCE(sum(nb_interventions),0) nb_interventions
	FROM plot_inter 
	RIGHT JOIN plot p ON p.topiaid = plot_inter.topiaid
	JOIN growingsystem gs ON gs.topiaid = p.growingsystem
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid
	LEFT JOIN growingsystem_networks gsn ON gs.topiaid = gsn.growingsystem
	LEFT JOIN network n ON gsn.networks = n.topiaid
	WHERE gp.type = 'DEPHY_FERME'
	AND gs.active IS TRUE
	AND d.active IS TRUE
GROUP BY p.topiaid, p.name, d.campaign, d.name, gs.name, gs.dephynumber, gs.active , gs.validated
ORDER BY réseau_IR, d.campaign, d.name, gs.name, gs.dephynumber)

UNION

-- Synthétisé

(WITH practicedsystem_inter AS (
    SELECT ps.topiaid, count(*) nb_interventions
    FROM practicedintervention pi
    JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
    JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
    JOIN practicedseasonalcropcycle pscc ON pscc.topiaid = pccn.practicedseasonalcropcycle
    JOIN practicedcropcycle pcc ON pcc.topiaid = pscc.topiaid
    JOIN practicedsystem ps ON ps.topiaid = pcc.practicedsystem
    GROUP by ps.topiaid
    
UNION

    SELECT ps.topiaid, count(*) nb_interventions
    FROM practicedintervention pi
    JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
    JOIN practicedperennialcropcycle ppcc ON ppcc.topiaid = pccp.practicedperennialcropcycle
    JOIN practicedcropcycle pcc ON pcc.topiaid = ppcc.topiaid
    JOIN practicedsystem ps ON ps.topiaid = pcc.practicedsystem
    GROUP by ps.topiaid
)

SELECT 'synthétisé' AS synthétisé_réalisé, string_agg(distinct n.name, ';') réseau_IR, d.campaign domaine_campagne, d.name domaine_nom, gs.name sdc_nom, gs.dephynumber sdc_num_dephy, gs.active sdc_actif, gs.validated sdc_validation,
ps.topiaid synthe_id_ou_parcelle_id, 
ps.name synthétisé_nom_ou_parcelle_nom,
NULL AS realise_parcelle_surface_ha, 
ps.campaigns synthétisé_série_de_campagnes, ps.validated synthétisé_validation,
COALESCE (sum(nb_interventions), 0) nb_interventions
FROM practicedsystem_inter
    RIGHT JOIN practicedsystem ps ON ps.topiaid = practicedsystem_inter.topiaid
    JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
    LEFT JOIN growingsystem_networks gsn ON gs.topiaid = gsn.growingsystem
    LEFT JOIN network n ON gsn.networks = n.topiaid
    JOIN growingplan gp ON gs.growingplan = gp.topiaid
    JOIN domain d ON gp.domain = d.topiaid
    WHERE gp.type = 'DEPHY_FERME'
    AND ps.active IS TRUE
    AND d.active IS TRUE
    GROUP BY d.campaign, d.name, gs.name, gs.dephynumber, gs.active, gs.validated, ps.topiaid, ps.name, ps.validated, ps.campaigns
    ORDER BY réseau_IR)

)

SELECT sr.synthétisé_réalisé, 

(WITH reseaux_parents AS 
	(SELECT n.name reseau_ir, string_agg(n2.name,';') reseau_it
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	GROUP BY n.name)

SELECT COALESCE(string_agg(distinct rp.reseau_it,';'), 'donnée non renseignée') 
FROM reseaux_parents rp
WHERE rp.reseau_ir in (select regexp_split_to_table(sr.réseau_ir, ';') )
) reseau_it, 

COALESCE(sr.réseau_IR, 'donnée non renseignée') réseau_IR, sr.domaine_campagne, sr.domaine_nom, sr.sdc_nom, sr.sdc_num_dephy, 
CASE sr.sdc_actif
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
END sdc_actif,
CASE sr.sdc_validation
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
END sdc_validation, 
sr.synthétisé_nom_ou_parcelle_nom, 
sr.realise_parcelle_surface_ha,
sr.synthétisé_série_de_campagnes, 
CASE sr.synthétisé_validation
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
END synthétisé_validation,

(SELECT DISTINCT 'oui'
	  FROM practicedcropcyclenode pccn
 LEFT JOIN practicedcropcycleconnection pccc1 ON pccc1.source = pccn.topiaid
 LEFT JOIN practicedcropcycleconnection pccc2 ON pccc2.target = pccn.topiaid
	  JOIN practicedseasonalcropcycle pscc ON pscc.topiaid = pccn.practicedseasonalcropcycle
      JOIN practicedcropcycle pcc ON pcc.topiaid = pscc.topiaid
      JOIN practicedsystem ps ON ps.topiaid = pcc.practicedsystem
	 WHERE (pccc1.source is null or pccc2.target is null)
	   AND ps.topiaid = sr.synthe_id_ou_parcelle_id
) synthétisé_rotation_incomplète,

sr.nb_interventions

FROM synthetise_realise sr
ORDER BY reseau_it, sr.réseau_IR, synthétisé_réalisé, domaine_campagne, domaine_nom;

-----------------------------------
-- Suivi des modèles décisionnels
-----------------------------------
-- Requête : Nombre de rubriques par modèles décisionnels + stats champs saisis

WITH modedeci AS (
	(WITH md_rubrique AS (
	SELECT 	mm.topiaid,
			count(sec.topiaid) nb_rubriques,
			count(case when trim(sec.agronomicobjective)='' then null else sec.agronomicobjective end) nb_objectifs_agro, 
			count(case when trim(sec.categoryobjective)='' then null else sec.categoryobjective end) nb_niveau_de_tolerance,			
			count(case when trim(sec.expectedresult)='' then null else sec.expectedresult end) nb_resultats_attendus,
			count(case when trim(sec.categorystrategy)='' then null else sec.categorystrategy end) nb_categ_strategie_globale
	FROM managementmode mm
	JOIN section sec ON sec.managementmode = mm.topiaid
	GROUP BY mm.topiaid)

	SELECT string_agg (n.name, ';') réseau_IR, 
	d.campaign domaine_campagne, 
	d.topiaid, 
	d.name domaine_nom, 
	gs.topiaid sdc_topiaid,
	gs.name sdc_nom, 
	gs.dephynumber sdc_num_dephy, 
	gs.active sdc_actif, 
	gs.validated sdc_validation, 
	mm.topiaid modele_deci, 
	CASE mm.category
	WHEN 'PLANNED' THEN 'Prévu'
	WHEN 'OBSERVED' THEN 'Constaté'
	END modele_deci_statut, 
	COALESCE(sum(nb_rubriques),0) nb_rubriques,
	COALESCE(sum(nb_objectifs_agro),0) nb_objectifs_agro,
	COALESCE(sum(nb_niveau_de_tolerance),0) nb_niveau_de_tolerance,
	COALESCE(sum(nb_resultats_attendus),0) nb_resultats_attendus,
	COALESCE(sum(nb_categ_strategie_globale),0) nb_categ_strategie_globale
	
	FROM md_rubrique
		RIGHT JOIN managementmode mm ON md_rubrique.topiaid = mm.topiaid
		RIGHT JOIN growingsystem gs ON mm.growingsystem = gs.topiaid 
		JOIN growingplan gp ON gs.growingplan = gp.topiaid
		JOIN domain d ON gp.domain = d.topiaid
		LEFT JOIN growingsystem_networks gsn ON gs.topiaid = gsn.growingsystem
		LEFT JOIN network n ON gsn.networks = n.topiaid
		WHERE gp.type = 'DEPHY_FERME'
		AND gs.active IS TRUE
		AND d.active IS TRUE

	GROUP BY d.campaign, d.topiaid, d.name, gs.topiaid, gs.name, gs.dephynumber, gs.active , gs.validated, mm.topiaid
	ORDER BY réseau_IR, d.campaign, d.name, gs.dephynumber)
 )

 SELECT 

 (WITH reseaux_parents AS 
	(SELECT n.name reseau_ir, string_agg(distinct n2.name,';') reseau_it
		FROM network_parents np
		JOIN network n ON n.topiaid = np.network
		JOIN network n2 ON np.parents = n2.topiaid
		GROUP BY n.name
	)
	SELECT distinct COALESCE(string_agg(distinct rp.reseau_it,';'),'donnée non renseignée') reseau_it
		FROM reseaux_parents rp
		WHERE rp.reseau_ir in (select regexp_split_to_table(modedeci.réseau_IR, ';') )
 ) reseau_it,
COALESCE(modedeci.réseau_IR, 'donnée non renseignée') réseau_IR,
modedeci.domaine_campagne,
modedeci.domaine_nom,
modedeci.sdc_topiaid,
modedeci.sdc_nom,
modedeci.sdc_num_dephy,
CASE modedeci.sdc_actif
WHEN true THEN 'oui'
WHEN false THEN 'non'
END sdc_actif,
CASE modedeci.sdc_validation
WHEN true THEN 'oui'
WHEN false THEN 'non'
END sdc_validation,
modedeci.modele_deci modele_deci_id,
modedeci.modele_deci_statut,
modedeci.nb_rubriques,
modedeci.nb_objectifs_agro,
modedeci.nb_niveau_de_tolerance,
modedeci.nb_resultats_attendus,
modedeci.nb_categ_strategie_globale,
(nb_objectifs_agro + nb_niveau_de_tolerance + nb_resultats_attendus + nb_categ_strategie_globale) nb_total_champs_saisis,
round((nb_objectifs_agro + nb_niveau_de_tolerance + nb_resultats_attendus + nb_categ_strategie_globale) * 100 /(NULLIF((nb_rubriques * 4),0)),1) pourcentage_champs_saisis,
(WITH md_nbstrategies AS
	(SELECT mm.topiaid topiaid, count(*) nb_strategies
		FROM managementmode mm
		JOIN section sec ON sec.managementmode = mm.topiaid
		JOIN strategy stra ON stra.section = sec.topiaid
		GROUP BY mm.topiaid)
	SELECT mds.nb_strategies
	FROM md_nbstrategies mds
	WHERE mds.topiaid = modedeci.modele_deci
	) nb_strategies

FROM modedeci;

----------
-- Divers
----------

-- Nom des réseaux, topiaid des réseaux, topiad des réseaux parents
WITH liste_reseaux AS
	(SELECT n.name reseau, n2.name reseau_parent
	FROM network_parents np
	JOIN network n ON n.topiaid = np.network
	JOIN network n2 ON np.parents = n2.topiaid
	WHERE n.name LIKE 'IR_%')
	
SELECT lr.reseau reseau_ir, string_agg (lr.reseau_parent,';') reseau_IT
FROM liste_reseaux lr
GROUP BY lr.reseau

--SDC non rattachés à un réseau
SELECT d.campaign domaine_campagne,d.name domaine_nom,
	gs.name sdc_nom, gs.dephynumber sdc_num_dephy, gs.active sdc_actif, gs.validated sdc_validation, 
	gs.modality sdc_modalité_de_suivi,gp.type
FROM growingsystem gs
JOIN growingplan gp ON gs.growingplan = gp.topiaid
JOIN domain d ON gp.domain = d.topiaid
LEFT JOIN growingsystem_networks gsn ON  gsn.growingsystem = gs.topiaid 
WHERE gsn.growingsystem IS NULL
and gp.type = 'DEPHY_FERME' or gp.type is null;