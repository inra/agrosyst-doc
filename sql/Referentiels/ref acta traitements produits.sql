﻿CREATE TABLE refactatraitementsproduit
(
  topiaid character varying(255) NOT NULL,
  topiaversion bigint NOT NULL,
  id_produit text NOT NULL,
  id_traitement integer NOT NULL,
  topiacreatedate timestamp without time zone,
  nom_produit text,
  code_traitement text,
  nom_traitement text,
  nodu boolean,
  source text,
  active boolean,
  CONSTRAINT refactatraitementsproduit_pkey PRIMARY KEY (topiaid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE refactatraitementsproduit
  OWNER TO agrosystuser;
GRANT ALL ON TABLE refactatraitementsproduit TO agrosystuser;
GRANT ALL ON TABLE refactatraitementsproduit TO ojolys;
