﻿-----------------------------------------------
-- Modifications RefActaTraitementsProduit RATP
-----------------------------------------------

-- Kumulan
UPDATE Refactatraitementsproduit
SET id_produit = '8092'
WHERE id_produit =  '3200'

-- Trilog
UPDATE Refactatraitementsproduit
SET id_produit = '8091'
WHERE id_produit =  '3204'

-- ALLIE STAR SX
UPDATE Refactatraitementsproduit
SET id_produit = '6864'
WHERE id_produit =  '5191'


