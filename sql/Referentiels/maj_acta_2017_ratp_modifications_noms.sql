﻿-- Table: modif_ratp_nom

-- DROP TABLE modif_ratp_nom;

CREATE TABLE modif_ratp_nom
(
  id_produit text NOT NULL,
  nom_produit text,
  nouveau_nom_produit text,

  CONSTRAINT modif_ratp_nom PRIMARY KEY (id_produit)
)

WITH (
  OIDS=FALSE
);

ALTER TABLE modif_abstractinput OWNER TO ojolys;
  
GRANT ALL ON TABLE modif_abstractinput TO ojolys;

GRANT ALL ON TABLE modif_abstractinput TO agrosystuser;


-- modifier nom dans radspc


-- modifier nom dans ratp
UPDATE refactatraitementsproduit ratp
SET nom_produit = nouveau_nom_produit
FROM modif_ratp_nom
WHERE ratp.id_produit = modif_ratp_nom.id_produit
