-----------------------------------------
-- Interventions Realise non anonyme
-----------------------------------------
-- 1.2 : ajout d'un filtre sur le champ ACTIVE (is true) sur tous les elements qui possedent ce champ en plus du domaine : sdc, dispositif, parcelle, zone
-- 2019-12-04 : ajout du stockage des resultats dans une table temporaire
--              ajout du code de la zone

-- suppression de la table temporaire de stockage des resultats de la requete
-- 2019-12-17 : ajout de 2 colonnes pour indiquer la présence de traitement chimiqe ou inoculation biologique des semis
--              correction de la requete de recuperation des especes semees
-- 2020-12    : ajout phase_id et phase pour les perennes
-- 2021-04	  : ajout prise en compte de la table de criteres
--				reactivation de l'ordre de tri
-- 2023-08 : optimisation du script -> reprise du code entier en reprennant la méthode appliquée pour interventions synthetise : division des sous requetes en tables temporaires par thématiques de colonnes
--- Tables de traitements intermediaires où les assolées et perennes sont reunies

-- Les especes et variete concernées par l intervention.

drop table if exists intervention_realise_cult_espvar;
CREATE TEMPORARY TABLE intervention_realise_cult_espvar as
-- Requete pour especes avec varietes GEVES
WITH esp_var as
  ((SELECT 
    ess.effectiveintervention intervention_id,
    cps.topiaid cps_id,
    cps.code cps_code,
    concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
            NULLIF(refesp.libelle_type_saisonnier_aee,''),NULLIF(refesp.libelle_destination_aee,''),'-',
            NULLIF(refvar.denomination,'')) as espvar
  from effectivespeciesstade ess
  join croppingplanspecies cps on ess.croppingplanspecies = cps.topiaid  
  JOIN refespece refesp ON cps.species = refesp.topiaid
  JOIN refvarietegeves refvar ON cps.variety = refvar.topiaid
  join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid
  ORDER BY cps.topiaid) 

  UNION

  -- Requete pour especes avec varietes PLANTGRAPE
  (SELECT 
    ess.effectiveintervention intervention_id,
    cps.topiaid cps_id,
    cps.code cps_code,
    --cps.croppingplanentry culture_id,
    concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
            NULLIF(refesp.libelle_type_saisonnier_aee,''),NULLIF(refesp.libelle_destination_aee,''),'-',
            NULLIF(refvar.variete,'')) as espvar
  from effectivespeciesstade ess
  join croppingplanspecies cps on ess.croppingplanspecies = cps.topiaid 
  JOIN refespece refesp ON cps.species = refesp.topiaid
  JOIN refvarieteplantgrape refvar ON cps.variety = refvar.topiaid
  join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid
  ORDER BY cps.topiaid)

  UNION

  -- Requete pour especes sans varietes
  (SELECT 
    ess.effectiveintervention intervention_id,
    cps.topiaid cps_id,
    cps.code cps_code,
    --cps.croppingplanentry culture_id,
    concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
            NULLIF(refesp.libelle_type_saisonnier_aee,''),NULLIF(refesp.libelle_destination_aee,'')) as espvar
  from effectivespeciesstade ess
  join croppingplanspecies cps on ess.croppingplanspecies = cps.topiaid 
  JOIN refespece refesp ON cps.species = refesp.topiaid
  join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid
  where cps.variety is null
  ORDER BY cps.topiaid)
  )
select 
  intervention_id,
  string_agg(espvar,' ; ') as especes_de_l_intervention
from esp_var 
group by intervention_id;


-- Actions : il peut y en avoir plusieurs pour une intervention
-- => concatenation des noms , mais pour les autres infos elle sont separees en colonnes par types d'actions

drop table if exists intervention_realise_actions;
CREATE TEMPORARY TABLE intervention_realise_actions as
with actions as (
  select 
  aa.effectiveintervention intervention_id,
  string_agg(refact.reference_label,' ; ') interventions_actions,
  string_agg(aa.seedtype, ', ') type_semence -- le type de semence est declaree dans abstractaction et non pas dans seedingactionspecies (voir ci dessous)
  from abstractaction aa 
  JOIN (select * from refinterventionagrosysttravailedi refact order by topiaid) refact ON aa.mainaction = refact.topiaid
  group by aa.effectiveintervention
),
prod_phyto as (
  select 
  ei.topiaid intervention_id,
  aa.proportionoftreatedsurface proportion_surface_traitee_phyto,
  aa.proportionoftreatedsurface * ei.spatialfrequency * ei.transitcount psci_phyto
  from abstractaction aa 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
  where refact.intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
),
lutte_bio as (
  select 
  ei.topiaid intervention_id,
  aa.proportionoftreatedsurface proportion_surface_traitee_lutte_bio,
  aa.proportionoftreatedsurface * ei.spatialfrequency * ei.transitcount psci_lutte_bio
  from abstractaction aa 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
  where refact.intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
),
irrigation as (
  select 
  ei.topiaid intervention_id,
  aa.waterquantityaverage quantite_eau_mm
  from abstractaction aa 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
  where refact.intervention_agrosyst = 'IRRIGATION'
),
semis as (
  select intervention_id, 
  string_agg(quantity :: text, ', ') densite_semis,
  string_agg(seedplantunit, ', ') unite_semis,
  string_agg(CASE treatment WHEN TRUE THEN 'oui' ELSE 'non' END, ', ') traitement_chimique_semis,
  string_agg(CASE biologicalseedinoculation WHEN TRUE THEN 'oui' else 'non' END, ', ') inoculation_biologique_semis
  from ( select 
  aa.effectiveintervention intervention_id, sas.quantity, sas.seedplantunit, sas.treatment , sas.biologicalseedinoculation
  from abstractaction aa
  join seedingactionspecies sas on aa.topiaid = sas.seedingaction 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  ORDER BY sas.topiaid ) sas2 -- pour avoir les quantite et unite dans le meme ordre
  group by sas2.intervention_id
),
esp_semis as (
  select
  sas2.intervention_id,
  string_agg(nom_sp, ' ; ') as especes_semees
    FROM (select aa.effectiveintervention intervention_id, 
          concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
              NULLIF(refesp.libelle_type_saisonnier_aee,''), NULLIF(refesp.libelle_destination_aee,'')) nom_sp
          FROM seedingactionspecies sas
          JOIN croppingplanspecies cps ON sas.speciescode = cps.code 
          join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry
          JOIN refespece refesp ON cps.species = refesp.topiaid
          -- filtration selon les campagnes
          join abstractaction aa on aa.topiaid = sas.seedingaction 
          join effectiveintervention ei on ei.topiaid = aa.effectiveintervention
          left join effectivecropcyclenode eccn on eccn.topiaid = ei.effectivecropcyclenode 
          left join effectivecropcyclephase eccp on eccp.topiaid = ei.effectivecropcyclephase 
          left join effectiveperennialcropcycle epc on epc.phase = eccp.topiaid 
          where cpe.topiaid = epc.croppingplanentry or cpe.topiaid = eccn.croppingplanentry 
          order by sas.topiaid) sas2
group by sas2.intervention_id
)
select
ei.topiaid intervention_id,
a.interventions_actions,
pp.proportion_surface_traitee_phyto,
pp.psci_phyto,
lb.proportion_surface_traitee_lutte_bio,
lb.psci_lutte_bio,
i.quantite_eau_mm,
a.type_semence, -- les types de semence sont dans abstract action et non pas dans seedingactionspecies
s.densite_semis,
s.unite_semis,
s.traitement_chimique_semis,
s.inoculation_biologique_semis,
esp.especes_semees
from effectiveintervention ei
join actions a on a.intervention_id = ei.topiaid
left join prod_phyto pp on pp.intervention_id = ei.topiaid
left join lutte_bio lb on lb.intervention_id = ei.topiaid
left join semis s on s.intervention_id = ei.topiaid
left join irrigation i on i.intervention_id = ei.topiaid
left join esp_semis esp on esp.intervention_id = ei.topiaid;

-- Combinaisons d outils.

drop table if exists intervention_realise_comboutils;
CREATE TEMPORARY TABLE intervention_realise_comboutils as
with tracteur as (
SELECT 
tc.topiaid toolscoupling_id,
refm.typemateriel1 tracteur_ou_automoteur
FROM toolscoupling tc
JOIN equipment e ON tc.tractor = e.topiaid
JOIN refmateriel refm ON e.refmateriel = refm.topiaid
),
outils as (
select 
i.toolscoupling_id,
string_agg(i.outils, ' ; ') outils
from (
  select 
  tc.topiaid toolscoupling_id,
  max(refm.typemateriel1) outils -- certaines combinaisons ont des equipements differents mais qui renvoient vers le meme type de materiel, on en garde qu'un seul. => group by refm.topiaid 
  from toolscoupling tc
  JOIN equipments_toolscoupling etc ON tc.topiaid = etc.toolscoupling
  JOIN equipment e ON etc.equipments = e.topiaid
  JOIN refmateriel refm ON e.refmateriel = refm.topiaid
  group by refm.topiaid,tc.topiaid 
) i
group by i.toolscoupling_id
)
select
ei.topiaid intervention_id, 
tc.topiaid combinaison_outils_id,
tc.toolscouplingname combinaison_outils_nom,
t.tracteur_ou_automoteur,
o.outils
from effectiveintervention ei
join effectiveintervention_toolcouplings eitc ON eitc.effectiveintervention = ei.topiaid
join toolscoupling tc on tc.topiaid = eitc.toolcouplings 
left join tracteur t on t.toolscoupling_id = tc.topiaid  -- left puisque certaines combinaisons n ont pas de tracteur : interv mannuelles
left join outils o on o.toolscoupling_id = tc.topiaid  -- left puisque certaines combinaisons n ont pas de outils
;


-- TABLES specifique aux perennes 
drop table if exists intervention_realise_culturespere;
CREATE TEMPORARY table intervention_realise_culturespere as
select 
  ei.topiaid intervention_id,
  cpe.topiaid culture_id,
  cpe.name culture_nom
FROM effectiveintervention ei
JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid  
join croppingplanentry cpe on epcc.croppingplanentry = cpe.topiaid;


-- TABLES specifiques aux assolees

drop table if exists intervention_realise_culturescibles;
CREATE TEMPORARY TABLE intervention_realise_culturescibles as
select 
ei.topiaid intervention_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
(eccn.rank +1)::text culture_rang,
cpe2.topiaid ci_id,
cpe2."name" ci_nom
FROM effectiveintervention ei
JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode  = eccn.topiaid
join croppingplanentry cpe on eccn.croppingplanentry = cpe.topiaid 
-- culture intermediaire, besoin de la connection. jointure selon la cible
left join effectivecropcycleconnection eccc on eccc.target = eccn.topiaid 
left join croppingplanentry cpe2 on eccc.intermediatecroppingplanentry = cpe2.topiaid 
;

-- Culture précédente 

drop table if exists intervention_realise_culturesprecedent;
CREATE TEMPORARY TABLE intervention_realise_culturesprecedent as
with esp as (
select 
cps.croppingplanentry cpe_id,
string_agg(distinct(concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''), -- le distinct permet de ne pas repeter une meme espece quand il y a plusieurs varietes
           NULLIF(refesp.libelle_type_saisonnier_aee,''), NULLIF(refesp.libelle_destination_aee,''))),' ; ') as esp_var
from croppingplanspecies cps
left JOIN refespece refesp ON cps.species = refesp.topiaid
group by cps.croppingplanentry
)
select 
ei.topiaid intervention_id,
cpe.topiaid precedent_id,
cpe.name precedent_nom,
e.esp_var precedent_especes_edi
FROM effectiveintervention ei
JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode  = eccn.topiaid -- noeud associe a l intervention 
join effectivecropcycleconnection eccc on eccc.target = eccn.topiaid -- par le biais de la connection, avoir le noeud source = precedent
JOIN effectivecropcyclenode eccn2 ON eccc.source= eccn2.topiaid
join croppingplanentry cpe on eccn2.croppingplanentry = cpe.topiaid 
-- les esp de la culture precedente. left puisque certaines cultures n'ont pas d'esp, ex : celles qui sont des precedents fictifs , ou alors erreur de saisie
left join esp e on e.cpe_id = cpe.topiaid
;


-- l ordre des colonne se fera lors du script execute to csv

---------------------------
-- Interventions Assolees
---------------------------

drop table if exists exports_agronomes_context_interventions_realise;
CREATE TABLE exports_agronomes_context_interventions_realise as
WITH toutes_interventions AS (
select
-- ce qui concerne l intervention directement
ei.topiaid intervention_id,
ei.name intervention_nom,
ei.type intervention_type,
ei.comment intervention_comment,
ei.startinterventiondate date_debut,
ei.endinterventiondate date_fin,
ei.spatialfrequency freq_spatiale,
ei.transitcount nombre_de_passage,
ei.spatialfrequency*ei.transitcount psci,
ei.workrate debit_de_chantier,
ei.workrateunit debit_de_chantier_unite,
ei.involvedpeoplecount nb_personne_mobili,
CASE ei.intermediatecrop WHEN true then 'oui' WHEN false then 'non' END concerne_la_ci,
-- ce qui concerne le contexte du realise
z.topiaid zone_id,
z."name" zone_nom,
z.code zone_code,
p.topiaid parcelle_id,
p."name" parcelle_nom,
gs.topiaid sdc_id,
gs."name" sdc_nom,
gs.code sdc_code,
crit.domaine_code, 
crit.domaine_id, 
crit.domaine_nom, 
crit.domaine_campagne,
-- la culture liee a intervention
'NA' phase_id,
'NA' phase,
cultcible.culture_id,
cultcible.culture_nom,
cultcible.culture_rang,
cultcible.ci_id,
cultcible.ci_nom,
-- les esp de l intervention
cultcible_espvar.especes_de_l_intervention,
-- la culture precedente
cultpre.precedent_nom,
cultpre.precedent_id,
cultpre.precedent_especes_edi,
-- ce qui concerne les actions
ac.interventions_actions, 
ac.proportion_surface_traitee_phyto,
ac.psci_phyto,
ac.proportion_surface_traitee_lutte_bio,
ac.psci_lutte_bio,
ac.quantite_eau_mm,
ac.type_semence,
ac.densite_semis,
ac.unite_semis,
ac.traitement_chimique_semis,
ac.inoculation_biologique_semis,
ac.especes_semees,
co.combinaison_outils_id,
co.combinaison_outils_nom,
co.tracteur_ou_automoteur,
co.outils
FROM effectiveintervention ei
JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode 
JOIN effectiveseasonalcropcycle esc ON esc.topiaid = eccn.effectiveseasonalcropcycle 
JOIN zone z ON esc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN growingsystem gs ON p.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
-- jointures des tables cree au prealable
join intervention_realise_culturescibles cultcible on cultcible.intervention_id = ei.topiaid
left join intervention_realise_cult_espvar cultcible_espvar on cultcible_espvar.intervention_id = ei.topiaid -- il y a des interventions qui n'ont pas d'esp. ex : sur une culture allees et abords, en assolee et perennes
left join intervention_realise_culturesprecedent cultpre on cultpre.intervention_id = ei.topiaid -- left puisque certaines cultures n ont pas de precedent
join intervention_realise_actions ac on ac.intervention_id = ei.topiaid
left join intervention_realise_comboutils co on co.intervention_id = ei.topiaid -- left puisque certaines interventions n ont pas de combinaisons d outils associees
where z.active = true and p.active = true and gs.active = true 

UNION

---------------------------
-- Interventions Perennes
---------------------------

select
-- ce qui concerne l intervention directement
ei.topiaid intervention_id,
ei.name intervention_nom,
ei.type intervention_type,
ei.comment intervention_comment,
ei.startinterventiondate date_debut,
ei.endinterventiondate date_fin,
ei.spatialfrequency freq_spatiale,
ei.transitcount nombre_de_passage,
ei.spatialfrequency*ei.transitcount psci,
ei.workrate debit_de_chantier,
ei.workrateunit debit_de_chantier_unite,
ei.involvedpeoplecount nb_personne_mobili,
CASE ei.intermediatecrop WHEN true then 'oui' WHEN false then 'non' END concerne_la_ci,
-- ce qui concerne le contexte du realise
z.topiaid zone_id,
z."name" zone_nom,
z.code zone_code,
p.topiaid parcelle_id,
p."name" parcelle_nom,
gs.topiaid sdc_id,
gs."name" sdc_nom,
gs.code sdc_code,
crit.domaine_code, 
crit.domaine_id, 
crit.domaine_nom, 
crit.domaine_campagne,
-- la culture liee a intervention
eccp.topiaid phase_id,
eccp.type phase,
cult.culture_id,
cult.culture_nom,
'NA' culture_rang,
'NA' ci_id,
'NA' ci_nom,
-- les esp de l intervention
cult_espvar.especes_de_l_intervention,
-- la culture precedente (assolees)
'NA' precedent_nom,
'NA' precedent_code,
'NA' precedent_especes_edi,
-- ce qui concerne les actions
ac.interventions_actions, 
ac.proportion_surface_traitee_phyto,
ac.psci_phyto,
ac.proportion_surface_traitee_lutte_bio,
ac.psci_lutte_bio,
ac.quantite_eau_mm,
ac.type_semence,
ac.densite_semis,
ac.unite_semis,
ac.traitement_chimique_semis,
ac.inoculation_biologique_semis,
ac.especes_semees,
co.combinaison_outils_id,
co.combinaison_outils_nom,
co.tracteur_ou_automoteur,
co.outils
FROM effectiveintervention ei
JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid  
JOIN zone z ON epcc.zone = z.topiaid
JOIN plot p ON z.plot = p.topiaid
JOIN growingsystem gs ON p.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
-- jointures des tables cree au prealable
join intervention_realise_culturespere cult on cult.intervention_id = ei.topiaid 
left join intervention_realise_cult_espvar cult_espvar on cult_espvar.intervention_id = ei.topiaid -- il y a des interventions qui n'ont pas d'esp. ex : sur une culture allees et abords, en assolee et perennes
join intervention_realise_actions ac on ac.intervention_id = ei.topiaid
left join intervention_realise_comboutils co on co.intervention_id = ei.topiaid
where z.active = true and p.active = true and gs.active = true
)
SELECT * FROM toutes_interventions
ORDER BY domaine_code, domaine_campagne, sdc_nom, parcelle_id, zone_id
;
