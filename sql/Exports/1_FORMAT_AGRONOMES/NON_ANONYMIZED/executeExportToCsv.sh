#!/bin/bash
declare -a query_name
declare -a query
repertoire_export=$HOME"/EXP_MASS_CSV/FORMAT_AGRONOMES/"
jour="$(date +%Y%m%d)"
prefixe="AGS_"$jour"_exports_agronomes_"
suffixe=".csv"

pg_host="147.100.179.208"
pg_port="5438"
pg_user="dephygraph_admin"
pg_password="REPLACE_WITH_PG_PASSWORD"
pg_database="agrosyst_export_perf_20220402"

query_name[1]="context_ateliers_elevage"
query[1]="
select replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(atelier_elevage_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as atelier_elevage_id,
replace(replace(atelier_elevage_type_animaux,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as atelier_elevage_type_animaux,
atelier_elevage_taille as atelier_elevage_taille,
replace(replace(atelier_elevage_unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as atelier_elevage_unite
from exports_agronomes_context_ateliers_elevage
"

query_name[2]="context_cultures"
query[2]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(culture_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_code,
replace(replace(culture_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_id,
replace(replace(culture_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_nom,
replace(replace(culture_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_type,
replace(replace(culture_melange_especes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_melange_especes,
replace(replace(culture_melange_varietes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_melange_varietes,
replace(replace(espece_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as espece_id,
replace(replace(espece_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as espece_code,
replace(replace(code_edi_espece_botanique,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as code_edi_espece_botanique,
replace(replace(code_edi_qualifiant,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as code_edi_qualifiant,
replace(replace(code_edi_type_saisonnier,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as code_edi_type_saisonnier,
replace(replace(code_edi_destination,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as code_edi_destination,
replace(replace(espece_edi_nom_botanique,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as espece_edi_nom_botanique,
replace(replace(espece_edi_qualifiant,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as espece_edi_qualifiant,
replace(replace(espece_edi_type_saisonnier,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as espece_edi_type_saisonnier,
replace(replace(espece_edi_destination,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as espece_edi_destination,
replace(replace(variete_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as variete_nom
from exports_agronomes_context_cultures
"

query_name[3]="context_domaines"
query[3]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
replace(replace(siret,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as siret,
domaine_campagne as domaine_campagne,
replace(replace(domaine_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_type,
replace(replace(departement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as departement,
replace(replace(commune,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as commune,
replace(replace(petite_region_agricole,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as petite_region_agricole,
replace(replace(domaine_zonage,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_zonage,
pct_SAU_zone_vulnerable as pct_sau_zone_vulnerable,
pct_sau_zone_excedent_structurel as pct_sau_zone_excedent_structurel,
pct_sau_zone_actions_complementaires as pct_sau_zone_actions_complementaires,
pct_sau_zone_natura_2000 as pct_sau_zone_natura_2000,
pct_sau_zone_erosion as pct_sau_zone_erosion,
pct_sau_perimetre_protection_captage as pct_sau_perimetre_protection_captage,
replace(replace(domain_description,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domain_description,
replace(replace(statut_juridique_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as statut_juridique_nom,
replace(replace(statut_juridique_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as statut_juridique_commentaire,
sau as sau,
replace(replace(cultures_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as cultures_commentaire,
replace(replace(autres_activites_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as autres_activites_commentaire,
replace(replace(mo_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as mo_commentaire,
nombre_associes as nombre_associes,
mo_familiale_et_associes as mo_familiale_et_associes,
mo_permanente as mo_permanente,
mo_temporaire as mo_temporaire,
mo_familiale_remuneration as mo_familiale_remuneration,
charges_salariales as charges_salariales,
mo_conduite_cultures_dans_domaine_expe as mo_conduite_cultures_dans_domaine_expe,
cotisation_msa as cotisation_msa, fermage_moyen as fermage_moyen,
aides_decouplees as aides_decouplees,
replace(replace(otex_18_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as otex_18_nom,
replace(replace(otex_70_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as otex_70_nom,
replace(replace(otex_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as otex_commentaire,
replace(replace(responsables_domaine,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as responsables_domaine
from exports_agronomes_context_domaines
"

query_name[4]="interventions_synthetisees"
query[4]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(systeme_synthetise_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_id,
replace(replace(systeme_synthetise_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_nom,
replace(replace(systeme_synthetise_campagnes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_campagnes,
replace(replace(systeme_synthetise_validation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_validation,
replace(replace(phase_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as phase_id,
replace(replace(phase,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as phase,
replace(replace(culture_precedent_rang_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_precedent_rang_id,
replace(replace(culture_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_code,
replace(replace(culture_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_nom,
replace(replace(ci_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_code,
replace(replace(ci_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_nom,
replace(replace(concerne_la_ci,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as concerne_la_ci,
replace(replace(especes_de_l_intervention,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as especes_de_l_intervention,
replace(replace(precedent_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_code,
replace(replace(precedent_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_nom,
replace(replace(precedent_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_especes_edi,
replace(replace(culture_rang,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_rang,
replace(replace(intervention_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_id,
replace(replace(intervention_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_type,
replace(replace(interventions_actions,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as interventions_actions,
replace(replace(intervention_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_nom,
replace(replace(intervention_comment,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_comment,
replace(replace(combinaison_outils_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outils_code,
replace(replace(combinaison_outils_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outils_nom,
replace(replace(tracteur_ou_automoteur,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as tracteur_ou_automoteur,
replace(replace(outils,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as outils,
replace(replace(date_debut,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as date_debut,
replace(replace(date_fin,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as date_fin,
freq_spatiale as freq_spatiale,
freq_temporelle as freq_temporelle,
psci as psci,
proportion_surface_traitee_phyto as proportion_surface_traitee_phyto,
psci_phyto as psci_phyto,
proportion_surface_traitee_lutte_bio as proportion_surface_traitee_lutte_bio,
psci_lutte_bio as psci_lutte_bio,
debit_de_chantier as debit_de_chantier,
replace(replace(debit_de_chantier_unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as debit_de_chantier_unite,
quantite_eau_mm as quantite_eau_mm,
replace(replace(especes_semees,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as especes_semees,
replace(replace(densite_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as densite_semis,
replace(replace(unite_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite_semis,
replace(replace(traitement_chimique_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as traitement_chimique_semis,
replace(replace(inoculation_biologique_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as inoculation_biologique_semis,
rang_intervention as rang_intervention
from exports_agronomes_context_interventions_synthetise
"

query_name[5]="interventions_realisees"
query[5]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(parcelle_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_id,
replace(replace(parcelle_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_nom,
replace(replace(zone_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_id,
replace(replace(zone_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_nom,
replace(replace(zone_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_code,
replace(replace(phase_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as phase_id,
replace(replace(phase,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as phase,
replace(replace(culture_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_id,
replace(replace(culture_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_nom,
replace(replace(ci_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_id,
replace(replace(ci_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_nom,
replace(replace(concerne_la_ci,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as concerne_la_ci,
replace(replace(especes_de_l_intervention,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as especes_de_l_intervention,
replace(replace(precedent_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_id,
replace(replace(precedent_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_nom,
replace(replace(precedent_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_especes_edi,
culture_rang as culture_rang,
replace(replace(intervention_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_id,
replace(replace(intervention_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_type,
replace(replace(interventions_actions,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as interventions_actions,
replace(replace(intervention_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_nom,
replace(replace(intervention_comment,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_comment,
replace(replace(combinaison_outils_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outils_id,
replace(replace(combinaison_outils_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outils_nom,
replace(replace(tracteur_ou_automoteur,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as tracteur_ou_automoteur,
replace(replace(outils,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as outils,
date_debut as date_debut,
date_fin as date_fin,
freq_spatiale as freq_spatiale,
nombre_de_passage as nombre_de_passage,
psci as psci,
proportion_surface_traitee_phyto as proportion_surface_traitee_phyto,
psci_phyto as psci_phyto,
proportion_surface_traitee_lutte_bio as proportion_surface_traitee_lutte_bio,
psci_lutte_bio as psci_lutte_bio,
debit_de_chantier as debit_de_chantier,
replace(replace(debit_de_chantier_unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as debit_de_chantier_unite,
quantite_eau_mm as quantite_eau_mm,
replace(replace(especes_semees,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as especes_semees,
replace(replace(densite_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as densite_semis,
replace(replace(unite_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite_semis,
replace(replace(traitement_chimique_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as traitement_chimique_semis,
replace(replace(inoculation_biologique_semis,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as inoculation_biologique_semis,
replace(replace(type_semence,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as type_semence
from exports_agronomes_context_interventions_realise
"


query_name[6]="intrants_realises"
query[6]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(parcelle_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_id,
replace(replace(parcelle_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_nom,
replace(replace(intervention_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_id,
replace(replace(intrant_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_id,
replace(replace(intrant_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_type,
replace(replace(intrant_ref_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_ref_id,
replace(replace(intrant_ref_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_ref_nom,
replace(replace(intrant_nom_utilisateur,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_nom_utilisateur,
replace(replace(intrant_code_amm,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_code_amm,
dose as dose,
volume_bouillie_hl as volume_bouillie_hl,
replace(replace(unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite,
replace(replace(biocontrole,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as biocontrole,
replace(replace(intrant_phyto_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_phyto_type,
replace(replace(intrant_phyto_cible_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_phyto_cible_nom,
replace(replace(forme_ferti_min,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forme_ferti_min,
n as n,
p2o5 as p2o5,
k2o as k2o,
bore as bore,
calcium as calcium,
fer as fer,
manganese as manganese,
molybdene as molybdene,
mgo as mgo,
oxyde_de_sodium as oxyde_de_sodium,
so3 as so3,
cuivre as cuivre,
zinc as zinc,
replace(replace(unite_teneur_ferti_orga,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite_teneur_ferti_orga,
replace(replace(ferti_effet_phyto_attendu,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ferti_effet_phyto_attendu,
prix as prix,
replace(replace(prix_unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as prix_unite
from exports_agronomes_context_intrants_realise
"

query_name[7]="intrants_synthetises"
query[7]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(systeme_synthetise_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_id,
replace(replace(intervention_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_id,
replace(replace(intrant_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_id,
replace(replace(intrant_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_type,
replace(replace(intrant_ref_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_ref_id,
replace(replace(intrant_ref_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_ref_nom,
replace(replace(intrant_nom_utilisateur,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_nom_utilisateur,
replace(replace(intrant_code_amm,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_code_amm,
dose as dose,
volume_bouillie_hl as volume_bouillie_hl,
replace(replace(unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite,
replace(replace(biocontrole,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as biocontrole,
replace(replace(intrant_phyto_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_phyto_type,
replace(replace(intrant_phyto_cible_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intrant_phyto_cible_nom,
replace(replace(forme_ferti_min,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forme_ferti_min,
n as n,
p2o5 as p2o5,
k2o as k2o,
bore as bore,
calcium as calcium,
fer as fer,
manganese as manganese,
molybdene as molybdene,
mgo as mgo,
oxyde_de_sodium as oxyde_de_sodium,
so3 as so3,
cuivre as cuivre,
zinc as zinc,
replace(replace(unite_teneur_ferti_orga,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite_teneur_ferti_orga,
replace(replace(ferti_effet_phyto_attendu,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ferti_effet_phyto_attendu,
prix as prix,
replace(replace(prix_unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as prix_unite
from exports_agronomes_context_intrants_synthetise
"

query_name[8]="materiel"
query[8]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(combinaison_outil_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outil_id,
replace(replace(combinaison_outil_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outil_code,
replace(replace(combinaison_outils_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as combinaison_outils_nom,
replace(replace(materiel_ref_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_ref_id,
replace(replace(materiel_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_id,
replace(replace(materiel_eta_cuma,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_eta_cuma,
replace(replace(materiel_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_nom,
replace(replace(materiel_caracteristique1,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_caracteristique1,
replace(replace(materiel_caracteristique2,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_caracteristique2,
replace(replace(materiel_caracteristique3,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_caracteristique3,
replace(replace(materiel_caracteristique4,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as materiel_caracteristique4,
utilisation_annuelle as utilisation_annuelle,
replace(replace(utilisation_annuelle_unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as utilisation_annuelle_unite,
coût_par_unite_travail_annuel as coût_par_unite_travail_annuel
from exports_agronomes_context_materiels
"

query_name[9]="parcelles_type"
query[9]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(parcelle_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_code,
replace(replace(parcelle_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_id,
replace(replace(parcelle_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_nom,
parcelle_surface as parcelle_surface,
replace(replace(code_postal,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as code_postal,
replace(replace(commune,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as commune,
replace(replace(parcelle_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_commentaire,
nombre_de_zones as nombre_de_zones,
replace(replace(parcelle_hors_zonage,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_hors_zonage,
replace(replace(zonages_parcelle,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zonages_parcelle,
replace(replace(equipement_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as equipement_commentaire,
replace(replace(drainage,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as drainage,
drainage_annee_realisation as drainage_annee_realisation,
replace(replace(protection_anti_gel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as protection_anti_gel,
replace(replace(protection_anti_gel_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as protection_anti_gel_type,
replace(replace(protection_anti_grele,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as protection_anti_grele,
replace(replace(protection_anti_pluie,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as protection_anti_pluie,
replace(replace(protection_anti_insectes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as protection_anti_insectes,
replace(replace(autre_equipement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as autre_equipement,
replace(replace(sol_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_commentaire,
replace(replace(sol_nom_ref,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_nom_ref,
replace(replace(texture_surface,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as texture_surface,
replace(replace(texture_sous_sol,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as texture_sous_sol,
replace(replace(sol_ph,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_ph,
pierrosite_moyenne as pierrosite_moyenne,
replace(replace(sol_profondeur,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_profondeur,
sol_profondeur_max as sol_profondeur_max,
teneur_mo as teneur_mo,
replace(replace(hydromorphie,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as hydromorphie,
replace(replace(calcaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as calcaire,
proportion_calcaire_total as proportion_calcaire_total,
proportion_calclaire_actif as proportion_calclaire_actif,
replace(replace(battance,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as battance,
replace(replace(irrigation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation,
replace(replace(fertirrigation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as fertirrigation,
replace(replace(origine_eau,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as origine_eau,
replace(replace(irrigation_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation_type,
replace(replace(irrigation_pompe,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation_pompe,
replace(replace(irrigation_position_tuyaux,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation_position_tuyaux,
replace(replace(pente,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as pente,
replace(replace(distance_cours_eau,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as distance_cours_eau,
replace(replace(bande_enherbee,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as bande_enherbee
from exports_agronomes_context_parcelles
"

query_name[10]="perennes_synthetisees"
query[10]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(systeme_synthetise_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_id,
replace(replace(systeme_synthetise_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_nom,
replace(replace(systeme_synthetise_campagnes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_campagnes,
replace(replace(systeme_synthetise_validation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_validation,
replace(replace(culture_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_code,
replace(replace(phase_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as phase_id,
replace(replace(phase,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as phase,
duree_phase_ans,
pourcentage_sole_sdc,
annee_plantation,
inter_rang,
espacement_sur_le_rang,
densite_plantation,
replace(replace(forme_fruitiere_verger,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forme_fruitiere_verger,
hauteur_frondaison as hauteur_frondaison,
epaisseur_frondaison as epaisseur_frondaison,
replace(replace(forme_fruitiere_vigne,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forme_fruitiere_vigne,
replace(replace(orientation_rangs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as orientation_rangs,
taux_mortalite_plantation as taux_mortalite_plantation,
annee_mesure_taux_mortalite as annee_mesure_taux_mortalite,
replace(replace(type_enherbement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as type_enherbement,
replace(replace(couvert_vegetal_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as couvert_vegetal_commentaire,
pollinisateurs as pollinisateurs,
pourcentage_de_pollinisateurs as pourcentage_de_pollinisateurs,
replace(replace(mode_repartition_pollinisateurs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as mode_repartition_pollinisateurs
from exports_agronomes_context_perennes_synthetise
"

query_name[11]="recoltes_realisees"
query[11]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_name,
replace(replace(parcelle_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_id,
replace(replace(parcelle_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_nom,
replace(replace(zone_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_id,
replace(replace(zone_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_nom,
replace(replace(intervention_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_id,
replace(replace(action_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as action_id,
replace(replace(type_action,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as type_action,
replace(replace(destination_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as destination_id,
replace(replace(destination_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as destination_nom,
rendement_moyen as rendement_moyen,
rendement_median as rendement_median,
rendement_min as rendement_min,
rendement_max as rendement_max,
replace(replace(unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite,
perc_commercialise as \"%commercialise\",
perc_autoconsomme as \"%autoconsomme\",
perc_non_valorise as \"%non_valorise\"
from exports_agronomes_context_recolte_realise
"

query_name[12]="recoltes_synthetisees"
query[12]="
select
replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne, replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(systeme_synthetise_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_id,
replace(replace(systeme_synthetise_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_nom,
replace(replace(systeme_synthetise_campagnes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_campagnes,
replace(replace(intervention_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as intervention_id,
replace(replace(action_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as action_id,
replace(replace(type_action,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as type_action,
replace(replace(destination_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as destination_id,
replace(replace(destination_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as destination_nom,
rendement_moyen as rendement_moyen,
rendement_median as rendement_median,
rendement_min as rendement_min,
rendement_max as rendement_max,
replace(replace(unite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as unite,
perc_commercialise as \"%commercialise\",
perc_autoconsomme as \"%autoconsomme\",
perc_non_valorise as \"%non_valorise\"
from exports_agronomes_context_recolte_synthetise
"

query_name[13]="assolees_synthetisees"
query[13]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') AS domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') AS sdc_nom,
replace(replace(systeme_synthetise_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_id,
replace(replace(systeme_synthetise_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') AS systeme_synthetise_nom,
replace(replace(systeme_synthetise_campagnes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_campagnes,
systeme_synthetise_validation as systeme_synthetise_validation,
replace(replace(culture_precedent_rang_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_precedent_rang_id,
replace(replace(culture_rotation_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_rotation_id,
replace(replace(culture_rang,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_rang,
replace(replace(culture_indicateur_branche,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_indicateur_branche,
replace(replace(culture_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_code,
replace(replace(culture_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_nom,
replace(replace(fin_rotation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as fin_rotation,
replace(replace(meme_campagne_culture_precedente,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as meme_campagne_culture_precedente,
replace(replace(culture_absente,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_absente,
replace(replace(culture_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_especes_edi,
replace(replace(ci_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_code,
replace(replace(ci_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_nom,
replace(replace(precedent_rotation_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_rotation_id,
replace(replace(precedent_rang,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_rang,
replace(replace(precedent_indicateur_branche,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_indicateur_branche,
replace(replace(precedent_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_code,
replace(replace(precedent_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_nom,
replace(replace(precedent_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_especes_edi,
frequence_connexion as frequence_connexion
from exports_agronomes_context_rotation_assolees_synthetise
"

query_name[14]="sdc"
query[14]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(dispositif_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_code,
replace(replace(dispositif_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_id,
replace(replace(dispositif_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_type,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(sdc_modalite_suivi_dephy,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_modalite_suivi_dephy,
replace(replace(sdc_code_dephy,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code_dephy,
replace(replace(sdc_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_commentaire,
replace(replace(sdc_valide,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_valide,
replace(replace(sdc_filiere,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_filiere,
replace(replace(sdc_type_production,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_type_production,
replace(replace(sdc_type_agriculture,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_type_agriculture,
replace(replace(sdc_strategie_categorie,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_strategie_categorie,
sdc_part_sau_domaine as sdc_part_sau_domaine,
sdc_part_mo_domaine as sdc_part_mo_domaine,
sdc_part_materiel_domaine as sdc_part_materiel_domaine,
replace(replace(couverts_associes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as couverts_associes,
replace(replace(ci_effet_allelo_ou_biocide,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_effet_allelo_ou_biocide,
replace(replace(ci_attractives_pour_auxiliaires,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_attractives_pour_auxiliaires,
replace(replace(cipan,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as cipan,
replace(replace(semis_precoce,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as semis_precoce,
replace(replace(semis_tardif,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as semis_tardif,
replace(replace(exportation_menu_pailles,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as exportation_menu_pailles,
replace(replace(destruction_litiere,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as destruction_litiere,
replace(replace(dose_adaptee_surf_foliaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dose_adaptee_surf_foliaire,
replace(replace(filet_anti_insectes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as filet_anti_insectes,
replace(replace(taille_organes_contamines,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as taille_organes_contamines,
replace(replace(taille_limitant_risques_sanitaires,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as taille_limitant_risques_sanitaires,
replace(replace(faible_densite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as faible_densite,
replace(replace(forte_densite,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forte_densite,
replace(replace(faible_ecartement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as faible_ecartement,
replace(replace(fort_ecartement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as fort_ecartement,
replace(replace(ajustement_fertilisation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ajustement_fertilisation,
replace(replace(ajustement_irrigation,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ajustement_irrigation,
replace(replace(desherbage_autre_forme,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as desherbage_autre_forme,
replace(replace(cultures_pieges,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as cultures_pieges,
replace(replace(desherbage_meca_frequent,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as desherbage_meca_frequent,
replace(replace(desherbage_meca_occasionnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as desherbage_meca_occasionnel,
replace(replace(desherbage_thermique,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as desherbage_thermique,
replace(replace(adaptation_lutte_a_la_parcelle,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as adaptation_lutte_a_la_parcelle,
replace(replace(optim_conditions_application,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as optim_conditions_application,
replace(replace(reduction_dose_autres_phyto,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as reduction_dose_autres_phyto,
replace(replace(reduction_dose_fongi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as reduction_dose_fongi,
replace(replace(reduction_dose_herbi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as reduction_dose_herbi,
replace(replace(reduction_dose_insec,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as reduction_dose_insec,
replace(replace(traitement_localise,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as traitement_localise,
replace(replace(utilisation_adjuvants,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as utilisation_adjuvants,
replace(replace(utilisation_seuils,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as utilisation_seuils,
replace(replace(utilisation_stim_defense,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as utilisation_stim_defense,
replace(replace(utilisation_oad,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as utilisation_oad,
replace(replace(var_competitives_adventice,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as var_competitives_adventice,
replace(replace(var_peu_sensibles_verse,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as var_peu_sensibles_verse,
replace(replace(var_peu_sensibles_maladies,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as var_peu_sensibles_maladies,
replace(replace(var_peu_sensibles_ravageurs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as var_peu_sensibles_ravageurs,
replace(replace(lutte_bio_confu_sexuelle,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as lutte_bio_confu_sexuelle,
replace(replace(lutte_bio_autre,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as lutte_bio_autre,
replace(replace(melange,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as melange,
replace(replace(gestion_residus,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as gestion_residus,
replace(replace(monoculture_rotation_courte,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as monoculture_rotation_courte,
replace(replace(rotation_cultures_rustiques,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as rotation_cultures_rustiques,
replace(replace(rotation_diversifiee_avec_pt,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as rotation_diversifiee_avec_pt,
replace(replace(rotation_diversifiee_sans_pt,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as rotation_diversifiee_sans_pt,
replace(replace(rotation_diversifiee_intro_une_culture,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as rotation_diversifiee_intro_une_culture,
replace(replace(enherbement_seme,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as enherbement_seme,
replace(replace(enherbement_naturel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as enherbement_naturel,
replace(replace(agroforesterie,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as agroforesterie,
replace(replace(arbres_bordure_parcelle,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as arbres_bordure_parcelle,
replace(replace(broyage_bordure,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as broyage_bordure,
replace(replace(gestion_bordure_de_bois,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as gestion_bordure_de_bois,
replace(replace(haies_anciennes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as haies_anciennes,
replace(replace(haies_jeunes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as haies_jeunes,
replace(replace(bandes_enherbees,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as bandes_enherbees,
replace(replace(bandes_fleuries,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as bandes_fleuries,
replace(replace(bois_bosquet,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as bois_bosquet,
replace(replace(decompactage_occasionnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as decompactage_occasionnel,
replace(replace(decompactage_frequent,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as decompactage_frequent,
replace(replace(faux_semis_ponctuels,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as faux_semis_ponctuels,
replace(replace(faux_semis_intensifs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as faux_semis_intensifs,
replace(replace(labour_occasionnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as labour_occasionnel,
replace(replace(labour_frequent,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as labour_frequent,
replace(replace(labour_systematique,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as labour_systematique,
replace(replace(semis_direct_occasionnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as semis_direct_occasionnel,
replace(replace(semis_direct_systematique,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as semis_direct_systematique,
replace(replace(strip_till_occasionnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as strip_till_occasionnel,
replace(replace(strip_till_frequent,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as strip_till_frequent,
replace(replace(tcs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as tcs,
replace(replace(reseaux_ir,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as reseaux_ir,
replace(replace(reseaux_it,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as reseaux_it,
replace(replace(codes_convention_dephy,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as codes_convention_dephy,
modes_commercialisation as modes_commercialisation
from exports_agronomes_context_sdc
"

query_name[15]="successions_assolees_realisees"
query[15]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
replace(replace(domaine_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne as domaine_campagne,
replace(replace(sdc_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_code,
replace(replace(sdc_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_id,
replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
replace(replace(parcelle_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_id,
replace(replace(zone_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_id,
replace(replace(zone_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_nom,
replace(replace(culture_rang,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_rang,
replace(replace(culture_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_id,
replace(replace(culture_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_nom,
replace(replace(culture_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_especes_edi,
replace(replace(ci_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_id,
replace(replace(ci_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as ci_nom,
replace(replace(precedent_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_id,
replace(replace(precedent_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_nom,
replace(replace(precedent_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as precedent_especes_edi
from exports_agronomes_context_succession_assolees_realise
"

query_name[16]="coordonnees_gps_domaines"
query[16]="
select domaine_id,
replace(replace(coordonnees_nom_centre_operationnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as coordonnees_nom_centre_operationnel,
replace(replace(coordonnees_description_centre_operationnel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as coordonnees_description_centre_operationnel,
longitude,
latitude
FROM exports_agronomes_context_coordonnees_gps_domaines
"

query_name[17]="context_dispositifs"
query[17]="
select replace(replace(domaine_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_code,
domaine_id,
replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
domaine_campagne,
replace(replace(dispositif_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_id,
replace(replace(dispositif_code,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_code,
replace(replace(dispositif_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_nom,
replace(replace(dispositif_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as dispositif_type
FROM exports_agronomes_context_dispositifs
"

query_name[18]="parcelle_type"
query[18]="
 select
 domaine_id,
 domaine_code,
 replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
 domaine_campagne,
 sdc_id,
 systeme_synthetise_id as systeme_synthetise_id,
 replace(replace(systeme_synthetise_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_nom,
 replace(replace(systeme_synthetise_campagnes,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as systeme_synthetise_campagnes,
 parcelle_type_id,
 replace(replace(parcelle_type_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_type_nom,
 parcelle_type_surface,
 replace(replace(parcelle_type_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_type_commentaire,
 (CASE parcelle_type_hors_zonage WHEN TRUE THEN 'oui' else 'non' END) AS parcelle_type_hors_zonage,
 replace(replace(equipement_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as equipement_commentaire,
 (CASE drainage WHEN TRUE THEN 'oui' else 'non' END) AS drainage,
 drainage_annee_realisation,
 (CASE protection_anti_gel WHEN TRUE THEN 'oui' else 'non' END) AS protection_anti_gel,
 replace(replace(protection_anti_gel_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as protection_anti_gel_type,
 (CASE protection_anti_grele WHEN TRUE THEN 'oui' else 'non' END) as protection_anti_grele,
 (CASE protection_anti_pluie WHEN TRUE THEN 'oui' else 'non' END) as protection_anti_pluie,
 (CASE protection_anti_insectes WHEN TRUE THEN 'oui' else 'non' END) as protection_anti_insectes,
 replace(replace(autre_equipement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as autre_equipement,
 replace(replace(sol_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_commentaire,
 replace(replace(texture_surface,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as texture_surface,
 replace(replace(texture_sous_sol,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as texture_sous_sol,
 replace(replace(sol_ph,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_ph,
 pierrosite_moyenne,
 replace(replace(sol_profondeur,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sol_profondeur,
 sol_profondeur_max,
 teneur_mo,
 (CASE hydromorphie WHEN TRUE THEN 'oui' else 'non' END) as hydromorphie,
 (CASE calcaire WHEN TRUE THEN 'oui' else 'non' END) as calcaire,
 proportion_calcaire_total,
 proportion_calclaire_actif,
 (CASE battance WHEN TRUE THEN 'oui' else 'non' END) as battance,
 (CASE irrigation WHEN TRUE THEN 'oui' else 'non' END) as irrigation,
 (CASE fertirrigation WHEN TRUE THEN 'oui' else 'non' END) as fertirrigation,
 replace(replace(origine_eau,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as origine_eau,
 replace(replace(irrigation_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation_type,
 replace(replace(irrigation_pompe,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation_pompe,
 replace(replace(irrigation_position_tuyaux,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as irrigation_position_tuyaux,
 replace(replace(pente,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as pente,
 replace(replace(distance_cours_eau,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as distance_cours_eau,
 replace(replace(bande_enherbee,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as bande_enherbee
 FROM exports_agronomes_context_parcelle_type
"

query_name[19]="perennes_realise"
query[19]="
 select
 domaine_code,
 domaine_id,
 replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
 domaine_campagne,
 sdc_code,
 sdc_id,
 replace(replace(sdc_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as sdc_nom,
 parcelle_id,
 replace(replace(parcelle_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as parcelle_nom,
 zone_id,
 replace(replace(zone_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_nom,
 culture_id,
 replace(replace(culture_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_nom,
 replace(replace(culture_especes_edi,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as culture_especes_edi,
 phase_id,
 phase,
 duree_phase_ans,
 annee_plantation,
 inter_rang,
 espacement_sur_le_rang,
 densite_plantation,
 replace(replace(forme_fruitiere_verger,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forme_fruitiere_verger,
 hauteur_frondaison,
 epaisseur_frondaison,
 replace(replace(forme_fruitiere_vigne,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as forme_fruitiere_vigne,
 replace(replace(orientation_rangs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as orientation_rangs,
 taux_mortalite_plantation,
 annee_mesure_taux_mortalite,
 replace(replace(type_enherbement,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as type_enherbement,
 replace(replace(couvert_vegetal_commentaire,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as couvert_vegetal_commentaire,
 (CASE pollinisateurs WHEN TRUE THEN 'oui' else 'non' END) as pollinisateurs,
 pourcentage_de_pollinisateurs,
 replace(replace(mode_repartition_pollinisateurs,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as mode_repartition_pollinisateurs
 FROM exports_agronomes_context_perennes_realise
 "

query_name[20]="zones"
query[20]="
 select
 domaine_code,
 domaine_id,
 replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
 domaine_campagne,
 parcelle_id,
 zone_id,
 replace(replace(zone_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_nom,
 zone_surface,
 latitude,
 longitude,
 replace(replace(zone_type,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as zone_type
 FROM exports_agronomes_context_zones
 "

query_name[21]="parcelles_non_rattachees"
query[21]="
 SELECT
 replace(replace(reseaux_it,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') AS reseaux_it,
 replace(replace(reseaux_ir,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') AS reseaux_ir,
 replace(replace(codes_convention_dephy,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') AS codes_convention_dephy,
 domaine_id,
 domaine_code,
 replace(replace(domaine_nom,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as domaine_nom,
 domaine_campagne,
 nb_parcelles_sans_sdc,
 nb_parcelles_avec_id_edaplos
 FROM exports_agronomes_parcelles_non_rattachees
 "

query_name[22]="reportregional"
query[22]="
  SELECT
  replace(replace(bdcr_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"ID du Bilan de campagne\",
  replace(replace(author,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Rédacteur du bilan de campagne\",
  replace(replace(name,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>')  as \"Nom du Bilan de campagne\",
  replace(replace(reseau,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Réseau\",
  campaign as \"Campagne\",
  replace(replace(highlights,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Faits marquants de l’année (climat, bioagresseurs…)\",
  rainfallmarchjune as \"Pluviométrie : nombre de mm du 1er mars au 30 juin\",
  rainfalljulyoctober as \"Nombre de jours de pluie du 1er mars au 30 juin\",
  rainydaysmarchjune as \"Pluviométrie : nombre de mm du 1er juillet au 31 octobre\",
  primarycontaminations as \"INOKI : nombre de contaminations primaires\",
  numberofdayswithprimarycontaminations as \"INOKI : nombre de jours avec contaminations primaires\",
  secondarycontaminations as \"RIM Pro : nombre de contaminations secondaires (fruit)\",
  rimsum as \"RIM Pro : somme des RIM sur l’année\",
  replace(replace(pestpressure_id,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"ID de pression\",
  replace(replace(reference_param,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Type de pression\",
  replace(replace(groupe_cible_maa,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Groupe cible\",
  replace(replace(refBioAgressorLabel,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Nom maladie/ravageur\",
  replace(replace(crops,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Cultures concernées\",
  replace(replace(pressureScale,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Pression de l’année (échelle régionale)\",
  replace(replace(pressureEvolution,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Evolution de la pression par rapport à la campagne précédente\",
  replace(replace(comment,CHR(13)||CHR(10),'<br>'),CHR(10),'<br>') as \"Commentaires\"
FROM perf_reportregional
"

mkdir -p $repertoire_export

# ajout de la configuration de authentication à la base de données postgresql
echo $pg_host:$pg_port:$pg_database:$pg_user:$pg_password > $HOME/.pgpass
chmod 600 $HOME/.pgpass

# fill up export table
echo "fill up export table with 'all-in-one.sql'"
psql -U$pg_user -h $pg_host -p $pg_port -d $pg_database -f "all-in-one.sql"

# export to csv
nb_queries=${#query[@]}
for ((i=1;i<=nb_queries;i++)) do

        echo "i= "$i"/"$nb_queries
        echo "lancement requete ${query_name[$i]}"

        #echo "requete = "
        #echo ${query[$i]}
        nom_fic=$prefixe${query_name[$i]}$suffixe
        fichier_export=$repertoire_export$nom_fic

        echo "fichier_export="$fichier_export
        #liste des noms de fichiers pour la compression
        lst_noms_fic=$lst_noms_fic" "$nom_fic

        commande_export="COPY("${query[$i]}") TO STDOUT WITH (FORMAT csv, HEADER, DELIMITER '@', FORCE_QUOTE *, NULL '')"

        #echo $commande_export
        psql -U$pg_user -h $pg_host -p $pg_port -d $pg_database  -c "$commande_export" > $fichier_export

        echo "${query_name[$i]}" exporté

        # il y a 22 requêtes dans ce script, + ce n'est pas normal
        if [ $i -gt 22 ]
        then
           echo "securite"
           break
    fi
done

fic_archive=$repertoire_export$prefixe"-archive".tar.gz
#echo tar -cvzf $fic_archive -C $repertoire_export $lst_noms_fic
tar -cvzf $fic_archive -C $repertoire_export $lst_noms_fic

#echo mail -s '[AGS] Fichiers de suivi des saisies des IR' -A $fic_archive agrosyst-team@inrae.fr <<< 'Fichiers de suivi des saisies des IR'
#mail -s '[AGS] Fichiers de suivi des saisies des IR' -A $fic_archive agrosyst-team@inrae.fr <<< 'Fichiers de suivi des saisies des IR'
#echo mail envoye

echo "fin du job"