-----------------------------------------------------
-- INTEGRATION DES BILANS DE CAMPAGNE SDC aux exports
-----------------------------------------------------

drop table IF EXISTS exports_agronomes_bilancampagne_sdc;
drop table IF EXISTS bcsdc_commun_filieres;


/* Pour l'instant le script utilise une table que l'on importe a partir d'un csv pour avoir les traductions entre le code qu'il y a en base et ce qui est affiché sur l'interface. Ces traductions ne sont pas présentes en base 
!!! Il faut donc importer au préalable le csv !!!
 */
-- select * from trad_libelles_reportsdc tlr ;
-- drop table trad_libelles_reportsdc;

/* Creation d'une table temporaire pour stocker le contexte du bilan de campagne sdc. Pour tous les BC sdc renseignés dans la table reportgrowingsystem (donc toutes filieres confondues)
 * Une fois que toutes les rubriques auront été insérées avec le BC sdc id, il restera a integrer le contexte grace à cette table et un update des lignes selon BC sdc id. Cela permet d'alléger le code
 */
CREATE temporary TABLE bcsdc_commun_filieres as
select 
rgs.author auteur,
rgs.topiaid bcsdc_id,
rgs.name bcsdc_nom,
rgs.code bcsdc_code,
rgs.reportregional bcregional_associe_id,
rr.name bcregional_associe_nom,
nr.reseau reseau,
d.topiaid domaine_id,
d.name domaine_nom,
d.code domaine_code,
d.type domaine_type,
d.campaign campagne,
gp.topiaid dispositif_id,
gp.name dispositif_nom,
gp.code dispositif_code,
gp.type dispositif_type,
gs.sector filiere_sdc,
rgs_sect.filiere_bcsdc,
gs.topiaid sdc_id,
gs.name sdc_nom,
gs.code sdc_code,
reftypa.reference_label sdc_type_agriculture,
gs.dephynumber sdc_code_dephy,
mm1.topiaid modele_descisionelassocie_prevu_id,
mm2.topiaid modele_descisionelassocie_obs_id,
-- faits marquants
rgs.iftestimationmethod methode_estimation_IFT_declares,
rgs.highlightsevolutions principales_evolutions_depuis_pz0,
rgs.highlightsmeasures mesure_specifique_annee,
rgs.highlightsperformances faits_marquants_conduiteculture_performancetechnique,
rgs.highlightsteachings enseignements_amelioration_sdc
from reportgrowingsystem rgs
left JOIN reportregional rr on rgs.reportregional = rr.topiaid -- pas toujours de BC regional associe (pareil pour le reseau puisque on l'a depuis BC regional)
left join (select nrg.reportregional, string_agg(distinct n.name,'|') reseau from networks_reportregional nrg -- Quand il y a plusieurs IR pour un meme id de rr, on agrege les noms d'IR sur la meme ligne  
	join network n on nrg.networks = n.topiaid group by nrg.reportregional) nr on nr.reportregional = rr.topiaid
join growingsystem gs on rgs.growingsystem = gs.topiaid 
join growingplan gp on gs.growingplan = gp.topiaid 
join domain d on gp.domain = d.topiaid
join reftypeagriculture reftypa on gs.typeagriculture = reftypa.topiaid 
left join (select * from managementmode where category = 'PLANNED') mm1 on gs.topiaid = mm1.growingsystem 
left join (select * from managementmode where category = 'OBSERVED') mm2 on gs.topiaid = mm2.growingsystem
join (select owner, string_agg(sectors,'|') filiere_bcsdc from reportgrowingsystem_sectors group by owner) rgs_sect on rgs_sect.owner = rgs.topiaid;

CREATE INDEX bcsdc_commun_filieres_idx1 on bcsdc_commun_filieres(bcsdc_id);


/* Creation de la table que l'on exportera. On va remplir au fur et a mesure selon les differentes fillieres et differentes rubriques
 * clé unique : bcsdc_id + 1 type de rubrique (maladie,ravageur,adventice,verse,alimentation minerale et rendement) + pour les maladies,ravageur,adventice : un bioagresseur (espece definie) et/ou groupe cible + un ensemble d'espece+varietes d'une culture  
 */
CREATE TABLE exports_agronomes_bilancampagne_sdc(
auteur text,
reseau text,
bcsdc_id varchar(255), bcsdc_nom text,bcsdc_code text,
bcregional_associe_id varchar(255), bcregional_associe_nom text,
domaine_id varchar(255), domaine_nom text,domaine_code text, 
domaine_type text,
campagne integer,
dispositif_id varchar(255), dispositif_nom text,dispositif_code text,
dispositif_type text,
filiere_sdc varchar(255), -- la filliere associee au sdc
filiere_bcsdc varchar(255), -- les fillieres cochees par l'utilisateur dans les bc sdc. Parfois il y en a plusieurs cochees 
sdc_id varchar(255), sdc_nom text,sdc_code text,
sdc_type_agriculture text,
sdc_code_dephy text,
modele_descisionelassocie_prevu_id varchar(255),
modele_descisionelassocie_obs_id varchar(255),
--
-- Generalites pour tous : faits marquants de l'annee
methode_estimation_IFT_declares text,
principales_evolutions_depuis_pz0 text,
mesure_specifique_annee text,
faits_marquants_conduiteculture_performancetechnique text,
enseignements_amelioration_sdc text,--
-- 1 ligne : specifique a 1 rurbique :  maladie ou a un ravageur ou une adventice ou verse ou rendement ou alimentation minerale , et ce pour chaque une culture // esp_variete
type_rubrique text,
culture_id varchar(255),culture_nom text,culture_code text,
espece_variete text,
--
-- MALADIES ET RAVAGEURS = bioagresseur
maitrise_bioagresseur_id varchar(255),
groupe_cible text,
bioagresseur text,
-- global : a l'echelle du sdc , on separe les IFT
maladie_sdc_IFT_f_chimique float8,
maladie_sdc_IFT_f_biocontrole float8,
maladie_sdc_qte_cuivre_kgha float8,
maladie_sdc_niveau_global_maitrise varchar(255),
ravageur_sdc_IFT_chimique float8,
ravageur_sdc_IFT_biocontrole float8,
ravageur_sdc_niveau_global_maitrise varchar(255),
-- pression 
bioagresseur_echelle_pression varchar(255),
bioagresseur_evolution_pression varchar(255),
bioagresseur_pression_agricommentaire text,
bioagresseur_inoculum_annee_precedente_arbo varchar(255),
-- resultats
-- echelle maitrise
bioagresseur_result_echelle_maitrise_code text,
bioagresseur_result_echelle_maitrise_libelle_filiere text,
-- qte
maladie_result_note_globale_attaque_feuille_vitif varchar(255),
maladie_result_note_globale_attaque_grappe_vitif varchar(255),
ravageur_result_note_globale_attaque_feuille_vitif varchar(255),
ravageur_result_note_globale_attaque_grappe_vitif varchar(255),
bioagresseur_parcelles_touchees_pourcent_arbo varchar(255),
bioagresseur_arbres_touches_pourcent_arbo varchar(255),
bioagresseur_fruits_dommage_pourcent_arbo varchar(255),
bioagresseur_feuillespousses_dommage_pourcent_arbo varchar(255),
bioagresseur_inoculum_annee_suivante_arbo varchar(255),
-- satisfaction 
bioagresseur_result_matrise_qualifiant varchar(255),
bioagresseur_result_agricommentaire text,
bioagresseur_result_conseillercommentaire text,
-- IFT 
maladie_nb_traitement_viti float8,
maladie_IFT_f_chimique_viti float8,
maladie_IFT_f_biocontrol_viti float8,
maladie_nb_traitement_arbo float8,
maladie_IFT_f_chimique_arbo float8,
maladie_IFT_f_biocontrol_arbo float8,
maladie_IFT_f_cultassolee float8,
maladie_IFT_commentaire_conseiller_experim text,
ravageur_nb_traitement_totaux_viti float8,
ravageur_nb_traitement_obligatoires_viti float8,
ravageur_IFT_chimique_viti float8,
ravageur_IFT_biocontrol_viti float8,
ravageur_nb_traitement_arbo float8,
ravageur_IFT_chimique_arbo float8,
ravageur_IFT_biocontrol_arbo float8,
ravageur_IFT_insect_cultassolee float8,
ravageur_IFT_autre_rava_cultassolee float8,
ravageur_IFT_commentaire_conseiller_experim text,
--- EXPE Bioagresseur
EXPE_bioagresseur_echelle_pression_marhorti integer,
EXPE_bioagresseur_result_echelle_maitrise_marhorti integer,
EXPE_maladie_result_note_fq_attaque_feuille_viti varchar(255),
EXPE_maladie_result_note_fq_attaque_grappe_viti varchar(255),
EXPE_maladie_result_note_intensite_attaque_feuille_viti varchar(255),
EXPE_maladie_result_note_intensite_attaque_grappe_viti varchar(255),
EXPE_ravageur_result_nb_perforations_100grappes_viti float8,
EXPE_bioagresseur_result_matrise_qualifiant_arbo varchar(255), -- plus de modalites proposees 
EXPE_maladie_IFT_f_chimique_marhorti float8,
EXPE_maladie_IFT_hors_biocontrol_marhorti float8,
EXPE_ravageur_IFT_insecticide_chimique_marhorti float8,
EXPE_ravageur_IFT_hors_biocontrol_marhorti float8,
--
-- ADVENTICE : global
maitrise_adventice_id varchar(255),
adventice_agresseur text,
-- pression
adventice_echelle_pression_vitif varchar(255),  -- sol nu,globalement nu ..., enherbement sans concurence ..., enherbement generalise , 4 modalites
adventice_echelle_pression_cultassolee varchar(255), -- nulle,faible , moyenne (1er ronds), forte (concurence ...) , 4 modalites
adventice_echelle_pression_arbo varchar(255), -- tres faible -> tres fort 5 modalites
adventice_pression_evolution_annee_precedente_arbof  varchar(255),
adventice_pression_agricommentaire text,
-- resultats
adventice_result_echelle_maitrise_arbof varchar(255), -- (sol propre toute saison, sol propre a certaine periode, enherbement …) , 4 modalites 
adventice_result_echelle_maitrise_cultassolee varchar(255), --(pas presence, presence sans concurence, presence de 1er ronds, concurence en voie de generali), 4 modalites
adventice_result_maitrise_qualifiant_viti varchar(255), -- (tres -> pas du tout satisfaisant) 4 modalites
adventice_result_maitrise_qualifiant varchar(255), -- satisfaisant ou non 2 modalites
adventice_result_niveau_maitrise_conseillercommentaire_arbof text,
adventice_result_niveau_maitrise_agricommentaire text,
-- IFT 
adventice_nbtrait_chim_herbicide_horsepamprage_viti float8,
adventice_nbtrait_biocontrol_herbicide_horsepamprage_viti float8,
adventice_nbtrait_chim_herbicide_epamprage_viti float8,
adventice_nbtrait_biocontrol_herbicide_epamprage_viti float8,
adventice_IFT_chim_herbicide_horsepamprage_viti float8,
adventice_IFT_biocontrol_herbicide_horsepamprage_viti float8,
adventice_IFT_chim_herbicide_epamprage_viti float8,
adventice_IFT_biocontrol_herbicide_epamprage_viti float8,
adventice_nb_traitement_arbo float8,
adventice_IFT_h_cultassolee float8,
adventice_IFT_herbicide_chimique_arbo float8,
adventice_IFT_herbicide_biocontrol_arbo float8,
adventice_commentaire_conseiller_experi text,
-- EXPE adventices
EXPE_adventice_echelle_pression_marhorti integer,-- 1 à 10
EXPE_adventice_result_echelle_maitrise_marhorti integer,-- 1 à 10 
EXPE_adventice_echelle_maitrise_visee_arbo varchar(255), -- ! en base est dans la meme colonne que echelle de maitrise 'masterscale' d'adventice , meme si c'est precise visé
EXPE_adventice_maitrise_qualifiant_arbo varchar(255), -- (tres -> pas du tout satisfaisant) 4 modalites
--
-- Maitrise de la verse 
verse_id varchar(255),
verse_echelle_risque varchar(255),
verse_risque_agricommentaire text,
verse_results_echelle_maitrise varchar(255),
verse_results_qualifiant varchar(255),
verse_results_agricommentaire text,
verse_IFT_regulateur float8,
verse_IFT_conseillercommentaire text,
--
-- Hydrique mineral 
maitrise_hydrique_mineral_id varchar(255),
irrigation varchar(255),
stress_hydrique varchar(255),
stress_azote varchar(255),
alimentation_minerale_hors_azote text,
stress_temperature_rayonnement varchar(255),
maitrise_alim_hydrique_mineral_commentaire text,
--
-- Rendement qualite 
rendement_qualite_id varchar(255),
objectif_rendement varchar(255),
rendement_cause1 varchar(255),
rendement_cause2 varchar(255),
rendement_cause3 varchar(255),
qualite_commentaire text,
rendementqualite_commentaires_global text);

-------------------------------------
---- POUR TOUTES FILLIERES SAUF VITICULTURE (viticulture est a part puisque referentiel variete different et les donnees ne sont pas dans les memes tables)
---- !!! il y a des cas où il y a 'viticulture + autre filliere' qui ont été cochées lors de la saisie du BC sdc
---- => quand ce n'est pas viticulture seul, on selectionne quand meme la ligne dans bcsdc_commun_filieres

-- ALIMENTATION HYDRIQUE ET MINERALE  
------------------------------------
insert into exports_agronomes_bilancampagne_sdc(type_rubrique,bcsdc_id,culture_id ,culture_nom,culture_code,espece_variete,
maitrise_hydrique_mineral_id,irrigation ,stress_hydrique ,stress_azote , alimentation_minerale_hors_azote ,stress_temperature_rayonnement ,maitrise_alim_hydrique_mineral_commentaire)
select 
'alim minerale' type_rubrique,
rgs.topiaid bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
fm.topiaid maitrise_hydrique_mineral_id,
trad1.traduction_interface irrigation ,
trad2.traduction_interface stress_hydrique ,
trad3.traduction_interface stress_azote ,
fm.mineralfood alimentation_minerale_hors_azote ,
trad4.traduction_interface stress_temperature_rayonnement ,
fm.comment maitrise_alim_hydrique_mineral_commentaire
from (select * from bcsdc_commun_filieres where filiere_bcsdc <> 'VITICULTURE') bcsdc 
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid
join foodmaster fm on rgs.topiaid = fm.foodmasterreportgrowingsystem
join crops_foodmaster cf on cf.foodmaster = fm.topiaid 
join croppingplanentry cpe on cpe.topiaid = cf.crops 
-- joindre les esp/varietes concernees :  		(left join puisque parfois il n'y a pas de espece associe a la culture)
-- a la table foodmaster_species on joint les esp puis varietes. on groupe selon l'id de foodmaster ET id culture pour concatener les esp/varietes  (meme principe pour les requetes suivants qui ne concerne pas la viticulture)
-- id de foodmaster + id culture = cle pour joindre au reste
left join (select fms.foodmaster, cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from foodmaster_species fms 
	join croppingplanspecies cps on fms.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid -- left join puisque la variete n'est pas obligatoire
	group by fms.foodmaster, cpe.topiaid) esp_var on esp_var.foodmaster = fm.topiaid and esp_var.culture_id = cpe.topiaid
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'irrigation') trad1 on fm.foodirrigation = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'stress hydrique mineral temperature') trad2 on fm.hydriquestress = trad2.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'stress hydrique mineral temperature') trad3 on fm.azotestress = trad3.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'stress hydrique mineral temperature') trad4 on fm.tempstress = trad4.nom_base;


-- rendement ,  par rapport a viti les donnees sont dans la table yieldloss 
-----------------------
insert into exports_agronomes_bilancampagne_sdc(type_rubrique,bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
rendement_qualite_id , objectif_rendement ,rendement_cause1,rendement_cause2,rendement_cause3,qualite_commentaire ,rendementqualite_commentaires_global)
select 
'rendement qualite' type_rubrique ,
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
yl.topiaid rendement_qualite_id , 
case 
	when yl.yieldobjectiveint is null then yl.yieldobjective
	when yl.yieldobjectiveint is not null then trad1.traduction_interface -- c'est une echelle integer mais il y a 4 modalites , les mm que celle avec menu deroulant sans echelle integer. on les regroupe sous la meme colonne de l export
end objectif_rendement,
yl.cause1 rendement_cause1,
yl.cause2 rendement_cause2,
yl.cause3 rendement_cause3,
yl.comment qualite_commentaire ,
yi.comment rendementqualite_commentaires_global
from (select * from bcsdc_commun_filieres where filiere_bcsdc <> 'VITICULTURE') bcsdc
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid
join yieldloss yl on rgs.topiaid = yl.reportgrowingsystem 
left join yieldinfo yi on rgs.topiaid = yi.reportgrowingsystem -- left join : le champs rendementqualite_commentaires_global n'est pas toujours renseigné
join crops_yieldloss cy on cy.yieldloss = yl.topiaid
join croppingplanentry cpe on cy.crops = cpe.topiaid 
left join (select syl.yieldloss , cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from species_yieldloss syl
	join croppingplanspecies cps on syl.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid
	group by syl.yieldloss, cpe.topiaid) esp_var on esp_var.yieldloss = yl.topiaid and esp_var.culture_id = cpe.topiaid
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'objectif rendement echelle int') trad1 on cast(yl.yieldobjectiveint as text) = trad1.nom_base;

-------------------------------------
---- INSERTION VITICULTURE ----

-- !!! en viti, les cultures ne sont associees que dans la rubrique alimentation minerale.
--  Pour les autres rubriques on suppose que toutes les cultures (et esp/varietes sont consernees par les données saisies)
-- ce qui fait que on joint la table croppingplanentry avec le lien du domaine

-- Insertion MALADIES 
---------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
maitrise_bioagresseur_id,
type_rubrique,
maladie_sdc_IFT_f_chimique,maladie_sdc_IFT_f_biocontrole,maladie_sdc_qte_cuivre_kgha,maladie_sdc_niveau_global_maitrise,
groupe_cible,bioagresseur,
bioagresseur_echelle_pression,bioagresseur_evolution_pression,bioagresseur_pression_agricommentaire,
bioagresseur_result_echelle_maitrise_code,bioagresseur_result_echelle_maitrise_libelle_filiere,
maladie_result_note_globale_attaque_feuille_vitif,maladie_result_note_globale_attaque_grappe_vitif,
EXPE_maladie_result_note_fq_attaque_feuille_viti,EXPE_maladie_result_note_intensite_attaque_feuille_viti,EXPE_maladie_result_note_fq_attaque_grappe_viti,EXPE_maladie_result_note_intensite_attaque_grappe_viti,
bioagresseur_result_matrise_qualifiant,bioagresseur_result_agricommentaire,bioagresseur_result_conseillercommentaire,
maladie_nb_traitement_viti,maladie_IFT_f_chimique_viti,maladie_IFT_f_biocontrol_viti)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
vpm.topiaid maitrise_bioagresseur_id,
'maladie' type_rubrique,
-- MALADIES : maitrise globale
rgs.vitidiseasechemicalfungicideift maladie_sdc_IFT_f_chimique,
rgs.vitidiseasebiocontrolfungicideift maladie_sdc_IFT_f_biocontrole,
rgs.vitidiseasecopperquantity maladie_sdc_qte_cuivre_kgha,
rgs.vitidiseasequalifier maladie_sdc_niveau_global_maitrise,
-- pression pour 1 maladie
refgrpcible.groupe_cible_maa groupe_cible,
refnui.reference_label bioagresseur,
trad3.traduction_interface bioagresseur_echelle_pression,
vpm.pressureevolution bioagresseur_evolution_pression,
vpm.pressurefarmercomment bioagresseur_pression_agricommentaire,
-- resultats
-- echelle maitrise
vpm.masterscale bioagresseur_result_echelle_maitrise_code,
trad4.traduction_interface bioagresseur_result_echelle_maitrise_libelle_filiere ,
trad1.traduction_interface maladie_result_note_globale_attaque_feuille_vitif,
trad2.traduction_interface maladie_result_note_globale_attaque_grappe_vitif,
vpm.leafdiseaseattackrateprecisevalue EXPE_maladie_result_note_fq_attaque_feuille_viti,
vpm.leafdiseaseattackintensityprecisevalue EXPE_maladie_result_note_intensite_attaque_feuille_viti,
vpm.grapediseaseattackrateprecisevalue EXPE_maladie_result_note_fq_attaque_grappe_viti,
vpm.grapediseaseattackintensityprecisevalue EXPE_maladie_result_note_intensite_attaque_grappe_viti,
vpm.qualifier bioagresseur_result_matrise_qualifiant,
vpm.resultfarmercomment bioagresseur_result_agricommentaire,
vpm.advisercomments bioagresseur_result_conseillercommentaire,
-- IFT 
vpm.treatmentcount maladie_nb_traitement_viti,
vpm.chemicalfungicideift maladie_IFT_f_chimique_viti,
vpm.biocontrolfungicideift maladie_IFT_f_biocontrol_viti
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%VITICULTURE%') bcsdc  -- on prend tous les bcsdc quand ils ont coche viticulture , pas ceux uniquement viticulture
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid  
join croppingplanentry cpe on cpe.domain = bcsdc.domaine_id
left join (select cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvarviti.variete||')', ''),'|') espece_variete 
	from croppingplanentry cpe
	join croppingplanspecies cps on cps.croppingplanentry = cpe.topiaid 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarieteplantgrape refvarviti on cps.variety = refvarviti.topiaid
	group by cpe.topiaid) esp_var on esp_var.culture_id = cpe.topiaid 
join vitipestmaster vpm on rgs.topiaid = vpm.reportgrowingsystemvitidiseasemaster
left join refnuisibleedi refnui on vpm.agressor = refnui.topiaid
left join (select distinct code_groupe_cible_maa,groupe_cible_maa from refciblesagrosystgroupesciblesmaa where active = true and groupe_cible_maa not in ('Cicadelles cercopides et psylles','Maladies des taches foliaires')) refgrpcible -- on retire les doublons de code 38 'Cicadelles cercopides et psylles' puisque ce nom est utilisé par le 37 , et le 82 puisqu'il y a deux orthographes 
		on refgrpcible.code_groupe_cible_maa = vpm.codegroupeciblemaa 
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'Note globale attaque maladie feuille') trad1 on vpm.leafdiseaseattackrate = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'Note globale attaque maladie grappe') trad2 on vpm.grapediseaseattackrate = trad2.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de pression maladie ravageur') trad3 on vpm.pressurescale = trad3.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise maladie ravageur viti') trad4 on vpm.masterscale = trad4.nom_base;

-- Insertion RAVAGEURS 
----------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
maitrise_bioagresseur_id,
type_rubrique,
ravageur_sdc_IFT_chimique ,ravageur_sdc_IFT_biocontrole, ravageur_sdc_niveau_global_maitrise,
groupe_cible,bioagresseur,
bioagresseur_echelle_pression,bioagresseur_evolution_pression,bioagresseur_pression_agricommentaire,bioagresseur_result_echelle_maitrise_code,bioagresseur_result_echelle_maitrise_libelle_filiere,
ravageur_result_note_globale_attaque_feuille_vitif,ravageur_result_note_globale_attaque_grappe_vitif,EXPE_ravageur_result_nb_perforations_100grappes_viti,
bioagresseur_result_matrise_qualifiant,bioagresseur_result_agricommentaire,bioagresseur_result_conseillercommentaire,
ravageur_nb_traitement_totaux_viti,ravageur_nb_traitement_obligatoires_viti,ravageur_IFT_chimique_viti,ravageur_IFT_biocontrol_viti)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
vpm.topiaid maitrise_bioagresseur_id,
'ravageur' type_rubrique,
-- RAVAGEURS : maitrise globale
rgs.vitipestchemicalpestift ravageur_sdc_IFT_chimique ,
rgs.vitipestbiocontrolpestift ravageur_sdc_IFT_biocontrole ,
rgs.vitipestqualifier ravageur_sdc_niveau_global_maitrise,
-- RAVAGEUR : specifique a 1 grp 
-- presion
refgrpcible.groupe_cible_maa groupe_cible,
refnui.reference_label bioagresseur,
trad3.traduction_interface bioagresseur_echelle_pression,
vpm.pressureevolution bioagresseur_evolution_pression,
vpm.pressurefarmercomment bioagresseur_pression_agricommentaire,
-- resultats
-- echelle maitrise
vpm.masterscale bioagresseur_result_echelle_maitrise_code,
trad4.traduction_interface bioagresseur_result_echelle_maitrise_libelle_filiere ,
trad1.traduction_interface ravageur_result_note_globale_attaque_feuille_vitif,
trad2.traduction_interface ravageur_result_note_globale_attaque_grappe_vitif,
vpm.nbpestgrapeperforation EXPE_ravageur_result_nb_perforations_100grappes_viti,
vpm.qualifier bioagresseur_result_matrise_qualifiant,
vpm.resultfarmercomment bioagresseur_result_agricommentaire,
vpm.advisercomments bioagresseur_result_conseillercommentaire,
-- IFT 
vpm.treatmentcount ravageur_nb_traitement_totaux_viti,
vpm.nbpesttraitementrequired ravageur_nb_traitement_obligatoires_viti,
vpm.chemicalfungicideift ravageur_IFT_chimique_viti,
vpm.biocontrolfungicideift ravageur_IFT_biocontrol_viti
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%VITICULTURE%') bcsdc
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid 
join croppingplanentry cpe on cpe.domain = bcsdc.domaine_id
left join (select cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvarviti.variete||')', ''),'|') espece_variete 
	from croppingplanentry cpe
	join croppingplanspecies cps on cps.croppingplanentry = cpe.topiaid 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarieteplantgrape refvarviti on cps.variety = refvarviti.topiaid
	group by cpe.topiaid) esp_var on esp_var.culture_id = cpe.topiaid 
join vitipestmaster vpm on rgs.topiaid = vpm.reportgrowingsystemvitipestmaster
left join refnuisibleedi refnui on vpm.agressor = refnui.topiaid
left join (select distinct code_groupe_cible_maa,groupe_cible_maa from refciblesagrosystgroupesciblesmaa where active = true and groupe_cible_maa not in ('Cicadelles cercopides et psylles','Maladies des taches foliaires')) refgrpcible -- on retire les doublons de code 38 'Cicadelles cercopides et psylles' puisque ce nom est utilisé par le 37 , et le 82 puisqu'il y a deux orthographes 
		on refgrpcible.code_groupe_cible_maa = vpm.codegroupeciblemaa 
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'Note globale attaque ravageurs feuille') trad1 on vpm.leafpestattackrate = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'Note globale attaque ravageurs grappe') trad2 on vpm.grapepestattackrate = trad2.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de pression maladie ravageur') trad3 on vpm.pressurescale = trad3.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise maladie ravageur viti') trad4 on vpm.masterscale = trad4.nom_base;


-- Insertion ADVENTICES 
-----------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
maitrise_adventice_id,
type_rubrique,
adventice_agresseur,
adventice_echelle_pression_vitif ,
adventice_pression_agricommentaire ,
adventice_result_maitrise_qualifiant_viti ,
adventice_result_niveau_maitrise_agricommentaire ,
adventice_nbtrait_chim_herbicide_epamprage_viti, -- deduction que suckering = epranprage
adventice_nbtrait_biocontrol_herbicide_epamprage_viti,
adventice_IFT_chim_herbicide_epamprage_viti,
adventice_IFT_biocontrol_herbicide_epamprage_viti,
adventice_nbtrait_chim_herbicide_horsepamprage_viti,
adventice_nbtrait_biocontrol_herbicide_horsepamprage_viti,
adventice_IFT_chim_herbicide_horsepamprage_viti,
adventice_IFT_biocontrol_herbicide_horsepamprage_viti)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
bcsdc.bcsdc_id maitrise_adventice_id, -- en viti , les infos sur les adventices sont renseignées dans la table rgs
'adventice' type_rubrique,
'Non precise en viti' adventice_agresseur,
-- ADVENTICES : pression
trad1.traduction_interface adventice_echelle_pression_vitif ,
rgs.vitiadventicepressurefarmercomment adventice_pression_agricommentaire ,
rgs.vitiadventicequalifier adventice_result_maitrise_qualifiant_viti ,
rgs.vitiadventiceresultfarmercomment adventice_result_niveau_maitrise_agricommentaire ,
rgs.vitisuckeringchemical adventice_nbtrait_chim_herbicide_epamprage_viti, -- deduction que suckering = epranprage
rgs.vitisuckeringbiocontrol adventice_nbtrait_biocontrol_herbicide_epamprage_viti,
rgs.vitisuckeringchemicalift adventice_IFT_chim_herbicide_epamprage_viti,
rgs.vitisuckeringbiocontrolift adventice_IFT_biocontrol_herbicide_epamprage_viti,
rgs.vitiherbotreatmentchemical adventice_nbtrait_chim_herbicide_horsepamprage_viti,
rgs.vitiherbotreatmentbiocontrol adventice_nbtrait_biocontrol_herbicide_horsepamprage_viti,
rgs.vitiherbotreatmentchemicalift adventice_IFT_chim_herbicide_horsepamprage_viti,
rgs.vitiherbotreatmentbiocontrolift adventice_IFT_biocontrol_herbicide_horsepamprage_viti
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%VITICULTURE%') bcsdc
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid
join croppingplanentry cpe on cpe.domain = bcsdc.domaine_id
left join (select cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvarviti.variete||')', ''),'|') espece_variete
	from croppingplanentry cpe
	join croppingplanspecies cps on cps.croppingplanentry = cpe.topiaid 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarieteplantgrape refvarviti on cps.variety = refvarviti.topiaid
	group by cpe.topiaid) esp_var on esp_var.culture_id = cpe.topiaid
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle pression adventice viti') trad1 on rgs.vitiadventicepressurescale = trad1.nom_base;

-- Insertion RENDEMENT ET QUALITE
-----------------------
insert into exports_agronomes_bilancampagne_sdc(type_rubrique,bcsdc_id,culture_id ,culture_nom,culture_code,espece_variete,
rendement_qualite_id,objectif_rendement,rendement_cause1,rendement_cause2,rendement_cause3,qualite_commentaire,rendementqualite_commentaires_global)
select 
'rendement qualite' type_rubrique,
rgs.topiaid bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
rgs.topiaid rendement_qualite_id,
rgs.vitiyieldobjective objectif_rendement,
rgs.vitilosscause1 rendement_cause1,
rgs.vitilosscause2 rendement_cause2,
rgs.vitilosscause3 rendement_cause3,
rgs.vitiyieldquality qualite_commentaire,
yi.comment rendementqualite_commentaires_global
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%VITICULTURE%') bcsdc
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid
join croppingplanentry cpe on cpe.domain = bcsdc.domaine_id
left join (select cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvarviti.variete||')', ''),'|') espece_variete 
	from croppingplanentry cpe
	join croppingplanspecies cps on cps.croppingplanentry = cpe.topiaid 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarieteplantgrape refvarviti on cps.variety = refvarviti.topiaid
	group by cpe.topiaid) esp_var on esp_var.culture_id = cpe.topiaid
left join yieldinfo yi on rgs.topiaid = yi.reportgrowingsystem -- left join : le champs rendementqualite_commentaires_global n'est pas toujours renseigné
where rgs.vitiyieldobjective is not null -- n'insert la ligne que si il y a des donnees de rendement qualite
	or rgs.vitilosscause1 is not null or rgs.vitilosscause2 is not null or rgs.vitilosscause3 is not null
	or rgs.vitiyieldquality is not null 
	or yi.comment is not null;

-- Insertion ALIMENTATION MINERALE  
-----------------------------------
insert into exports_agronomes_bilancampagne_sdc(type_rubrique,bcsdc_id,culture_id ,culture_nom,culture_code,espece_variete,
maitrise_hydrique_mineral_id,irrigation ,stress_hydrique ,stress_azote , alimentation_minerale_hors_azote ,stress_temperature_rayonnement ,maitrise_alim_hydrique_mineral_commentaire)
select 
'alim minerale' type_rubrique,
rgs.topiaid bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
fm.topiaid maitrise_hydrique_mineral_id,
trad1.traduction_interface irrigation ,
trad2.traduction_interface stress_hydrique ,
trad3.traduction_interface stress_azote ,
fm.mineralfood alimentation_minerale_hors_azote ,
trad4.traduction_interface stress_temperature_rayonnement ,
fm.comment maitrise_alim_hydrique_mineral_commentaire
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%VITICULTURE%') bcsdc
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid
join foodmaster fm on rgs.topiaid = fm.foodmasterreportgrowingsystem
join crops_foodmaster cf on cf.foodmaster = fm.topiaid 
join croppingplanentry cpe on cpe.topiaid = cf.crops 
left join (select fms.foodmaster, cpe.topiaid culture_id , string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvarviti.variete||')', ''),'|') espece_variete 
	from foodmaster_species fms
	join croppingplanspecies cps on fms.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarieteplantgrape refvarviti on cps.variety = refvarviti.topiaid 
	group by fms.foodmaster,cpe.topiaid) esp_var on esp_var.foodmaster = fm.topiaid and esp_var.culture_id = cpe.topiaid 
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'irrigation') trad1 on fm.foodirrigation = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'stress hydrique mineral temperature') trad2 on fm.hydriquestress = trad2.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'stress hydrique mineral temperature') trad3 on fm.azotestress = trad3.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'stress hydrique mineral temperature') trad4 on fm.tempstress = trad4.nom_base;

-------------------------------------
---- INSERTION CULTURES ASSOLEES : MARAICHAGE , CULTURES_TROPICALES, GRANDES_CULTURES , POLYCULTURE_ELEVAGE , HORTICULTURE ----

-- Insertion MALADIES & ravageurs
---------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
type_rubrique,
maitrise_bioagresseur_id,
groupe_cible,
bioagresseur,
bioagresseur_echelle_pression,EXPE_bioagresseur_echelle_pression_marhorti,bioagresseur_pression_agricommentaire,
bioagresseur_result_echelle_maitrise_code,bioagresseur_result_echelle_maitrise_libelle_filiere,EXPE_bioagresseur_result_echelle_maitrise_marhorti,bioagresseur_result_matrise_qualifiant,bioagresseur_result_agricommentaire,
maladie_IFT_f_cultassolee,EXPE_maladie_IFT_f_chimique_marhorti,EXPE_maladie_IFT_hors_biocontrol_marhorti,
ravageur_IFT_insect_cultassolee,EXPE_ravageur_IFT_insecticide_chimique_marhorti,ravageur_IFT_autre_rava_cultassolee,EXPE_ravageur_IFT_hors_biocontrol_marhorti,
ravageur_IFT_commentaire_conseiller_experim)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
case 
	when cm.cropdiseasemasterreportgrowingsystem is not null then 'maladie'
	when cm.croppestmasterreportgrowingsystem is not null then 'ravageur'
end type_rubrique,
-- specifique a une maladie
pm.topiaid maitrise_bioagresseur_id,
refgrpcible.groupe_cible_maa groupe_cible,
refnui.reference_label bioagresseur,
trad1.traduction_interface bioagresseur_echelle_pression,
pm.pressurescaleint EXPE_bioagresseur_echelle_pression_marhorti,
pm.pressurefarmercomment bioagresseur_pression_agricommentaire,
pm.masterscale bioagresseur_result_echelle_maitrise_code,
trad2.traduction_interface bioagresseur_result_echelle_maitrise_libelle_filiere,
pm.masterscaleint EXPE_bioagresseur_result_echelle_maitrise_marhorti,
pm.qualifier bioagresseur_result_matrise_qualifiant,
pm.resultfarmercomment bioagresseur_result_agricommentaire,
case when cm.cropdiseasemasterreportgrowingsystem is not null and bcsdc.dispositif_type = 'DEPHY_FERME' then cm.iftmain end maladie_IFT_f_cultassolee,
case when cm.cropdiseasemasterreportgrowingsystem is not null and bcsdc.dispositif_type = 'DEPHY_EXPE' then cm.iftmain end EXPE_maladie_IFT_f_chimique_marhorti,
case when cm.cropdiseasemasterreportgrowingsystem is not null and bcsdc.dispositif_type = 'DEPHY_EXPE' then cm.ifthorsbiocontrole end EXPE_maladie_IFT_hors_biocontrol_marhorti,
case when cm.croppestmasterreportgrowingsystem is not null and bcsdc.dispositif_type = 'DEPHY_FERME' then cm.iftmain end ravageur_IFT_insect_cultassolee,
case when cm.croppestmasterreportgrowingsystem is not null and bcsdc.dispositif_type = 'DEPHY_EXPE' then cm.iftmain end EXPE_ravageur_IFT_insecticide_chimique_marhorti,
case when cm.croppestmasterreportgrowingsystem is not null then cm.iftother end ravageur_IFT_autre_rava_cultassolee, -- qq soit ferme ou expe
case when cm.croppestmasterreportgrowingsystem is not null and bcsdc.dispositif_type = 'DEPHY_EXPE' then cm.ifthorsbiocontrole end EXPE_ravageur_IFT_hors_biocontrol_marhorti,
cm.advisercomments ravageur_IFT_commentaire_conseiller_experim 
from (select * from bcsdc_commun_filieres where filiere_bcsdc like any (array['%MARAICHAGE%','%CULTURES_TROPICALES%','%GRANDES_CULTURES%','%POLYCULTURE_ELEVAGE%','%HORTICULTURE%'])) bcsdc 
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid 
join croppestmaster cm on (cm.cropdiseasemasterreportgrowingsystem = rgs.topiaid) or (cm.croppestmasterreportgrowingsystem = rgs.topiaid)
join pestmaster pm on pm.croppestmaster = cm.topiaid 
left join refnuisibleedi refnui on pm.agressor = refnui.topiaid
left join (select distinct code_groupe_cible_maa,groupe_cible_maa from refciblesagrosystgroupesciblesmaa where active = true and groupe_cible_maa not in ('Cicadelles cercopides et psylles','Maladies des taches foliaires')) refgrpcible -- on retire les doublons de code 38 'Cicadelles cercopides et psylles' puisque ce nom est utilisé par le 37 , et le 82 puisqu'il y a deux orthographes 
		on refgrpcible.code_groupe_cible_maa = pm.codegroupeciblemaa 
join croppestmaster_crops cpc on (cpc.croppestmaster = cm.topiaid)
join croppingplanentry cpe on cpc.crops = cpe.topiaid 
left join (select cpms.croppestmaster, cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from croppestmaster_species cpms
	join croppingplanspecies cps on cpms.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid
	group by cpms.croppestmaster, cpe.topiaid) esp_var on esp_var.croppestmaster = cpc.croppestmaster and esp_var.culture_id = cpe.topiaid 
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de pression maladie ravageur') trad1 on pm.pressurescale = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise maladie ravageur assolee') trad2 on pm.masterscale = trad2.nom_base;

-- Insertion ADVENTICES
---------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
type_rubrique,
maitrise_adventice_id,
adventice_agresseur,
adventice_echelle_pression_cultassolee,EXPE_adventice_echelle_pression_marhorti,
adventice_pression_agricommentaire,
adventice_result_echelle_maitrise_cultassolee,EXPE_adventice_result_echelle_maitrise_marhorti,
adventice_result_maitrise_qualifiant,
adventice_result_niveau_maitrise_agricommentaire,
adventice_IFT_h_cultassolee,
adventice_commentaire_conseiller_experi)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
'adventice' type_rubrique,
-- specifique a une maladie
pm.topiaid maitrise_adventice_id,
refadv.adventice adventice_agresseur,
trad1.traduction_interface adventice_echelle_pression_cultassolee,
pm.pressurescaleint EXPE_adventice_echelle_pression_marhorti,
pm.pressurefarmercomment adventice_pression_agricommentaire,
trad2.traduction_interface adventice_result_echelle_maitrise_cultassolee,
pm.masterscaleint EXPE_adventice_result_echelle_maitrise_marhorti,
pm.qualifier adventice_result_maitrise_qualifiant,
pm.resultfarmercomment adventice_result_niveau_maitrise_agricommentaire,
cm.iftmain adventice_IFT_h_cultassolee,
cm.advisercomments adventice_commentaire_conseiller_experi
from (select * from bcsdc_commun_filieres where filiere_bcsdc like any (array['%MARAICHAGE%','%CULTURES_TROPICALES%','%GRANDES_CULTURES%','%POLYCULTURE_ELEVAGE%','%HORTICULTURE%'])) bcsdc  
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid 
join croppestmaster cm on cm.cropadventicemasterreportgrowingsystem = rgs.topiaid
join pestmaster pm on pm.croppestmaster = cm.topiaid 
left join refadventice refadv on pm.agressor = refadv.topiaid 
left join refnuisibleedi refnui on pm.agressor = refnui.topiaid -- certaines adventices sont dans le referentiel des nuisibles
join croppestmaster_crops cpc on (cpc.croppestmaster = cm.topiaid)
join croppingplanentry cpe on cpc.crops = cpe.topiaid 
left join (select cpms.croppestmaster, cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from croppestmaster_species cpms
	join croppingplanspecies cps on cpms.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid
	group by cpms.croppestmaster, cpe.topiaid) esp_var on esp_var.croppestmaster = cpc.croppestmaster and esp_var.culture_id = cpe.topiaid 
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle pression adventice assolee') trad1 on pm.pressurescale = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise adventice assolee') trad2 on pm.masterscale = trad2.nom_base;

-- Insertion Maitrise de la verse 
-----------------------
insert into exports_agronomes_bilancampagne_sdc(type_rubrique,bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
verse_id , verse_echelle_risque ,verse_risque_agricommentaire ,verse_results_echelle_maitrise ,
verse_results_qualifiant, verse_results_agricommentaire , verse_IFT_regulateur , verse_IFT_conseillercommentaire)
select 
'maitrise verse' type_rubrique ,
bcsdc.bcsdc_id, cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
vm.topiaid verse_id ,
trad1.traduction_interface verse_echelle_risque ,
vm.riskfarmercomment verse_risque_agricommentaire ,
trad2.traduction_interface verse_results_echelle_maitrise ,
vm.qualifier verse_results_qualifiant, 
vm.resultfarmercomment verse_results_agricommentaire , 
vm.iftmain verse_IFT_regulateur , 
vm.advisercomments verse_IFT_conseillercommentaire 
from (select * from bcsdc_commun_filieres where filiere_bcsdc like any (array['%MARAICHAGE%','%CULTURES_TROPICALES%','%GRANDES_CULTURES%','%POLYCULTURE_ELEVAGE%','%HORTICULTURE%'])) bcsdc
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid 
join versemaster vm on rgs.topiaid = vm.reportgrowingsystem
join crops_versemaster cvm on cvm.versemaster = vm.topiaid  
join croppingplanentry cpe on cvm.crops = cpe.topiaid 
left join (select sv.versemaster, cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from species_versemaster sv
	join croppingplanspecies cps on sv.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid
	group by sv.versemaster, cpe.topiaid) esp_var on esp_var.versemaster = vm.topiaid and esp_var.culture_id = cpe.topiaid 
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de risque verse') trad1 on vm.riskscale = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise verse') trad2 on vm.masterscale = trad2.nom_base;

-------------------------------------
---- INSERTION ARBORICULTURE

-- Insertion MALADIES & RAVAGEURS
---------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,
maitrise_bioagresseur_id,type_rubrique,
maladie_sdc_IFT_f_chimique,maladie_sdc_IFT_f_biocontrole,maladie_sdc_qte_cuivre_kgha,maladie_sdc_niveau_global_maitrise,
ravageur_sdc_IFT_chimique,ravageur_sdc_IFT_biocontrole,ravageur_sdc_niveau_global_maitrise,
groupe_cible,bioagresseur,bioagresseur_inoculum_annee_precedente_arbo,
bioagresseur_echelle_pression,bioagresseur_evolution_pression,bioagresseur_result_echelle_maitrise_code,bioagresseur_result_echelle_maitrise_libelle_filiere,
bioagresseur_parcelles_touchees_pourcent_arbo,bioagresseur_arbres_touches_pourcent_arbo,bioagresseur_fruits_dommage_pourcent_arbo,bioagresseur_feuillespousses_dommage_pourcent_arbo,
bioagresseur_inoculum_annee_suivante_arbo,bioagresseur_result_matrise_qualifiant,EXPE_bioagresseur_result_matrise_qualifiant_arbo,bioagresseur_result_agricommentaire,bioagresseur_result_conseillercommentaire,
maladie_nb_traitement_arbo,maladie_IFT_f_chimique_arbo,maladie_IFT_f_biocontrol_arbo,
ravageur_nb_traitement_arbo,ravageur_IFT_chimique_arbo,ravageur_IFT_biocontrol_arbo)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
apm.topiaid maitrise_bioagresseur_id,
case 
	when acpm.arbodiseasemasterreportgrowingsystem is not null then 'maladie'
	when acpm.arbopestmastergrowingsystem is not null then 'ravageur'
end type_rubrique,
-- MALADIES : maitrise globale
case when acpm.arbodiseasemasterreportgrowingsystem is not null then rgs.arbochemicalfungicideift end maladie_sdc_IFT_f_chimique,
case when acpm.arbodiseasemasterreportgrowingsystem is not null then rgs.arbobiocontrolfungicideift end maladie_sdc_IFT_f_biocontrole,
case when acpm.arbodiseasemasterreportgrowingsystem is not null then rgs.arbocopperquantity end maladie_sdc_qte_cuivre_kgha,
case when acpm.arbodiseasemasterreportgrowingsystem is not null then rgs.arbodiseasequalifier end maladie_sdc_niveau_global_maitrise,
-- ravageurs : maitrise globale
case when acpm.arbopestmastergrowingsystem is not null then rgs.arbochemicalpestift end ravageur_sdc_IFT_chimique,
case when acpm.arbopestmastergrowingsystem is not null then rgs.arbobiocontrolpestift end ravageur_sdc_IFT_biocontrole,
case when acpm.arbopestmastergrowingsystem is not null then rgs.arbopestqualifier end ravageur_sdc_niveau_global_maitrise,
-- pression pour 1 maladie
refgrpcible.groupe_cible_maa groupe_cible, 
refnui.reference_label bioagresseur,
apm.previousyearinoculum bioagresseur_inoculum_annee_precedente_arbo,
trad1.traduction_interface bioagresseur_echelle_pression,
apm.pressureevolution bioagresseur_evolution_pression,
apm.masterscale bioagresseur_result_echelle_maitrise_code,
trad2.traduction_interface bioagresseur_result_echelle_maitrise_libelle_filiere,
apm.percentaffectedplots bioagresseur_parcelles_touchees_pourcent_arbo,
apm.percentaffectedtrees bioagresseur_arbres_touches_pourcent_arbo,
apm.percentdamagefruits bioagresseur_fruits_dommage_pourcent_arbo,
apm.percentdamageleafs bioagresseur_feuillespousses_dommage_pourcent_arbo,
apm.nextyearinoculum bioagresseur_inoculum_annee_suivante_arbo,
case when bcsdc.dispositif_type = 'DEPHY_FERME' then apm.qualifier end bioagresseur_result_matrise_qualifiant,
case when bcsdc.dispositif_type = 'DEPHY_EXPE' then apm.qualifier end EXPE_bioagresseur_result_matrise_qualifiant_arbo,
apm.resultfarmercomment bioagresseur_result_agricommentaire,
apm.advisercomments bioagresseur_result_conseillercommentaire,
-- MALADIES : IFT
case when acpm.arbodiseasemasterreportgrowingsystem is not null then acpm.treatmentcount end maladie_nb_traitement_arbo,
case when acpm.arbodiseasemasterreportgrowingsystem is not null then acpm.chemicalpestift end maladie_IFT_f_chimique_arbo,
case when acpm.arbodiseasemasterreportgrowingsystem is not null then acpm.biocontrolpestift end maladie_IFT_f_biocontrol_arbo,
-- ravageurs : IFT
case when acpm.arbopestmastergrowingsystem is not null then acpm.treatmentcount end ravageur_nb_traitement_arbo,
case when acpm.arbopestmastergrowingsystem is not null then acpm.chemicalpestift end ravageur_IFT_chimique_arbo,
case when acpm.arbopestmastergrowingsystem is not null then acpm.biocontrolpestift end ravageur_IFT_biocontrol_arbo
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%ARBORICULTURE%') bcsdc  
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid  
join arbocroppestmaster acpm on (acpm.arbodiseasemasterreportgrowingsystem = rgs.topiaid) or (acpm.arbopestmastergrowingsystem = rgs.topiaid)
join arbopestmaster apm on apm.arbocroppestmaster = acpm.topiaid 
left join refnuisibleedi refnui on apm.agressor = refnui.topiaid
left join (select distinct code_groupe_cible_maa,groupe_cible_maa from refciblesagrosystgroupesciblesmaa where active = true and groupe_cible_maa not in ('Cicadelles cercopides et psylles','Maladies des taches foliaires')) refgrpcible -- on retire les doublons de code 38 'Cicadelles cercopides et psylles' puisque ce nom est utilisé par le 37 , et le 82 puisqu'il y a deux orthographes 
		on refgrpcible.code_groupe_cible_maa = apm.codegroupeciblemaa 
join arbocroppestmaster_crops ac on (ac.arbocroppestmaster = acpm.topiaid)
join croppingplanentry cpe on ac.crops = cpe.topiaid 
left join (select acs.arbocroppestmaster, cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from arbocroppestmaster_species acs
	join croppingplanspecies cps on acs.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid
	group by acs.arbocroppestmaster, cpe.topiaid) esp_var on esp_var.arbocroppestmaster = acpm.topiaid and esp_var.culture_id = cpe.topiaid
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de pression maladie ravageur') trad1 on apm.pressurescale = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise maladie ravageur arbo') trad2 on apm.masterscale = trad2.nom_base ;

-- Insertion ADVENTICE
---------------------
insert into exports_agronomes_bilancampagne_sdc(bcsdc_id,culture_id,culture_nom,culture_code,espece_variete,maitrise_adventice_id,
type_rubrique,adventice_agresseur,adventice_echelle_pression_arbo,adventice_pression_evolution_annee_precedente_arbof,
EXPE_adventice_echelle_maitrise_visee_arbo,adventice_result_echelle_maitrise_arbof,EXPE_adventice_maitrise_qualifiant_arbo,adventice_result_maitrise_qualifiant,
adventice_result_niveau_maitrise_agricommentaire,adventice_result_niveau_maitrise_conseillercommentaire_arbof,
adventice_nb_traitement_arbo,adventice_IFT_herbicide_chimique_arbo,adventice_IFT_herbicide_biocontrol_arbo)
select 
bcsdc.bcsdc_id,
cpe.topiaid culture_id,
cpe.name culture_nom,
cpe.code culture_code,
esp_var.espece_variete,
aadm.topiaid maitrise_adventice_id,
'adventice' type_rubrique,
-- pression pour 1 adventice
refadv.adventice adventice_agresseur,
aadm.grassinglevel adventice_echelle_pression_arbo ,
aadm.grassingevolution adventice_pression_evolution_annee_precedente_arbof,
case when bcsdc.dispositif_type = 'DEPHY_EXPE' then trad1.traduction_interface end EXPE_adventice_echelle_maitrise_visee_arbo,
case when bcsdc.dispositif_type = 'DEPHY_FERME' then trad2.traduction_interface end adventice_result_echelle_maitrise_arbof,
case when bcsdc.dispositif_type = 'DEPHY_EXPE' then aadm.qualifier end EXPE_adventice_maitrise_qualifiant_arbo, -- en expe arbo il ya 2 modalites en plus 
case when bcsdc.dispositif_type = 'DEPHY_FERME' then aadm.qualifier end adventice_result_maitrise_qualifiant,
aadm.resultfarmercomment adventice_result_niveau_maitrise_agricommentaire,
aadm.advisercomments adventice_result_niveau_maitrise_conseillercommentaire_arbof,
acadm.treatmentcount adventice_nb_traitement_arbo,
acadm.chemicalpestift adventice_IFT_herbicide_chimique_arbo,
acadm.biocontrolpestift adventice_IFT_herbicide_biocontrol_arbo
from (select * from bcsdc_commun_filieres where filiere_bcsdc like '%ARBORICULTURE%') bcsdc  
join reportgrowingsystem rgs on bcsdc.bcsdc_id = rgs.topiaid  
join arbocropadventicemaster acadm on acadm.reportgrowingsystem = rgs.topiaid
join arboadventicemaster aadm on aadm.arbocropadventicemaster = acadm.topiaid 
left join refadventice refadv on aadm.agressor = refadv.topiaid
left join refnuisibleedi refnui on aadm.agressor = refnui.topiaid -- certaines adventices sont dans le referentiel des nuisibles
join arbocropadventicemaster_crops aad_c on (aad_c.arbocropadventicemaster = acadm.topiaid)
join croppingplanentry cpe on aad_c.crops = cpe.topiaid 
left join (select aadvs.arbocropadventicemaster, cpe.topiaid culture_id,string_agg(refesp.libelle_espece_botanique|| coalesce('('||refvar.denomination ||')', ''),'|')espece_variete
	from arbocropadventicemaster_species aadvs
	join croppingplanspecies cps on aadvs.species = cps.topiaid 
	join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry 
	join refespece refesp on cps.species = refesp.topiaid 
	left join refvarietegeves  refvar on cps.variety = refvar.topiaid
	group by aadvs.arbocropadventicemaster, cpe.topiaid) esp_var on esp_var.arbocropadventicemaster = acadm.topiaid and esp_var.culture_id = cpe.topiaid
-- traductions des libelles
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise visee adv arboexpe') trad1 on aadm.masterscale = trad1.nom_base 
left join (select * from trad_libelles_reportsdc where nom_rubrique = 'echelle de maitrise arbo ferme') trad2 on aadm.masterscale = trad2.nom_base ;

--------------------------
--- FIN DES INSERTIONS ---
--- REMPLISSAGE DES COLONNES DE CONTEXT : UPDATE . Selon la clé de l'id du bilan de campagne
--- Pour toutes les filleres en meme temps

UPDATE exports_agronomes_bilancampagne_sdc as tabfinal
SET 
	auteur = tab2.auteur, reseau = tab2.reseau,
	bcsdc_nom = tab2.bcsdc_nom , bcsdc_code = tab2.bcsdc_code, 
	bcregional_associe_id = tab2.bcregional_associe_id, bcregional_associe_nom = tab2.bcregional_associe_nom,
	domaine_id = tab2.domaine_id, domaine_nom = tab2.domaine_nom, domaine_code = tab2.domaine_code,
	domaine_type = tab2.domaine_type, campagne = tab2.campagne, 
	dispositif_id = tab2.dispositif_id, dispositif_nom = tab2.dispositif_nom, dispositif_code = tab2.dispositif_code, dispositif_type = tab2.dispositif_type,
	filiere_sdc = tab2.filiere_sdc,filiere_bcsdc = tab2.filiere_bcsdc,
	sdc_id = tab2.sdc_id,sdc_nom=tab2.sdc_nom,sdc_code=tab2.sdc_code,sdc_type_agriculture = tab2.sdc_type_agriculture,sdc_code_dephy=tab2.sdc_code_dephy,
	modele_descisionelassocie_obs_id = tab2.modele_descisionelassocie_obs_id,modele_descisionelassocie_prevu_id = tab2.modele_descisionelassocie_prevu_id,
	methode_estimation_IFT_declares = tab2.methode_estimation_IFT_declares,
	principales_evolutions_depuis_pz0 = tab2.principales_evolutions_depuis_pz0,mesure_specifique_annee=tab2.mesure_specifique_annee,
	faits_marquants_conduiteculture_performancetechnique=tab2.faits_marquants_conduiteculture_performancetechnique,enseignements_amelioration_sdc=tab2.enseignements_amelioration_sdc
from bcsdc_commun_filieres tab2
where tabfinal.bcsdc_id = tab2.bcsdc_id ;

-- Suppression des lignes où le dispositif n'est pas dans la table exports_agronomes_criteres = que les domaines et dispositifs actifs
delete from exports_agronomes_bilancampagne_sdc where dispositif_id not in (select dispositif_id from exports_agronomes_criteres);


