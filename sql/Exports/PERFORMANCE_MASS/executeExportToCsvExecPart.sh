
mkdir -p $repertoire_export

# ajout de la configuration de authentication à la base de données postgresql
echo $pg_host:$pg_port:$pg_database:$pg_user:$pg_password > $HOME/.pgpass
chmod 600 $HOME/.pgpass

# export to csv
nb_queries=${#query[@]}
for ((i=1;i<=nb_queries;i++)) do

        echo "i= "$i"/"$nb_queries
        echo "lancement requete ${query_name[$i]}"

        #echo "requete = "
        #echo ${query[$i]}
        nom_fic=$prefixe${query_name[$i]}$suffixe
        fichier_export=$repertoire_export$nom_fic

        echo "fichier_export="$fichier_export
        #liste des noms de fichiers pour la compression
        lst_noms_fic=$lst_noms_fic" "$nom_fic

        commande_export="COPY("${query[$i]}") TO STDOUT WITH (FORMAT csv, HEADER, DELIMITER '@', FORCE_QUOTE *, NULL '')"

        #echo $commande_export
        psql -U$pg_user -h $pg_host -p $pg_port -d $pg_database  -c "$commande_export" > $fichier_export

        echo "${query_name[$i]}" exporté

        # il y a 12 requêtes dans ce script, + ce n'est pas normal
        if [ $i -gt 12 ]
        then
           echo "securite"
           break
    fi
done

fic_archive=$repertoire_export$prefixe$arch_sufix.tar.gz
fic_zip=$repertoire_export$prefixe$arch_sufix.zip
#echo tar -cvzf $fic_archive -C $repertoire_export $lst_noms_fic
tar -cvzf $fic_archive -C $repertoire_export $lst_noms_fic
cd $repertoire_export
zip $fic_zip $lst_noms_fic
#echo mail -s '[AGS] Fichiers de suivi des saisies des IR' -A $fic_archive agrosyst-team@inrae.fr <<< 'Fichiers de suivi des saisies des IR'
#mail -s '[AGS] Fichiers de suivi des saisies des IR' -A $fic_archive agrosyst-team@inrae.fr <<< 'Fichiers de suivi des saisies des IR'
#echo mail envoye

echo "fin du job"
