DROP TABLE IF EXISTS exports_agronomes_usages_intrants_realise; 
DROP TABLE IF EXISTS exports_agronomes_context_input_usages;


-- Table usage_id + action_id
create temporary table exports_agronomes_context_input_usages as 
  	select a.topiaid usage_id, aa.topiaid action_id
  	from abstractinputusage a
	left join mineralproductinputusage m ON m.topiaid = a.topiaid  
	left join biologicalproductinputusage b on b.topiaid = a.topiaid 
	left join pesticideproductinputusage p on p.topiaid = a.topiaid
	left join organicproductinputusage o ON o.topiaid = a.topiaid 
	left join potinputusage pot ON pot.topiaid = a.topiaid 
	left join substrateinputusage sub ON sub.topiaid = a.topiaid 
	left join otherproductinputusage other on other.topiaid = a.topiaid 
	join abstractaction aa on aa.topiaid in (m.mineralfertilizersspreadingaction, b.biologicalcontrolaction, p.pesticidesspreadingaction, o.organicfertilizersspreadingaction, pot.otheraction, sub.otheraction, other.otheraction)
	union
	  select a.topiaid usage_id, aa.topiaid action_id 
  from abstractinputusage a
 	join irrigationinputusage i on i.topiaid = a.topiaid 
 	join abstractaction aa on aa.irrigationinputusage = i.topiaid
 	union
  -- espece de semence
    select a.topiaid usage_id, aa.topiaid action_id 
  from abstractinputusage a
	join seedspeciesinputusage su on su.topiaid = a.topiaid 
	join seedlotinputusage slu on su.seedlotinputusage = slu.topiaid 
	join abstractaction aa on slu.seedingactionusage = aa.topiaid
  union
  --lot de semence quand il n'y a pas d'esp associee
    select a.topiaid usage_id, aa.topiaid action_id 
  from abstractinputusage a
	join seedlotinputusage slu on slu.topiaid = a.topiaid 
	join abstractaction aa on slu.seedingactionusage = aa.topiaid
  join domainseedlotinput dl on dl.topiaid = slu.domainseedlotinput 
  left join domainseedspeciesinput dssi on dssi.domainseedlotinput = dl.topiaid 
  where dssi.topiaid is null 
  union
  -- traitement de semence
 	  select a.topiaid usage_id, aa.topiaid action_id 
  from abstractinputusage a
	join seedproductinputusage spu on spu.topiaid = a.topiaid 
	join seedspeciesinputusage su on su.topiaid = spu.seedspeciesinputusage 
	join seedlotinputusage slu on su.seedlotinputusage = slu.topiaid 
	join abstractaction aa on slu.seedingactionusage = aa.topiaid;

-------------------------------
-- Intrants_Realise
------------------------------- 

CREATE TABLE exports_agronomes_usages_intrants_realise (
  domaine_code text,
  domaine_id character varying(255),
  domaine_nom text,
  domaine_campagne integer,
  sdc_id character varying(255),
  sdc_code text,
  sdc_nom text,
  parcelle_id character varying(255),
  parcelle_nom text,
  zone_id character varying(255),
  zone_nom text,
  intervention_id character varying(255),
  usage_id character varying(255),
  intrant_id character varying(255),
  intrant_type text,
  intrant_ref_id character varying(255),
  intrant_ref_nom text,
  intrant_nom_utilisateur text,
  dose double precision,
  volume_bouillie_hl double precision,
  unite text,
  profondeur_semis_cm  double precision,
  biocontrole text,
  intrant_phyto_type text,
  intrant_phyto_cible_nom text,
  forme_ferti_min text,
  n double precision,
  p2o5 double precision,
  k2o double precision,
  bore double precision,
  calcium double precision,
  fer double precision,
  manganese double precision,
  molybdene double precision,
  mgo double precision,
  oxyde_de_sodium double precision,
  so3 double precision,
  cuivre double precision,
  zinc double precision,
  cao double precision,
  s double precision,
  unite_teneur_ferti_orga character varying(255),
  ferti_effet_phyto_attendu text,
  prix double precision,
  prix_unite text
);


-- Cultures assolees
INSERT INTO exports_agronomes_usages_intrants_realise (
  SELECT
  crit.domaine_code,
  crit.domaine_id,
  crit.domaine_nom,
  crit.domaine_campagne,
  gs.topiaid sdc_id,
  gs.code sdc_code,
  gs.name sdc_nom,
  p.topiaid parcelle_id,
  p.name parcelle_nom,
  z.topiaid zone_id,
  z.name zone_nom,
  ei.topiaid intervention_id,
  cu.usage_id usage_id,
  null intrant_id,
  null intrant_type,
  null intrant_ref_id,
  NULL intrant_ref_nom,
  null intrant_nom_utilisateur,
  null dose,
  aa.boiledquantity volume_bouillie_hl,
  null unite,
  null profondeur_semis_cm,
  null biocontrole,
  null intrant_phyto_type,
  null intrant_phyto_cible_nom,
  null forme_ferti_min,
  null n,
  null p2o5,
  null k2o,
  null bore,
  null calcium,
  null fer,
  null manganese,
  null molybdene,
  null mgo,
  null oxyde_de_sodium,
  null so3,
  null cuivre,
  null zinc,
  null cao,
  null s,
  null unite_teneur_ferti_orga,
  null ferti_effet_phyto_attendu,
  NULL prix,
  NULL prix_unite
  FROM abstractaction aa
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
  INNER JOIN effectiveseasonalcropcycle ecc ON eccn.effectiveseasonalcropcycle = ecc.topiaid
  INNER JOIN zone z ON ecc.zone = z.topiaid
  INNER JOIN plot p ON z.plot = p.topiaid
  INNER JOIN growingsystem gs ON p.growingsystem = gs.topiaid
  INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
  inner join exports_agronomes_context_input_usages cu on aa.topiaid = cu.action_id
  WHERE gs.active IS TRUE
  AND p.active IS TRUE
  AND z.active IS true
);

-- Cultures perennes
INSERT INTO exports_agronomes_usages_intrants_realise (
	SELECT
    crit.domaine_code,
    crit.domaine_id,
    crit.domaine_nom,
    crit.domaine_campagne,
    gs.topiaid sdc_id,
    gs.code sdc_code,
    gs.name sdc_nom,
    p.topiaid parcelle_id,
    p.name parcelle_nom,
    z.topiaid zone_id,
    z.name zone_nom,
    ei.topiaid intervention_id, 
    cu.usage_id usage_id,
    null intrant_id,
	null intrant_type,
	null intrant_ref_id,
	NULL intrant_ref_nom,
	null intrant_nom_utilisateur,
	null dose,
	aa.boiledquantity volume_bouillie_hl,
	null unite,
  null profondeur_semis_cm,
	null biocontrole,
	null intrant_phyto_type,
	null intrant_phyto_cible_nom,
	null forme_ferti_min, null  n, null p2o5, null k2o, null bore, null calcium, null fer, null manganese,
	null molybdene, null mgo, null oxyde_de_sodium, null so3, null cuivre, null zinc, null cao, null s,
	null unite_teneur_ferti_orga,
	null ferti_effet_phyto_attendu,
	NULL prix,
	NULL prix_unite
	FROM abstractaction aa
	INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
	JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
	JOIN effectiveperennialcropcycle epcc ON epcc.phase = eccp.topiaid
	JOIN zone z ON epcc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	INNER JOIN growingsystem gs ON p.growingsystem = gs.topiaid
	INNER JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
	inner join exports_agronomes_context_input_usages cu on aa.topiaid = cu.action_id
	WHERE gs.active IS TRUE
	AND p.active IS TRUE
	AND z.active IS true
);


CREATE INDEX exports_agronomes_usages_intrants_realise_inputid_idx ON exports_agronomes_usages_intrants_realise (usage_id);


-- FERTILISATION MINÉRALE
update exports_agronomes_usages_intrants_realise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname  ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
n = r.n,
p2o5 = r.p2o5,
k2o = r.k2o,
bore = r.bore,
calcium  = r.calcium ,
fer = r.fer ,
manganese = r.manganese ,
molybdene = r.molybdene ,
mgo = r.mgo ,
oxyde_de_sodium = r.oxyde_de_sodium ,
so3 = r.so3 ,
cuivre = r.cuivre ,
zinc = r.zinc ,
intrant_ref_nom = r.type_produit ,
forme_ferti_min = r.forme,
ferti_effet_phyto_attendu =  CASE  d.phytoeffect
    WHEN true THEN 'oui'
    WHEN false THEN 'non'
  end
from mineralproductinputusage m 
join abstractinputusage u on m.topiaid = u.topiaid 
join domainmineralproductinput d on m.domainmineralproductinput = d.topiaid 
LEFT join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
LEFT join reffertiminunifa r on d.refinput = r.topiaid 
LEFT join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;
 

-- GÉNÉRALE, contient les traitements de semences
update exports_agronomes_usages_intrants_realise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname,
intrant_ref_nom = r.nom_produit,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
intrant_phyto_type = d.producttype ,
biocontrole = case r.nodu when true then 'oui' when false then 'non' end
from abstractphytoproductinputusage p 
join abstractinputusage u on p.topiaid = u.topiaid 
join domainphytoproductinput d on p.domainphytoproductinput = d.topiaid 
LEFT join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
LEFT join refactatraitementsproduit r on d.refinput = r.topiaid 
LEFT join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;


-- IRRIGATION
update exports_agronomes_usages_intrants_realise e set
unite = d.usageunit,
intrant_nom_utilisateur = a2.inputname,
intrant_id = d.topiaid,
intrant_type = u.inputtype,
dose = u.qtavg
from irrigationinputusage i 
join abstractinputusage u on i.topiaid = u.topiaid 
join domainirrigationinput d on i.domainirrigationinput = d.topiaid 
join abstractdomaininputstockunit a2 on a2.topiaid = d.topiaid
join abstractaction a on a.irrigationinputusage = i.topiaid 
where u.topiaid = e.usage_id;

-- ESPECES DE SEMENCES.
update exports_agronomes_usages_intrants_realise e SET
intrant_id = dl.topiaid,
unite = dl.usageunit,
intrant_nom_utilisateur = a2.inputname,
intrant_ref_id = dssi.topiaid,
intrant_ref_nom = resp.libelle_espece_botanique,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
profondeur_semis_cm = s.deepness
from seedspeciesinputusage s 
join abstractinputusage u on s.topiaid = u.topiaid 
join domainseedspeciesinput dssi on s.domainseedspeciesinput = dssi.topiaid
join domainseedlotinput dl on dl.topiaid = dssi.domainseedlotinput  
join croppingplanspecies cps on cps.topiaid = dssi.speciesseed 
join refespece resp on resp.topiaid = cps.species 
join abstractdomaininputstockunit a2 on a2.topiaid = dl.topiaid
left join inputprice i on i.topiaid = dssi.seedprice 
where u.topiaid = e.usage_id;

-- Lots de semence, uniquement lorsque au sein de ce lot il n'y a aucune especes déclarées (aucunes esp dans la culture)
update exports_agronomes_usages_intrants_realise e SET
intrant_id = dl.topiaid,
unite = dl.usageunit,
intrant_nom_utilisateur = a2.inputname,
intrant_type = u.inputtype
from seedlotinputusage slu
join abstractinputusage u on slu.topiaid = u.topiaid 
join domainseedlotinput dl on dl.topiaid = slu.domainseedlotinput 
join abstractdomaininputstockunit a2 on a2.topiaid = dl.topiaid
left join domainseedspeciesinput dssi on dssi.domainseedlotinput = dl.topiaid
where dssi.topiaid is null and u.topiaid = e.usage_id;


-- FERTILISATION ORGANIQUE
update exports_agronomes_usages_intrants_realise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg,
n = d.n,
p2o5 = d.p2o5,
k2o = d.k2o,
mgo = d.mgo,
cao = d.cao,
s = d.s,
intrant_ref_nom = r.libelle ,
unite_teneur_ferti_orga = r.unite_teneur_ferti_orga
from organicproductinputusage o 
join abstractinputusage u on o.topiaid = u.topiaid 
join domainorganicproductinput d on o.domainorganicproductinput = d.topiaid 
join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
LEFT join inputprice i on su.inputprice = i.topiaid 
LEFT join reffertiorga r on d.refinput = r.topiaid 
where u.topiaid = e.usage_id;


-- INTRANTS AUTRES
update exports_agronomes_usages_intrants_realise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg
from otherproductinputusage o 
join abstractinputusage u on o.topiaid = u.topiaid 
join domainotherinput d on o.domainotherinput  = d.topiaid 
LEFT join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
LEFT join inputprice i on su.inputprice = i.topiaid
where u.topiaid = e.usage_id;


-- POTS 
update exports_agronomes_usages_intrants_realise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg
from potinputusage p
join abstractinputusage u on p.topiaid = u.topiaid 
join domainpotinput d on p.domainpotinput  = d.topiaid 
LEFT join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
LEFT join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;


-- SUBSTRATS
update exports_agronomes_usages_intrants_realise e SET
intrant_id = d.topiaid,
intrant_ref_id = d.refinput ,
unite = d.usageunit,
intrant_nom_utilisateur = su.inputname ,
intrant_type = u.inputtype,
prix = i.price ,
prix_unite = i.priceunit, 
dose = u.qtavg
from substrateinputusage s
join abstractinputusage u on s.topiaid = u.topiaid 
join domainsubstrateinput d on s.domainsubstrateinput  = d.topiaid 
LEFT join abstractdomaininputstockunit su on d.topiaid = su.topiaid 
LEFT join inputprice i on su.inputprice = i.topiaid 
where u.topiaid = e.usage_id;


  
 -- 20 secondes : Récupération et aggrégations des espèces cibles, mis à jour le 11/10/2023
UPDATE exports_agronomes_usages_intrants_realise eacir SET intrant_phyto_cible_nom = (
    SELECT string_agg(
      (SELECT rnedi.reference_label FROM RefNuisibleEDI rnedi WHERE rnedi.topiaid = ppt.target AND ppt.abstractphytoproductinputusage = eacir.usage_id  UNION
       SELECT ra.adventice FROM RefAdventice ra WHERE ra.topiaid = ppt.target AND ppt.abstractphytoproductinputusage = eacir.usage_id ),'|')
       FROM phytoproducttarget ppt
       WHERE ppt.abstractphytoproductinputusage = eacir.usage_id
	   AND ppt0.topiaid = ppt.topiaid
  )
  FROM phytoproducttarget ppt0
  WHERE ppt0.abstractphytoproductinputusage = eacir.usage_id
  AND eacir.intrant_type IN ('APPLICATION_DE_PRODUITS_PHYTOSANITAIRES', 'LUTTE_BIOLOGIQUE', 'TRAITEMENT_SEMENCE');

