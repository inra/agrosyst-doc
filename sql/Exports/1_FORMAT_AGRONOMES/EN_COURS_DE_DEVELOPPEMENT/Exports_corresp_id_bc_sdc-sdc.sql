--------------------------
-- bilan de campagne SdC
--------------------------
-- creation de table commentee en attendant finition et validation
-- create table exports_agronomes_bilan_campagne_sdc as

SELECT 
crit.domaine_id, 
crit.dispositif_id, 
gs.topiaid sdc_id, 
rgs.topiaid bilan_de_campagne_sdc_id,

-- data from exports agronomes criteres
crit.domaine_campagne domaine_campagne,
crit.dispositif_type dispositif_type,

-- data from reportGrowingSystem
rgs.highlightsevolutions,
rgs.highlightsmeasures,
rgs.highlightsperformances,
rgs.highlightsteachings,
rgs.arbochemicalfungicideift,
rgs.arbobiocontrolfungicideift,
rgs.arbocopperquantity,
rgs.arbochemicalpestift,
rgs.vitidiseasechemicalfungicideift,
rgs.vitidiseasebiocontrolfungicideift,
rgs.vitidiseasecopperquantity,
rgs.vitipestchemicalpestift,
rgs.vitipestbiocontrolpestift,
rgs.vitiadventicepressurefarmercomment,
rgs.vitiadventiceresultfarmercomment,
rgs.vitiherbotreatmentchemical,
rgs.vitiherbotreatmentbiocontrol,
rgs.vitiherbotreatmentchemicalift,
rgs.vitiherbotreatmentbiocontrolift,
rgs.vitisuckeringchemical,
rgs.vitisuckeringbiocontrol,
rgs.vitisuckeringchemicalift,
rgs.vitisuckeringbiocontrolift,
rgs.vitiyieldquality,
rgs.growingsystem,
rgs.reportregional,
rgs.iftestimationmethod,
rgs.vitidiseasequalifier,
rgs.vitipestqualifier,
rgs.vitiadventicequalifier,
rgs.vitiyieldobjective,
rgs.vitiadventicepressurescale,
rgs.vitilosscause1,
rgs.vitilosscause2,
rgs.vitilosscause3,
rgs.arbodiseasequalifier,
rgs.arbopestqualifier,

-- data from growingSystem
gs.sector sdc_filiere, 
gs.production sdc_type_production,
gs.categorystrategy sdc_strategie_categorie,
gs.affectedarearate sdc_part_SAU_domaine, 
gs.affectedworkforcerate sdc_part_MO_domaine, 
gs.domainstoolsusagerate sdc_part_materiel_domaine

FROM ReportGrowingSystem rgs
JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
WHERE gs.active IS TRUE;