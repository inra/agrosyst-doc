﻿
-- connaître tous les cas du synthétisé
select hav.topiaid, pccn. rank, ps.topiaid ps,pi.name pi, hav.topiacreatedate, cpe.name, re.code_espece_botanique as recb, re.code_qualifiant_aee recq, 
(re.libelle_espece_botanique, re.libelle_qualifiant_aee) re_nom, 
rd.code_espece_botanique rdcb, COALESCE(rd.code_qualifiant_aee,'') rdcq, 
rd.espece rd_espece, rd.destination, hav.yealdaverage, hav.yealdunit, rd.sector
FROM practicedintervention pi
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccn ON pccc.target = pccn.topiaid
JOIN practicedcropcycle pcc ON pccn.practicedseasonalcropcycle = pcc.topiaid
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN croppingplanentry cpe ON cpe.topiaid = 
 	(SELECT topiaid FROM croppingplanentry cpe2 WHERE cpe2.code = pccn.croppingplanentrycode LIMIT 1)
JOIN abstractaction aa ON pi.topiaid = aa.practicedintervention
RIGHT JOIN harvestingactionvalorisation hav ON aa.topiaid = hav.harvestingaction
JOIN refdestination rd ON hav.destination = rd.topiaid
JOIN croppingplanspecies cps ON cps.topiaid = 
	(SELECT topiaid FROM croppingplanspecies cps2 WHERE cps2.code = hav.speciescode LIMIT 1)
JOIN refespece re ON cps.species = re.topiaid
WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl'
AND (
	-- les espèces dont le qualifiant de la destination choisie est différent de ce qui a été saisi au niveau du domaine
	(
	re.code_espece_botanique = rd.code_espece_botanique
	AND COALESCE(re.code_qualifiant_aee,'') != COALESCE(rd.code_qualifiant_aee,'')
	)
OR 
	-- les espèces ayant une destination générique dont la filière n'est pas bonne
	(rd.code_espece_botanique IS NULL
	AND (rd.sector) NOT IN (select sector FROM refspeciestosector rss WHERE rss.code_espece_botanique = re.code_espece_botanique AND COALESCE(rss.code_qualifiant_aee,'') = COALESCE(re.code_qualifiant_aee,'')))
)
ORDER BY re.libelle_espece_botanique, ps.topiaid, pccn.rank, pi.topiaid;



-- synthétisé + pérenne - aucun résultat
-- SELECT hav.topiaid, * 
-- FROM practicedintervention pi
-- JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
-- JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
-- JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
-- JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
-- JOIN croppingplanentry cpe ON cpe.topiaid = 
--  	(SELECT topiaid FROM croppingplanentry cpe2 WHERE cpe2.code = ppcc.croppingplanentrycode LIMIT 1)
-- JOIN abstractaction aa ON pi.topiaid = aa.practicedintervention
-- JOIN harvestingactionvalorisation hav ON aa.topiaid = hav.harvestingaction
-- JOIN refdestination rd ON hav.destination = rd.topiaid
-- JOIN croppingplanspecies cps ON cps.topiaid = 
-- 	(SELECT topiaid FROM croppingplanspecies cps2 WHERE cps2.code = hav.speciescode LIMIT 1)
-- JOIN refespece re ON cps.species = re.topiaid
-- WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl'
-- AND (
-- 	-- les espèces dont le qualifiant de la destination choisie est différent de ce qui a été saisi au niveau du domaine
-- 	(
-- 	re.code_espece_botanique = rd.code_espece_botanique
-- 	AND COALESCE(re.code_qualifiant_aee,'') != COALESCE(rd.code_qualifiant_aee,'')
-- 	)
-- OR 
-- 	-- les espèces ayant une destination générique dont la filière n'est pas bonne
-- 	(rd.code_espece_botanique IS NULL
-- 	AND (rd.sector) NOT IN (select sector FROM refspeciestosector rss WHERE rss.code_espece_botanique = re.code_espece_botanique AND COALESCE(rss.code_qualifiant_aee,'') = COALESCE(re.code_qualifiant_aee,'')))
-- )
-- ORDER BY hav.topiaid, ps.topiaid, pi.topiaid;



-- les cas du réalisé assolé
SELECT hav.topiaid, * 
FROM effectiveintervention ei
JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
JOIN zone z ON escc.zone = z.topiaid
JOIN croppingplanentry cpe ON eccn.croppingplanentry = cpe.topiaid
JOIN abstractaction aa ON ei.topiaid = aa.effectiveintervention
JOIN harvestingactionvalorisation hav ON aa.topiaid = hav.harvestingaction
JOIN refdestination rd ON hav.destination = rd.topiaid
JOIN croppingplanspecies cps ON cps.topiaid = 
	(SELECT topiaid FROM croppingplanspecies cps2 WHERE cps2.code = hav.speciescode LIMIT 1)
JOIN refespece re ON cps.species = re.topiaid
WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl'
AND (
	-- les espèces dont le qualifiant de la destination choisie est différent de ce qui a été saisi au niveau du domaine
	(
	re.code_espece_botanique = rd.code_espece_botanique
	AND COALESCE(re.code_qualifiant_aee,'') != COALESCE(rd.code_qualifiant_aee,'')
	)
OR 
	-- les espèces ayant une destination générique dont la filière n'est pas bonne
	(rd.code_espece_botanique IS NULL
	AND (rd.sector) NOT IN (select sector FROM refspeciestosector rss WHERE rss.code_espece_botanique = re.code_espece_botanique AND COALESCE(rss.code_qualifiant_aee,'') = COALESCE(re.code_qualifiant_aee,'')))
)
ORDER BY hav.topiaid, z.topiaid, eccn.rank, ei.topiaid;


-- réalisé pérenne - auncun résultat
-- SELECT hav.topiaid, * 
-- FROM effectiveintervention ei
-- JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
-- JOIN effectiveperennialcropcycle epcc ON eccp.topiaid = epcc.phase
-- JOIN zone z ON epcc.zone = z.topiaid
-- JOIN croppingplanentry cpe ON epcc.croppingplanentry = cpe.topiaid
-- JOIN abstractaction aa ON ei.topiaid = aa.effectiveintervention
-- JOIN harvestingactionvalorisation hav ON aa.topiaid = hav.harvestingaction
-- JOIN refdestination rd ON hav.destination = rd.topiaid
-- JOIN croppingplanspecies cps ON cps.topiaid = 
-- 	(SELECT topiaid FROM croppingplanspecies cps2 WHERE cps2.code = hav.speciescode LIMIT 1)
-- JOIN refespece re ON cps.species = re.topiaid
-- WHERE aa.topiadiscriminator = 'fr.inra.agrosyst.api.entities.action.HarvestingActionImpl'
-- AND (
-- 	-- les espèces dont le qualifiant de la destination choisie est différent de ce qui a été saisi au niveau du domaine
-- 	(
-- 	re.code_espece_botanique = rd.code_espece_botanique
-- 	AND COALESCE(re.code_qualifiant_aee,'') != COALESCE(rd.code_qualifiant_aee,'')
-- 	)
-- OR 
-- 	-- les espèces ayant une destination générique dont la filière n'est pas bonne
-- 	(rd.code_espece_botanique IS NULL
-- 	AND (rd.sector) NOT IN (select sector FROM refspeciestosector rss WHERE rss.code_espece_botanique = re.code_espece_botanique AND COALESCE(rss.code_qualifiant_aee,'') = COALESCE(re.code_qualifiant_aee,'')))
-- )
-- ORDER BY hav.topiaid, z.topiaid, ei.topiaid;


-- tous les cas se présentant réalisé + synthétisé
select hav.topiacreatedate, re.code_espece_botanique as recb, re.code_qualifiant_aee recq, 
(re.libelle_espece_botanique, re.libelle_qualifiant_aee) re_nom, 
rd.code_espece_botanique rdcb, rd.code_qualifiant_aee rdcq, 
rd.espece rd_espece, rd.destination,hav.yealdunit hav_unit, rd.yealdunit rd_unit, rd.sector
FROM harvestingactionvalorisation hav 
JOIN refdestination rd ON hav.destination = rd.topiaid
JOIN croppingplanspecies cps ON cps.topiaid = 
	(SELECT topiaid FROM croppingplanspecies cps2 WHERE cps2.code = hav.speciescode LIMIT 1)
JOIN refespece re ON cps.species = re.topiaid
LEFT JOIN abstractaction aa ON hav.harvestingaction = aa.topiaid
WHERE (
	-- les espèces dont le qualifiant de la destination choisie est différent de ce qui a été saisi au niveau du domaine
	(
	re.code_espece_botanique = rd.code_espece_botanique
	AND COALESCE(re.code_qualifiant_aee,'') != COALESCE(rd.code_qualifiant_aee,'')
	)
OR 
	-- les espèces ayant une destination générique dont la filière n'est pas bonne
	(rd.code_espece_botanique IS NULL
	AND (rd.sector) NOT IN (select sector FROM refspeciestosector rss WHERE rss.code_espece_botanique = re.code_espece_botanique AND COALESCE(rss.code_qualifiant_aee,'') = COALESCE(re.code_qualifiant_aee,'')))
)
-- les 2 filtres suivants c'est parce qu'il y a des harvestingactionvalorisation sans action et des actions sans intervention
AND hav.harvestingaction IS NOT NULL
AND (aa.effectiveintervention IS NOT NULL OR aa.practicedintervention IS NOT NULL)
ORDER BY hav.topiacreatedate DESC;


-- Requête detectant tous les cas de figure se présentant
SELECT re.code_espece_botanique as recb, re.code_qualifiant_aee recq, 
(re.libelle_espece_botanique, re.libelle_qualifiant_aee) re_nom, 
rd.code_espece_botanique rdcb, COALESCE(rd.code_qualifiant_aee,'') rdcq, 
rd.espece rd_espece, rd.destination, hav.yealdunit hav_unit, rd.yealdunit rd_unit, rd.sector,
count(*)
FROM harvestingactionvalorisation hav 
JOIN refdestination rd ON hav.destination = rd.topiaid
JOIN croppingplanspecies cps ON cps.topiaid = 
	(SELECT topiaid FROM croppingplanspecies cps2 WHERE cps2.code = hav.speciescode LIMIT 1)
JOIN refespece re ON cps.species = re.topiaid
LEFT JOIN abstractaction aa ON hav.harvestingaction = aa.topiaid
WHERE (
	-- les espèces dont le qualifiant de la destination choisie est différent de ce qui a été saisi au niveau du domaine
	(
	re.code_espece_botanique = rd.code_espece_botanique
	AND COALESCE(re.code_qualifiant_aee,'') != COALESCE(rd.code_qualifiant_aee,'')
	)
OR 
	-- les espèces ayant une destination générique dont la filière n'est pas bonne
	(rd.code_espece_botanique IS NULL
	AND (rd.sector) NOT IN (select sector FROM refspeciestosector rss WHERE rss.code_espece_botanique = re.code_espece_botanique AND COALESCE(rss.code_qualifiant_aee,'') = COALESCE(re.code_qualifiant_aee,'')))
)
-- les 2 filtres suivants c'est parce qu'il y a des harvestingactionvalorisation sans action et des actions sans intervention
AND hav.harvestingaction IS NOT NULL
AND (aa.effectiveintervention IS NOT NULL OR aa.practicedintervention IS NOT NULL)
GROUP BY re.code_espece_botanique,re.code_qualifiant_aee,(re.libelle_espece_botanique, re.libelle_qualifiant_aee), 
rd.code_espece_botanique, COALESCE(rd.code_qualifiant_aee,''),
rd.espece,rd.destination, rd.sector, hav.yealdunit, rd.yealdunit;

SELECT * from refdestination where code_espece_botanique = 'ZAJ' AND code_qualifiant_aee = 'H33';
