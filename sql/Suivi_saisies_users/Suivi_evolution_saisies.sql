﻿---------------------------------
-- Requêtes intiales d'Estelle --
---------------------------------

SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) from effectiveintervention
GROUP BY yearmonth
ORDER BY yearmonth;

SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) from practicedintervention
GROUP BY yearmonth
ORDER BY yearmonth;

SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) from growingsystem
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- nombre de comptes actifs totaux + créés entre 2 dates
SELECT count(*) FROM agrosystuser 
WHERE active IS TRUE
-- AND email ilike '%inra%'
AND topiacreatedate < '01-01-2017';

SELECT count(*) FROM agrosystuser 
WHERE topiacreatedate > '31-12-2015'
AND topiacreatedate < '01-01-2017'
-- AND email ilike '%inra%'
AND active IS TRUE;


-----------------------------------
-- Requêtes reprises avec Estelle--
-----------------------------------

-- Nombre de domaines actifs créés entre 2 dates
SELECT count(*) FROM domain
WHERE active IS TRUE
AND topiacreatedate > '31-12-2015'
AND topiacreatedate < '01-01-2017';

-- Nombre de SdC actifs dans des domaines actifs
SELECT date_trunc('month', gs.topiacreatedate) as yearmonth, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and d.active IS TRUE
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- Nombre de SdC actifs dans des domaines actifs entre 2 dates
SELECT count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and d.active IS TRUE
AND gs.topiacreatedate > '31-12-2015'
AND gs.topiacreatedate < '01-01-2017';

-- Nombre de SdC actifs validés dans des domaines actifs
SELECT date_trunc('month', gs.topiacreatedate) as yearmonth, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gs.validated IS TRUE and d.active IS TRUE
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- Nombre de SdC actifs validés dans des domaines actifs entre 2 dates
SELECT count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gs.validated IS TRUE and d.active IS TRUE
AND gs.topiacreatedate > '31-12-2015'
AND gs.topiacreatedate < '01-01-2017';

-- Nombre de synthétisés actifs 
SELECT date_trunc('month', ps.topiacreatedate) as yearmonth, count(*)
FROM practicedsystem ps
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
WHERE ps.active IS TRUE 
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- Nombre de synthétisés validés
SELECT date_trunc('month', ps.topiacreatedate) as yearmonth, count(*)
FROM practicedsystem ps
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
WHERE ps.active IS TRUE and ps.validated IS TRUE 
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- Nombre d'interventions du réalisé des domaines actifs
WITH inter_domain AS (
	SELECT ei.*
	FROM effectiveintervention ei
	JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
	JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
	JOIN zone z ON escc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN domain d ON p.domain = d.topiaid
	WHERE d.active is TRUE

	UNION

	SELECT ei.*
	FROM effectiveintervention ei
	JOIN effectiveperennialcropcycle epcc ON ei.effectivecropcyclephase = epcc.phase
	JOIN zone z ON epcc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN domain d ON p.domain = d.topiaid
	WHERE d.active is TRUE
)
SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) as count
FROM inter_domain
GROUP BY yearmonth
ORDER BY yearmonth

-- Nombre d'interventions du réalisé créées entre 2 dates, dans des domaines actifs 
WITH inter_domain AS (
	SELECT ei.*
	FROM effectiveintervention ei
	JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
	JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
	JOIN zone z ON escc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN domain d ON p.domain = d.topiaid
	WHERE d.active is TRUE

	UNION

	SELECT ei.*
	FROM effectiveintervention ei
	JOIN effectiveperennialcropcycle epcc ON ei.effectivecropcyclephase = epcc.phase
	JOIN zone z ON epcc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN domain d ON p.domain = d.topiaid
	WHERE d.active is TRUE
)
SELECT count(*)
FROM inter_domain
WHERE topiacreatedate > '31-12-2015'
AND topiacreatedate < '01-01-2017';

-- Nombre d'interventions du synthétisé des domaines actifs
WITH inter_domain AS (
	SELECT pi.*
	FROM practicedintervention pi
	JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
	JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
	JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	WHERE d.active is TRUE

	UNION

	SELECT pi.*
	FROM practicedintervention pi
	JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
	JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
	JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
	JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	WHERE d.active is TRUE
	)
SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) as count
FROM inter_domain
GROUP BY yearmonth
ORDER BY yearmonth



-- Nombre d'interventions du synthétisé créées entre deux dates, dans des domaines actifs
WITH inter_domain AS (
	SELECT pi.*
	FROM practicedintervention pi
	JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
	JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
	JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	WHERE d.active is TRUE

	UNION

	SELECT pi.*
	FROM practicedintervention pi
	JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
	JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
	JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
	JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	WHERE d.active is TRUE
	)
SELECT count(*)
FROM inter_domain
WHERE topiacreatedate > '31-12-2015'
AND topiacreatedate < '01-01-2017';