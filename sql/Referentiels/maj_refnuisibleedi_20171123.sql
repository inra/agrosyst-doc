﻿-- Tout d'abord on s'assurer qu'une savegarde de dans l'état actuel existe 
-- Lorsque la table n'a pas été mise à jour depuis la dernière sauvegarde 
-- stockée, on a déjà une copie


------------------------------------------------------------------------------------------
-- On crée une table pour importer les données de mise à jour
------------------------------------------------------------------------------------------


CREATE TABLE public.modif_refnuisibleedi
(
  reference_id_old integer NOT NULL,
  action text,
  reference_id_new integer,
  CONSTRAINT modif_refnuisibleedi_pkey PRIMARY KEY (reference_id_old)
)
WITH (
  OIDS=FALSE
);

-------------------------------------------------------------------------------------------------
-- Migration des saisies : on recherche les saisies éventuelles des refnuisiblesedi en question
-------------------------------------------------------------------------------------------------

Select * FROM vitipestmaster
WHERE agressor IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM arbopestmaster
WHERE agressor IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM refnuisibleedi_sectors
WHERE owner IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);
-- 1 saisie à modifier (au 23/11/17)


Select * FROM pestmaster
WHERE agressor IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM agressors_pestpressure
WHERE agressors IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM observation
WHERE pest IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM section
WHERE bioagressor IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM managementmode
WHERE historical IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM phytoproductinput_targets
WHERE targets IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

Select * FROM decisionrule
WHERE bioagressor IN
	(Select topiaid from refnuisibleedi WHERE reference_id IN
		(select reference_id_old from modif_refnuisibleedi where action IN
		('migrer les données + supprimer la ligne'))
	);

--------------------------------------------------------------------------------------------
-- Migration des saisies 
--------------------------------------------------------------------------------------------

Au final, je ne suis pas sûre que l on doit mettre à jour refnuisibleedi_sectors car c est juste 
l info du lien à la filiere et à partir du moment où on désactive la ligne dans refnuisibleedi, 
ça suffit je pense

-- WITH table_corres_id AS (
-- 	-- La table avec les données de mise à jour donne les reference_id 
-- 	-- (péréférés car ce sont les mêmes dans agrosyst.fr et demo-test)
-- 	-- Cette requête permet d'avoir les correspondances de topiaid
-- 	Select rne_old.topiaid as topiaid_old, rne_new.topiaid as topiaid_new 
-- 	FROM modif_refnuisibleedi mrne
-- 	JOIN refnuisibleedi rne_old ON mrne.reference_id_old = rne_old.reference_id
-- 	JOIN refnuisibleedi rne_new ON mrne.reference_id_new = rne_new.reference_id
-- 	WHERE action IN ('migrer les données + supprimer la ligne')
-- )
-- 
-- UPDATE refnuisibleedi_sectors rne_s
-- SET owner = (SELECT topiaid_new FROM table_corres_id WHERE topiaid_old = rne_s.owner)
-- -- /!\ one ne veut modifier que les lignes qui ont besoin d'être modifiées
-- WHERE owner IN
-- 	(Select topiaid from refnuisibleedi WHERE reference_id IN
-- 		(select reference_id_old from modif_refnuisibleedi where action IN
-- 		('migrer les données + supprimer la ligne'))
-- 	);

---------------------------------------------------------------------------------------------
-- Désactivation des lignes dans refnuisibleedi
---------------------------------------------------------------------------------------------

UPDATE refnuisibleedi 
SET active = false 
WHERE topiaid IN 
	(Select topiaid from refnuisibleedi WHERE reference_id IN
			(select reference_id_old from modif_refnuisibleedi where action IN
			('migrer les données + supprimer la ligne'))
		);

----------------------------------------------------------------------------------------------
-- On modifie les reference_id pour ça corresponde aux nouveaux reference_id
----------------------------------------------------------------------------------------------

UPDATE refnuisibleedi rne
SET reference_id = (SELECT reference_id_new FROM modif_refnuisibleedi WHERE reference_id_old = rne.reference_id)
WHERE reference_id IN 
	(select reference_id_old from modif_refnuisibleedi where action IN
		('modif_reference_id'));

-----------------------------------------------------------------------------------------------
-- Faire une maj dans l'instance locale et faire quelques contrôles avant maj en prod
-- ex : voir s'il y a bien le nombre de lignes créées attendues (à vérifier avec les agronomes)
-----------------------------------------------------------------------------------------------




 