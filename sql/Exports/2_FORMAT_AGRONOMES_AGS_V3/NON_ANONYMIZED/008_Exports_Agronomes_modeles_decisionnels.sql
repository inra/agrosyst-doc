drop table if exists exports_agronomes_modeles_decisionnels;

-----------------------------------------------
-- Exports_Agronomes_modeles_decisionnels NON ANONYME
-----------------------------------------------

-- Modifications
-- 12/04/2023 : creation du script pour l'export. La table finale a pour cle unique : Modele_decionel_id , type_rubrique, une strategie / levier explication et une culture
-- Utilisation de la table temporaire du script 007_bc_regional refgrpcibleunique. On ne la recree pas ici
-- 29/08/2023 : simplification du code vis à vis des groupes cibles

-----------------------
-- MODELES DECISIONNELS
-----------------------

create table exports_agronomes_modeles_decisionnels as 
select 
mm.topiaid modele_decisionnel_id,
sec.topiaid section_id,
str.topiaid strategie_id,
d.name domaine_nom,  --domaine
d.topiaid domaine_id,
d.code domaine_code,
d.type domaine_type,
d.campaign campagne,
gp.name dispositif_nom,
gp.code dispositif_code,
gp.topiaid dispositif_id,
gp.type dispositif_type,
gs.sector filliere,
gs.name sdc_nom,     -- sdc
gs.topiaid sdc_id,
gs.code sdc_code,
reftypa.reference_label sdc_type_agriculture,
gs.dephynumber sdc_code_dephy,
mm.category categorie_md,
mm.mainchanges principaux_changements,
mm.changereason raison_changement,
mm.mainchangesfromplanned principaux_changements_depuisprevu,
mm.changereasonfromplanned raison_changements_depuisprevu,
sec.sectiontype type_rubrique,  -- section = rubrique
sec.bioagressortype type_bioagresseur,
refgrpcible.groupe_cible_maa grouble_cible,
case 
	when sec.bioagressor like '%RefNuisibleEDI%' then refnui.reference_label
	when sec.bioagressor like '%RefAdventice%' then refadv.adventice
end bioagresseur_considere , 
sec.categoryobjective categorie_objectif,
sec.agronomicobjective objectif_agronomique,
sec.expectedresult resultat_attendu,
sec.categorystrategy categorie_strategie, 
sec.damagetype type_dommage,
reflev.lever levier,
str.strategytype type_levier,
str.implementationtype type_strategie,
str.explanation levier_explication,
cpe.name culture_nom,
cpe.code culture_code,
cpe.topiaid culture_id
FROM section sec
left JOIN strategy str ON sec.topiaid = str.section -- left join : une section (rubrique) n'a pas toujours de strategie
left JOIN refstrategylever reflev on str.refstrategylever = reflev.topiaid
left JOIN refadventice refadv on sec.bioagressor = refadv.topiaid 
left JOIN refnuisibleedi refnui on sec.bioagressor = refnui.topiaid
-- Pour avoir les groupes cibles, on retire le doublons de code 38 'Cicadelles cercopides et psylles' puisque ce nom est utilisé par le 37 , et le 82 puisqu'il y a deux orthographes
left join (select distinct code_groupe_cible_maa,groupe_cible_maa 
           from refciblesagrosystgroupesciblesmaa where active = true and groupe_cible_maa not in ('Cicadelles cercopides et psylles','Maladies des taches foliaires')  
          ) refgrpcible on refgrpcible.code_groupe_cible_maa = sec.codegroupeciblemaa
left JOIN crops_strategy cs on str.topiaid = cs.strategy  -- la culture n'est pas forcement renseignee
left JOIN croppingplanentry cpe on cs.crops = cpe.topiaid 
JOIN managementmode mm on sec.managementmode = mm.topiaid
JOIN growingsystem gs on mm.growingsystem = gs.topiaid 
join reftypeagriculture reftypa on gs.typeagriculture = reftypa.topiaid 
JOIN growingplan gp on gs.growingplan = gp.topiaid 
JOIN domain d on gp.domain = d.topiaid 
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON d.topiaid = crit.domaine_id;
