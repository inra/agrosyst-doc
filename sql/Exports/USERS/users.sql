create table linked_network as
	select
		agrosystuser as identifiant,
		string_agg(NULLIF(n.name, ''), ',') as list_reseau, 
		string_agg(NULLIF(n.codeconventiondephy, ''), ',') as list_codeconventiondephy
	from networkmanager nm
	left join network n on nm.network = n.topiaid
	group by agrosystuser;

create table export_users as
select
	a.topiaid identifiant,
	a.email adresse_mail,
	a.lastname nom,
	a.firstname prenom,
	a.organisation structure,
	a.active actif,
	l_n.list_reseau as liste_reseau,
	l_n.list_codeconventiondephy as liste_code_convention
from agrosystuser a
left join linked_network l_n on l_n.identifiant = a.topiaid;

