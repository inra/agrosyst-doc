#!/bin/bash

###
# Agrosyst
#
# Copyright (C) 2022 INRAE, CodeLutin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###
echo "-> Annonymized"
cat executeExportToCsvHeader > executeExportToCsv.sh
find . -maxdepth 1 -type f -name '*.sql' -exec sh -c '
    declare -i i
    i=1
    for pathname do
        fullPath=$pathname
        pathname=$( basename "$pathname" )
        printf "%s\n" "query_name[$i]=\"${pathname%.*}\"" >> executeExportToCsv.sh
        printf "%s\n" "query[$i]=\""  >> executeExportToCsv.sh
        cat $fullPath >> executeExportToCsv.sh
        printf "\"\n" >> executeExportToCsv.sh
        i+=1
    done' sh {} +
cat executeExportToCsvExecPart.sh >> executeExportToCsv.sh
echo "ficher executeExportToCsv.sh généré"
echo "exécution du script"
sh executeExportToCsv.sh