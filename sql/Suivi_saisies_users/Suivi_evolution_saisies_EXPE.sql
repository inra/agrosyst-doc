﻿
------------------------
-- Requêtes DEPHY-EXPE
------------------------

-- Nombre de SdC actifs validés dans des domaines actifs par filière
SELECT sector, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gs.validated IS TRUE and d.active IS TRUE and gp.type= 'DEPHY_EXPE'
GROUP BY sector 

-- Nombre de SdC actifs dans des domaines actifs par filière
SELECT sector, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and d.active IS TRUE and gp.type= 'DEPHY_EXPE'
GROUP BY sector 

-- Nombre de SdC actifs dans des domaines actifs par filière et par campagne
SELECT sector, campaign, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and d.active IS TRUE and gp.type= 'DEPHY_EXPE'
GROUP BY sector, campaign

-- Evolution du nombre de SdC actifs validés dans des domaines actifs
SELECT date_trunc('month', gs.topiacreatedate) as yearmonth, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gs.validated IS TRUE and d.active IS TRUE and gp.type= 'DEPHY_EXPE'
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- Evolution du nombre de synthétisés validés dans des domaines actifs
SELECT date_trunc('month', ps.topiacreatedate) as yearmonth, count(*)
FROM practicedsystem ps
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
JOIN growingplan gp ON gs.growingplan = gp.topiaid
WHERE gs.active IS TRUE and ps.validated IS TRUE and gp.type= 'DEPHY_EXPE'
GROUP BY yearmonth--, code
ORDER BY yearmonth;

-- Evolution du nombre d'interventions du réalisé des domaines actifs
WITH inter_domain AS (
	SELECT ei.*
	FROM effectiveintervention ei
	JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
	JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
	JOIN zone z ON escc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN growingsystem gs ON p.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON p.domain = d.topiaid -- indiquer que c'est les EXPE
	WHERE d.active is TRUE and gp.type= 'DEPHY_EXPE'

	UNION

	SELECT ei.*
	FROM effectiveintervention ei
	JOIN effectiveperennialcropcycle epcc ON ei.effectivecropcyclephase = epcc.phase
	JOIN zone z ON epcc.zone = z.topiaid
	JOIN plot p ON z.plot = p.topiaid
	JOIN growingsystem gs ON p.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON p.domain = d.topiaid
	WHERE d.active is TRUE and gp.type= 'DEPHY_EXPE'
)
SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) as count
FROM inter_domain
GROUP BY yearmonth
ORDER BY yearmonth

-- Evolution du nombre d'interventions du synthétisé des domaines actifs
WITH inter_domain AS (
	SELECT pi.*
	FROM practicedintervention pi
	JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
	JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
	JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	WHERE d.active is TRUE and gp.type= 'DEPHY_EXPE'

	UNION

	SELECT pi.*
	FROM practicedintervention pi
	JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
	JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
	JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
	JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	WHERE d.active is TRUE and gp.type= 'DEPHY_EXPE'
	)
SELECT date_trunc('month', topiacreatedate) as yearmonth, count(*) as count
FROM inter_domain
GROUP BY yearmonth
ORDER BY yearmonth