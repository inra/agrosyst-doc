﻿

-- à la place de join, il faudra mettre where
-- UPDATE refactadosagespc rads


-- SET dosage_spc_valeur = mrads.dosage_spc_2, dosage_spc_unite = mrads.dosage_spc_unite_2
SELECT *
FROM refactadosagespc rads
JOIN modif_refactadosagespc mrads
-- WHERE rads.topiaid = mrads.topiaid
ON rads.id_produit = mrads.id_produit
AND	rads.id_culture = mrads.id_culture
AND	rads.id_traitement = mrads.id_traitement
AND	rads.dosage_spc_valeur = mrads.dosage_spc_valeur
AND	rads.dosage_spc_commentaire = mrads.dosage_spc_commentaire
AND	rads.commentaire_agrosyst = mrads.commentaire_agrosyst
WHERE (rads.id_produit, 
	rads.id_culture,
	rads.id_traitement ,
	rads.dosage_spc_valeur,
	rads.dosage_spc_commentaire,
	rads.commentaire_agrosyst) IN 
	(SELECT id_produit,
	 id_culture,
	 id_traitement,
	 dosage_spc_valeur,
	 dosage_spc_commentaire,
	  commentaire_agrosyst FROM modif_refactadosagespc
	WHERE TRIM(action) IN ('modifier dose unité')
	)
	