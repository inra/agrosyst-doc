﻿-- listing des utilisateurs avec quelques caractéristiques
select topiaid, topiaversion, topiacreatedate, lastname, firstname, organisation, active from agrosystuser
-- where active = true
order by lastname, firstname;

-- Pour envoi de mails personnalisés à partir d'un fichier csv --
select email, lastname, firstname from agrosystuser 
WHERE active IS TRUE
AND email NOT LIKE '%codelutin.com'
order by lastname;

-- Pour envoyer un mail non personnalisé à tous les utilisateurs
-- Le résultat de cette requête peut-être collé dans le champ "Black Carbon Copy"
-- La requête renvoie les emails séparés par un ;
select string_agg(email, ';')
FROM (
	SELECT email, lastname, firstname FROM agrosystuser 
	WHERE active IS TRUE
	AND email NOT LIKE '%codelutin.com'
	AND email NOT IN ('rgp@inra.fr', 'egsc@inra.fr',
		'rd@inra.fr','egsi@inra.fr','rr@inra.fr',
		'demo@inra.fr','esi@inra.fr')
	ORDER BY lastname, firstname, email
) AS foo;

