﻿
WITH essai AS
	(SELECT DISTINCT practicedseasonalcropcycle
	FROM practicedcropcyclenode pccn
	JOIN practicedcropcycleconnection pccc ON pccc.target = pccn.topiaid
	WHERE notusedforthiscampaign IS TRUE )

SELECT gs.name, ps.name, essai.practicedseasonalcropcycle
FROM essai
JOIN practicedcropcycle pcc ON essai.practicedseasonalcropcycle = pcc.topiaid 
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
