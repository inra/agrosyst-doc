--------------------------------------
-- Parcelles non rattachées à un SDC
--------------------------------------
DROP TABLE IF EXISTS exports_agronomes_parcelles_non_rattachees;

CREATE TABLE exports_agronomes_parcelles_non_rattachees AS

WITH domaines_filtres AS (
SELECT
  d.topiaid domaine_id,
  d.code domaine_code,
  d.name domaine_nom,
  d.campaign campagne,
  gs.topiaid sdc

FROM growingsystem gs
JOIN growingplan gp ON gs.growingplan = gp.topiaid
JOIN domain d ON gp.domain = d.topiaid
WHERE
    d.active IS TRUE
    AND gp.active IS TRUE
    AND gs.active IS TRUE
    AND d.campaign > 2000
    AND gp.type <> 'NOT_DEPHY'
),

parcelles_filtrees AS (
SELECT
    DISTINCT z.plot plot
FROM zone z -- sélection pour le réalisé en cultures pérennes
JOIN effectiveperennialcropcycle epcc ON epcc.zone = z.topiaid
JOIN effectivecropcyclephase eccp ON eccp.topiaid = epcc.phase
JOIN effectiveintervention ei ON ei.effectivecropcyclephase = eccp.topiaid
JOIN plot p ON z.plot = p.topiaid
WHERE
    z.area > 0
	AND z.active IS TRUE
UNION
SELECT
    DISTINCT z.plot plot
FROM zone z -- sélection pour le réalisé en cultures pérennes
JOIN effectiveseasonalcropcycle escc ON escc.zone = z.topiaid
JOIN effectivecropcyclenode eccn ON eccn.effectiveseasonalcropcycle = escc.topiaid
JOIN effectiveintervention ei ON ei.effectivecropcyclenode = eccn.topiaid
JOIN plot p ON z.plot = p.topiaid
WHERE
    z.area > 0
	AND z.active IS TRUE
)

SELECT
  string_agg(DISTINCT n2.name, '|') reseaux_it,
  string_agg(DISTINCT n.name, '|') reseaux_ir,
  string_agg(DISTINCT n.codeconventiondephy, '|') codes_convention_dephy,
  d.domaine_id domaine_id,
  d.domaine_code domaine_code,
  d.domaine_nom domaine_nom,
  d.campagne domaine_campagne,
  count(DISTINCT p.topiaid) nb_parcelles_sans_sdc,
  count(DISTINCT p.topiaid) FILTER (WHERE p.edaplosissuerid IS NOT NULL) nb_parcelles_avec_id_edaplos
FROM plot p
JOIN parcelles_filtrees pf ON pf.plot = p.topiaid
JOIN domaines_filtres d ON p.domain = d.domaine_id
JOIN growingsystem_networks gsn ON gsn.growingsystem = d.sdc
JOIN network n ON n.topiaid = gsn.networks
JOIN network_parents np ON np.network = n.topiaid
JOIN network n2 ON np.parents = n2.topiaid
WHERE p.growingsystem IS NULL
AND p.active IS TRUE
GROUP BY 4, 5, 6, 7
ORDER BY 1
;
