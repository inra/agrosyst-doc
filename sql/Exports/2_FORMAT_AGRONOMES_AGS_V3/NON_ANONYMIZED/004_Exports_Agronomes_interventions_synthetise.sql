------------------------------------------------
-- Interventions Synthetise non anonymes
------------------------------------------------
-- 1.2 : ajout d'un filtre sur le champ ACTIVE (is true) sur tous les elements qui possedent ce champ en plus du domaine : sdc, dispositif, synthetise
-- 2019-12-04 : ajout du stockage des resultats dans une table temporaire
-- 2019-17-12 : ajout de 2 colonnes pour indiquer la presence de traitement chimique ou inoculation biologique des semis
-- 2020-10    : ajout de l'id de la phase pour les cultures perennes
-- 2020-12    : ajout de la phase pour les cultures perennes
-- 2021-04	  : ajout prise en compte de la table de criteres
--				tri par domain_code/campagne plutot que domaine_id/campagne
-- suppression de la table temporaire de stockage des resultats de la requete
-- optimisation du script. Reprise du code entier pour diviser les sous requetes en tables temporaires par thématiques de colonnes

 -- to keep
create index if not exists exports_agronomes_criteres_dispositif_id_index on exports_agronomes_criteres(dispositif_id); 
create index if not exists practicedcropcycle_practicedsystem_idx on practicedcropcycle(practicedsystem);
create index if not exists practicedsystem_topiaid_idx on practicedsystem(topiaid);
 
DROP TABLE IF EXISTS exports_agronomes_context_interventions_synthetise;


-- !! A garder en tete qu'il y a plusieurs "types de synthetises"
-- 1) synthetises monoannuels saisis correctement : annee de synthetise = annee du domaine attache 
-- 2) synthetises monoannuels saisis incorrectement : annee de synthetise != annee du domaine attache => ex : un synthetise attaché à 2011, mais qui a comme campagne 2012
-- => le reste de l'interface ne propose que des elements de la campagne 2012
-- 3) synthetises pluriannuels saisis correctement ou incorrectement : campagne de rattachement n'est pas dans la série de campagne saisie 

-- => on les traite donc tous de la meme facon pour filtrer les lignes d'un meme code (culture, combinaison ...) :
--  ne sont retenues uniquement celle(s) qui sont dans la série de campagne saisie du synthetise

-- Ex : pour les cultures
-- Puisque on a acces seulement aux codes il faut pouvoir filtrer les lignes d'un meme code , choisir les lignes qui correspond aux campagnes renseignées dans le synthetise
-- 1 : jointure de la table critere avec la table culture => avoir la campagne associee a chaque culture 
-- 2 : jointure de la table intervention_synthetise_context avec l'intervention => avoir les campagnes associees au synthetise
-- 3 : filtration des lignes avec where : pour que les lignes de cultures concernent que celles des campagnes du synthetise
--JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id
--join intervention_synthetise_context context on context.intervention_id = pi.topiaid 
--AND crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)

-- !! On a quand meme besoin de choisir une lignes parmi celles retenues pour les cas pluriannuels => max().


-- TABLE DE CONTEXTE : permet d'associer à une intervention_id , la campagne du domaine auquel elle est attachée + la série de campagne saisie du synthetise
drop table if exists intervention_synthetise_context;
CREATE TEMPORARY TABLE intervention_synthetise_context as
select 
  pi.topiaid intervention_id,
  pccc.topiaid connection_id,
  pccnt.topiaid noeud_cible_id,
  'NA' phase_id,
  'NA' perenial_id,
  ps.topiaid synthetise_id,
  gs.topiaid sdc_id,
  crit.dispositif_id,
  crit.domaine_id,
  crit.domaine_campagne,
  ps.campaigns campagnes_synthe
FROM practicedintervention pi
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid -- noeud cible : lie a l intervention 
JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
union 
select 
  pi.topiaid intervention_id,
  'NA' connection_id,
  'NA' noeud_cible_id,
  pcp.topiaid phase_id,
  ppc.topiaid perenial_id,
  ps.topiaid synthetise_id,
  gs.topiaid sdc_id,
  crit.dispositif_id,
  crit.domaine_id,
  crit.domaine_campagne,
  ps.campaigns campagnes_synthe
FROM practicedintervention pi
JOIN practicedcropcyclephase pcp on pi.practicedcropcyclephase = pcp.topiaid
join practicedperennialcropcycle ppc on pcp.practicedperennialcropcycle = ppc.topiaid 
JOIN practicedcropcycle pcc ON ppc.topiaid = pcc.topiaid
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
;

CREATE INDEX intervention_synthetise_context_idx1 on intervention_synthetise_context(intervention_id);


--- Tables de traitements intermediaires où les assolées et perennes sont reunies

-- Les especes et variete concernées par l intervention.
-- !! Changement par rapport à l'ancienne version : ajout d'un distinct(espvar) 
-- => supprimer les doublons d'espece-variete dans la concatenation puisque en selectionnant toutes les années inclues dans la série de campagne du synthetise, il y a des repétitions d'especes


drop table if exists intervention_synthetise_cult_espvar;
CREATE TEMPORARY TABLE intervention_synthetise_cult_espvar as
-- Requete pour especes avec varietes GEVES
WITH esp_var as
  ((SELECT 
    pss.practicedintervention intervention_id,
    cps.topiaid cps_id,
    cps.code cps_code,
    concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
            NULLIF(refesp.libelle_type_saisonnier_aee,''),NULLIF(refesp.libelle_destination_aee,''),'-',
            NULLIF(refvar.denomination,'')) as espvar
  from practicedspeciesstade pss
  join croppingplanspecies cps on pss.speciescode = cps.code 
  JOIN refespece refesp ON cps.species = refesp.topiaid
  JOIN refvarietegeves refvar ON cps.variety = refvar.topiaid
  join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid
  -- filtration selon les campagnes
  JOIN intervention_synthetise_context context on context.intervention_id = pss.practicedintervention 
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
  where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
  ORDER BY cps.topiaid) 

  UNION

  -- Requete pour especes avec varietes PLANTGRAPE
  (SELECT 
    pss.practicedintervention intervention_id,
    cps.topiaid cps_id,
    cps.code cps_code,
    --cps.croppingplanentry culture_id,
    concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
            NULLIF(refesp.libelle_type_saisonnier_aee,''),NULLIF(refesp.libelle_destination_aee,''),'-',
            NULLIF(refvar.variete,'')) as espvar
  from practicedspeciesstade pss
  join croppingplanspecies cps on pss.speciescode = cps.code 
  JOIN refespece refesp ON cps.species = refesp.topiaid
  JOIN refvarieteplantgrape refvar ON cps.variety = refvar.topiaid
  join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid
  -- filtration selon les campagnes
  JOIN intervention_synthetise_context context on context.intervention_id = pss.practicedintervention 
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
  where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
  ORDER BY cps.topiaid)

  UNION

  -- Requete pour especes sans varietes
  (SELECT 
    pss.practicedintervention intervention_id,
    cps.topiaid cps_id,
    cps.code cps_code,
    --cps.croppingplanentry culture_id,
    concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
            NULLIF(refesp.libelle_type_saisonnier_aee,''),NULLIF(refesp.libelle_destination_aee,'')) as espvar
  from practicedspeciesstade pss
  join croppingplanspecies cps on pss.speciescode = cps.code 
  JOIN refespece refesp ON cps.species = refesp.topiaid
  join croppingplanentry cpe on cps.croppingplanentry = cpe.topiaid
  -- filtration selon les campagnes
  JOIN intervention_synthetise_context context on context.intervention_id = pss.practicedintervention 
  JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
  where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
  and cps.variety is null
  ORDER BY cps.topiaid)
  )
select 
  intervention_id,
  string_agg(distinct(espvar),' ; ') as especes_de_l_intervention
from esp_var 
group by intervention_id;


-- Actions : il peut y en avoir plusieurs pour une intervention
-- => concatenation des noms , mais pour les autres infos elle sont separees en colonnes par types d'actions

drop table if exists intervention_synthetise_actions;
CREATE TEMPORARY TABLE intervention_synthetise_actions as
with actions as (
  select 
  aa.practicedintervention intervention_id,
  string_agg(refact.reference_label,' ; ') interventions_actions
  from abstractaction aa 
  JOIN (select * from refinterventionagrosysttravailedi refact order by topiaid) refact ON aa.mainaction = refact.topiaid
  group by aa.practicedintervention
),
prod_phyto as (
  select 
  pi.topiaid intervention_id,
  aa.proportionoftreatedsurface proportion_surface_traitee_phyto,
  proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency psci_phyto
  from abstractaction aa 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  join practicedintervention pi on pi.topiaid = aa.practicedintervention
  where refact.intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
),
lutte_bio as (
  select 
  pi.topiaid intervention_id,
  aa.proportionoftreatedsurface proportion_surface_traitee_lutte_bio,
  proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency psci_lutte_bio
  from abstractaction aa 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  join practicedintervention pi on pi.topiaid = aa.practicedintervention
  where refact.intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
),
irrigation as (
  select 
  pi.topiaid intervention_id,
  aa.waterquantityaverage quantite_eau_mm
  from abstractaction aa 
  JOIN refinterventionagrosysttravailedi refact ON aa.mainaction = refact.topiaid
  join practicedintervention pi on pi.topiaid = aa.practicedintervention
  where refact.intervention_agrosyst = 'IRRIGATION'
),
semis as (
  select intervention_id, 
  string_agg(quantity :: text, ', ') densite_semis,
  string_agg(seedplantunit, ', ') unite_semis,
  string_agg(CASE treatment WHEN TRUE THEN 'oui' ELSE 'non' END, ', ') traitement_chimique_semis,
  string_agg(CASE biologicalseedinoculation WHEN TRUE THEN 'oui' else 'non' END, ', ') inoculation_biologique_semis
  from ( select 
  aa.practicedintervention intervention_id, sas.quantity, sas.seedplantunit, sas.treatment , sas.biologicalseedinoculation
  from abstractaction aa
  join seedingactionspecies sas on aa.topiaid = sas.seedingaction 
  ORDER BY sas.topiaid ) sas2 -- pour avoir les quantite et unite dans le meme ordre
  group by sas2.intervention_id
),
esp_semis as (
  select
  sas2.intervention_id,
  string_agg(nom_sp, ' ; ') as especes_semees
    FROM (select aa.practicedintervention intervention_id, 
          concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''),
              NULLIF(refesp.libelle_type_saisonnier_aee,''), NULLIF(refesp.libelle_destination_aee,'')) nom_sp
    FROM seedingactionspecies sas
    JOIN abstractaction aa ON sas.seedingaction = aa.topiaid
    JOIN croppingplanspecies cps ON sas.speciescode = cps.code 
    join croppingplanentry cpe on cpe.topiaid = cps.croppingplanentry
    JOIN refespece refesp ON cps.species = refesp.topiaid
    -- filtration selon les campagnes
    JOIN intervention_synthetise_context context on context.intervention_id = aa.practicedintervention
    JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
    where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
    order by sas.topiaid) sas2
  group by sas2.intervention_id
)
select
pi.topiaid intervention_id,
a.interventions_actions,
pp.proportion_surface_traitee_phyto,
pp.psci_phyto,
lb.proportion_surface_traitee_lutte_bio,
lb.psci_lutte_bio,
i.quantite_eau_mm,
s.densite_semis,
s.unite_semis,
s.traitement_chimique_semis,
s.inoculation_biologique_semis,
esp.especes_semees
from practicedintervention pi
join actions a on a.intervention_id = pi.topiaid
left join prod_phyto pp on pp.intervention_id = pi.topiaid
left join lutte_bio lb on lb.intervention_id = pi.topiaid
left join semis s on s.intervention_id = pi.topiaid
left join irrigation i on i.intervention_id = pi.topiaid
left join esp_semis esp on esp.intervention_id = pi.topiaid;

-- Combinaisons d outils. la combinaison d'outils choisie est celle liée au domaine auquel le synthetise est attaché
-- precedement, celle choisie était celle ayant le plus long nom, et les autres 

drop table if exists intervention_synthetise_comboutils;
CREATE TEMPORARY TABLE intervention_synthetise_comboutils as
with combinom as (
select
pi.topiaid intervention_id, 
max(tc.topiaid) toolscoupling_id,
max(tc.toolscouplingname) combinaison_outils_nom
from practicedintervention pi
join practicedintervention_toolscouplingcodes pitc ON pitc.owner = pi.topiaid
join toolscoupling tc on tc.code = pitc.toolscouplingcodes
-- filtration selon les campagnes
JOIN intervention_synthetise_context context on context.intervention_id = pi.topiaid 
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON tc.domain = crit.domaine_id 
where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
group by pi.topiaid
), tracteur as (
SELECT 
tc.topiaid toolscoupling_id,
refm.typemateriel1 tracteur_ou_automoteur
FROM toolscoupling tc
JOIN equipment e ON tc.tractor = e.topiaid
JOIN refmateriel refm ON e.refmateriel = refm.topiaid
),
outils as (
select 
i.toolscoupling_id,
string_agg(i.outils, ' ; ') outils
from (
  select 
  tc.topiaid toolscoupling_id,
  max(refm.typemateriel1) outils -- certaines combinaisons ont des equipements differents mais qui renvoient vers le meme type de materiel, on en garde qu'un seul. => group by refm.topiaid 
  from toolscoupling tc
  JOIN equipments_toolscoupling etc ON tc.topiaid = etc.toolscoupling
  JOIN equipment e ON etc.equipments = e.topiaid
  JOIN refmateriel refm ON e.refmateriel = refm.topiaid
  group by refm.topiaid,tc.topiaid 
) i
group by i.toolscoupling_id
)
select
pi.topiaid intervention_id, 
pitc.toolscouplingcodes combinaison_outils_code,
c.combinaison_outils_nom ,
t.tracteur_ou_automoteur,
o.outils
from practicedintervention pi
join practicedintervention_toolscouplingcodes pitc ON pitc.owner = pi.topiaid
left join combinom c on c.intervention_id = pi.topiaid 
left join tracteur t on t.toolscoupling_id = c.toolscoupling_id -- left puisque certaines combinaisons n ont pas de tracteur : interv mannuelles
left join outils o on o.toolscoupling_id = c.toolscoupling_id -- left puisque certaines combinaisons n ont pas de outils
;

-- TABLES specifique aux perennes 
drop table if exists intervention_synthetise_culturespere;
CREATE TEMPORARY table intervention_synthetise_culturespere as
select 
  pi.topiaid intervention_id,
  max(cpe.name) culture_nom,
  max(cpe.code) culture_code
FROM practicedintervention pi
JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
join croppingplanentry cpe on ppcc.croppingplanentrycode = cpe.code 
-- filtration selon les campagnes
JOIN intervention_synthetise_context context on context.intervention_id = pi.topiaid 
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
group by pi.topiaid;


-- TABLES specifiques aux assolees

drop table if exists intervention_synthetise_culturescibles;
CREATE TEMPORARY TABLE intervention_synthetise_culturescibles as
with culture_principale as (
select 
pi.topiaid intervention_id,
max(cpe.name) culture_nom,
max(cpe.code) culture_code,
max((pccnt.rank +1)::text) culture_rang
FROM practicedintervention pi
JOIN intervention_synthetise_context context on context.intervention_id = pi.topiaid 
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid  
join croppingplanentry cpe on pccnt.croppingplanentrycode = cpe.code 
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
group by pi.topiaid
), culture_intermediaire as (
select 
pi.topiaid intervention_id,
max(cpe.name) ci_nom,
max(cpe.code) ci_code
FROM practicedintervention pi
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid  
join croppingplanentry cpe on pccc.intermediatecroppingplanentrycode = cpe.code 
-- filtration selon les campagnes
JOIN intervention_synthetise_context context on context.intervention_id = pi.topiaid 
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
group by pi.topiaid
)
select 
cp.intervention_id,
cp.culture_nom,
cp.culture_code, 
cp.culture_rang,
ci.ci_nom,
ci.ci_code
from culture_principale cp 
left join culture_intermediaire ci on cp.intervention_id = ci.intervention_id 
;

-- Culture précédente 
-- !! Changement par rapport à l'ancienne version : ajout d'un distinct(...) 
-- => supprimer les doublons d'espece dans la concatenation puisque en selectionnant toutes les années inclues dans la série de campagne du synthetise, il y a des repétitions d'especes
-- + suppression des "doublons" qui apparaissent quand la culture précédente a plusieurs variétés d'une meme esp, mais ce champs n'incluant pas les variétés, on se retrouve avec "blé tendre ; blé tendre ; blé tendre"

drop table if exists intervention_synthetise_culturesprecedent;
CREATE TEMPORARY TABLE intervention_synthetise_culturesprecedent as
with culture_precedent as (
select 
  pi.topiaid intervention_id,
  max(cpe.name) precedent_nom,
  string_agg(distinct(concat_ws(' ', NULLIF(refesp.libelle_espece_botanique,''), NULLIF(refesp.libelle_qualifiant_aee,''), -- le distinct permet de ne pas repeter une meme espece quand il y a plusieurs varietes
           NULLIF(refesp.libelle_type_saisonnier_aee,''), NULLIF(refesp.libelle_destination_aee,''))),' ; ') as esp_var
FROM practicedintervention pi
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid -- noeud source 
JOIN croppingplanentry cpe on pccns.croppingplanentrycode = cpe.code 
-- les esp de la culture precedente. left puisque certaines cultures n'ont pas d'esp, ex : celles qui sont des precedents fictifs , ou alors erreur de saisie
left join croppingplanspecies cps on cps.croppingplanentry = cpe.topiaid 
left JOIN refespece refesp ON cps.species = refesp.topiaid 
-- filtration selon les campagnes
JOIN intervention_synthetise_context context on context.intervention_id = pi.topiaid
JOIN (select distinct domaine_id,domaine_code,domaine_nom,domaine_campagne from exports_agronomes_criteres) crit ON cpe.domain = crit.domaine_id 
where crit.domaine_campagne IN (SELECT regexp_split_to_table(context.campagnes_synthe, ', ') :: integer)
group by pi.topiaid
)
select 
  pi.topiaid intervention_id,
  pccc.topiaid culture_precedent_rang_id, -- le nom de la colonne ne correspond pas vraiment, ce serait plutot connection_id
  cp.precedent_nom,
  pccns.croppingplanentrycode precedent_code,
  cp.esp_var precedent_especes_EDI
FROM practicedintervention pi
join intervention_synthetise_context context on context.intervention_id = pi.topiaid
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid -- noeud source 
join culture_precedent cp on cp.intervention_id = pi.topiaid
;


-- l ordre des colonne se fera lors du script execute to csv

CREATE TABLE exports_agronomes_context_interventions_synthetise as
---------------------------
-- Interventions Assolees
---------------------------
WITH toutes_interventions AS (
select
-- ce qui concerne l intervention directement
pi.topiaid intervention_id,
pi.name intervention_nom,
pi.type intervention_type,
pi.comment intervention_comment,
pi.rank rang_intervention, -- pas mis en +1 dans l'historique
pi.startingperioddate date_debut,
pi.endingperioddate date_fin,
pi.spatialfrequency freq_spatiale,
pi.temporalfrequency freq_temporelle,
pi.spatialfrequency*pi.temporalfrequency psci,
pi.workrate debit_de_chantier,
pi.workrateunit debit_de_chantier_unite,
pi.involvedpeoplenumber nb_personne_mobili,
CASE pi.intermediatecrop WHEN true then 'oui' WHEN false then 'non' END concerne_la_ci,
-- ce qui concerne le contexte du synthetise
ps.topiaid systeme_synthetise_id,
ps."name" systeme_synthetise_nom,
ps.campaigns systeme_synthetise_campagnes,
CASE ps.validated WHEN true then 'oui' WHEN false then 'non' END systeme_synthetise_validation,
gs.topiaid sdc_id,
gs."name" sdc_nom,
gs.code sdc_code,
crit.domaine_code, 
crit.domaine_id, 
crit.domaine_nom, 
crit.domaine_campagne,
-- la culture liee a intervention
'NA' phase_id,
'NA' phase,
cultcible.culture_nom,
cultcible.culture_code,
cultcible.culture_rang,
cultcible.ci_nom,
cultcible.ci_code,
-- les esp de l intervention
cultcible_espvar.especes_de_l_intervention,
-- la culture precedente
cultpre.culture_precedent_rang_id,
cultpre.precedent_nom,
cultpre.precedent_code,
cultpre.precedent_especes_edi,
-- ce qui concerne les actions
ac.interventions_actions, 
ac.proportion_surface_traitee_phyto,
ac.psci_phyto,
ac.proportion_surface_traitee_lutte_bio,
ac.psci_lutte_bio,
ac.quantite_eau_mm,
ac.densite_semis,
ac.unite_semis,
ac.traitement_chimique_semis,
ac.inoculation_biologique_semis,
ac.especes_semees,
co.combinaison_outils_code,
co.combinaison_outils_nom,
co.tracteur_ou_automoteur,
co.outils
FROM practicedintervention pi
JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid -- noeud cible : lie a l intervention 
JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
-- jointures des tables cree au prealable
join intervention_synthetise_culturescibles cultcible on cultcible.intervention_id = pi.topiaid
left join intervention_synthetise_cult_espvar cultcible_espvar on cultcible_espvar.intervention_id = pi.topiaid -- il y a des interventions qui n'ont pas d'esp. ex : sur une culture allees et abords, en assolee et perennes
left join intervention_synthetise_culturesprecedent cultpre on cultpre.intervention_id = pi.topiaid -- left puisque certaines cultures n ont pas de precedent
join intervention_synthetise_actions ac on ac.intervention_id = pi.topiaid
left join intervention_synthetise_comboutils co on co.intervention_id = pi.topiaid -- left puisque certaines interventions n ont pas de combinaisons d outils associees
where gs.active IS TRUE and ps.active IS TRUE

UNION

---------------------------
-- Interventions Perennes
---------------------------

select
-- ce qui concerne l intervention directement
pi.topiaid intervention_id,
pi.name intervention_nom,
pi.type intervention_type,
pi.comment intervention_comment,
pi.rank rang_intervention, -- pas mis en +1 dans l'historique
pi.startingperioddate date_debut,
pi.endingperioddate date_fin,
pi.spatialfrequency freq_spatiale,
pi.temporalfrequency freq_temporelle,
pi.spatialfrequency*pi.temporalfrequency psci,
pi.workrate debit_de_chantier,
pi.workrateunit debit_de_chantier_unite,
pi.involvedpeoplenumber nb_personne_mobili,
CASE pi.intermediatecrop WHEN true then 'oui' WHEN false then 'non' END concerne_la_ci,
-- ce qui concerne le contexte du synthetise
ps.topiaid systeme_synthetise_id,
ps."name" systeme_synthetise_nom,
ps.campaigns systeme_synthetise_campagnes,
CASE ps.validated WHEN true then 'oui' WHEN false then 'non' END systeme_synthetise_validation,
gs.topiaid sdc_id,
gs."name" sdc_nom,
gs.code sdc_code,
crit.domaine_code, 
crit.domaine_id, 
crit.domaine_nom, 
crit.domaine_campagne,
-- la culture liee a intervention
pccp.topiaid phase_id,
pccp.type phase,
cult.culture_nom,
cult.culture_code,
'NA' culture_rang,
'NA' ci_nom,
'NA' ci_code,
-- les esp de l intervention
cult_espvar.especes_de_l_intervention,
-- la culture precedente (assolees)
'NA' culture_precedent_rang_id,
'NA' precedent_nom,
'NA' precedent_code,
'NA' precedent_especes_edi,
-- ce qui concerne les actions
ac.interventions_actions, 
ac.proportion_surface_traitee_phyto,
ac.psci_phyto,
ac.proportion_surface_traitee_lutte_bio,
ac.psci_lutte_bio,
ac.quantite_eau_mm,
ac.densite_semis,
ac.unite_semis,
ac.traitement_chimique_semis,
ac.inoculation_biologique_semis,
ac.especes_semees,
co.combinaison_outils_code,
co.combinaison_outils_nom,
co.tracteur_ou_automoteur,
co.outils
FROM practicedintervention pi
JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
join practicedcropcycle pcc on pcc.topiaid = ppcc.topiaid
join practicedsystem ps on ps.topiaid = pcc.practicedsystem 
JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
JOIN exports_agronomes_criteres crit ON gs.growingplan = crit.dispositif_id
-- jointures des tables cree au prealable
join intervention_synthetise_culturespere cult on cult.intervention_id = pi.topiaid 
left join intervention_synthetise_cult_espvar cult_espvar on cult_espvar.intervention_id = pi.topiaid -- il y a des interventions qui n'ont pas d'esp. ex : sur une culture allees et abords, en assolee et perennes
join intervention_synthetise_actions ac on ac.intervention_id = pi.topiaid
left join intervention_synthetise_comboutils co on co.intervention_id = pi.topiaid
where gs.active IS TRUE and ps.active IS TRUE
) 
SELECT * FROM toutes_interventions
ORDER BY domaine_code, domaine_campagne, sdc_nom, systeme_synthetise_nom, rang_intervention
;


-- control de cle unique , si non , renverra un message de violation de contrainte avec un exemple
-- mais ne bloquera pas la génération de l'export !
alter table exports_agronomes_interv_synthe
add constraint exports_agronomes_context_interventions_synthetise_PK
PRIMARY KEY (intervention_id);



