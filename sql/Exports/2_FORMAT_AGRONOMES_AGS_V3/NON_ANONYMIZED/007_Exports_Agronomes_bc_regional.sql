DROP TABLE IF EXISTS exports_agronomes_bilan_campagne_regional;

-------------------------------
-- bilan de campagne regional
-------------------------------

-- Modifications :
-- 12/04/2023 : reprise du code du bilan de campagne regional : insertions de colonnes en plus et division en 2 cas en fonction de la saisie ou non du groupe cible
-- la table finale a pour cle unique : BCR_id , type_pression , un grpcible ou un ravageur defini
-- 29/08/2023 : simplification des groupes cibles et de la clé unique : bilan_campagne_regional_id + pression_agresseur_id
-- pour la colonne nom_maladie_ravageur, concaténation des labels du référentiel des nuisibles lorsque plusieurs maladies pour une même observation de pression sont sélectionnées


-----------------------------
-- Bilan campagne regionaux 
-----------------------------

create table exports_agronomes_bilan_campagne_regional as 
select 
rr.topiaid bilan_campagne_regional_id,
pp.topiaid pression_agresseur_id,
rr.name bilan_campagne_regional_nom,
rr.code bilan_campagne_regional_code,
rr.author auteur, 
nr.reseau reseaux,
rrs2.sectors secteurs,
rrss2.sectorspecies arbo_esp,
rr.campaign campagne,
rr.highlights faits_marquants,
rr.rainfallmarchjune pluvio_marsjuin,
rr.rainydaysmarchjune jourspluie_marsjuin,
rr.rainfalljulyoctober pluvio_juiloct,
case -- on renseigne soit des ravageurs soit des maladies. Mais dans la table pp, cela est rempli dans deux colonnes differentes. Je regroupe en 1 colonne avec deux modalites 
	when pp.diseasepressurereportregional like '%ReportRegional%' then 'maladies'
	when pp.pestpressurereportregional like '%ReportRegional%' then 'ravageurs'
end type_pression,
refgrpcible.groupe_cible_maa groupe_cible,
ap2.nuisibles_agg nom_maladie_ravageur,
pp.crops culture_concernee,
pp.pressurescale pression_annee,
pp.pressureevolution pression_evolution,
pp.comment commentaires,
rr.primarycontaminations INOKI_nb_contaminationprimaire,
rr.numberofdayswithprimarycontaminations INOKI_nbjours_contamitationprimaire,
rr.secondarycontaminations RIMpro_nb_contaminationsecondaire,
rr.rimsum RIMpro_sommeRIM
from reportregional rr
-- la plupart des bilans de campagnes sont crés mais pas remplis, on fait donc des left join
left join pestpressure pp on (rr.topiaid = pp.diseasepressurereportregional or rr.topiaid = pp.pestpressurereportregional) 
-- Pour avoir les groupes cibles, on retire le doublons de code 38 'Cicadelles cercopides et psylles' puisque ce nom est utilisé par le 37 , et le 82 puisqu'il y a deux orthographes
left join (select distinct code_groupe_cible_maa,groupe_cible_maa 
           from refciblesagrosystgroupesciblesmaa where active = true and groupe_cible_maa not in ('Cicadelles cercopides et psylles','Maladies des taches foliaires')  
          ) refgrpcible on (pp.codegroupeciblemaa = refgrpcible.code_groupe_cible_maa)
-- pour les maladies, on peut en mettre plusieurs pour une meme pression en maladie
left JOIN (select ap.pestpressure, string_agg(refnui.reference_label, ' ; ') nuisibles_agg 
                  from agressors_pestpressure ap
                  join refnuisibleedi refnui on ap.agressors = refnui.topiaid 
                  group by ap.pestpressure) ap2 on pp.topiaid = ap2.pestpressure
-- Pour la suite, il peut y avoir plusieurs (ou aucuns) reseaux, secteurs et especes pour arbo qui ont été cochés, donc on fait un left 
join (select nrg.reportregional, string_agg(distinct n.name,'|') reseau -- Quand il y a plusieurs IR pour un meme id de rr, on agrege les noms d'IR sur la meme ligne
  from networks_reportregional nrg   
	join network n on nrg.networks = n.topiaid group by nrg.reportregional) nr on nr.reportregional = rr.topiaid
left join (select owner, string_agg(sectors, ' ; ') sectors from reportregional_sectors rrs group by owner) rrs2  on rr.topiaid = rrs2.owner
left join (select owner, string_agg(sectorspecies, ' ; ' ) sectorspecies from reportregional_sectorspecies rrss group by owner) rrss2 on rr.topiaid = rrss2.owner
;


