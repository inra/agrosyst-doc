﻿DELETE FROM PRICE WHERE domain = 'domainId';

-- phytoproducttarget
DELETE FROM phytoproducttarget WHERE phytoproductinput IN (
  SELECT ai.topiaid FROM abstractinput ai 
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM phytoproducttarget WHERE phytoproductinput IN (
  SELECT ai.topiaid FROM abstractinput ai 
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- abstractinput
DELETE FROM abstractinput WHERE topiaid IN (
  SELECT ai.topiaid FROM abstractinput ai 
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM abstractinput WHERE topiaid IN (
  SELECT ai.topiaid FROM abstractinput ai 
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);


-- SeedingActionSpecies
DELETE FROM seedingactionspecies WHERE topiaid IN (
  SELECT sas.topiaid FROM seedingactionspecies sas 
  INNER JOIN abstractaction aa ON  aa.topiaid = seedingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM seedingactionspecies WHERE topiaid IN (
  SELECT sas.topiaid FROM seedingactionspecies sas 
  INNER JOIN abstractaction aa ON  aa.topiaid = seedingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

--harvestingactionvalorisation
DELETE FROM harvestingactionvalorisation WHERE topiaid IN (
  SELECT hav.topiaid FROM harvestingactionvalorisation hav 
  INNER JOIN abstractaction aa ON  aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM harvestingactionvalorisation WHERE topiaid IN (
  SELECT hav.topiaid FROM harvestingactionvalorisation hav 
  INNER JOIN abstractaction aa ON  aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- action
DELETE FROM abstractaction WHERE topiaid IN (
  SELECT aa.topiaid FROM abstractaction aa
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM abstractaction WHERE topiaid IN (
  SELECT aa.topiaid FROM abstractaction aa
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

--effectivespeciesstade
DELETE FROM effectivespeciesstade WHERE topiaid IN (
  SELECT ess.topiaid FROM effectivespeciesstade ess
  INNER JOIN effectiveintervention ei ON ei.topiaid = ess.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM effectivespeciesstade WHERE topiaid IN (
  SELECT ess.topiaid FROM effectivespeciesstade ess
  INNER JOIN effectiveintervention ei ON ei.topiaid = ess.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- effectiveintervention_toolcouplings
DELETE FROM effectiveintervention_toolcouplings WHERE effectiveintervention IN (
  SELECT ei.topiaid FROM effectiveintervention ei
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM effectiveintervention_invlovedrules WHERE effectiveintervention IN (
  SELECT ei.topiaid FROM effectiveintervention ei
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM effectiveintervention_toolcouplings WHERE effectiveintervention IN (
  SELECT ei.topiaid FROM effectiveintervention ei
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);


-- intervention
DELETE FROM effectiveintervention WHERE topiaid IN (
  SELECT ei.topiaid FROM effectiveintervention ei
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM effectiveintervention WHERE topiaid IN (
  SELECT ei.topiaid FROM effectiveintervention ei
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

--phase
DELETE FROM effectivecropcyclephase WHERE topiaid IN (
  SELECT phase.topiaid FROM effectivecropcyclephase phase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- effectiveperennialcropcycle
DELETE FROM effectiveperennialcropcycle WHERE topiaid IN (
  SELECT epcc.topiaid FROM effectiveperennialcropcycle epcc
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);


-- effectivecropcycleconnection
DELETE FROM effectivecropcycleconnection WHERE topiaid IN (
  SELECT eccc.topiaid FROM effectivecropcycleconnection eccc 
  INNER JOIN effectivecropcyclenode eccn ON eccc.target = eccn.topiaid
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

DELETE FROM effectivecropcycleconnection WHERE topiaid IN (
  SELECT eccc.topiaid FROM effectivecropcycleconnection eccc 
  INNER JOIN effectivecropcyclenode eccn ON eccc.source = eccn.topiaid
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- effectivecropcyclenode
DELETE FROM effectivecropcyclenode WHERE topiaid IN (
  SELECT eccn.topiaid FROM effectivecropcyclenode eccn
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- effectiveseasonalcropcycle
DELETE FROM effectiveseasonalcropcycle WHERE topiaid IN (
  SELECT escc.topiaid FROM effectiveseasonalcropcycle escc
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- zone
DELETE FROM zone WHERE topiaid IN (
  SELECT zone.topiaid FROM zone zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  WHERE plot.domain = 'domainId'
);


-- solHorizon
DELETE FROM solHorizon WHERE topiaid IN (
  SELECT sh.topiaid FROM solHorizon sh
  INNER JOIN plot plot ON sh.basicplot = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- performance_plots
DELETE FROM performance_plots WHERE plots IN (
  SELECT perf.plots FROM performance_plots perf
  INNER JOIN plot plot ON perf.plots = plot.topiaid
  WHERE plot.domain = 'domainId'
);

-- plot_lineage
DELETE FROM plot_lineage WHERE owner IN (
  SELECT lin.owner FROM plot_lineage lin
  INNER JOIN plot plot ON lin.owner = plot.topiaid
  WHERE plot.domain = 'domainId'
);

--plot
DELETE FROM plot WHERE topiaid IN (
  SELECT p.topiaid FROM plot p
  WHERE p.domain = 'domainId'
);

--aliment
DELETE FROM aliment WHERE topiaid IN (
  SELECT a.topiaid FROM aliment a
  INNER JOIN ration rat ON rat.topiaid = a.ration
  INNER JOIN cattle cat ON cat.topiaid = rat.cattle
  INNER JOIN livestockunit lsu ON lsu.topiaid = cat.livestockunit
  WHERE lsu.domain = 'domainId'
);

-- ration
DELETE FROM ration WHERE topiaid IN (
  SELECT rat.topiaid FROM ration rat
  INNER JOIN cattle cat ON cat.topiaid = rat.cattle
  INNER JOIN livestockunit lsu ON lsu.topiaid = cat.livestockunit
  WHERE lsu.domain = 'domainId'
);

-- cattle
DELETE FROM cattle WHERE topiaid IN (
  SELECT cat.topiaid FROM cattle cat
  INNER JOIN livestockunit lsu ON lsu.topiaid = cat.livestockunit
  WHERE lsu.domain = 'domainId'
);

-- livestockunit
DELETE FROM livestockunit WHERE topiaid IN (
  SELECT lsu.topiaid FROM livestockunit lsu
  WHERE lsu.domain = 'domainId'
);


-- mainsactions_toolscoupling
DELETE FROM mainsactions_toolscoupling WHERE toolscoupling IN (
  SELECT tc.topiaid FROM toolscoupling tc WHERE tc.domain = 'domainId'
);

-- equipments_toolscoupling
DELETE FROM equipments_toolscoupling WHERE toolscoupling IN (
  SELECT tc.topiaid FROM toolscoupling tc WHERE tc.domain = 'domainId'
);

-- toolscoupling
DELETE FROM toolscoupling WHERE domain = 'domainId';

-- equipment
DELETE FROM equipment WHERE domain = 'domainId';


-- les tables suivantes ne doivent pas contenir de valeurs normalement suite à un import edaplos


DELETE FROM effectiveintervention_invlovedrules WHERE invlovedrules IN (
  SELECT eir.topiaid FROM effectiveinvolvedrule eir
  INNER JOIN decisionrule dr ON eir.decisionrule = dr.topiaid
  INNER JOIN rules_strategy rs ON rs.rules = dr.topiaid
  INNER JOIN strategy st ON st.topiaid = rs.strategy
  INNER JOIN section se ON se.topiaid = st.section
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

DELETE FROM effectiveinvolvedrule WHERE decisionrule IN (
  SELECT dr.topiaid FROM decisionrule dr
  INNER JOIN rules_strategy rs ON rs.rules = dr.topiaid
  INNER JOIN strategy st ON st.topiaid = rs.strategy
  INNER JOIN section se ON se.topiaid = st.section
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

DELETE FROM decisionrulecrop WHERE topiaid IN (
  SELECT drc.decisionrulecrop FROM decisionrule_decisionrulecrop drc
  INNER JOIN decisionrule dr ON dr.topiaid = drc.decisionrule
  INNER JOIN rules_strategy rs ON rs.rules = dr.topiaid
  INNER JOIN strategy st ON st.topiaid = rs.strategy
  INNER JOIN section se ON se.topiaid = st.section
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

DELETE FROM decisionrule_decisionrulecrop WHERE decisionrule IN (
  SELECT dr.topiaid FROM decisionrule dr
  INNER JOIN rules_strategy rs ON rs.rules = dr.topiaid
  INNER JOIN strategy st ON st.topiaid = rs.strategy
  INNER JOIN section se ON se.topiaid = st.section
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

DELETE FROM decisionrule WHERE topiaid IN (
  SELECT dr.topiaid FROM decisionrule dr
  INNER JOIN rules_strategy rs ON rs.rules = dr.topiaid
  INNER JOIN strategy st ON st.topiaid = rs.strategy
  INNER JOIN section se ON se.topiaid = st.section
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);


-- crops_strategy                                                                 
DELETE FROM crops_strategy WHERE crops IN (
    SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

-- strategy
DELETE FROM strategy WHERE section IN (
    SELECT se.topiaid FROM section se
    INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
    INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    WHERE gp.domain = 'domainId'
);

-- rules_strategy
DELETE FROM rules_strategy WHERE strategy IN (
    SELECT st.topiaid FROM strategy st
    INNER JOIN section se ON se.topiaid = st.section
    INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
    INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId'
);

-- rules_strategy
DELETE FROM section WHERE topiaid IN (
    SELECT se.topiaid FROM section se
    INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
    INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    WHERE gp.domain = 'domainId'
);

DELETE FROM managementmode WHERE topiaid IN (
    SELECT mm.topiaid FROM managementmode mm
    INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    WHERE gp.domain = 'domainId'
);

--arbocroppestmaster_species
DELETE FROM arbocroppestmaster_species WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

--arboyieldloss_species
--DELETE FROM arboyieldloss_species WHERE species IN (
--  SELECT cps.topiaid FROM croppingplanspecies cps
--  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
--  WHERE cpe.domain = 'domainId'
--);

--arbocropadventicemaster_species
DELETE FROM arbocropadventicemaster_species WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

--arbocropadventicemaster_crops
DELETE FROM arbocropadventicemaster_crops WHERE crops IN(
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

--arbocroppestmaster_crops
DELETE FROM arbocroppestmaster_crops WHERE crops IN(
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

--arboyieldloss_crops
--DELETE FROM arboyieldloss_crops WHERE crops IN(
--  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
--);

--arboadventicemaster
DELETE FROM arboadventicemaster WHERE arbocropadventicemaster IN ( 
    SELECT aam.topiaid FROM arbocropadventicemaster aam
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = reportgrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId');

--arbocropadventicemaster
DELETE FROM arbocropadventicemaster WHERE reportgrowingsystem IN (
    SELECT rgs.topiaid FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId');


--arbopestmaster
DELETE FROM arbopestmaster WHERE arbocroppestmaster IN(
SELECT acpm.topiaid FROM arbocroppestmaster acpm
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = acpm.arbodiseasemasterreportgrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId'
);

DELETE FROM arbopestmaster WHERE arbocroppestmaster IN(
SELECT acpm.topiaid FROM arbocroppestmaster acpm
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = acpm.arbopestmastergrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId'
);

--arbopestmaster
DELETE FROM arbopestmaster WHERE arbocroppestmaster IN(
SELECT acpm.topiaid FROM arbocroppestmaster acpm
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = acpm.arbodiseasemasterreportgrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId'
);

--arbocroppestmaster
DELETE FROM arbocroppestmaster WHERE CONCAT (arbodiseasemasterreportgrowingsystem,arbopestmastergrowingsystem) IN(
SELECT rgs.topiaid FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId'
);

--arboyieldloss
--DELETE FROM arboyieldloss WHERE reportgrowingsystem IN(
--SELECT rgs.topiaid FROM reportgrowingsystem rgs
--    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
--    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
--    WHERE gp.domain = 'domainId'
--);


-- vitipestmaster
DELETE FROM vitipestmaster WHERE CONCAT (reportgrowingsystemvitidiseasemaster, reportgrowingsystemvitipestmaster) IN ( SELECT rgs.topiaid FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
    WHERE gp.domain = 'domainId');


-- croppestmaster_species
DELETE FROM croppestmaster_species WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

DELETE FROM croppestmaster_crops WHERE crops IN(
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

-- pestmaster
DELETE FROM pestmaster WHERE croppestmaster IN(
    SELECT cpm.topiaid FROM croppestmaster cpm
    WHERE CONCAT(cpm.cropadventicemasterreportgrowingsystem, cpm.cropdiseasemasterreportgrowingsystem, cpm.croppestmasterreportgrowingsystem) IN (
        SELECT rgs.topiaid FROM reportgrowingsystem rgs
        INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
        INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
        WHERE gp.domain = 'domainId'
    )
);

-- croppestmaster
DELETE FROM croppestmaster WHERE CONCAT(cropadventicemasterreportgrowingsystem, cropdiseasemasterreportgrowingsystem, croppestmasterreportgrowingsystem) IN (
    SELECT rgs.topiaid FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    WHERE gp.domain = 'domainId'
);

-- species_versemaster
DELETE FROM species_versemaster WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

-- crops_versemaster
DELETE FROM crops_versemaster WHERE crops IN (
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

-- species_yieldloss
DELETE FROM species_yieldloss WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

-- crops_yieldloss
DELETE FROM crops_yieldloss WHERE crops IN (
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

-- yieldloss
DELETE FROM yieldloss WHERE reportgrowingsystem IN (
  SELECT rgs.topiaid FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    WHERE gp.domain = 'domainId'
);

-- species_versemaster
DELETE FROM species_versemaster WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

-- crops_versemaster
DELETE FROM crops_versemaster WHERE crops IN (
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

--versemaster
DELETE FROM versemaster WHERE reportgrowingsystem IN (
  SELECT rgs.topiaid FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    WHERE gp.domain = 'domainId'
);

-- foodmaster_species
DELETE FROM foodmaster_species WHERE species IN (
  SELECT cps.topiaid FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  WHERE cpe.domain = 'domainId'
);

-- crops_foodmaster
DELETE FROM crops_foodmaster WHERE crops IN (
  SELECT topiaid FROM croppingplanentry WHERE domain = 'domainId'
);

-- foodmaster
DELETE FROM foodmaster WHERE foodmasterreportgrowingsystem IN (
  SELECT rgs.topiaid FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    WHERE gp.domain = 'domainId'
);

--reportgrowingsystem_sectors
DELETE FROM reportgrowingsystem_sectors WHERE owner IN(
  SELECT rgs.topiaid FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    WHERE gp.domain = 'domainId'
);

-- reportgrowingsystem
DELETE FROM reportgrowingsystem  WHERE growingsystem IN (
  SELECT gs.topiaid FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid 
  WHERE gp.domain = 'domainId'
); 

-- croppingplanspecies
DELETE FROM croppingplanspecies WHERE croppingplanentry IN (
  SELECT cpe.topiaid FROM croppingplanentry cpe WHERE cpe.domain = 'domainId'
);

-- croppingplanentry
DELETE FROM croppingplanentry WHERE domain = 'domainId';

--domains_performance
DELETE FROM domains_performance WHERE domains = 'domainId';

--growingsystem_networks
DELETE FROM growingsystem_networks WHERE growingsystem IN (
  SELECT gs.topiaid FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

-- growingsystems_performance
DELETE FROM growingsystems_performance WHERE growingsystems IN (
  SELECT gs.topiaid FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

-- characteristics_growingsystem
DELETE FROM characteristics_growingsystem WHERE growingsystem IN (
  SELECT gs.topiaid FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  WHERE gp.domain = 'domainId'
);

-- growingsystem
DELETE FROM growingsystem WHERE growingplan IN (
  SELECT gp.topiaid FROM growingplan gp WHERE gp.domain = 'domainId'
);

-- growingplan
DELETE FROM growingplan WHERE domain = 'domainId';

-- ground
DELETE FROM ground WHERE domain = 'domainId';

-- geopoint
DELETE FROM geopoint WHERE domain = 'domainId';

-- weatherstation
UPDATE domain SET defaultweatherstation = NULL WHERE topiaid = 'domainId';
DELETE FROM weatherstation WHERE domain = 'domainId';

-- domain
DELETE FROM domain WHERE topiaid = 'domainId';

-- reste à faire (entre autres)
-- practicedsystem

