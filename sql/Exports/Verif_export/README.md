### Scripts permettant de faire des vérifications des exports de données 

### Structure
Il y a deux scripts : 
- main.sh : fait des vérifications uniquement sur les nouveaux exports, à partir des csv générés : fichiers vides, separateurs de fin de ligne, encodage, 

- main.py qui est exécuté par main.sh : comparaison par rapport à l'ancien export : le nombre de ligne est plus élevé, les clés uniques ...

### Données 
Le script a besoin d'un repertoire data/ avec :
- data/liste_table.csv pour obtenir les clés unique de chaque table.
- data/old : contient les anciens exports
- data/new : contient les nouveaux exports

Pour les répertoires old et new, les données sont des archives .zip : pour data/old/anonyme/ et data/old/non_anonyme/
- _brute.zip : données brutes
- _perf.zip : données de performances 


### Execution
Pour executer la procédure de vérification : 
```
	bash main.sh
```
