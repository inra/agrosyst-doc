﻿
---------------------------------
-- Requête Réalisé + Synthétisé
---------------------------------

WITH synthetise_realise AS (

-- Réalisé

(
WITH plot_inter as 
    (
    -- nb d'interventions réalisées par parcelle, cycles d'sp assolées
    SELECT p.topiaid, count(*) nb_interventions
    FROM effectiveintervention ei
    JOIN effectivecropcyclenode eccn ON ei.effectivecropcyclenode = eccn.topiaid
    JOIN effectiveseasonalcropcycle escc ON eccn.effectiveseasonalcropcycle = escc.topiaid
    JOIN zone z ON escc.zone = z.topiaid
    JOIN plot p ON z.plot = p.topiaid
    GROUP BY p.topiaid

    -- un UNION simple supprimerait les doublons    
    UNION ALL

    -- nb d'interventions réalisées par parcelles, sp pérennes
    SELECT p.topiaid, count(*) nb_interventions
    FROM effectiveintervention ei
    JOIN effectivecropcyclephase eccp ON ei.effectivecropcyclephase = eccp.topiaid
    JOIN effectiveperennialcropcycle epcc ON eccp.topiaid = epcc.phase
    JOIN zone z ON epcc.zone = z.topiaid
    JOIN plot p ON z.plot = p.topiaid
    GROUP BY p.topiaid
    ),
domain_expe_ou_sans_dispo as 
	( -- liste des domaines sans dispositifs ou avec des dispositifs EXPE
	SELECT d.topiaid, d.campaign, d.name, d.active, gp.type FROM domain d
	LEFT JOIN growingplan gp ON d.topiaid = gp.domain
	WHERE (gp.type = 'DEPHY_EXPE' OR gp.type IS NULL)
	)

-- On joint les parcelles avec leur nombre d'interventions, aux domaines EXPE ou non typés et le cas échéant à leur SdC, dispositif
SELECT 'réalisé' AS synthétisé_réalisé, string_agg (n.name, ' - ') réseau, 
	deosd.campaign domaine_campagne, deosd.name domaine_nom, 
	gp.name dispositif_nom, gp.type dispositif_type,
	gs.name sdc_nom, gs.dephynumber sdc_num_dephy, gs.active sdc_actif, gs.validated sdc_validation, 
	p.topiaid synthe_parcelle_id, p.name synthétisé_nom_parcelle_nom, NULL AS synthétisé_série_de_campagnes, NULL AS synthétisé_validation,
	COALESCE(sum(nb_interventions),0) nb_interventions
FROM plot p
	LEFT JOIN plot_inter pi ON p.topiaid = pi.topiaid
	JOIN domain_expe_ou_sans_dispo deosd ON p.domain = deosd.topiaid
	LEFT JOIN growingplan gp ON deosd.topiaid = gp.domain
	LEFT JOIN growingsystem gs ON gs.topiaid = p.growingsystem
	LEFT JOIN growingsystem_networks gsn ON gs.topiaid = gsn.growingsystem
	LEFT JOIN network n ON gsn.networks = n.topiaid
WHERE (gs.active IS TRUE OR gs.active IS NULL) -- Important d'ajouter "OR gs.active IS NULL" pour les parcelles sans SdC
	AND deosd.active IS TRUE
GROUP BY p.topiaid, p.name, deosd.campaign, deosd.name, gp.name, gp.type, gs.name, gs.dephynumber, gs.active , gs.validated
ORDER BY deosd.name, deosd.campaign, réseau, gp.name, gp.type, gs.name, gs.dephynumber
)

UNION ALL

-- Synthétisé

(
WITH practicedsystem_inter AS (
    SELECT ps.topiaid, count(*) nb_interventions
    FROM practicedintervention pi
    JOIN practicedcropcycleconnection pccc ON pccc.topiaid = pi.practicedcropcycleconnection
    JOIN practicedcropcyclenode pccn ON pccn.topiaid = pccc.target
    JOIN practicedseasonalcropcycle pscc ON pscc.topiaid = pccn.practicedseasonalcropcycle
    JOIN practicedcropcycle pcc ON pcc.topiaid = pscc.topiaid
    JOIN practicedsystem ps ON ps.topiaid = pcc.practicedsystem
    GROUP by ps.topiaid
    
UNION ALL

    SELECT ps.topiaid, count(*) nb_interventions
    FROM practicedintervention pi
    JOIN practicedcropcyclephase pccp ON pccp.topiaid = pi.practicedcropcyclephase
    JOIN practicedperennialcropcycle ppcc ON ppcc.topiaid = pccp.practicedperennialcropcycle
    JOIN practicedcropcycle pcc ON pcc.topiaid = ppcc.topiaid
    JOIN practicedsystem ps ON ps.topiaid = pcc.practicedsystem
    GROUP by ps.topiaid
)

SELECT 'synthétisé' AS synthétisé_réalisé, string_agg(n.name, ' - ') réseau, 
	d.campaign domaine_campagne, d.name domaine_nom, 
	gp.name dispositif_nom, gp.type dispositif_type,
	gs.name sdc_nom, gs.dephynumber sdc_num_dephy, gs.active sdc_actif, gs.validated sdc_validation, 
	ps.topiaid synthe_parcelle_id, ps.name synthétisé_nom_parcelle_nom, ps.campaigns synthétisé_série_de_campagnes, ps.validated synthétisé_validation,
	COALESCE (sum(nb_interventions), 0) nb_interventions
FROM practicedsystem ps
	LEFT JOIN practicedsystem_inter psi ON ps.topiaid = psi.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid	
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid
	LEFT JOIN growingsystem_networks gsn ON gs.topiaid = gsn.growingsystem
	LEFT JOIN network n ON gsn.networks = n.topiaid
WHERE gp.type = 'DEPHY_EXPE'
	AND ps.active IS TRUE
	AND d.active IS TRUE
GROUP BY d.campaign, d.name, gp.name, gp.type, gs.name, gs.dephynumber, gs.active, gs.validated, ps.topiaid, ps.name, ps.validated, ps.campaigns
ORDER BY réseau, d.name, gp.name, gs.name, ps.name
)

)

SELECT sr.synthétisé_réalisé, sr.réseau, sr.domaine_campagne, sr.domaine_nom, sr.dispositif_nom, sr.dispositif_type, sr.sdc_nom, sr.sdc_num_dephy, 
CASE sr.sdc_actif
WHEN true THEN 'oui'
WHEN false THEN 'non'
END sdc_actif,
CASE sr.sdc_validation
WHEN true THEN 'oui'
WHEN false THEN 'non'
END sdc_validation, 
sr.synthe_parcelle_id, sr.synthétisé_nom_parcelle_nom, sr.synthétisé_série_de_campagnes, 
CASE sr.synthétisé_validation
WHEN true THEN 'oui'
WHEN false THEN 'non'
END synthétisé_validation,
sr.nb_interventions

FROM synthetise_realise sr
ORDER BY sr.domaine_nom, sr.domaine_campagne, sr.dispositif_nom,  sr.sdc_nom
