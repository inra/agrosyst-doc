﻿
------------------------
-- Requêtes DEPHY-FERME
------------------------

-- Nombre de SdC actifs dans des domaines actifs par filière et par campagne
SELECT sector, campaign, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gp.active IS TRUE and d.active IS TRUE and gp.type= 'DEPHY_FERME'
GROUP BY sector, campaign
ORDER BY sector, campaign

-- Nombre de SdC actifs validés dans des domaines actifs par filière et par campagne
SELECT sector, campaign, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gs.validated IS TRUE and gp.active IS TRUE AND d.active IS TRUE and gp.type= 'DEPHY_FERME'
GROUP BY sector, campaign
ORDER BY sector, campaign

-- Nombre de systèmes synthétisés actifs validés de SdC non validés dans des domaines actifs par filière et par campagne
SELECT sector, campaign, count(*)
FROM practicedsystem ps
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and ps.validated IS TRUE and gs.validated IS FALSE and gp.active IS TRUE AND d.active IS TRUE and gp.type= 'DEPHY_FERME'
GROUP BY sector, campaign
ORDER BY sector, campaign

-- Nombre de SdC actifs non validés dans des domaines actifs par filière et par campagne, avec au moins une intervention -- à compléter !!!
SELECT sector, campaign, count(*)
FROM growingsystem gs
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
WHERE gs.active IS TRUE and gs.validated IS FALSE and d.active IS TRUE and gp.type= 'DEPHY_FERME'
GROUP BY sector, campaign
ORDER BY sector, campaign