﻿--SELECT ps.topiaid, ps.campaigns, d.campaign, ps.active ps_active, gs.active gs_active, gp.active gp_active, d.active d_active, p.active p_active, z.active z_active
--FROM practicedsystem ps
--INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
--INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
--INNER JOIN domain d ON d.topiaid = gp.domain
--LEFT JOIN plot p ON p.growingsystem = gs.topiaid
--LEFT JOIN zone z ON z.plot = p.topiaid
--WHERE (d.campaign < 2000 OR d.campaign >= 2025)
--AND d.active IS false
--GROUP BY 1,2,3,4,5,6,7,8, 9;

-- requête qui remonte l'état actif ou non de certaines entitées liées aux domaine
--SELECT
--  d.topiaid,
--  gp.active as gp_active,
--  gs.active as gs_active,
--  p.active as plot_active,
--  z.active as zone_active,
--  ps.active as ps_active
--FROM domain d
--LEFT JOIN growingplan gp ON gp.domain = d.topiaid
--LEFT JOIN growingsystem gs ON gs.growingplan = gp.topiaid
--LEFT JOIN practicedsystem ps ON ps.growingsystem = gs.topiaid
--LEFT JOIN plot p ON p.domain = d.topiaid
--LEFT JOIN zone z ON z.plot = p.topiaid
--WHERE d.active IS false
--AND (d.campaign < 2000 OR d.campaign >= 2025);

--SELECT d.campaign, gs.active gs_active, gp.active gp_active, d.active d_active, p.active p_active, z.active z_active
--FROM growingsystem gs
--INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
--INNER JOIN domain d ON d.topiaid = gp.domain
--LEFT JOIN plot p ON p.growingsystem = gs.topiaid
--LEFT JOIN zone z ON z.plot = p.topiaid
--WHERE (d.campaign < 2000 OR d.campaign >= 2025)
--AND d.active IS false;

CREATE TEMPORARY TABLE campaigns_to_ps_domain_to_remove AS (
  SELECT
    (regexp_split_to_table(ps.campaigns, ', ' ) :: integer) AS ps_campaign,
    ps.topiaid AS ps_id,
    ps.growingsystem AS ps_gs
  FROM practicedsystem ps
  INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
  INNER JOIN domain d ON d.topiaid = gp.domain
  WHERE (d.campaign < 2000 OR d.campaign >= 2025)
  AND ps.active IS true
  AND d.active IS false
);

CREATE TEMPORARY TABLE ps_move_on_valid_gs AS (
  SELECT
    ps.ps_id AS ps_id,
    gs2.topiaid AS gs_id,
    d.campaign AS domain_campaign,
    ps.ps_campaign AS ps_campaign
  FROM campaigns_to_ps_domain_to_remove ps
  INNER JOIN growingsystem gs1 ON ps.ps_gs = gs1.topiaid
  INNER JOIN growingsystem gs2 ON gs1.code = gs2.code AND gs2.active IS TRUE
  INNER JOIN growingplan gp ON gp.topiaid = gs2.growingplan
  INNER JOIN domain d ON d.topiaid = gp.domain
  WHERE (d.campaign >= 2000 AND d.campaign < 2025)
  AND d.active IS true
  AND ps.ps_campaign = d.campaign
  GROUP BY 1,2,3,4
  ORDER BY ps_id, gs_id, domain_campaign, ps_campaign
);

UPDATE practicedsystem ps SET growingsystem = (
  SELECT psToM.gs_id FROM ps_move_on_valid_gs psToM
  WHERE psToM.ps_id = ps.topiaid
  ORDER BY domain_campaign
  LIMIT 1
) WHERE EXISTS (
    SELECT 1 FROM ps_move_on_valid_gs psToM
    WHERE psToM.ps_id = ps.topiaid
);

CREATE TEMPORARY TABLE ps_to_remove AS (
  SELECT ps.topiaid ps_id, gs.topiaid ps_gs
  FROM practicedsystem ps
  INNER JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
  INNER JOIN domain d ON d.topiaid = gp.domain
  WHERE (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
  GROUP BY ps_id, ps_gs
);

--1
DELETE FROM price p
WHERE EXISTS
  (SELECT 1 FROM ps_to_remove pstr WHERE p.practicedsystem = pstr.ps_id);

CREATE TEMPORARY TABLE performancetoremove AS (
  SELECT pz.performance AS performance FROM performance_zones pz WHERE EXISTS (
    SELECT 1
    FROM zone z
    INNER JOIN plot p ON p.topiaid = z.plot AND p.active IS false
    INNER JOIN domain d ON p.domain = d.topiaid
    WHERE pz.zones = z.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
    AND z.active IS false
  )
  UNION
    SELECT pp.performance AS performance FROM performance_plots pp WHERE EXISTS (
      SELECT 1
      FROM plot p
      INNER JOIN domain d ON p.domain = d.topiaid
      WHERE pp.plots = p.topiaid
      AND (d.campaign < 2000 OR d.campaign >= 2025)
      AND d.active IS false
      AND p.active IS false
    )
  UNION
    SELECT gsp.performance AS performance FROM growingsystems_performance gsp WHERE EXISTS (
      SELECT 1
      FROM growingsystem gs
      INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
      INNER JOIN domain d ON d.topiaid = gp.domain
      WHERE gsp.growingsystems = gs.topiaid
      AND (d.campaign < 2000 OR d.campaign >= 2025)
      AND d.active IS false
      AND gs.active IS false
    )
  UNION
    SELECT dp.performance AS performance FROM domains_performance dp WHERE EXISTS (
      SELECT 1
        FROM domain d
        WHERE dp.domains = d.topiaid
        AND (d.campaign < 2000 OR d.campaign >= 2025)
        AND d.active IS false
    )
);

--2
DELETE FROM performance_scenariocodes psc WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr
  WHERE psc.owner = pr.performance
  AND NOT EXISTS(
    SELECT pz.performance FROM performance_zones pz WHERE pz.performance = pr.performance
    UNION
    SELECT pp.performance FROM performance_plots pp WHERE pp.performance = pr.performance
    UNION
    SELECT gsp.performance FROM growingsystems_performance gsp WHERE gsp.performance = pr.performance
    UNION
    SELECT dp.performance FROM domains_performance dp WHERE dp.performance = pr.performance
  )
);

--3
DELETE FROM performancefile pf WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr
  WHERE pf.performance =  pr.performance
  AND NOT EXISTS(
    SELECT pz.performance FROM performance_zones pz WHERE pz.performance = pr.performance
    UNION
    SELECT pp.performance FROM performance_plots pp WHERE pp.performance = pr.performance
    UNION
    SELECT gsp.performance FROM growingsystems_performance gsp WHERE gsp.performance = pr.performance
    UNION
    SELECT dp.performance FROM domains_performance dp WHERE dp.performance = pr.performance
  )
);
--4
DELETE FROM indicatorfilter_performance ip WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr
  WHERE ip.performance =  pr.performance
  AND NOT EXISTS(
    SELECT pz.performance FROM performance_zones pz WHERE pz.performance = pr.performance
    UNION
    SELECT pp.performance FROM performance_plots pp WHERE pp.performance = pr.performance
    UNION
    SELECT gsp.performance FROM growingsystems_performance gsp WHERE gsp.performance = pr.performance
    UNION
    SELECT dp.performance FROM domains_performance dp WHERE dp.performance = pr.performance
  )
);
--5
DELETE FROM performance_zones pz WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE pz.performance = pr.performance
);
--6
DELETE FROM performance_plots pp WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE pp.performance = pr.performance
);
--7
DELETE FROM realise_echelle_culture rec WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE rec.performance_id = pr.performance
);
--8
DELETE FROM realise_echelle_intervention rei WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE rei.performance_id = pr.performance
);
--9
DELETE FROM realise_echelle_parcelle rep WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE rep.performance_id = pr.performance
);
--10
DELETE FROM realise_echelle_sdc recds WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE recds.performance_id = pr.performance
);
--11
DELETE FROM realise_echelle_zone rez WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE rez.performance_id = pr.performance
);
--12
DELETE FROM growingsystems_performance gsp WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE gsp.performance = pr.performance
);
--13
DELETE FROM performancefile pf WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE pf.performance = pr.performance
);
--14
DELETE FROM synthetise_echelle_culture sec WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE sec.performance_id = pr.performance
);
--15
DELETE FROM synthetise_echelle_intervention sei WHERE EXISTS  (
  SELECT 1 FROM performancetoremove pr WHERE sei.performance_id = pr.performance
);
--16
DELETE FROM synthetise_echelle_synthetise ses WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE ses.performance_id = pr.performance
);
--17
DELETE FROM domains_performance dp WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE dp.performance = pr.performance
);
--18
DELETE FROM performance_scenariocodes psc WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE psc.owner = pr.performance
);
--19
DELETE FROM indicatorfilter_performance ifp WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr WHERE ifp.performance = pr.performance
);
--20
DELETE FROM growingsystems_performance gsp WHERE EXISTS (
    SELECT 1
    FROM growingsystem gs
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON d.topiaid = gp.domain AND d.active IS false
    WHERE gsp.growingsystems = gs.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
    AND gs.active IS false
);
--21
DELETE FROM performance_zones pz WHERE EXISTS (
    SELECT 1
    FROM zone z
    INNER JOIN plot p ON p.topiaid = z.plot
    INNER JOIN domain d ON p.domain = d.topiaid
    WHERE pz.zones = z.topiaid
    AND d.active IS false
    AND (d.campaign < 2000 OR d.campaign >= 2025)
);
--22
DELETE FROM performance_plots pp WHERE EXISTS (
    SELECT 1
    FROM plot p
    INNER JOIN domain d ON p.domain = d.topiaid
    WHERE pp.plots = p.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
    AND p.active IS false
);
--23
DELETE FROM performance p WHERE EXISTS (
  SELECT 1 FROM performancetoremove pr
  WHERE p.topiaid = pr.performance
  AND NOT EXISTS(
    SELECT pz.performance FROM performance_zones pz WHERE pz.performance = pr.performance
    UNION
    SELECT pp.performance FROM performance_plots pp WHERE pp.performance = pr.performance
    UNION
    SELECT gsp.performance FROM growingsystems_performance gsp WHERE gsp.performance = pr.performance
    UNION
    SELECT dp.performance FROM domains_performance dp WHERE dp.performance = pr.performance
  )
);
--24
DELETE FROM phytoproducttarget ppt WHERE EXISTS (
  SELECT 1 FROM abstractinput ai
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE ppt.phytoproductinput = ai.topiaid
);
--25
DELETE FROM phytoproducttarget ppt WHERE EXISTS (
  SELECT 1 FROM abstractinput ai
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedperennialcropcycle ppcc ON ppcc.topiaid = phase.practicedperennialcropcycle
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = ppcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE ppt.phytoproductinput = ai.topiaid
);
--26
DELETE FROM abstractinput ai0 WHERE EXISTS (
  SELECT 1
  FROM abstractinput ai
  INNER JOIN abstractaction aa ON (
      ai.seedingaction = aa.topiaid
      OR ai.biologicalcontrolaction = aa.topiaid
      OR ai.organicfertilizersspreadingaction = aa.topiaid
      OR ai.mineralfertilizersspreadingaction = aa.topiaid
      OR ai.pesticidesspreadingaction = aa.topiaid
      OR ai.harvestingaction = aa.topiaid
      OR ai.irrigationaction = aa.topiaid
      OR ai.maintenancepruningvinesaction = aa.topiaid
      OR ai.otheraction = aa.topiaid)
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE ai0.topiaid = ai.topiaid

  UNION

  SELECT 1
  FROM abstractinput ai
  INNER JOIN abstractaction aa ON (
     ai.seedingaction = aa.topiaid
     OR ai.biologicalcontrolaction = aa.topiaid
     OR ai.organicfertilizersspreadingaction = aa.topiaid
     OR ai.mineralfertilizersspreadingaction = aa.topiaid
     OR ai.pesticidesspreadingaction = aa.topiaid
     OR ai.harvestingaction = aa.topiaid
     OR ai.irrigationaction = aa.topiaid
     OR ai.maintenancepruningvinesaction = aa.topiaid
     OR ai.otheraction = aa.topiaid)
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE ai0.topiaid = ai.topiaid
);
--27
DELETE FROM HarvestingActionValorisation hav0 WHERE EXISTS (
  SELECT 1
  FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON hav.harvestingaction = aa.topiaid
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE hav0.topiaid = hav.topiaid
  UNION
  SELECT 1
  FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON hav.harvestingaction = aa.topiaid
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE hav0.topiaid = hav.topiaid
);
--28
DELETE FROM harvestingaction_winevalorisations hawv0 WHERE EXISTS (
  SELECT 1
  FROM harvestingaction_winevalorisations hawv
  INNER JOIN abstractaction aa ON hawv.owner = aa.topiaid
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE hawv0.owner = hawv.owner
  UNION
  SELECT 1
  FROM harvestingaction_winevalorisations hawv
  INNER JOIN abstractaction aa ON hawv.owner = aa.topiaid
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE hawv0.owner = hawv.owner
);
--29
DELETE FROM SeedingActionSpecies sas0 WHERE EXISTS (
  SELECT 1
  FROM seedingactionspecies sas
  INNER JOIN abstractaction aa ON sas.seedingaction = aa.topiaid
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE sas0.topiaid = sas.topiaid

  UNION

  SELECT 1
  FROM seedingactionspecies sas
  INNER JOIN abstractaction aa ON sas.seedingaction = aa.topiaid
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE sas0.topiaid = sas.topiaid
);
--30
DELETE FROM abstractaction aa0 WHERE EXISTS (
  SELECT 1
  FROM abstractaction aa
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE aa0.topiaid = aa.topiaid

  UNION

  SELECT 1
  FROM abstractaction aa
  INNER JOIN practicedintervention pi ON pi.topiaid = aa.practicedintervention
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE aa0.topiaid = aa.topiaid
);
--31
DELETE FROM practicedspeciesstade pss0 WHERE EXISTS (
  SELECT 1
  FROM practicedintervention pi
  INNER JOIN practicedspeciesstade pss ON pss.practicedintervention = pi.topiaid
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pss0.topiaid = pss.topiaid

  UNION

  SELECT 1
  FROM practicedintervention pi
  INNER JOIN practicedspeciesstade pss ON pss.practicedintervention = pi.topiaid
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pss0.topiaid = pss.topiaid
);
--32
DELETE FROM practicedintervention_toolscouplingcodes pitcc WHERE EXISTS (
  SELECT 1
  FROM practicedintervention pi
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pitcc.owner = pi.topiaid

  UNION

  SELECT 1
  FROM practicedintervention pi
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pitcc.owner = pi.topiaid
);
--33
DELETE FROM practicedintervention pi0 WHERE EXISTS (
  SELECT 1
  FROM practicedintervention pi
  INNER JOIN practicedcropcyclephase phase ON pi.practicedcropcyclephase = phase.topiaid
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pi0.topiaid = pi.topiaid

  UNION

  SELECT 1
  FROM practicedintervention pi
  INNER JOIN practicedcropcycleconnection connection ON pi.practicedcropcycleconnection = connection.topiaid
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pi0.topiaid = pi.topiaid
);
--34
DELETE FROM practicedcropcycleconnection pccc WHERE EXISTS (
  SELECT 1
  FROM practicedcropcycleconnection connection
  INNER JOIN practicedcropcyclenode node ON connection.target = node.topiaid
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pccc.topiaid = connection.topiaid
);
--35
DELETE FROM practicedcropcyclenode pccn WHERE EXISTS (
  SELECT 1
  FROM practicedcropcyclenode node
  INNER JOIN practicedcropcycle pcc ON pcc.topiaid = node.practicedseasonalcropcycle
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pccn.topiaid = node.topiaid
);
--36
DELETE FROM practicedseasonalcropcycle pscc WHERE EXISTS (
  SELECT 1
  FROM practicedcropcycle pcc
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pscc.topiaid = pcc.topiaid
);
--37
DELETE FROM practicedcropcyclespecies pccs WHERE EXISTS (
  SELECT 1
  FROM practicedcropcycle pcc
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pccs.cycle = pcc.topiaid
);
--38
DELETE FROM practicedcropcyclephase pccp WHERE EXISTS (
  SELECT 1
  FROM practicedcropcyclephase phase
  INNER JOIN practicedcropcycle pcc ON phase.practicedperennialcropcycle = pcc.topiaid
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pccp.topiaid = phase.topiaid
);
--39
DELETE FROM practicedperennialcropcycle ppcc WHERE EXISTS (
  SELECT 1
  FROM practicedcropcycle pcc
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE ppcc.topiaid = pcc.topiaid
);
--40
DELETE FROM practicedcropcycle pcc0 WHERE EXISTS (
  SELECT 1
  FROM practicedcropcycle pcc
  INNER JOIN ps_to_remove pstr ON pcc.practicedsystem = pstr.ps_id
  WHERE pcc0.topiaid = pcc.topiaid
);
--41
DELETE FROM practicedplot pp WHERE EXISTS (
  SELECT 1 FROM ps_to_remove
  WHERE pp.practicedsystem = ps_id
);
--42
DELETE FROM practicedsystem ps WHERE EXISTS (
  SELECT 1 FROM ps_to_remove
  WHERE ps.topiaid = ps_id
);
--43
DELETE FROM growingsystem_networks gsn WHERE EXISTS (
  SELECT 1 FROM ps_to_remove
  WHERE gsn.growingsystem = ps_gs
);
--44
DELETE FROM MarketingDestinationObjective mdo WHERE EXISTS (
  SELECT 1 FROM ps_to_remove
  WHERE mdo.growingsystem = ps_gs
);
--45
DELETE FROM zone z0 WHERE EXISTS (
  SELECT 1 FROM zone z
  INNER JOIN plot p ON p.topiaid = z.plot
  INNER JOIN ps_to_remove pstr ON pstr.ps_gs = p.growingsystem
  WHERE z0.topiaid = z.topiaid
  AND z.active IS FALSE
);
--46
DELETE FROM plot p0 WHERE EXISTS (
  SELECT 1 FROM plot p
  INNER JOIN ps_to_remove pstr ON pstr.ps_gs = p.growingsystem
  WHERE p0.topiaid = p.topiaid
  AND p.active IS false
);

CREATE TEMPORARY TABLE growingplantoremove AS (
  SELECT gp.topiaid
  FROM growingplan gp
  INNER JOIN growingsystem gs ON gs.growingplan = gp.topiaid
  INNER JOIN ps_to_remove pstr ON pstr.ps_gs = gs.topiaid
  WHERE gp.active IS false
  AND NOT EXISTS (
    SELECT 1 FROM plot p
    WHERE p.growingsystem = gs.topiaid
    AND p.active IS true
  )
);
--47
DELETE FROM characteristics_growingsystem cgs WHERE EXISTS (
  SELECT 1
  FROM growingsystem gs
  INNER JOIN ps_to_remove pstr ON pstr.ps_gs = gs.topiaid
  WHERE cgs.growingsystem = gs.topiaid
  AND gs.active IS false
  AND NOT EXISTS (
    SELECT 1 FROM plot p
    WHERE p.growingsystem = gs.topiaid
    AND p.active IS true
  )
);
--48
DELETE FROM growingsystem gs0 WHERE EXISTS (
  SELECT 1
  FROM growingsystem gs
  INNER JOIN ps_to_remove pstr ON pstr.ps_gs = gs.topiaid
  WHERE gs0.topiaid = gs.topiaid
  AND gs.active IS false
  AND NOT EXISTS (
    SELECT 1 FROM plot p
    WHERE p.growingsystem = gs.topiaid
    AND p.active IS true
  )
);
--49
DELETE FROM growingplan gp WHERE EXISTS (
  SELECT 1 FROM growingplantoremove gptr
  WHERE gptr.topiaid = gp.topiaid
);
--50
-- equipments_toolscoupling
DELETE FROM equipments_toolscoupling etc WHERE EXISTS (
  SELECT 1 FROM toolscoupling tc
  INNER JOIN domain d ON tc.domain = d.topiaid
  WHERE tc.domain = d.topiaid
    AND etc.toolscoupling = tc.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);

--51
DELETE FROM domains_performance dp WHERE EXISTS (
  SELECT 1
  FROM domain d
  WHERE dp.domains = d.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--52
DELETE FROM price p WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE p.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--53
---- phytoproducttarget
DELETE FROM phytoproducttarget ppt WHERE EXISTS (
  SELECT 1 FROM abstractinput ai
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ppt.phytoproductinput = ai.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);

--54
DELETE FROM phytoproducttarget ppt WHERE EXISTS (
  SELECT 1 FROM abstractinput ai
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ppt.phytoproductinput = ai.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--55
-- abstractinput
DELETE FROM abstractinput ai0 WHERE EXISTS (
  SELECT 1 FROM abstractinput ai
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ai0.topiaid = ai.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--56
DELETE FROM abstractinput ai0 WHERE EXISTS (
  SELECT 1 FROM abstractinput ai
  INNER JOIN abstractaction aa ON  aa.topiaid = CONCAT(ai.organicfertilizersspreadingaction, ai.mineralfertilizersspreadingaction, ai.pesticidesspreadingaction, ai.seedingaction, ai.biologicalcontrolaction, ai.harvestingaction, ai.irrigationaction, ai.maintenancepruningvinesaction)
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ai0.topiaid = ai.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--57
-- SeedingActionSpecies
DELETE FROM seedingactionspecies sas0 WHERE EXISTS (
  SELECT 1 FROM seedingactionspecies sas
  INNER JOIN abstractaction aa ON  aa.topiaid = seedingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE sas0.topiaid = sas.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--58
DELETE FROM seedingactionspecies sas0 WHERE EXISTS (
  SELECT 1 FROM seedingactionspecies sas
  INNER JOIN abstractaction aa ON  aa.topiaid = seedingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE sas0.topiaid = sas.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--59
--harvestingactionvalorisation
DELETE FROM harvestingactionvalorisation hav0 WHERE EXISTS (
  SELECT 1 FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON  aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE hav0.topiaid = hav.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--60
DELETE FROM harvestingactionvalorisation hav0 WHERE EXISTS (
  SELECT 1 FROM harvestingactionvalorisation hav
  INNER JOIN abstractaction aa ON  aa.topiaid = hav.harvestingaction
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE hav0.topiaid = hav.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--61
-- action
DELETE FROM abstractaction aa0 WHERE EXISTS (
  SELECT 1 FROM abstractaction aa
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE aa0.topiaid = aa.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--62
DELETE FROM abstractaction aa0 WHERE EXISTS (
  SELECT 1 FROM abstractaction aa
  INNER JOIN effectiveintervention ei ON ei.topiaid = aa.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE aa0.topiaid = aa.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--63
--effectivespeciesstade
DELETE FROM effectivespeciesstade ess0 WHERE EXISTS (
  SELECT 1 FROM effectivespeciesstade ess
  INNER JOIN effectiveintervention ei ON ei.topiaid = ess.effectiveintervention
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ess0.topiaid = ess.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--64
DELETE FROM effectivespeciesstade ess0 WHERE EXISTS (
  SELECT 1 FROM effectivespeciesstade ess
  INNER JOIN effectiveintervention ei ON ei.topiaid = ess.effectiveintervention
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ess0.topiaid = ess.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--65
-- effectiveintervention_toolcouplings
DELETE FROM effectiveintervention_toolcouplings eitc WHERE EXISTS (
  SELECT 1 FROM effectiveintervention ei
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE eitc.effectiveintervention = ei.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--66
DELETE FROM effectiveintervention_toolcouplings eitc WHERE EXISTS (
  SELECT 1 FROM effectiveintervention ei
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE eitc.effectiveintervention = ei.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);

--67
-- intervention
DELETE FROM effectiveintervention ei0 WHERE EXISTS (
  SELECT 1 FROM effectiveintervention ei
  INNER JOIN effectivecropcyclephase phase ON phase.topiaid = ei.effectivecropcyclephase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ei0.topiaid = ei.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--68
DELETE FROM effectiveintervention ei0 WHERE EXISTS (
  SELECT 1 FROM effectiveintervention ei
  INNER JOIN effectivecropcyclenode eccn ON eccn.topiaid = ei.effectivecropcyclenode
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE ei0.topiaid = ei.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--69
-- effectivecropcyclespecies
DELETE FROM effectivecropcyclespecies eccs WHERE EXISTS (
  SELECT 1 FROM effectiveperennialcropcycle epcc
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE eccs.effectiveperennialcropcycle = epcc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--70
-- effectiveperennialcropcycle
DELETE FROM effectiveperennialcropcycle epcc0 WHERE EXISTS (
  SELECT 1 FROM effectiveperennialcropcycle epcc
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE epcc0.topiaid = epcc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--71
--phase
DELETE FROM effectivecropcyclephase eccp0 WHERE EXISTS (
  SELECT 1 FROM effectivecropcyclephase phase
  INNER JOIN effectiveperennialcropcycle epcc ON epcc.phase = phase.topiaid
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE eccp0.topiaid = phase.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--72
-- effectiveperennialcropcycle
DELETE FROM effectiveperennialcropcycle epcc0 WHERE EXISTS (
  SELECT 1 FROM effectiveperennialcropcycle epcc
  INNER JOIN zone zone ON zone.topiaid = epcc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE epcc0.topiaid = epcc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);

--73
-- effectivecropcycleconnection
DELETE FROM effectivecropcycleconnection epccc0 WHERE EXISTS (
  SELECT 1 FROM effectivecropcycleconnection eccc
  INNER JOIN effectivecropcyclenode eccn ON eccc.target = eccn.topiaid
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE epccc0.topiaid = eccc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--74
DELETE FROM effectivecropcycleconnection epccc0 WHERE EXISTS (
  SELECT 1 FROM effectivecropcycleconnection eccc
  INNER JOIN effectivecropcyclenode eccn ON eccc.source = eccn.topiaid
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE epccc0.topiaid = eccc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--75
-- effectivecropcyclenode
DELETE FROM effectivecropcyclenode eccn0 WHERE EXISTS (
  SELECT 1 FROM effectivecropcyclenode eccn
  INNER JOIN effectiveseasonalcropcycle escc ON escc.topiaid = eccn.effectiveseasonalcropcycle
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE eccn0.topiaid = eccn.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--76
-- effectiveseasonalcropcycle
DELETE FROM effectiveseasonalcropcycle escc0 WHERE EXISTS (
  SELECT 1 FROM effectiveseasonalcropcycle escc
  INNER JOIN zone zone ON zone.topiaid = escc.zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE escc0.topiaid = escc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--77
-- zone
DELETE FROM zone z0 WHERE EXISTS (
  SELECT 1 FROM zone zone
  INNER JOIN plot plot ON zone.plot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE z0.topiaid = zone.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);

--78
-- solHorizon
DELETE FROM solHorizon sh0 WHERE EXISTS(
  SELECT 1 FROM solHorizon sh
  INNER JOIN plot plot ON sh.basicplot = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE sh0.topiaid = sh.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--79
-- performance_plots
DELETE FROM performance_plots pp WHERE EXISTS(
  SELECT 1 FROM performance_plots perf
  INNER JOIN plot plot ON perf.plots = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE pp.plots = perf.plots
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--80
-- plot_lineage
DELETE FROM plot_lineage pl WHERE EXISTS (
  SELECT 1 FROM plot_lineage lin
  INNER JOIN plot plot ON lin.owner = plot.topiaid
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE pl.owner = lin.owner
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--81
--plot
DELETE FROM plot p0 WHERE EXISTS (
  SELECT 1 FROM plot plot
  INNER JOIN domain d ON plot.domain = d.topiaid
  WHERE p0.topiaid = plot.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--82
--aliment
DELETE FROM aliment a0 WHERE EXISTS (
  SELECT 1 FROM aliment a
  INNER JOIN ration rat ON rat.topiaid = a.ration
  INNER JOIN cattle cat ON cat.topiaid = rat.cattle
  INNER JOIN livestockunit lsu ON lsu.topiaid = cat.livestockunit
  INNER JOIN domain d ON lsu.domain = d.topiaid
  WHERE a0.topiaid = a.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--83
-- ration
DELETE FROM ration r0 WHERE EXISTS (
  SELECT 1 FROM ration rat
  INNER JOIN cattle cat ON cat.topiaid = rat.cattle
  INNER JOIN livestockunit lsu ON lsu.topiaid = cat.livestockunit
  INNER JOIN domain d ON lsu.domain = d.topiaid
  WHERE r0.topiaid = rat.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--84
-- cattle
DELETE FROM cattle cat0 WHERE EXISTS (
  SELECT 1 FROM cattle cat
  INNER JOIN livestockunit lsu ON lsu.topiaid = cat.livestockunit
  INNER JOIN domain d ON lsu.domain = d.topiaid
  WHERE cat.topiaid = cat0.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--85
-- livestockunit
DELETE FROM livestockunit lstu WHERE EXISTS (
  SELECT 1
      FROM domain d
      WHERE lstu.domain = d.topiaid
      AND (d.campaign < 2000 OR d.campaign >= 2025)
      AND d.active IS false
);
--86
-- mainsactions_toolscoupling
DELETE FROM mainsactions_toolscoupling matc WHERE EXISTS (
  SELECT 1 FROM toolscoupling tc
  INNER JOIN domain d ON tc.domain = d.topiaid
  WHERE matc.toolscoupling = tc.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--87
-- toolscoupling
DELETE FROM toolscoupling tc WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE tc.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);
--88
-- equipment
DELETE FROM equipment eqp WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE eqp.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);

-- les tables suivantes ne doivent pas contenir de valeurs normalement suite à un import edaplos
--89
-- crops_strategy
DELETE FROM crops_strategy cs WHERE EXISTS (
    SELECT 1 FROM croppingplanentry ce
    INNER JOIN domain d ON ce.domain = d.topiaid
    WHERE cs.crops =  ce.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--90
-- rules_strategy
DELETE FROM rules_strategy rs WHERE EXISTS (
    SELECT 1 FROM strategy st
    INNER JOIN section se ON se.topiaid = st.section
    INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
    INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE rs.strategy = st.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--91
--arbocroppestmaster_species
DELETE FROM arbocroppestmaster_species acpms WHERE EXISTS (
  SELECT 1 FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE acpms.species = cps.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--92
--arbocropadventicemaster_species
DELETE FROM arbocropadventicemaster_species aams WHERE EXISTS (
  SELECT 1 FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE aams.species = cps.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--93
--arbocropadventicemaster_crops
DELETE FROM arbocropadventicemaster_crops aamc WHERE EXISTS (
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE aamc.crops =  cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--94
--arbocroppestmaster_crops
DELETE FROM arbocroppestmaster_crops acpmc WHERE EXISTS(
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE acpmc.crops =  cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--95
--arboadventicemaster
DELETE FROM arboadventicemaster aam WHERE EXISTS (
    SELECT 1 FROM arbocropadventicemaster acam
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = reportgrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE aam.arbocropadventicemaster = acam.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--96
--arbocropadventicemaster
DELETE FROM arbocropadventicemaster acam WHERE EXISTS (
    SELECT 1 FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE acam.reportgrowingsystem = rgs.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--97
--arbopestmaster
DELETE FROM arbopestmaster apm WHERE EXISTS(
SELECT 1 FROM arbocroppestmaster acpm
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = acpm.arbodiseasemasterreportgrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE apm.arbocroppestmaster = acpm.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--98
DELETE FROM arbopestmaster apm WHERE EXISTS(
SELECT 1 FROM arbocroppestmaster acpm
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = acpm.arbopestmastergrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE apm.arbocroppestmaster = acpm.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--99
--arbopestmaster
DELETE FROM arbopestmaster apm WHERE EXISTS(
SELECT 1 FROM arbocroppestmaster acpm
    INNER JOIN reportgrowingsystem rgs ON rgs.topiaid = acpm.arbodiseasemasterreportgrowingsystem
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE apm.arbocroppestmaster = acpm.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--100
--arbocroppestmaster
DELETE FROM arbocroppestmaster acpm WHERE EXISTS(
SELECT 1 FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE CONCAT(acpm.arbodiseasemasterreportgrowingsystem, acpm.arbopestmastergrowingsystem) = rgs.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--101
-- vitipestmaster
DELETE FROM vitipestmaster vpm WHERE EXISTS (
  SELECT 1 FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON gs.topiaid = rgs.growingsystem
    INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE CONCAT (vpm.reportgrowingsystemvitidiseasemaster, vpm.reportgrowingsystemvitipestmaster) =  rgs.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--102
-- croppestmaster_species
DELETE FROM croppestmaster_species cpms WHERE EXISTS (
    SELECT 1 FROM croppingplanspecies cps
    INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
    INNER JOIN domain d ON cpe.domain = d.topiaid
    WHERE cpms.species = cps.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--103
DELETE FROM croppestmaster_crops cpmc WHERE  EXISTS (
    SELECT 1 FROM croppingplanentry cpe
    INNER JOIN domain d ON cpe.domain = d.topiaid
    WHERE cpmc.crops = cpe.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--104
-- pestmaster
DELETE FROM pestmaster pm WHERE EXISTS (
    SELECT 1 FROM croppestmaster cpm
    INNER JOIN reportgrowingsystem rgs ON CONCAT(cpm.cropadventicemasterreportgrowingsystem, cpm.cropdiseasemasterreportgrowingsystem, cpm.croppestmasterreportgrowingsystem) = rgs.topiaid
    INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE pm.croppestmaster = cpm.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--105
-- croppestmaster
DELETE FROM croppestmaster cpm WHERE EXISTS (
    SELECT 1 FROM reportgrowingsystem rgs
    INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
    INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
    INNER JOIN domain d ON gp.domain = d.topiaid
    WHERE CONCAT(cpm.cropadventicemasterreportgrowingsystem, cpm.cropdiseasemasterreportgrowingsystem, cpm.croppestmasterreportgrowingsystem) = rgs.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false
);
--106
-- species_versemaster
DELETE FROM species_versemaster svm WHERE EXISTS (
  SELECT 1 FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE svm.species = cps.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--107
-- crops_versemaster
DELETE FROM crops_versemaster cvm WHERE EXISTS (
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE cvm.crops = cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--108
-- species_yieldloss
DELETE FROM species_yieldloss syl WHERE EXISTS (
  SELECT 1 FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE syl.species = cps.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--109
-- crops_yieldloss
DELETE FROM crops_yieldloss cyl WHERE EXISTS (
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE cyl.crops =  cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--110
-- yieldloss
DELETE FROM yieldloss yl WHERE EXISTS (
  SELECT 1 FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE yl.reportgrowingsystem = rgs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--111
-- species_versemaster
DELETE FROM species_versemaster svm WHERE EXISTS (
  SELECT 1 FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE svm.species = cps.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--112
-- crops_versemaster
DELETE FROM crops_versemaster cvm WHERE EXISTS (
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE cvm.crops = cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--113
--versemaster
DELETE FROM versemaster vm WHERE EXISTS (
  SELECT 1
  FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE vm.reportgrowingsystem = rgs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--114
-- foodmaster_species
DELETE FROM foodmaster_species fms WHERE EXISTS (
  SELECT 1 FROM croppingplanspecies cps
  INNER JOIN croppingplanentry cpe ON cpe.topiaid = cps.croppingplanentry
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE fms.species = cps.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--115
-- crops_foodmaster
DELETE FROM crops_foodmaster cfm WHERE EXISTS (
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE cfm.crops = cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--116
-- foodmaster
DELETE FROM foodmaster fm WHERE EXISTS (
  SELECT 1 FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE fm.foodmasterreportgrowingsystem = rgs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--117
--reportgrowingsystem_sectors
DELETE FROM reportgrowingsystem_sectors rgss WHERE EXISTS(
  SELECT 1 FROM reportgrowingsystem rgs
  INNER JOIN growingsystem gs ON rgs.growingsystem = gs.topiaid
  INNER JOIN growingplan gp ON gp.topiaid = gs.growingplan
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE rgss.owner = rgs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--118
-- reportgrowingsystem
DELETE FROM reportgrowingsystem rgs WHERE EXISTS (
  SELECT 1 FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE rgs.growingsystem = gs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--119
-- croppingplanspecies
DELETE FROM croppingplanspecies cps WHERE EXISTS (
  SELECT 1 FROM croppingplanentry cpe
  INNER JOIN domain d ON cpe.domain = d.topiaid
  WHERE cps.croppingplanentry = cpe.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--120
-- croppingplanentry
DELETE FROM croppingplanentry cpe WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE cpe.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);
--121
--domains_performance
DELETE FROM domains_performance dp WHERE EXISTS (
  SELECT 1
  FROM domain d
  WHERE dp.domains = d.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false);
--122
--growingsystem_networks
DELETE FROM growingsystem_networks gsn WHERE EXISTS (
  SELECT 1 FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE gsn.growingsystem = gs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--123
-- growingsystems_performance
DELETE FROM growingsystems_performance gsp WHERE EXISTS (
  SELECT 1 FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE gsp.growingsystems = gs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--124
-- strategy
DELETE FROM strategy st WHERE EXISTS (
  SELECT 1 FROM section se
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE st.section = se.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--125
-- rules_strategy
DELETE FROM rules_strategy rs WHERE EXISTS (
  SELECT 1 FROM strategy st
  INNER JOIN section se ON se.topiaid = st.section
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE rs.strategy = st.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--126
-- rules_strategy
DELETE FROM section sec WHERE EXISTS (
  SELECT 1 FROM section se
  INNER JOIN managementmode mm ON mm.topiaid = se.managementmode
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE sec.topiaid = se.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--127
DELETE FROM managementmode mm0 WHERE EXISTS (
  SELECT 1 FROM managementmode mm
  INNER JOIN growingsystem gs ON gs.topiaid = mm.growingsystem
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE mm0.topiaid = mm.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--128
-- characteristics_growingsystem
DELETE FROM characteristics_growingsystem cgs WHERE EXISTS (
  SELECT 1 FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE cgs.growingsystem = gs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--129
-- marketingdestinationobjective
DELETE FROM marketingdestinationobjective mdo WHERE EXISTS (
  SELECT 1 FROM growingsystem gs
  INNER JOIN growingplan gp ON gs.growingplan = gp.topiaid
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE mdo.growingsystem = gs.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--130
-- growingsystem
DELETE FROM growingsystem gs WHERE EXISTS (
  SELECT 1 FROM growingplan gp
  INNER JOIN domain d ON gp.domain = d.topiaid
  WHERE gs.growingplan = gp.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false
);
--131
-- growingplan
DELETE FROM growingplan gp WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE gp.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);
--132
-- ground
DELETE FROM ground gr WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE gr.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);
--133
-- geopoint
DELETE FROM geopoint gop WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE gop.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);

-- weatherstation
UPDATE domain d0 SET defaultweatherstation = NULL WHERE EXISTS (
   SELECT 1
   FROM domain d
   WHERE d0.topiaid = d.topiaid
   AND (d.campaign < 2000 OR d.campaign >= 2025)
   AND d.active IS false);
--134
DELETE FROM weatherstation ws WHERE EXISTS (
    SELECT 1
    FROM domain d
    WHERE ws.domain = d.topiaid
    AND (d.campaign < 2000 OR d.campaign >= 2025)
    AND d.active IS false);
--135
-- domain
DELETE FROM domain d0 WHERE EXISTS (
  SELECT 1
  FROM domain d
  WHERE d0.topiaid = d.topiaid
  AND (d.campaign < 2000 OR d.campaign >= 2025)
  AND d.active IS false);

-- à passer à la prochaine release
--UPDATE plot SET active = false WHERE domain = 'fr.inra.agrosyst.api.entities.Domain_9a85965d-7bab-4226-bcc0-fd72d8cc1492';
--UPDATE zone z SET active = false WHERE EXISTS (SELECT 1 FROM plot p WHERE z.plot = p.topiaid AND p.domain = 'fr.inra.agrosyst.api.entities.Domain_9a85965d-7bab-4226-bcc0-fd72d8cc1492');