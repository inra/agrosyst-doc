﻿---------------------------------------
-- Onglet Interventions synthétisées
---------------------------------------
-- Filtre : EF Legumes
---------------------------------------

-- peut-être qu'il faudra ajouter les variétés, suivant ce que demande ceux qui analyseront les données


WITH toutes_interventions AS (

---------------------------
-- Interventions Assolées
---------------------------
		
	(SELECT 
		d.code domaine_code,
		d.topiaid domaine_id,
		d.name domaine_nom, -- à anonymiser
		d.campaign domaine_campagne,
		gs.code sdc_code,
		gs.topiaid sdc_id,
		gs.name sdc_nom,
		ps.topiaid système_synthétisé_id,
		ps.name système_synthétisé_nom,
		ps.campaigns système_synthétisé_campagnes,
		CASE ps.validated 
			WHEN true then 'oui'
			WHEN false then 'non'
		END système_synthétisé_validation,
		pccc.topiaid culture_précédent_rang_id, -- topiaid de la connexion
		pccnt.croppingplanentrycode culture_code,
		(-- Requête donnant pour un code culture et les campagnes du synthétisé, le nom d'une culture
			SELECT max(cpe2.name) as name	
			FROM croppingplanentry cpe2
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE cpe2.code = pccnt.croppingplanentrycode
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer) -- campagnes du synthétisé
			GROUP BY cpe2.code	
		) culture_nom,
		pccc.intermediatecroppingplanentrycode ci_code,
		(-- Même chose avec la culture intermédiaire
			SELECT max(cpe2.name) as name	
			FROM croppingplanentry cpe2
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE cpe2.code = pccc.intermediatecroppingplanentrycode
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
			GROUP BY cpe2.code	
		) ci_nom,
		CASE intermediatecrop 
			WHEN true then 'oui'
			WHEN false then 'non'
		END concerne_la_ci,
		
		(WITH esp_var AS
			(-- Requête filtrée pour espèces avec variétés GEVES
			SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
			NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvg2.denomination),' ; ') AS espvar
			FROM practicedspeciesstade pss2
			JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
			JOIN refespece re2 ON cps2.species = re2.topiaid
			JOIN refvarietegeves rvg2 ON cps2.variety = rvg2.topiaid
			JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE pss2.practicedintervention = pi.topiaid
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)

			UNION 
     
			-- Requête filtrée pour espèces avec variétés PLANTGRAPE
			SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
			NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-',rvpg2.variete),' ; ') AS espvar
			FROM practicedspeciesstade pss2
			JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
			JOIN refespece re2 ON cps2.species = re2.topiaid
			JOIN refvarieteplantgrape rvpg2 ON cps2.variety = rvpg2.topiaid
			JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE pss2.practicedintervention = pi.topiaid
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)	
				
			UNION
		
			-- Requête filtrée pour espèces sans variétés
			SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
			NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') AS espvar
			FROM practicedspeciesstade pss2
			JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
			JOIN refespece re2 ON cps2.species = re2.topiaid
			JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE pss2.practicedintervention = pi.topiaid
			AND cps2.variety IS null
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer) 
			
			)
			
			SELECT string_agg(espvar, ';')
			FROM esp_var
			) especes_de_l_intervention,
		
		pccns.croppingplanentrycode précédent_code,
		(
			-- Requête donnant pour un code culture et les campagnes du synthétisé, le nom d'une culture
			SELECT max(cpe2.name) as name	
			FROM croppingplanentry cpe2
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE cpe2.code = pccns.croppingplanentrycode
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
			GROUP BY cpe2.code	
		) précédent_nom,
		(	WITH source_species AS (
				-- Requête donnant pour un code de culture et les campagnes du synthétisé, la liste des espèces concernées, sans doublons
				SELECT max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), 
					NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
				FROM croppingplanentry cpe2
				JOIN croppingplanspecies cps2 ON cpe2.topiaid = cps2.croppingplanentry
				JOIN refespece re2 ON cps2.species = re2.topiaid
				JOIN domain d2 ON cpe2.domain = d2.topiaid
				WHERE cpe2.code = pccns.croppingplanentrycode
				AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
				GROUP BY species)
			SELECT string_agg(nom_sp,' ; ')
			FROM source_species		
		) précédent_espèces_EDI, 
		(pccnt.rank+1)::text culture_rang,
		pi.topiaid intervention_id,
		pi.type	intervention_type,
		(	SELECT string_agg(riate2.reference_label, ' ; ')
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE aa2.practicedintervention = pi.topiaid
		) interventions_actions,
		pi.name intervention_nom,
		pi.comment intervention_comment,
		pit.toolscouplingcodes combinaison_outils_code,
		(	SELECT max(t2.toolscouplingname)
			FROM toolscoupling t2
			WHERE t2.code = pit.toolscouplingcodes
		) combinaison_outils_nom,
		( 	-- Requête donnant pour un code de CO et les campagnes du synthétisé, le tracteur ou l'automoteur concerné, sans doublons
			SELECT typemateriel1
			FROM toolscoupling t2
			JOIN equipment e2 ON t2.tractor = e2.topiaid
			JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
			JOIN domain d2 ON t2.domain = d2.topiaid
			WHERE t2.code = pit.toolscouplingcodes
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
			LIMIT 1
		) tracteur_ou_automoteur,
		(	WITH materiel_noms AS (
				-- Requête donnant pour un code de CO et les campagnes du synthétisé, la liste des outils concernés, sans doublons
				SELECT max(typemateriel1) as type_materiel_1
				FROM toolscoupling t2
				JOIN equipments_toolscoupling et2 ON t2.topiaid = et2.toolscoupling
				JOIN equipment e2 ON et2.equipments = e2.topiaid
				JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
				JOIN domain d2 ON t2.domain = d2.topiaid
				WHERE t2.code = pit.toolscouplingcodes		
				AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
				GROUP BY e2.refmateriel	
			)
			SELECT string_agg(type_materiel_1, ' ; ')
			FROM materiel_noms
		) outils,	
		pi.startingperioddate date_debut,
		pi.endingperioddate date_fin,
		pi.spatialfrequency freq_spatiale,
		pi.temporalfrequency freq_temporelle,
		pi.spatialfrequency*pi.temporalfrequency psci,
		(	SELECT proportionoftreatedsurface--||' %'
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
			AND aa2.practicedintervention = pi.topiaid
		) proportion_surface_traitée_phyto,
		(	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
			AND aa2.practicedintervention = pi.topiaid
		) psci_phyto,	
		(	SELECT proportionoftreatedsurface--||' %'
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
			AND aa2.practicedintervention = pi.topiaid
		) proportion_surface_traitée_lutte_bio,
		(	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
			AND aa2.practicedintervention = pi.topiaid
		) psci_lutte_bio,
		pi.workrate débit_de_chantier,
		pi.workrateunit débit_de_chantier_unité,
		(	SELECT waterquantityaverage 
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'IRRIGATION'
			AND aa2.practicedintervention = pi.topiaid
		) quantité_eau_mm,
		(	WITH species_code_and_name as (
				-- Requête donnant les noms des sp semées pour une action de semis et les campagnes du synthétisé, sans doublons
				SELECT max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), 
								NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
				FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
				JOIN croppingplanspecies cps2 ON sas2.speciescode = cps2.code
				JOIN refespece re2 ON cps2.species = re2.topiaid
				JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
				JOIN domain d2 ON cpe2.domain = d2.topiaid
				WHERE aa2.practicedintervention = pi.topiaid
				AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
				GROUP BY sas2.speciescode, sas2.topiaid
				ORDER BY sas2.topiaid
			)
			SELECT string_agg(scan2.nom_sp, ' ; ')
			FROM species_code_and_name scan2
		) espèces_semées,
		(	SELECT string_agg(quantity :: text, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre)
				(SELECT quantity, aa2.practicedintervention
				FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
				WHERE aa2.practicedintervention = pi.topiaid
				ORDER BY sas2.topiaid) AS foo
			GROUP BY practicedintervention
		) densité_semis,
		(	SELECT string_agg(seedplantunit, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre)
				(SELECT seedplantunit, aa2.practicedintervention
				FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
				WHERE aa2.practicedintervention = pi.topiaid
				ORDER BY sas2.topiaid) AS foo
			GROUP BY practicedintervention
		) unité_semis,
		pi.rank rang_intervention


	FROM practicedintervention pi
	JOIN practicedcropcycleconnection pccc ON pi.practicedcropcycleconnection = pccc.topiaid
	JOIN practicedcropcyclenode pccnt ON pccc.target = pccnt.topiaid
	JOIN practicedcropcyclenode pccns ON pccc.source = pccns.topiaid
	JOIN practicedcropcycle pcc ON pccnt.practicedseasonalcropcycle = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	LEFT JOIN practicedintervention_toolscouplingcodes pit ON pi.topiaid = pit.owner
	
WHERE d.active IS TRUE

AND d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (SELECT growingplan FROM growingsystem WHERE topiaid IN
            (SELECT growingsystem FROM growingsystem_networks gn 
		JOIN network_parents np ON np.network = gn.networks 
			WHERE parents IN ('fr.inra.agrosyst.api.entities.Network_f8e65fea-d27e-41f1-a05a-6a84eff54b7e',
			'fr.inra.agrosyst.api.entities.Network_eac1185f-63c1-453b-bf34-879ab77ac531',
			'fr.inra.agrosyst.api.entities.Network_035948d7-d5e2-4aca-84c2-7a06e771cc03' ) -- Topiaid des IT de EF_LEGUMES (je n'ai pas trouvé mieux comme manière de requêter)
	     )
          )
     )


	)
	
	UNION 
	
-----------------------------
-- Interventions Pérennes
-----------------------------	
	
	(SELECT 
		d.code domaine_code,
		d.topiaid domaine_id,
		d.name domaine_nom, -- à anonymiser
		d.campaign domaine_campagne,
		gs.code sdc_code,
		gs.topiaid sdc_id,
		gs.name sdc_nom,
		ps.topiaid système_synthétisé_id,
		ps.name système_synthétisé_nom,
		ps.campaigns système_synthétisé_campagnes,
		CASE ps.validated 
			WHEN true then 'oui'
			WHEN false then 'non'
		END système_synthétisé_validation,
		'NA' culture_précédent_rang_id, -- topiaid de la connexion
		ppcc.croppingplanentrycode culture_code,
		(	-- Requête donnant pour un code culture et les campagnes du synthétisé, le nom d'une culture
			SELECT max(cpe2.name) as name	
			FROM croppingplanentry cpe2
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE cpe2.code = ppcc.croppingplanentrycode
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
			GROUP BY cpe2.code	
		) culture_nom,
		'NA' ci_code,
		'NA' ci_nom,
		'NA' concerne_la_ci,
		(WITH esp_var AS
			(-- Requête filtrée pour espèces avec variétés GEVES
			SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
			NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvg2.denomination),' ; ') AS espvar
			FROM practicedspeciesstade pss2
			JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
			JOIN refespece re2 ON cps2.species = re2.topiaid
			JOIN refvarietegeves rvg2 ON cps2.variety = rvg2.topiaid
			JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE pss2.practicedintervention = pi.topiaid
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)

			UNION 
     
			-- Requête filtrée pour espèces avec variétés PLANTGRAPE
			SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
			NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''),'-', rvpg2.variete),' ; ') AS espvar
			FROM practicedspeciesstade pss2
			JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
			JOIN refespece re2 ON cps2.species = re2.topiaid
			JOIN refvarieteplantgrape rvpg2 ON cps2.variety = rvpg2.topiaid
			JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE pss2.practicedintervention = pi.topiaid
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
					
			UNION
		
			-- Requête filtrée pour espèces sans variétés
			SELECT string_agg(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''),
			NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,'')),' ; ') AS espvar
			FROM practicedspeciesstade pss2
			JOIN croppingplanspecies cps2 ON pss2.speciescode= cps2.code
			JOIN refespece re2 ON cps2.species = re2.topiaid
			JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
			JOIN domain d2 ON cpe2.domain = d2.topiaid
			WHERE pss2.practicedintervention = pi.topiaid
			AND cps2.variety IS null
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
			
			)
			
		SELECT string_agg(espvar, ';')
		FROM esp_var
		) especes_de_l_intervention,
	
		'NA' précédent_code,
		'NA' précédent_nom,
		'NA' précédent_espèces_EDI, 
		'NA' culture_rang,
		pi.topiaid intervention_id,
		pi.type	intervention_type,
		(	SELECT string_agg(riate2.reference_label, ' ; ')
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE aa2.practicedintervention = pi.topiaid
		) interventions_actions,
		pi.name intervention_nom,
		pi.comment intervention_comment,
		pit.toolscouplingcodes combinaison_outils_code,
		(	SELECT max(t2.toolscouplingname)
			FROM toolscoupling t2
			WHERE t2.code = pit.toolscouplingcodes
		) combinaison_outils_nom,
		( 	-- Requête donnant pour un code de CO et les campagnes du synthétisé, le tracteur ou l'automoteur concerné, sans doublons
			SELECT typemateriel1
			FROM toolscoupling t2
			JOIN equipment e2 ON t2.tractor = e2.topiaid
			JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
			JOIN domain d2 ON t2.domain = d2.topiaid
			WHERE t2.code = pit.toolscouplingcodes
			AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
			LIMIT 1
		) tracteur_ou_automoteur,
		(	WITH materiel_noms AS (
				-- Requête donnant pour un code de CO et les campagnes du synthétisé, la liste des outils concernés, sans doublons
				SELECT max(typemateriel1) as type_materiel_1
				FROM toolscoupling t2
				JOIN equipments_toolscoupling et2 ON t2.topiaid = et2.toolscoupling
				JOIN equipment e2 ON et2.equipments = e2.topiaid
				JOIN refmateriel rm2 ON e2.refmateriel = rm2.topiaid
				JOIN domain d2 ON t2.domain = d2.topiaid
				WHERE t2.code = pit.toolscouplingcodes		
				AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
				GROUP BY e2.refmateriel
			)
			SELECT string_agg(type_materiel_1, ' ; ')
			FROM materiel_noms
		) outils,	
		pi.startingperioddate date_debut,
		pi.endingperioddate date_fin,
		pi.spatialfrequency freq_spatiale,
		pi.temporalfrequency freq_temporelle,
		pi.spatialfrequency*pi.temporalfrequency psci,
		(	SELECT proportionoftreatedsurface--||' %'
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
			AND aa2.practicedintervention = pi.topiaid
		) proportion_surface_traitée_phyto,
		(	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'
			AND aa2.practicedintervention = pi.topiaid
		) psci_phyto,	
		(	SELECT proportionoftreatedsurface--||' %'
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
			AND aa2.practicedintervention = pi.topiaid
		) proportion_surface_traitée_lutte_bio,
		(	SELECT proportionoftreatedsurface * pi.spatialfrequency * pi.temporalfrequency
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'LUTTE_BIOLOGIQUE'
			AND aa2.practicedintervention = pi.topiaid
		) psci_lutte_bio,
		pi.workrate débit_de_chantier,
		pi.workrateunit débit_de_chantier_unité,
		(	SELECT waterquantityaverage 
			FROM abstractaction aa2
			JOIN refinterventionagrosysttravailedi riate2 ON aa2.mainaction = riate2.topiaid
			WHERE intervention_agrosyst = 'IRRIGATION'
			AND aa2.practicedintervention = pi.topiaid
		) quantité_eau_mm,
		(	WITH species_code_and_name as (
				-- Requête donnant les noms des sp semées pour une action de semis et les campagnes du synthétisé, sans doublons
				Select max(concat_ws(' ', NULLIF(re2.libelle_espece_botanique,''), NULLIF(re2.libelle_qualifiant_aee,''), 
								NULLIF(re2.libelle_type_saisonnier_aee,''), NULLIF(re2.libelle_destination_aee,''))) nom_sp
				FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
				JOIN croppingplanspecies cps2 ON sas2.speciescode = cps2.code
				JOIN refespece re2 ON cps2.species = re2.topiaid
				JOIN croppingplanentry cpe2 ON cps2.croppingplanentry = cpe2.topiaid
				JOIN domain d2 ON cpe2.domain = d2.topiaid
				WHERE aa2.practicedintervention = pi.topiaid
				AND d2.campaign IN (SELECT regexp_split_to_table(ps.campaigns, ', ') :: integer)
				GROUP BY sas2.speciescode, sas2.topiaid
				ORDER BY sas2.topiaid
			)
			SELECT string_agg(scan2.nom_sp, ' ; ')
			FROM species_code_and_name scan2
		) espèces_semées,
		(	SELECT string_agg(quantity :: text, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre)
				(SELECT quantity, aa2.practicedintervention
				FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
				WHERE aa2.practicedintervention = pi.topiaid
				ORDER BY sas2.topiaid) AS foo
			GROUP BY practicedintervention
		) densité_semis,
		(	SELECT string_agg(seedplantunit, ', ') FROM -- sous requête pour avoir les espèces, la quantité et les unités dans le même ordre)
				(SELECT seedplantunit, aa2.practicedintervention
				FROM seedingactionspecies sas2
				JOIN abstractaction aa2 ON sas2.seedingaction = aa2.topiaid
				WHERE aa2.practicedintervention = pi.topiaid
				ORDER BY sas2.topiaid) AS foo
			GROUP BY practicedintervention
		) unité_semis,
		pi.rank rang_intervention


	FROM practicedintervention pi
	JOIN practicedcropcyclephase pccp ON pi.practicedcropcyclephase = pccp.topiaid
	JOIN practicedperennialcropcycle ppcc ON pccp.practicedperennialcropcycle = ppcc.topiaid
	JOIN practicedcropcycle pcc ON ppcc.topiaid = pcc.topiaid
	JOIN practicedsystem ps ON pcc.practicedsystem = ps.topiaid
	JOIN growingsystem gs ON ps.growingsystem = gs.topiaid
	JOIN growingplan gp ON gs.growingplan = gp.topiaid
	JOIN domain d ON gp.domain = d.topiaid 
	LEFT JOIN practicedintervention_toolscouplingcodes pit ON pi.topiaid = pit.owner

WHERE d.active IS TRUE

AND d.topiaid IN
    (SELECT domain from growingplan Where topiaid IN
        (SELECT growingplan FROM growingsystem WHERE topiaid IN
            (SELECT growingsystem FROM growingsystem_networks gn 
		JOIN network_parents np ON np.network = gn.networks 
			WHERE parents IN ('fr.inra.agrosyst.api.entities.Network_f8e65fea-d27e-41f1-a05a-6a84eff54b7e',
			'fr.inra.agrosyst.api.entities.Network_eac1185f-63c1-453b-bf34-879ab77ac531',
			'fr.inra.agrosyst.api.entities.Network_035948d7-d5e2-4aca-84c2-7a06e771cc03' ) -- Topiaid des IT de EF_LEGUMES (je n'ai pas trouvé mieux comme manière de requêter)
	     )
          )
     )

	)
)
SELECT * FROM toutes_interventions
ORDER BY domaine_id, domaine_campagne, sdc_nom, système_synthétisé_nom, rang_intervention
