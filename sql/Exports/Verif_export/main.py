import re
import pandas as pd
import numpy as np
import zipfile

# Couleurs : \033[1 : bold , 0 : normal ;numcolorm
titres1='\033[1;34m'
titres2='\033[1;36m'
NC='\033[0m' # No Color

erreur='\033[0;31m' + 'ERREUR : ' + NC 
ok='\033[1;32m' # vert
warning='\033[0;35m' + 'ATTENTION : ' + NC 


# REPERTOIRES donnees
rep_old="./data/old/"
rep_new="./data/new/"

mode=["anonyme","non_anonyme"]
type_fichier=["_brute.zip","_perf.zip"]

#### FONCTIONS USED ####

def check_table_missing(df_uniq_key,list_file_new,type_fichier) :
    """ Vérifie que toutes les tables sont présentes, à partir de df_uniq_key
    agruments:
    df_uniq_key -- dataframe, df associant un fichier et sa cle unique
    list_file_new -- list, liste des fichiers de l'archive du nouvel export
    type_fichier -- chr, "_brute.zip" ou "_perf.zip"

    return: None
    print() messages erreur
    """
    tables = df_uniq_key["table"].values.tolist()
    
    if type_fichier == "_brute.zip":
        tables = [ x for x in tables if "performance" not in x ]
    else:
        tables = [ x for x in tables if "performance" in x ]
    
    list_file_new_split=list_file_new.copy()
    for f in range(0,len(list_file_new)):
        list_file_new_split[f] = re.split(r"\d+_|\.", list_file_new[f])[1] #sep by 0-9 + \

    for t in tables:
        if t not in list_file_new_split:
            print(erreur, t, "la table n'est pas présente dans le nouvel export")


def uniq_key(list_file_new,df_uniq_key,archive_new) :
    """ Verification des cles uniques des fichiers du nouvel export.
    agruments:
    list_file_new -- list, liste des fichiers à tester
    df_uniq_key -- dataframe, df associant un fichier et sa cle unique
    repepository_data_new -- chr, repertoire des fichiers du nouvel export.

    return: None
    print() messages erreur
    """
    for f in list_file_new:
        problem = False
        f_split = re.split(r"\d+_|\.", f)[1] #sep by 0-9 + \.
        tables = df_uniq_key["table"].values.tolist()
        if f_split not in tables:
            problem = True
            print(warning, f,  "n'a pas de cle unique repertoriée")
        else:
            dfnew = pd.read_csv(archive_new.open(f), delimiter='@', low_memory=False)
            cle = df_uniqkey.at[tables.index(f_split),"primary_key"]
            
            if all(item in dfnew.columns for item in cle.split(",")):
                df_without_dup = dfnew[cle.split(",")].drop_duplicates()
                if df_without_dup.shape[0] != dfnew.shape[0] : 
                    problem = True
                    print(erreur, f, "la table a",dfnew.shape[0] - df_without_dup.shape[0] , "doublons ")
            else : 
                problem = True
                print(warning, "la cle unique", cle.split(","), "n'existe pas dans la table ou bien le fichier n'a pas le bon separateur", f)
    
        if problem == False : 
            print(f,": ............. " + ok + "OK" + NC)
        else :
            print("")

    return None
    

def check_unique_column(list_file_new,archive_new):
    """
    Verification des cles uniques des fichiers du nouvel export.
    agruments:
    list_file_new -- list, liste des fichiers à tester
    archive_new -- chr, path vers l'archive des fichiers du nouvel export.

    return: None
    print() messages erreur
    """
    
    problem = False
    for f in list_file_new:
        dfnew = pd.read_csv(archive_new.open(f), delimiter='@', low_memory=False, header=None)
        colonnes = list(dfnew.loc[0])

        count = [colonnes.count(c) for c in np.unique(colonnes)]
        not_unique_columns = [np.unique(colonnes)[i] for i in range(0,len(count)) if count[i] > 1]
        
        if len(not_unique_columns) > 0:
            print(erreur, f, "Les colonnes ont le même nom", not_unique_columns)
        else :
            print(f,": ............ " + ok + "OK" + NC)



def comparaison_dimensions(list_file_new,list_file_old,archive_new,archive_old) :
    """ Comparaison du nombre de lignes et colonnes des fichiers.
    arguments:
    list_file_new -- list, liste des fichiers à tester
    list_file_old -- list, liste des fichiers anciens
    archive_old -- chr, path vers l'archive des fichiers de ancien export.
    archive_new -- chr, path vers l'archive des fichiers du nouvel export.

    return: None
    print() messages erreur
    """
    problem = False
    for f in list_file_new:
        if (f in list_file_old):
            dfnew = pd.read_csv(archive_new.open(f), delimiter='@', low_memory=False)
            dfold = pd.read_csv(archive_old.open(f), delimiter='@', low_memory=False)

            if dfnew.shape[0] < dfold.shape[0]:
                if problem == False :
                    print("")
                print(erreur, f, "le nombre de ligne a diminué", "anciens :", dfold.shape[0], "nouveaux : ", dfnew.shape[0])
                problem=True
            if dfnew.shape[1] != dfold.shape[1]:
                if problem == False :
                    print("")
                print(erreur, f, "le nombre de colonne est différent", "anciens :", dfold.shape[1], "nouveaux : ", dfnew.shape[1])
                problem=True
        if problem == False : 
            print(f,": ............ " + ok + "OK" + NC)
        else :
            print("")

    return None


def control_colonne_refesp(list_file_new,file_to_open,archive_new) :
    """ Controle du contenu de la colonne espece_edi_nom_botanique de la table culture
    arguments:
    file_to_open -- chr, nom du fichier à tester "exports_agronomes_context_cultures.csv"
    archive_new -- chr, path vers l'archive des fichiers du nouvel export.

    return: None
    print() messages erreur
    """
    problem = False
        
    for f in list_file_new:
        if file_to_open in f:
            dfnew = pd.read_csv(archive_new.open(f), delimiter='@', low_memory=False)
            esp = dfnew.loc[dfnew['espece_edi_nom_botanique'].isnull() != True,'espece_edi_nom_botanique'].sort_values().drop_duplicates().tolist()
            
            list_pb = []
            for e in esp:
                if e.isupper() or e.islower():
                    if problem == False :
                            print("")
                    list_pb.append(e)
                    problem = True
                
                if e[1:len(e)].isupper():
                    if problem == False :
                            print("")
                    list_pb.append(e)
                    problem = True

            if problem == False : 
                print(f, ": ............. " + ok + "OK" + NC)
            else :
                print(erreur, file_to_open, "la colonne espece_edi_nom_botanique (provenant du référentiel esp) n'est pas homogene :")
                print(list_pb)
    return None


# Importer des données au prealable
df_uniqkey = pd.read_csv("data/liste_table.csv", delimiter=';', low_memory=False)

## Pour les deux modes anonymes et non anonymes + fichiers brutes ou perf :
for m in mode:
    for type in type_fichier:
        print("------------------------------------")
        print(titres1 + "Pour : ", rep_new + m + "/" + m + type + NC)
        archive_new = zipfile.ZipFile(rep_new + m + "/" + m + type, 'r')
        list_file_new=archive_new.namelist()
        archive_old = zipfile.ZipFile(rep_old + m + "/" + m + type, 'r')
        list_file_old=archive_old.namelist()

        ## execution
        print(titres2 + "Table manquante" + NC)
        check_table_missing(df_uniqkey,list_file_new,type)

        print(titres2 + "Verification des cles uniques" + NC)
        uniq_key(list_file_new,df_uniqkey,archive_new)
        
        print(titres2 + "Verification des colonnes uniques" + NC)
        check_unique_column(list_file_new,archive_new)
        
        print(titres2 + "Comparaison du nombre de lignes et colonnes des fichiers" + NC)
        comparaison_dimensions(list_file_new,list_file_old,archive_new,archive_old)

        if type == "_brute.zip":
            print(titres2 + "Controle de la colonne espece_edi_nom_botanique" + NC)
            filenew = "exports_agronomes_context_cultures.csv"
            control_colonne_refesp(list_file_new,filenew,archive_new)